<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hotel Waldorf | Booking.</title>
	  <link rel="shortcut icon" type="image/png" href="{{url('img/favicon.png')}}"/>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	  <!-- Font Awesome -->
	  <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
	   {!!Html::style('css/bootstrap.min.css')!!}
     {!!Html::style('vendors/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css')!!}
	   {!!Html::style('css/panelWaldorf.css')!!}
     {!!Html::style('css/indexWaldorf.css')!!}
   </head>
    <body style="display: block;">
      <input type="hidden" value="{{e($modal)}}" id="modal"/>
      <input type="hidden" value="{{e($usuario)}}" id="usuario"/>
      <div class="row background-container" style="margin:0px;">
        <div class="container-waldorf">
          <div id="home-topage">
    				<div id="logoWaldorf"></div>
            <div id="Carousel-Waldorf" class="carousel slide carousel-fade" data-ride="carousel">
          		<div id="Inner-Waldorf" class="carousel-inner" role="listbox">
          			<div class="item active">
          				<div class="item-topage" style=" background-image:url({{URL('img/bookign.jpg')}})"></div>
          			</div>
          		</div>
          	</div>
    			</div>
          <div id="navbarWaldorf-container" class="navbarWaldorf-ini">
            <nav id="navbarWaldorf" class="navbar navbar-default" role="navigation">
              <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse"
                  data-target=".navbar-ex1-collapse">
                <span class="sr-only">Desplegar navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!--a class="navbar-brand" href="#">Logotipo</a-->
              </div>
              <div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-collapse-waldorf">
                <ul class="nav navbar-nav fixed-space-li">
                  <li class="link"><a style="width:150px !important">Hotel Waldorf/Booking</a></li>
                  <li class="link"><a href="{{url('booking/salir/'.Crypt::encrypt($regreso))}}" style=""><i class="fa fa-arrow-left"></i> Regresar</a></li>
                </ul>
              </div>
            </nav>
          </div>
          <!-- Historia -->
        	<div id="Historia">
            <div class="row" style="padding:0px;margin:0px;">
        			<div class="col-xs-10 col-xs-offset-1" style="padding:0px;">
                @if(!isset($_REQUEST['d']))
                  <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
						@if($tipo=="1")
							<iframe src="{{$desarrollo}}" width="100%" height="800px" style="border:0"></iframe>
						@else

							<iframe src="{{$produccion}}" width="100%" height="800px" style="border:0"></iframe>
						@endif
                  </div>
                @else
                  <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                    <?php
                      //codigo de seguridad. Novared lo provee
					  $secureKey = NULL;
					  if($tipo=="1"){
						$secureKey = $keydev;
					  }else{
						$secureKey = $keyprod;
					  }
					  
                      if(isset($_REQUEST['d'])){
                        $datab64 = $_REQUEST['d'];
                        $chk = strtolower($_REQUEST['chk']);
                        $chkCalc = md5($secureKey.$datab64);

                        //Chequeamos que los datos no han sido modificados
                        if($chk == $chkCalc){
                            $json = base64_decode($datab64);
                            $data = json_decode($json, true);

                            //datos de la transaccion

                            if($data['cod'] == 0){//codigo de aprobacion. La transaccion es aprobado si este valor es 0
                              echo '<div class="text-center alert alert-success col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4" role="success">
                                        <span class="glyphicon glyphicon-check text-center" aria-hidden="true"></span>Transaccion Aprobada
                                    </div>';
                            }else{
                              echo '<div class="text-center alert alert-success col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4" role="success">
                                        <span class="glyphicon glyphicon-check text-center" aria-hidden="true"></span>Transaccion no Existosa
                                    </div>';
                            }

                            $reservData = $data['reserv'];
                            echo "<div class='row'>
                                    <p class=\"col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3\" style=\"padding-top:15px;font-size:14px;color:black\">
                                        Cliente: ".$reservData['clte']['nombre'].' '.$reservData['clte']['apellido'].'
                                    </p>
                                </div><br/>'; //nombre y apellido del cliente
                            echo "<div class=\"row\">
                                    <p style=\"margin-left:15px;padding-top:15px;font-size:14px;color:black\">
                                        <strong>
                                            VER COMPROBANTE <a href='".$data['rec']."'>Ver
                                            <span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>
                                        </strong>
                                    </p></div>"; //URL del recibo
                            echo "<div class='row'>
                                    <div class='col-md-6'>
                                        <p style='font-size:14px;padding-top:15px;color:black' class='centra'>
                                            DETALLES DE LA RESERVACI&Oacute;N <span class='fa fa-search' aria-hidden='true'></span>
                                        </p>
                                        <p style='padding-left:20px;color:black' class='centra'>
                                            N&uacute;mero de la Reservaci&oacute;n <strong>'". $reservData['cod']."'</strong>
                                        </p><br/>
                                    </div>
                                    <div class='col-md-6'></div>
                                </div>";
                              //datos de la reservacion
                            echo "<div class='row'>
                                    <div class='col-md-6'>
                                        <p style='padding-left:20px;color:black' class='text-center'>
                                            Fecha de Entrada <strong>". $reservData['desde'].'</strong>
                                        </p>
                                    </div>
                                    <div class=\'col-md-6\'>
                                        <p style=\'padding-left:20px;color:black\' class=\'text-center\'>
                                            Fecha de Salida <strong>'.$reservData['hasta'].'</strong>
                                        </p>
                                    </div>
                                </div><br/>';

                              //detalles de la reservacion
                            $montoTotal = 0;

                            echo "<div class=\"row\">
                                  <div class=\"table-responsive\" style=\"padding-left:15px;padding-right:15px;color:black;\">
                                      <table class=\"table\" style=\"padding-top:15px;\">
                                          <thead><tr><th>COD HAB</th><th>NOMBRE</th><th>CNTD HAB</th><th>CNTD PERSONAS</th>".
                                          "<th>CNTD NI&Ntilde;OS</th><th>MONTO</th></tr></thead><tbody>";

                            foreach($reservData['det'] as $detalle)
                            {
                                echo "<tr><td>".$detalle['ref'].'</td>'; //codigo de habitacion
                                echo "<td>".$detalle['nom'].'</td>'; //nombre de habitacion
                                echo "<td>".$detalle['cant'].'</td>'; //cantidad de habitaciones rentadas
                                echo "<td>".$detalle['per'].'</td>'; //cantidad de personas para esta habitacion
                                echo "<td>".$detalle['nin'].'</td>'; //cantidad de niños para esta habitacion
                                echo '<td>'.$detalle['monto'].'</td></tr>';
                              $montoTotal += $detalle['monto']; //monto de la reservacion de la habitacion actual
                            }

                            echo
                              "	</tbody></table>
                            </div></div>";
                            echo "</br>
                                <div class='row'>
                                    <div class='col-md-8'></div>
                                    <div class='col-md-4'>
                                        <p style='padding-top:15px;padding-bottom:15px;font-size:14px;color:black;text-right'>
                                            CONSUMO TOTAL: <strong>".$montoTotal."</strong>
                                        </p>
                                    </div>
                                </div></a>"; //URL del recibo
                          }else{
                            //error en el calculo de integridad de los datos
                            echo 'Error: datos invalidos';
                          }
                        }
                      ?>
                  </div>
                @endif
          		</div>
            </div>
        	</div>
        	<hr style="margin-top:40px;margin-bottom:40px;border:0;border-top:1px solid #333;width:83.33333%;background-color:transparent;" align="center">
          <!-- footer -->
    			<div id="footer" class="row" style="margin:0px;padding:0px;">
    				<div class="row" style="margin:10px 0px;padding:0px 10px;">
    					<div class="col-xs-10 col-xs-offset-1">
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<a href="#" class="btn-footer" id="btn-home">HOME</a>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<span class="btn-footer" id="btn-nuestroh">NUESTRO HOTEL</span>
    							<div id="footer-nuestroh" style="display:block">
    								<ul id="nu" style="list-style: none;color:#FFF;padding-left: 0px;">
    									<li><a href="">Historia</a></li>
    									<li><a href="">Valores Corporativos</a></li>
    									<li><a href="">Vision Emprendedora</a></li>
    								</ul>
    							</div>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<span class="btn-footer" id="btn-habitaciones">HABITACIONES</span>
    							<div id="footer-habitaciones" style="display:block">
    								<ul id="ha" style="list-style: none;color:#FFF;padding-left: 0px;">
    									<li><a href="{{url('Habitacion/KingStandar')}}">King Standar</a></li>
    									<li><a href="{{url('Habitacion/KingDeluxe')}}">King Deluxe</a></li>
    									<li><a href="{{url('Habitacion/DobleSuperior')}}">Doble Superior</a></li>
    									<li><a href="{{url('Habitacion/KingSuperior')}}">King Superior ADA</a></li>
    									<li><a href="{{url('Habitacion/SingleStandar')}}">Single Standar</a></li>
    								</ul>
    							</div>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<a href="#" class="btn-footer" id="btn-restaurante">RESTAURANTE</a>
    						</div>
    					</div>
    				</div>
    				<div class="row" style="margin:10px 0px;padding:0px 10px;">
    					<i id="location"></i>
    					<p class="text-center" style="color:#fff;">Avda Urdaneta cruce con Bellas Artes. Edf. WALDORF. Caracas - Venezuela. Telf: +58 212 574.68.72/574.67.73</p>
    					<ul id="footer-social" class="nav navbar-nav" style="float:initial;width:40px;margin:10px auto;display: block;">
    						<!--li><a href="#">#1</a></li-->
    						<li><a href="#" class="inst" target="_blank"></a></li>
    						<li><a href="#" class="face" target="_blank"></a></li>
    						<!--li><a href="#">#4</a></li-->
    					</ul>
    				</div>
    				<p id="copy">&copy; 2016 Hotel Waldorf.J-00018918-8. Todos los derechos reservados Powered by Hacienda Creativa.</p>
    			</div>
    		</div>
      </div>
      @include("modales.booklogin")
      {!!Html::script('js/jquery-3.1.1.min.js')!!}
    	{!!Html::script('js/bootstrap.min.js')!!}
      <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
      {!!Html::script('js/booking.js')!!}
  </body>
</html>
