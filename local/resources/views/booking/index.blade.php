<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hotel Waldorf | Booking.</title>
	  <link rel="shortcut icon" type="image/png" href="{{url('img/favicon.png')}}"/>
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	  <!-- Font Awesome -->
	  <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
	   {!!Html::style('css/bootstrap.min.css')!!}
     {!!Html::style('vendors/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css')!!}
	   {!!Html::style('css/panelWaldorf.css')!!}
     {!!Html::style('css/indexWaldorf.css')!!}
   </head>
    <body style="display: block;">
      <input type="hidden" value="{{e($modal)}}" id="modal"/>
      <input type="hidden" value="{{e($usuario)}}" id="usuario"/>
      <div class="row background-container" style="margin:0px;">
        <div class="container-waldorf">
          <div id="home-topage">
    				<div id="logoWaldorf"></div>
            <div id="Carousel-Waldorf" class="carousel slide carousel-fade" data-ride="carousel">
          		<div id="Inner-Waldorf" class="carousel-inner" role="listbox">
          			<div class="item active">
          				<div class="item-topage" style=" background-image:url({{URL('img/bookign.jpg')}})"></div>
          			</div>
          		</div>
          	</div>
    			</div>
          <div id="navbarWaldorf-container" class="navbarWaldorf-ini">
            <nav id="navbarWaldorf" class="navbar navbar-default" role="navigation">
              <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse"
                  data-target=".navbar-ex1-collapse">
                <span class="sr-only">Desplegar navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!--a class="navbar-brand" href="#">Logotipo</a-->
              </div>
              <div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-collapse-waldorf">
                <ul class="nav navbar-nav fixed-space-li">
                  @if($usuario!=NULL)
                    <li class="link"><a style="width:150px !important">Hotel Waldorf/Booking</a></li>
                    <li class="link"><a href="{{url('booking/salir')}}" style=""><i class="fa fa-arrow-left"></i> Regresar</a></li>
                  @endif
                </ul>
              </div>
            </nav>
          </div>
          <!-- Historia -->
        	<div id="Historia">
            <div class="row" style="padding:0px;margin:0px;">
        			<div class="col-xs-10 col-xs-offset-1" style="padding:0px;">
                @if($usuario!=NULL)
                  <label class="conrtol-label">Usuario - {{e($usuario->user_name)}}</label>
                @endif
                <p class="titulo text-right">Configuración</p>
                @if(Session::has("mensaje"))<p class="titulo text-left respuesta">{{Session::get("mensaje")}}</p>@endif
                <form id="form-configbooking" action="{{url('booking/savesetup')}}" method="post">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                  <div class="row">
                    <div id="url-developer" class="col-xs-6">
                        <label class="control-label">Desarrollo</label>
                        <input type="url" name="developer" id="developer" class="input-style form-control" placeholder="Link - Desarrollo" value="{{e($desarrollo)}}"/>
                        <label id="campourldeveloper" class="alerta">El formato ingresado es incorrecto.</label>
                    </div>
                    <div id="url-developer" class="col-xs-6">
                        <label class="control-label">Producción</label>
                        <input type="url" name="produccion" id="produccion" class="input-style form-control" placeholder="Link - Producción" value="{{e($produccion)}}"/>
                        <label id="campourlproduccion" class="alerta">El formato ingresado es incorrecto.</label>
                    </div>
                    <div id="url-developer" class="col-xs-6">
                        <label class="control-label">Key</label>
						@if($tipo=="1")
							<input type="url" name="key" id="key" class="input-style form-control" placeholder="Link - Producción" value="{{e($keydev)}}"/>
						@else
							<input type="url" name="key" id="key" class="input-style form-control" placeholder="Link - Producción" value="{{e($keyprod)}}"/>
						@endif
                        <label id="campobooking" class="alerta">El formato ingresado es incorrecto.</label>
                    </div>
                    <div id="url-developer" class="col-xs-6">
                      <label class="control-label">Modo</label>
                      <select class="form-control" id="tipo" name="tipo">
                          <option value="NULL">Seleccione el modo a usar.</<option>
                          @if($tipo!="" && $tipo=="0")
                            <option value="0" selected>Desarrollo</<option>
                            <option value="1">Producción</<option>
                          @else
                            <option value="0">Desarrollo</<option>
                            <option value="1">Producción</<option>
                          @endif
                      </select>
                      <label class="alerta" id="campotipo">El modo seleccionado es incorrecto.</label>
                    </div>
                    <div id="url-developer" class="col-xs-12">
                      	<button type="button" id="btnActivar">Activar</button>
                    </div>
                  </div>
                </form>
          		</div>
            </div>
        	</div>
        	<hr style="margin-top:40px;margin-bottom:40px;border:0;border-top:1px solid #333;width:83.33333%;background-color:transparent;" align="center">
          <!-- footer -->
    			<div id="footer" class="row" style="margin:0px;padding:0px;">
    				<div class="row" style="margin:10px 0px;padding:0px 10px;">
    					<div class="col-xs-10 col-xs-offset-1">
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<a href="#" class="btn-footer" id="btn-home">HOME</a>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<span class="btn-footer" id="btn-nuestroh">NUESTRO HOTEL</span>
    							<div id="footer-nuestroh" style="display:block">
    								<ul id="nu" style="list-style: none;color:#FFF;padding-left: 0px;">
    									<li><a href="">Historia</a></li>
    									<li><a href="">Valores Corporativos</a></li>
    									<li><a href="">Vision Emprendedora</a></li>
    								</ul>
    							</div>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<span class="btn-footer" id="btn-habitaciones">HABITACIONES</span>
    							<div id="footer-habitaciones" style="display:block">
    								<ul id="ha" style="list-style: none;color:#FFF;padding-left: 0px;">
    									<li><a href="{{url('Habitacion/KingStandar')}}">King Standar</a></li>
    									<li><a href="{{url('Habitacion/KingDeluxe')}}">King Deluxe</a></li>
    									<li><a href="{{url('Habitacion/DobleSuperior')}}">Doble Superior</a></li>
    									<li><a href="{{url('Habitacion/KingSuperior')}}">King Superior ADA</a></li>
    									<li><a href="{{url('Habitacion/SingleStandar')}}">Single Standar</a></li>
    								</ul>
    							</div>
    						</div>
    						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
    							<a href="#" class="btn-footer" id="btn-restaurante">RESTAURANTE</a>
    						</div>
    					</div>
    				</div>
    				<div class="row" style="margin:10px 0px;padding:0px 10px;">
    					<i id="location"></i>
    					<p class="text-center" style="color:#fff;">Avda Urdaneta cruce con Bellas Artes. Edf. WALDORF. Caracas - Venezuela. Telf: +58 212 574.68.72/574.67.73</p>
    					<ul id="footer-social" class="nav navbar-nav" style="float:initial;width:40px;margin:10px auto;display: block;">
    						<!--li><a href="#">#1</a></li-->
    						<li><a href="#" class="inst" target="_blank"></a></li>
    						<li><a href="#" class="face" target="_blank"></a></li>
    						<!--li><a href="#">#4</a></li-->
    					</ul>
    				</div>
    				<p id="copy">&copy; 2016 Hotel Waldorf.J-00018918-8. Todos los derechos reservados Powered by Hacienda Creativa.</p>
    			</div>
    		</div>
      </div>
      @include("modales.booklogin")
      {!!Html::script('js/jquery-3.1.1.min.js')!!}
    	{!!Html::script('js/bootstrap.min.js')!!}
      <script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>
      {!!Html::script('js/booking.js')!!}
  </body>
</html>
