<div class="modal fade" id="historialgastronomia" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/HISTORIAL</h4>
			</div>
			<div class="modal-body">
        <div class="row">
          <h2>Use una imagen del historial</h2>
          <hr>
          <div class="col-xs-12 col-md-12 fixed-border">
						<div id="picturering" class="ocultar">
							<label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
							<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
						</div>
						<div id="resultadoprofile" class="">
							<label id="successchange" class="center alert-success fixed-wait fixed-po">Modificación Sastifactoria</label>
						</div>
						<div id="removesastifactorio" class="">
							<label id="removerchange" class="center alert-success fixed-wait fixed-po">Imagen removida Sastifactoriamente</label>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<form id="form-gastrohistorial">
									<div class="form-group">
											<div class="col-xs-12 col-md-8">
													<select class="form-control" id="filtroselect">
															<option value="-1">Seleccione la opción de agrado</option>
															<option value="1" selected>Primera Imagen</option>
															<option value="2">Segunda Imagen</option>
															<option value="3">Tercera Imagen</option>
															<option value="4">Cuarta Imagen</option>
													</select>
											</div>
											<div class="col-xs-12 col-md-4">
													<button id="filtrar" type="button" class="float-left" disabled>Filtrar</button>
											</div>
									</div>
							</form>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable-gastronomia" class="table table-striped table-bordered">
								<thead>
 								 <tr>
									 @if($confirmar)
									 	<th>Imagen</th>
									 @else
 									 	<th>Imagen</th>
 									 	<th>Opciones</th>
									 @endif
 								 </tr>
 							 </thead>
 							 <tbody>
								 @if($confirmar)
								 <tr>
									 <td>
										 <label class="alerta">No posee imágenes en el historial</label>
										 <img src="{{url('img/default.jpg')}}" class="img-responsive">
									 </td>
								 </tr>
								 @else
								 	@foreach($pictures as $partes)
									 	@if($partes->tipo=="1")
										 	@foreach($partes->img as $img)
										 	<tr>
											 	<td><img src="{{url($img)}}" class="img-responsive"></td>
												<td>
														<button type="button" id="historialbtn" class="fixed-button-historial">Cambiar</button>
														<button type="button" id="removerhistorial" class="fixed-button-historial">Remover</button>
												</td>
											</tr>
											@endforeach
										@endif
									 @endforeach
								 @endif
							 	</tbody>
							</table>
						</div>
          </div>
        </div>
        <div class="row fixed-row-btn">
            <hr class="colorhr">
            <div class="col-xs-12 col-md-12">
                <button type="button" id="btnclose" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
			</div>
		</div>
	</div>
</div>
