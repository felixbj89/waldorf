<div class="modal fade" id="picturerooms" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo fixed-waitv2" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF/IMÁGENES</h4>
			</div>
			<div class="modal-body">
        <div id="picturehabring" class="ocultar">
          <label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
          <img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
        </div>
        <div id="resultadoprofile">
          <label id="successthumbnail" class="center alert-success fixed-wait fixed-po">Modificación Sastifactoria</label>
        </div>
				<form id="form-propiedades">
	        <input type="hidden" value="" id="texto" name="texto"/>
					<input type="hidden" value="" id="propilist" name="propilist"/>
	        <div class="row fixed-row">
	          <p class="text-left alerta">Busque una Habitación</p>
	          <hr>
	          <div id="" class="col-xs-12 col-md-8">
	            <label class="alerta">Habitaciones</label>
	            <select id="selectcuartospicture" class="input-style form-control">

	            </select>
	            <label class="control-label alerta" id="selectvalidopic">Seleccione una habitación válida</label>
	            <label class="control-label alerta" id="disabledsearchpic">Recuerde buscar una habitación válida</label>
	          </div>
	          <div class="col-xs-12 col-md-4">
	            <button type="button" id="btnseepicturehab" class="center">Buscar</button>
	          </div>
	        </div>
	        <div class="row fixed-pos-row">
            <div id="file_picture" class="col-xs-12 col-md-6">
              <div class="form-group">
                <label class="alerta">Cargar Imagen</label>
                <input type="file" name="picture_rooms" id="picture_rooms" class="input-style form-control" onchange='javascript:openFile(event)'/>
                <label class="alerta" id="fileobligatorio">El campo es obligatorio, intente nuevamente</label>
                <label class="alerta" id="fileformato">Solo se permiten imágenes JPG/PNG, intente nuevamente</label>
                <label class="alerta" id="filesize">Peso máximo por imagen 500K, intente nuevamente</label>
                <label class="alerta" id="fileresol">No cumple con la resolución, intente nuevamente</label>
								<label class="alerta" id="filerror">Debe cargar una habitación, intente nuevamente</label>
                <label class="alerta" id="todosobligatorio2">Los campos son obligatorio, intente nuevamente</label>
              </div>
            </div>
            <div id="file_picture" class="col-xs-12 col-md-6">
              <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
                <h3 class="fixed-position-header">Thumbnails</h3>
                <hr>
                <div class="">
                    <img src="{{url('img/default.jpg')}}" id="preview" class="fixed-picture-config">
                    <label class="alerta" id="todosobligatorio7">Los campos son obligatorio, intente nuevamente</label>
                </div>
              </div>
            </div>
		      </div>
					<div class="row fixed-pos-row">
						<h3 class="fixed-position-header">Slider</h3>
						<hr>
						<div class="col-xs-12 col-md-12">
							<button id="btneditar" type="button" class="fixed-button pull-left no-float not-width" onclick="javascript:verpicture()">
									Añadir
									<i class="fa fa-plus"></i>
							</button>
						</div>
					</div>
	        <div class="row fixed-pos-row">
	          <div class="col-xs-6">
	              <button type="button" id="btnmodifypictures" class="center">Guardar</button>
	          </div>
	          <div class="col-xs-6">
	              <button type="button" id="btncrearclose" class="center" data-dismiss="modal">Cerrar</button>
	          </div>
		      </div>
				</form>
			</div>
		</div>
	</div>
</div>
