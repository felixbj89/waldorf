<div class="modal fade" id="profile" role="dialog">
	<div class="modal-dialog fixed-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/MI PERFIL</h4>
			</div>
			<div class="modal-body">
				<p class="text-left alerta">{{Lang::get("message.backendprofile")}}</p>
				<div id="profilering" class="ocultar">
					<label class="center alerta fixed-wait aux-wait">Espere un momento</label>
					<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
				</div>
				<div id="resultadoprofile" class="ocultar">
					<label class="center alert-success fixed-wait">Modificación Sastifactoria</label>
				</div>
				<div id="warningprofile" class="ocultar">
					<label class="center alert-warning fixed-wait">No han habido cambios en su perfil</label>
				</div></br>
				<form id="form-profile">
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
					<input type="hidden" name="passhidden" id="passhidden" value="{{ $usuario->user_password }}" />
					<div class="row">
						<div id="nombre_user" class="col-xs-12 col-md-6">
							<div class="form-group">
								<label class="alerta">Nombre</label>
								<input type="text" name="nombre" id="nombre" readonly="true" class="input-style form-control" placeholder="{{e($nombre)}}" value="{{e($nombre)}}"/>
							</div>
						</div>
						<div id="correo_user" class="col-xs-12 col-md-6">
							<div class="form-group">
								<label class="alerta">E-mail</label>
								<input type="text" name="correo" id="correo" readonly="true" class="input-style form-control" placeholder="{{e($email)}}" value="{{e($email)}}"/>
							</div>
						</div>
						<div id="login_cliente" class="col-xs-12">
							<div class="form-group">
								<label class="alerta">Login/Username</label>
								<input type="text" name="userlogin" id="userlogin" class="input-style form-control" placeholder="* Login/Username" value="{{e($login)}}"/>
							</div>
							<label id="campologin" class="alerta">El campo es obligatorio</label>
						</div>
						<div id="password_cliente" class="col-xs-12">
							<div class="form-group">
								<label class="alerta">Password</label>
								<input type="password" name="userpassword" id="userpassword" class="input-style form-control" placeholder="* Password"/>
							</div>
							<label id="campopassword" class="alerta">El campo es obligatorio</label>
						</div>
						<div id="password_cliente" class="col-xs-12">
							<div class="form-group">
								<label class="alerta">Confirmar</label>
								<input type="password" name="userconfirmar" id="userconfirmar" class="input-style form-control" placeholder="* Confirme su Password"/>
							</div>
							<label id="campoconfirmar" class="alerta">No coinciden las password ingresadas</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<button type="button" id="btnSave">Guardar</button>
						</div>
						<div class="col-xs-6">
							<button type="button" id="btnClose" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
