	<!-- Modal Galeria Habitaciones-->
	<div class="modal fade" id="ModalGallHab" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<i id="Iclose" class="titulo" data-dismiss="modal">X</i>
					<h4 id="Modal-Titulo" class="tituloDeco modal-title text-center"></h4>
				</div>
				<div id="Modal-BodyGH" class="modal-body">
					<div id="contentLigbox" class="row">
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal LightBox -->
	<div id="BootstrapModalImgBox" class="modal">
		<span class="close" id="ModalImgBoxclose">×</span>
		<!--img class="modal-content" id="ModalImgBox" src=""-->
		<!--div id="caption"></div-->
		<div id="BootstrapCarouselLightBox" class="carousel slide carousel-fade" data-ride="carousel"  data-interval="false">
			<div id="BootstrapCarouse-innerlLightBox" class="carousel-inner" role="listbox">
			</div>
			<div class="controlLightBox controlLightBox-right"><img class="arrowLightBox" src="{{url('img/next.png')}}" id="arrowLightBoxNext"></div>
			<div class="controlLightBox controlLightBox-left"><img class="arrowLightBox" src="{{url('img/prev.png')}}" id="arrowLightBoxPrev"></div>
		</div>
	</div>