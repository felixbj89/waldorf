<div class="modal fade" id="editprofile" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/PERFIL</h4>
			</div>
			<div class="modal-body">
				<p class="text-left alerta">{{Lang::get("message.backendprofile")}}</p>
				<div id="editring" class="">
					<label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
					<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
				</div>
				<div id="resultadoedit" class="ocultar">
					<label class="center alert-success fixed-wait">Edición Sastifactoria</label>
				</div>
				<form id="form-editar">
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
					<input type="hidden" name="userpasshidden" id="userpasshidden"/>
          <input type="hidden" name="rutaeditaruser" id="rutaeditaruser" value="{{route('admin/editregistrado')}}"
					<div class="row">
						<div id="nombre_user" class="col-xs-12 col-md-6">
							<div class="form-group">
								<label class="alerta">Nombre</label>
								<input type="text" name="usuarionombre" id="usuarionombre" readonly="true" class="input-style form-control"/>
							</div>
						</div>
						<div id="correo_user" class="col-xs-12 col-md-6">
							<div class="form-group">
								<label class="alerta">E-mail</label>
								<input type="text" name="usuariocorreo" id="usuariocorreo" readonly="true" class="input-style form-control"/>
							</div>
						</div>
            <div id="perfil_user" class="col-xs-12 col-md-12">
              <div class="form-group">
                <label class="alerta">Perfil</label>
                <select id="usuarioperfil" name="usuarioperfil" class="input-style form-control">
                  <option value="-1" class="fuente">Seleccione el perfil deseado</option>
                  <option value="1" class="fuente">Administrador</option>
									<option value="2" class="fuente">Roperador</option>
									<option value="3" class="fuente">Operador</option>
                </select>
                <label class="alerta" id="usuarioperfiltipo">Seleccione un perfil correcto, intente nuevamente</label>
              </div>
            </div>
            <div id="perfil_user" class="col-xs-12 col-md-3">
              <div class="form-group">
                <label class="alerta">Estado</label>
                <div class="">
                    <label id="userestado">

                    </label>
                  </div>
              </div>
            </div>
						<div id="login_cliente" class="col-xs-12 col-md-9">
							<div class="form-group">
								<label class="alerta">Login/Username</label>
								<input type="text" name="uregistradologin" id="uregistradologin" class="input-style form-control" placeholder="* Login/Username"/>
							</div>
							<label class="alerta" id="camporegistradologin">Introduzca un login válido, intente nuevamente</label>
						</div>
						<div id="password_cliente" class="col-xs-12">
							<div class="form-group">
								<label class="alerta">Password</label>
								<input type="password" name="uregistradopassword" id="uregistradopassword" class="input-style form-control" placeholder="* Password"/>
							</div>
							<label id="camporegistradopassword" class="alerta">El campo es obligatorio</label>
						</div>
						<div id="password_cliente" class="col-xs-12">
							<div class="form-group">
								<label class="alerta">Confirmar</label>
								<input type="password" name="userregistradoconfirmar" id="userregistradoconfirmar" class="input-style form-control" placeholder="* Confirme su Password"/>
							</div>
							<label class="alerta" id="camporegistradocpassword">No coinciden las password ingresada, intente nuevamente</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<button type="button" id="btnEditarUsuarioRegistrado">Editar</button>
						</div>
						<div class="col-xs-6">
							<button type="button" id="btnClose" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
