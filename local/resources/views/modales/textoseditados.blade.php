<div class="modal fade" id="textoseditados" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF/TEXTOS EDITADOS</h4>
			</div>
			<div class="modal-body">
        <div class="row fixed-pos-row">
	            <div class="col-xs-12">
                <label class="control-label alerta">Título</label>
	                <input class="form-control input-style" readonly="true" type="text" id="tituloeditado">
	            </div>
              <div id="subtitulo" class="col-xs-12">
                <label class="control-label alerta">Sub-título</label>
	                <input class="form-control input-style" readonly="true" type="text" id="subtituloeditado">
	            </div>
              <div class="col-xs-12">
                <label class="control-label alerta">Texto</label>
	                <p class="input-style" readonly="true" id="mensajeedutadi"></p>
	            </div>
	            <div class="col-xs-12">
	                <button type="button" id="btncrearclose" class="center" data-dismiss="modal">Aceptar</button>
	            </div>
	        </div>
			</div>
		</div>
	</div>
</div>
