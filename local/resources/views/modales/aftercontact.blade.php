<div class="modal fade" id="despuesenviar" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF</h4>
			</div>
			<div class="modal-body">
				<div class="row">
	            <div class="col-xs-12">
	                <p class="text-center alerta mensaje">{{Lang::get("message.despues")}}</p>
	            </div>
	            <div class="col-xs-12">
	                <button type="button" id="btnClose" class="center" data-dismiss="modal">Aceptar</button>
	            </div>
	        </div>
			</div>
		</div>
	</div>
</div>
