<!-- Modal LightBox -->
<div id="modalgalhitoria" class="modal">
	<span id="closeModal" data-dismiss="modal">X</span>
	<div id="BootstrapCarouselLightBox" class="carousel slide carousel-fade" data-ride="carousel"  data-interval="false">
		<ol id="LightBox-indicators" class="carousel-indicators">		
			{!!$galeria["miniatura"]!!}
		</ol>
		<div id="BootstrapCarouse-innerlLightBox" class="carousel-inner" role="listbox" style="height:100%;">		
			{!!$galeria["galeria"]!!}
		</div>
		<div id="arrowLightBoxNext" class="controlLightBox controlLightBox-right"><img class="arrowLightBox" src="{{url('img/next.png')}}"></div>
		<div id="arrowLightBoxPrev" class="controlLightBox controlLightBox-left"><img class="arrowLightBox" src="{{url('img/prev.png')}}"></div>
	</div>
</div>