<div class="modal fade" id="documentacion" role="dialog">
	<div class="modal-dialog fixed-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/{{$title_modal}}: DOCUMENTACIÓN</h4>
			</div>
			<div class="modal-body">
				<form id="form-profile">
					<div class="row center">
						<div class="col-xs-12">
							<label class="control-label clearfix">Recuerde que:</label>
							<div class="row">
								<div class="col-xs-12">
									<ul>
										<li>{{e($recuerde)}}</li>
										<li>
											<span>Para cargar una imagen debe:</span>
											<ul>
												<li><span>Hacer click en el cuadro de la imagen</span></li>
											</ul>
										</li>
										<li>Luego de haber cargado la imagen, click en Agregar</li>
										<li>El sistema le respondera según sea el caso</li>
										<li>Para el historial solo es permitido 6 imágenes en total.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row center">
						<div class="col-xs-12">
							<button type="button" id="btnClose" data-dismiss="modal">Aceptar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
