<div class="modal fade" id="historialhistory" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/HISTORIAL</h4>
			</div>
			<div class="modal-body">
        <div class="row">
          <h2>Use una imagen del historial</h2>
          <hr>
          <div class="col-xs-12 col-md-12 fixed-border">
						<div id="picturering" class="ocultar">
							<label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
							<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
						</div>
						<div id="resultadoprofile">
							<label id="successchange" class="center alert-success fixed-wait fixed-po">Modificación Sastifactoria</label>
						</div>
						<div id="removesastifactorio" class="">
							<label id="removerchange" class="center alert-success fixed-wait fixed-po">Imagen removida Sastifactoriamente</label>
						</div>
						<table id="datatable-history" class="table table-striped table-bordered">
							 <thead>
								 <tr>
									 @if(count($pictures)==0)
									 <th>Imagen</th>
									 @else
									 	<th>Imagen</th>
									 	<th>Opciones</th>
									 @endif
								 </tr>
							 </thead>
							 <tbody>
								 @if(count($pictures)==0)
								 	<tr>
										<td>
											<label class="alerta">No posee imágenes en el historial</label>
											<img src="{{url('img/default.jpg')}}" class="img-responsive">
										</td>
								 	</tr>
								 @else
										@foreach($pictures as $pic)
											<tr data-date="{{$pic->picture_date}}" data-status="{{$pic->id}}">
												<td><img src="{{url($pic->picture_path)}}" id="newimg_{{$pic->id}}" class="img-responsive"></td>
												<td><button type="button" id="historialbtn" class="fixed-button-historial">Cambiar</button>
														<button type="button" id="removerhistorial" class="fixed-button-historial">Remover</button>
												</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</thead>
						</table>
          </div>
        </div>
        <div class="row fixed-row-btn">
            <hr class="colorhr">
            <div class="col-xs-12 col-md-12">
                <button type="button" id="btnclose" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
			</div>
		</div>
	</div>
</div>
