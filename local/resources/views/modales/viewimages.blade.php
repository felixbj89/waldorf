<div class="modal fade" id="viewimages" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF/SLIDER PRINCIPAL</h4>
			</div>
			<div class="modal-body">
        <div class="row fixed-pos-row">
	            <div class="col-xs-12">
                <img id="imagenes" src="{{url('images/default.jpg')}}" class="img-responsive img-thumbnail"/>
	            </div>
	            <div class="col-xs-12">
	                <button type="button" id="btncrearclose" class="center" data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
			</div>
		</div>
	</div>
</div>
