<div class="modal fade" id="textoprincipal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF/TEXTO PRINCIPAL</h4>
			</div>
			<div class="modal-body">
        <div id="picturering" class="ocultar">
          <label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
          <img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
        </div>
        <div id="resultadoprofile">
          <label id="successchange" class="center alerta fixed-wait fixed-po">Guardado Sastifactoria</label>
        </div>
        <input type="hidden" value="" id="texto" name="texto"/>
        <div class="row form-group fixed-row">
          <hr>
          <div id="" class="col-xs-12 col-md-12">
            <label class="alerta">Título</label>
            <input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título a usar"/>
            <label class="alerta" id="titleobligatorio">El campo es obligatorio, intente nuevamente</label>
            <label class="alerta" id="todosobligatorio10">Los campos son obligatorio, intente nuevamente</label>
          </div>
          <div class="col-xs-12 col-md-12">
            <label class="alerta">Texto a ingresar</label>
				<textarea name="descr" id="descr" cols="10" rows="5" class="input-style form-control summernote"></textarea>
            </div>
          </div>
        </div>
        <div class="row form-group  fixed-pos-row">
          <div class="col-xs-6">
              <button type="button" id="btnaddtextoprincipal" class="center">Guardar</button>
          </div>
          <div class="col-xs-6">
              <button type="button" id="btncrearclose" class="center" data-dismiss="modal" style="position: relative;top: 0rem !important;">Cerrar</button>
          </div>
	      </div>
			</div>
		</div>
	</div>
</div>
