<div class="modal fade" id="confirmdelete" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF</h4>
			</div>
			<div class="modal-body">
				<div class="row">
            <input type="hidden" id="correohidden"/>
	          <div class="col-xs-12">
	              <p class="text-center alerta mensaje">{{Lang::get("message.seguro")}}</p>
	          </div>
	          <div class="col-xs-6">
	              <button type="button" id="btnremover" class="center" data-dismiss="modal">Remover</button>
	          </div>
            <div class="col-xs-6">
	              <button type="button" id="btnremoverclose" class="center" data-dismiss="modal">Cerrar</button>
	          </div>
	      </div>
			</div>
		</div>
	</div>
</div>
