<div class="modal fade" id="antesenviar" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF</h4>
			</div>
			<div class="modal-body">
				<p class="text-left alerta mensaje">{{Lang::get("message.antes")}}</p>
				<div id="profilering" class="ocultar">
					<label class="center alerta fixed-wait">Gracias por habernos escogido.</label>
					<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
				</div>
			</div>
		</div>
	</div>
</div>