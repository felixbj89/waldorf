<div class="modal fade" id="acceder" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="loginclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center">HOTEL WALDORF/ADMIN</h4>
			</div>
			<div class="modal-body">
				<p class="text-left">{{Lang::get("message.backendlogin")}}</p>
				<form id="form-login" action="{{url('admin/dashboard')}}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div class="row">
						<div id="login_cliente" class="col-xs-12">
							<div class="form-group">
								<input type="text" name="login" id="login" class="input-style form-control" placeholder="* Login"/>
							</div>
							<label id="campologin" class="alerta">El campo es obligatorio</label>
						</div>
						<div id="password_cliente" class="col-xs-12">
							<div class="form-group">
								<input type="password" name="password" id="password" class="input-style form-control" placeholder="* Password"/>
							</div>
							<label id="campopassword" class="alerta">El campo es obligatorio</label>
						</div>
					</div>
					<div class="row">
						<button type="button" id="btnEnviar">Entrar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
