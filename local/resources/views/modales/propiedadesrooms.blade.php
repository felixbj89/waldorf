<div class="modal fade" id="propiedadesrooms" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo fixed-waitv2" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF/PROPIEDADES</h4>
			</div>
			<div class="modal-body">
        <div id="propiedadesring" class="ocultar">
          <label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
          <img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
        </div>
        <div id="resultadoprofile">
          <label id="successpropiedades" class="center alert-success fixed-wait fixed-po">Modificación Sastifactoria</label>
        </div>
				<form id="form-propiedades" class="tam">
	        <input type="hidden" value="" id="texto" name="texto"/>
					<input type="hidden" value="" id="propilist" name="propilist"/>
	        <div class="row fixed-row">
	          <p class="text-left alerta modal-pos">Busque una Habitación</p>
	          <hr>
	          <div id="" class="col-xs-12 col-md-8">
	            <label class="alerta">Habitaciones</label>
	            <select id="selectcuartos" class="input-style form-control">

	            </select>
	            <label class="control-label alerta" id="selectvalido">Seleccione una habitación válida</label>
	            <label class="control-label alerta" id="disabledsearch">Recuerde buscar una habitación válida</label>
	          </div>
	          <div class="col-xs-12 col-md-4">
	            <button type="button" id="btnseepropiedades" class="center">Buscar</button>
	          </div>
	        </div>
	        <div class="row fixed-pos-row">
	          <div id="titulo_picture" class="col-xs-12 col-md-12">
	            <div class="form-group">
	              <label class="alerta">Título a usar</label>
	              <input disabled type="text" name="titulohabitacion" id="titulohabitacion" class="input-style form-control" placeholder="* Titulo"/>
	              <label class="alerta" id="propiedadesobligatorio1">El campo es obligatorio, intente nuevamente</label>
	              <label class="alerta" id="propiedadesantessave">Antes de guardar debe buscar una habitación válida, intente nuevamente</label>
	              <!--<label class="alerta" id="todosobligatorio3">Los campos son obligatorio, intente nuevamente</label>-->
	            </div>
	          </div>
		      </div>
	        <div class="row fixed-pos-row">
	          <div id="titulo_picture" class="col-xs-12 col-md-6">
	            <div class="form-group">
	              <label class="alerta">Capacidad Máxima</label>
	              <input disabled type="number" name="capacidad" id="capacidad" class="input-style form-control" placeholder="* Capacidad Máxima" min="1" max="4"/>
	              <!--<label class="alerta" id="todosobligatorio4">Los campos son obligatorio, intente nuevamente</label>-->
	            </div>
	          </div>
	          <div class="col-xs-12 col-md-6">
	            <div class="form-group">
	              <label class="alerta">Activo</label>
	              <select disabled id="onhab" name="onhab" class="input-style form-control">
	                  <option value="NULL" class="input-style">Seleccione una opción de su agrado</option>
	                  <option value="1" class="input-style">Activo</option>
	                  <option value="0" class="input-style">Inactivo</option>
	              </select>
	              <label class="alerta" id="estadoobligatorio">El campo es obligatorio, intente nuevamente</label>
	              <!--<label class="alerta" id="todosobligatorio9">Los campos son obligatorio, intente nuevamente</label>-->
	            </div>
	          </div>
	        </div>
	        <div id="checkinout" class="row">
	          <div class="col-xs-12 col-md-6">
	            <div class="form-group">
	              <label class="alerta">Check In</label>
	              <input disabled type="text" name="checkin" id="checkin" class="time start input-style form-control" placeholder="* Check In"/>
	              <label class="alerta" id="checkinobligatorio">El campo es obligatorio, intente nuevamente</label>
	              <!--<label class="alerta" id="todosobligatorio5">Los campos son obligatorio, intente nuevamente</label>-->
	            </div>
	          </div>
	          <div class="col-xs-12 col-md-6">
	            <div class="form-group">
	              <label class="alerta">Check Out</label>
	              <input disabled type="text" name="checkout" id="checkout" class="time end input-style form-control" placeholder="* Check Out"/>
	              <label class="alerta" id="checkoutobligatorio">El campo es obligatorio, intente nuevamente</label>
	              <!--<label class="alerta" id="todosobligatorio6">Los campos son obligatorio, intente nuevamente</label>-->
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <h3 class="fixed-position-header modal-pos">Propiedades de la Habitación</h3>
	          <hr>
	          <div class="col-xs-12 col-md-8 col-lg-8 col-xl-8">
	            <div class="form-group">
	              <div class="form-group">
	                <label class="alerta">Propiedades</label>
	                <input disabled type="text" name="propiedadeshab" id="propiedadeshab" class="time end input-style form-control" placeholder="* Propiedades"/>
	                <!--<label class="alerta" id="propiedadesobligatorio">El campo es obligatorio, intente nuevamente</label>
	                <label class="alerta" id="todosobligatorio10">Los campos son obligatorio, intente nuevamente</label>-->
	              </div>
	            </div>
	          </div>
	          <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
	            <div class="form-group">
	                <button type="button" id="btnAddproperties">Añadir</button>
	            </div>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-xs-12 col-md-12 col-lg-12 col-xl-12">
	              <table id="datatable-propiedades" class="table table-striped table-bordered">
	                <thead>
	                    <tr>
	                      <th>Característica</th>
	                      <th>Opciones</th>
	                    </tr>
	                </thead>
	                <tbody>
	                </tbody>
	              </table>
	          </div>
	        </div>
	        <div class="row fixed-pos-row">
	          <div class="col-xs-6">
	              <button type="button" id="btnmodifypropieades" class="center">Guardar</button>
	          </div>
	          <div class="col-xs-6">
	              <button type="button" id="btncrearclose" class="center" data-dismiss="modal">Cerrar</button>
	          </div>
		      </div>
				</form>
			</div>
		</div>
	</div>
</div>
