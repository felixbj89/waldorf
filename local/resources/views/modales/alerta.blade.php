<div class="modal fade" id="alerta" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/AVISO</h4>
			</div>
			<div class="modal-body">
        <div class="row">
          <hr>
          <div class="col-xs-12 col-md-12 fixed-border">
						<div id="picturering" class="">
							<label id="errormensajelogin" class="center alerta fixed-wait fixed-wait-aux"></label>
						</div>
          </div>
        </div>
        <div class="row fixed-row-btn">
            <hr class="colorhr">
            <div class="col-xs-12 col-md-12">
                <button type="reset" id="btnclose" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
			</div>
		</div>
	</div>
</div>
