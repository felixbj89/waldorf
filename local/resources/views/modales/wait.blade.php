<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/AVISO</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-12 fixed-border">
						<div id="picturering" class="">
							<label id="mensaje" class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
						</div>
					</div>
					<div class="col-xs-12 col-md-12 fixed-border">
						<img src="{{url('img/ring.svg')}}" class="img-responsive center">
					</div>
				</div>
				<div class="row fixed-row-btn">
					<hr class="colorhr">
				</div>
			</div>
		</div>
	</div>
</div>
