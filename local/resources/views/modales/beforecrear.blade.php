<div class="modal fade" id="antescrear" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="wait" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta title-waldorf">HOTEL WALDORF</h4>
			</div>
			<div class="modal-body">
				<p id="mensajepropiedades" class="text-left alerta mensaje">{{Lang::get("message.antescrear")}}</p>
				<div id="profilering" class="">
					<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
				</div>
			</div>
		</div>
	</div>
</div>
