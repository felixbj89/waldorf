<div class="modal fade" id="editarow" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/EDITAR</h4>
			</div>
			<div class="modal-body">
				<div class="row form-group">
					<div class="col-xs-12 col-md-12 fixed-border">
						<label class="clearfix control-label">Propiedad</label>
						<input type="text" id="rowedit" name="rowedit" class="form-control">
					</div>
				</div>
				<div class="row fixed-row-btn">
					<hr class="colorhr">
					<div class="col-xs-12 col-md-12">
						<button type="reset" id="btnREditar">Aceptar</button>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>
