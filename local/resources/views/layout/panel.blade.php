<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	
	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Nuestro hotel recibe el nombre Waldorf en honor al afamado Waldorf of Astoria, Ubicado en la Candelaria  en Caracas, Venezuela ven a Hotel Waldorf calidad y servicio a tu alcance."/>
    <title>Hotel Waldorf</title>
	<link rel="shortcut icon" type="image/png" href="{{url('img/favicon.png')}}"/>
	<!-- Font Awesome -->
	<link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
	{!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('vendors/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css')!!}
	{!!Html::style('css/panelWaldorf.css')!!}
	@yield("Mycss")
  </head>
  <body style="display: block;">
	<input type="hidden" id="abrirmodal" value="{{$modallogin}}"/>
	@if($usuario!=NULL)
		<div class="row aux-row" style="margin:0px;">
			<div class="container-waldorf">
				<div id="navbarWaldorf-container" class="navbarWaldorf-ini">
					<nav id="navbarWaldorf" class="navbar navbar-default" role="navigation">
					  <div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
								data-target=".navbar-ex1-collapse">
						  <span class="sr-only">Desplegar navegación</span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						</button>
						<!--a class="navbar-brand" href="#">Logotipo</a-->
					  </div>
					  <div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-collapse-waldorf">
						<ul class="nav navbar-nav fixed-space-li">
						  <li><a href="{{url('admin/dashboard')}}" class="link @yield('Home-nav')">Dashboard</a></li>
                          <li><a href="{{url('admin/resuperior')}}" class="link @yield('Home-nav')">Restaurante</a></li>
						  <li><a href="{{url('admin/habitaciones')}}" class="link @yield('Home-nav')">Habitaciones</a></li>
						</ul>
						<ul id="navbar-social" class="nav navbar-nav navbar-right" style="margin:15px 0px!important;">
							<!--li><a href="#">#1</a></li-->
							<li><a href="{{url('admin/salir')}}"><i class="fa fa-power-off icon-config"></i></a></li>
							<!--li><a href="#">#4</a></li-->
						</ul>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	@endif
  @if(Session::has('error'))
    <input type="hidden" value="{{Session::get('error')}}" id="error"/>
  @endif
	<div class="row background-container" style="margin:0px;">
		<div class="container-waldorf">
			<!-- Home Slider-->
			<div id="home-topage">
				<div id="logoWaldorf"></div>
				@yield("SliderWaldorf")
			</div>
			<!-- Barra Nav -->
			<div id="navbarWaldorf-container" class="navbarWaldorf-ini">
				<nav id="navbarWaldorf" class="navbar navbar-default" role="navigation">
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-ex1-collapse">
					  <span class="sr-only">Desplegar navegación</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
					<!--a class="navbar-brand" href="#">Logotipo</a-->
				  </div>
				  <div class="collapse navbar-collapse navbar-ex1-collapse" id="navbar-collapse-waldorf">
					<ul class="nav navbar-nav fixed-space-li">
						<li><a href="{{url('/')}}" class="link @yield('Home-nav') homePOS">Home</a></li>
						<li><a href="{{url('hotel')}}" class="link @yield('NuestroHotel-nav')">El Hotel</a></li>
						<li><a href="{{url('Habitaciones')}}" class="link @yield('Habitaciones-nav')">Habitaciones</a></li>
						<li><a href="{{url('Restaurante')}}" class="link @yield('Restaurante-nav')">Restaurante</a></li>
						<li><a href="@yield('tolinks')" class="link">Contacto</a></li>
					</ul>
					<form id="nav-btn-reserva" class="navbar-form navbar-left">
						<button id="btn-wal-res" type="button" class="">Reserva</button>
					</form>
					<ul id="navbar-social" class="nav navbar-nav navbar-right" style="margin:15px 0px!important;">
						<!--li><a href="#">#1</a></li-->
						<li><a href="https://www.instagram.com/hotelwaldorfccs/?hl=en" class="inst" target="_blank"></a></li>
						<li><a href="https://www.facebook.com/HotelWaldorfCCS/?fref=ts" class="face" target="_blank"></a></li>
						<li><a href="https://twitter.com/hotelwaldorfccs" class="twitter" target="_blank"></a></li>
					</ul>
				  </div>
				</nav>
			</div>
			@yield("body-page")
			<!-- footer -->
			<div id="footer" class="row" style="margin:0px;padding:0px;">
				<div class="row" style="margin:10px 0px;padding:0px 10px;">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
							<a href="{{url('/')}}" class="btn-footer" id="btn-home">HOME</a>
						</div>
						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
							<span class="btn-footer" id="btn-nuestroh">El HOTEL</span>
							<div id="footer-nuestroh" style="display:block">
								<ul id="nu" style="list-style: none;color:#FFF;padding-left: 0px;">
									<li><a href="{{url('hotel')}}/historia">Historia</a></li>
									<li><a href="">Valores Corporativos</a></li>
									<li><a href="">Vision Emprendedora</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
							<span class="btn-footer" id="btn-habitaciones">HABITACIONES</span>
							<div id="footer-habitaciones" style="display:block">
								<ul id="ha" style="list-style: none;color:#FFF;padding-left: 0px;">
									<?php $i = 1; ?>
									@foreach($cuartos as $cu)
										<?php $c = json_decode($cu->rooms_especificaciones);?>
										@if($c[0]->activo==1)
											<li><a href="{{url('Habitacion/'.$i++)}}">{{e($c[1]->titulo)}}</a></li>
										@endif
									@endforeach
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12 text-center" style="padding:5px">
							<a href="{{url('Restaurante')}}" class="btn-footer" id="btn-restaurante">RESTAURANTE</a>
						</div>
					</div>
				</div>
				<div class="row" style="margin:10px 0px;padding:0px 10px;">
					<a href="https://www.google.co.ve/maps/dir/''/hotel+waldorf+caracas/data=!4m5!4m4!1m0!1m2!1m1!1s0x8c2a592d27509917:0x7bdcdfb71507103a?sa=X&ved=0ahUKEwif9L_3lNLTAhVETCYKHfAgB4QQ9RcIiAEwDw" target="_blank"><i id="location"></i></a>
					<p class="text-center" style="color:#fff;">Av. La Industria, Esquina Campo Elias a Puente Anauco, Edif. Hotel Waldorf PB, Urbanización La Candelaria</br>Caracas, Venezuela. Telf Master: +58  212 507.33.00</p>
					<ul id="footer-social" class="nav navbar-nav" style="float:initial;width:66px;margin:10px auto;display: block;">
						<!--li><a href="#">#1</a></li-->
						<li><a href="https://www.instagram.com/hotelwaldorfccs/?hl=en" class="inst" target="_blank"></a></li>
						<li><a href="https://www.facebook.com/HotelWaldorfCCS/?fref=ts" class="face" target="_blank"></a></li>
						<li><a href="https://twitter.com/hotelwaldorfccs" class="twitter" target="_blank"></a></li>
					</ul>
				</div>
				<p id="copy">&copy; 2016 Hotel Waldorf.J-00018918-8. Todos los derechos reservados Powered by <a href="http://www.haciendacreativa.com.ve">Hacienda Creativa.</a></p>
			</div>
		</div>
		<!--div id="footer">
			<div class="container-waldorf-footer">

				<!--<div class="row" style="padding:10px;background-color:#e4dcca;margin:0px auto;max-width:980px;"><div id="footerWaldorf" class="row" style="margin:0px;"></div></div>-->
			<!--/div>
		</div-->
	</div>
	<!-- Boton Reserva -->
	<div id="BotonRes" class="container-waldorf" style="height:0px;">
		<form id="nav-btn-reserva-float" style="height:0px;" class="oculto">
			<button id="btn-wal-res-float" type="button">Reserva</button>
		</form>
	</div>
	@yield("modal-page")
	@include("modales.backlogin")
  @include("modales.beforecontact")
  @include("modales.aftercontact")
  @include("modales.beforereserva")
  @include("modales.afterreserva")
  @include("modales.alerta")
	{!!Html::script('js/jquery-3.1.1.min.js')!!}
	{!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('vendors/maskedinput/jquery.maskedinput.min.js')!!}
    {!!Html::script('js/moment.js')!!}
    {!!Html::script('js/es.js')!!}
    {!!Html::script('vendors/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js')!!}
	{!!Html::script('js/panelWaldorf.js')!!}
  {!!Html::script('js/contactForm.js')!!}
	@yield("Myscript")
  

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96696190-1', 'auto');
  ga('send', 'pageview');

</script>

 
  </body>
</html>
