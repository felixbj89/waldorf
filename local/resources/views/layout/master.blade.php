<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{Lang::get('message.titulo')}}</title>
		<!-- Bootstrap -->
		<link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<!-- NProgress -->
		<link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
		<!-- CSS Style -->
		<link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
		<link href="{{asset('css/backend.css')}}" rel="stylesheet">
		@yield("mi-css")
		<link rel="shortcut icon" type="image/png" href="{{url('img/favicon.png')}}"/>
	</head>
	<body class="nav-md" style="padding-right:0px !important;">
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col menu_fixed mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar">
					<div class="left_col scroll-view">
						<div class="navbar nav_title" style="border: 0;">
							<a href="{{url('admin/salir')}}" class="site_title">
							@if($usuario->user_status==0)
								<i class='fa fa-power-off off'></i>
							@else
								<i class='fa fa-power-off on'></i>
							@endif
							<span>Hotel Waldorf</span></a>
						</div>
						<div class="clearfix"></div>
						<!-- menu profile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="{{url('img/waldorf.png')}}" alt="Hotel Waldorf" class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span>Bienvenido,</span>
								<h2>{{e($usuario->user_name)}}</h2>
							</div>
						</div>
					<!-- /menu profile quick info -->
				<br />
				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<h3>General</h3>
						<ul class="nav side-menu">
							<li>
								<a href="{{url('admin/dashboard')}}">
									<i class="fa fa-dashboard"></i> Inicio
								</a>
							</li>
							<li>
								<a data-toggle="modal" data-target="#profile">
									<i class="fa fa-user"></i> Mi Perfil
								</a>
							</li>
							@if($usuario->user_role == 0 || $usuario->user_role == 1 || $usuario->user_role == 3)
								<li>
									<a>
										<i class="fa fa-users"></i> Usuarios
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu">
										<li><a href="{{url('admin/usuarios')}}">Crear</a></li>
										<li><a href="{{url('admin/listar')}}">Listar</a></li>
									</ul>
								</li>
							@endif
							@if($usuario->user_role == 0 || $usuario->user_role == 1 || $usuario->user_role == 3)
								<li>
									<a>
										<i class="fa fa-home"></i> Home
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu">
										<li><a href="{{url('admin/crearhslider')}}">Portada Principal</a></li>
										<li><a href="{{url('admin/crearshslider')}}">Galeria Habitaciones</a></li>
										<li>
											<a>
												Hotel/Historia
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_inferior')">
												<li><a href="{{url('admin/history')}}">Crear</a></li>
												<li><a href="{{url('admin/hishome')}}">Historial</a></li>
											</ul>
										</li>
										<li><a href="{{url('admin/gastronomia')}}">Gastronomía</a></li>
										<li>
											<a>
												Contacto
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_inferior')">
												<li><a href="{{url('admin/crearicontacto')}}">Crear</a></li>
												<li><a href="{{url('admin/vercfotos')}}">Historial</a></li>
											</ul>
										</li>
										<li>
											<a>
												Promociones
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_inferior')">
												<li><a href="{{url('admin/crearpromociones')}}">Crear</a></li>
												<li><a href="{{url('admin/verpromos')}}">Historial</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="@yield('subactive')">
									<a>
										<i class="fa fa-building"></i> El Hotel
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu" style="@yield('displaysub')">
										<li>
											<a>
												Foto Principal
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_superior')">
												<li><a href="{{url('admin/crearsehprincipal')}}">Crear</a></li>
												<li><a href="{{url('admin/verehfotos')}}">Historial</a></li>
											</ul>
										</li>
										<li>
											<a href="{{url('admin/crearhotelgaleria')}}">
												Galeria: El Hotel
											</a>
										</li>
									</ul>
								</li>
								<li class="@yield('subactive')">
									<a>
										<i class="fa fa-hotel"></i> Habitación
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu" style="@yield('displaysub')">
										<li><a href="{{url('admin/habitaciones')}}">Actualizar</a></li>
										<li>
											<a>
												Foto Principal
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_superior')">
												<li><a href="{{url('admin/crearshprincipal')}}">Crear</a></li>
												<li><a href="{{url('admin/verhfotos')}}">Historial</a></li>
											</ul>
										</li>
										<li>
											<a>
												Foto Secundaria
												<span class="fa fa-chevron-down"></span>
											</a>
											<ul class="nav child_menu" style="@yield('banner_inferior')">
												<li><a href="{{url('admin/crearhinferior')}}">Crear</a></li>
												<li><a href="{{url('admin/verhifotos')}}">Historial</a></li>
											</ul>
										</li>
									</ul>
								</li>
							@endif
							<li>
								<a>
									<i class="fa fa-cutlery"></i> Restaurante
									<span class="fa fa-chevron-down"></span>
								</a>
								<ul class="nav child_menu">
									<li>
										<a>
											Foto Principal
											<span class="fa fa-chevron-down"></span>
										</a>
										<ul class="nav child_menu" style="@yield('banner_superior')">
											<li><a href="{{url('admin/crearsrprincipal')}}">Crear</a></li>
											<li><a href="{{url('admin/verrfotos')}}">Historial</a></li>
										</ul>
									</li>
									<li>
										<a>
											Foto Secundaria
											<span class="fa fa-chevron-down"></span>
										</a>
										<ul class="nav child_menu" style="@yield('banner_inferior')">
											<li><a href="{{url('admin/crearrinferior')}}">Crear</a></li>
											<li><a href="{{url('admin/verrifotos')}}">Historial</a></li>
										</ul>
									</li>
									<li><a href="{{url('admin/rslider')}}">Galería: Restaurante</a></li>
									<li><a href="{{url('admin/eventos')}}">Eventos</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /sidebar menu -->
			</div>
		</div>
		<!-- top navigation -->
		<div class="top_nav">
			<div class="nav_menu">
				<nav>
					<div class="nav toggle">
						<a id="menu_toggle"><i class="fa fa-bars"></i></a>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<img src="{{url('img/waldorf.png')}}" alt="">{{e($usuario->user_name)}}
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="#" data-toggle="modal" data-target="#profile"> Perfil</a></li>
								<li><a href="{{url('admin/salir')}}"><i class="fa fa-sign-out pull-right"></i> Salir</a></li>
							</ul>
						</li>
						@yield("notificaciones-proximas")
						@yield("notificaciones-rangos")
						<li role="presentation">
							<a href="{{url('admin/salir')}}" aria-expanded="false">
								<i class="fa fa-sign-out"></i>
							</a>
						</li>
						<li role="presentation">
							<a href="{{url('admin/bloqueado')}}" aria-expanded="false">
								<i class="fa fa-lock"></i>
							</a>
						</li>
						<li role="presentation">
							<a href="{{url('admin/autenticado')}}" aria-expanded="false">
								<i class="fa fa-link"></i>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- /top navigation -->
		<!-- page content -->
		@section("page-body")
			<div id="panel" class="right_col" role="main">
				<div class="x_panel">
					<div class="x_title">
						<h2>Accesos Directos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row top_tiles">
							@yield("go-rooms")
							@yield("go-principal")		
							@yield("go-promociones")
						</div>
					</div>
				</div>
				<div class="x_panel">
					<div class="x_title">
						<h2>Visitas Generales</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@yield("go-visitas")
					</div>
				</div>
				<div id="subpanel" class="x_panel">
					<div class="x_title">
						<h2>Resumenes Generales</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@yield("go-navegadores")
						@yield("go-dispositivos")
					</div>
				</div>
				<div class="x_panel">
					<div class="x_title">
						<h2 class="pull-left">Ultimos Registros</h2>
						<h2 class="pull-right">Resumenes Generales</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						@yield("go-registros")
						@yield("go-secciones")
					</div>
				</div>
				<!-- footer content -->
				<footer>
					<div class="pull-right">
						&copy; 2016 Hotel Waldorf V2.5, Powered By <a href="https://www.haciendacreativa.com.ve">Hacienda Creativa C.A.</a>
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		@show
		@yield("body")
		@include("modales.profile")
		@include("modales.textoseditados")
		@include("modales.viewimages")
		@yield("modales")
		<!-- jQuery -->
		<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
		<!-- Bootstrap -->
		<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
		<!-- FastClick -->
		<script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
		<!-- NProgress -->
		<script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
		<!-- Chart.js -->
		<script src="{{asset('vendors/Chart.js/dist/Chart.min.js')}}"></script>
		<!-- jQuery Sparklines -->
		<script src="{{asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
		<!-- Flot -->
		<script src="{{asset('vendors/Flot/jquery.flot.js')}}"></script>
		<script src="{{asset('vendors/Flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('vendors/Flot/jquery.flot.time.js')}}"></script>
		<script src="{{asset('vendors/Flot/jquery.flot.stack.js')}}"></script>
		<script src="{{asset('vendors/Flot/jquery.flot.resize.js')}}"></script>
		<!-- Flot plugins -->
		<script src="{{asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
		<script src="{{asset('vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
		<script src="{{asset('vendors/flot.curvedlines/curvedLines.js')}}"></script>
		<!-- DateJS -->
		<script src="{{asset('vendors/DateJS/build/date.js')}}"></script>
		<!-- bootstrap-daterangepicker -->
		<script src="{{asset('js/moment/moment.min.js')}}"></script>
		<script src="{{asset('js/datepicker/daterangepicker.js')}}"></script>
		<!-- Custom Theme Scripts -->
		<script src="{{asset('build/js/custom.min.js')}}"></script>
		<script src="{{asset('js/backend.js')}}"></script>
		@yield("mi-script")
		@yield("mi-dashboard")
	</body>
</html>
