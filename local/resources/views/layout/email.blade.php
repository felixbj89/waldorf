<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>HC | ACTIVAR</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="dist/css/style.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="plugins/iCheck/square/blue.css">
		<!-- JS -->
		<!-- jQuery 2.1.4 -->
		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="dist/js/main.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div style="display:block;margin-left:auto;margin-right:auto;">
			<!--<img src="{{$message->embed('logohcoriginal.jpg')}}" style="display:block;margin-left:43%;margin-right:auto;width:180px"/>-->
		</div>
		<div style="display:block;margin-left:auto;margin-right:auto">
			@if($formulario[0]["value"]=="eventos")
                <p style="text-align:left">
                    Vino por <span style="color:gray">Recepción</span>
                </p>
            @elseif($formulario[0]["value"]=="restaurante")
                <p style="text-align:left">
                    Vino por <span style="color:gray">Grupo y Convenciones</span>
                </p>
            @elseif($formulario[0]["value"]=="mercadeo")
                <p style="text-align:left">
                    Vino por <span style="color:gray">Reservación</span>
                </p>
            @endif
            <p style="text-align:left">
                Persona <span style="color:gray">{{e($formulario[1]["value"])}}</span>
            </p>
			<p style="text-align:left">
				Correo <span style="color:red">{{e($formulario[2]["value"])}}</span>
			</p>
            <p style="text-align:left">
				Asunto <span style="color:red">{{e($formulario[3]["value"])}}</span>
			</p>
			<p style="text-align:left">
				Tel&eacute;fono <span style="color:red">{{e($formulario[4]["value"])}}</span>
			</p>
			<p style="text-align:justify">
				{{e($formulario[5]["value"])}}
			</p>
		</div>
		</br>
		<div style="text-align:center;padding-top:5px">
			<span>Equipo de Marketing</span>
		</div>
		<div style="text-align:center">
			<strong>Hotel Waldorf</strong>
		</div>
		<div style="text-align:center">
			<span>J-31291786-5</span>
		</div>
	</body>
</html>