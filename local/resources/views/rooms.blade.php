@extends("layout.panel")
@section("Habitaciones-nav")
	active
@endsection
@section("Mycss")
	{!!Html::style('css/roomsWaldorf.css')!!}
@endsection
@section("SliderWaldorf")
	<div id="slider-topage" style="background-image:url({{url($urlsuperior)}});" alt="{{$nombresuperior}}"></div>
@endsection
@section("tolinks")
	{{url('Home/Contacto')}}
@endsection
@section("body-page")
	<!-- Habitaciones -->
	<div id="Habitaciones" class="grid">
		@if($notmach==0)
			<div id="KingStandar-grid" class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="KingDeluxe-grid" class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="DobleSuperior-grid" class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="KingSuperior-grid" class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="SingleStandar-grid" class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="grid-Text" class="grid-item-width grid-item-width-2 grid-item-height">
				<input type="hidden" value="{{$textopri}}" id="textonuevoevento"/>
				<div class="grid-item-text">
					<table class="TableP"><tbody><tr><td>
						<p class="titulo text-center">{{e($titulopri)}}</p>
						<p id="ParrafoHabitaciones">

						</p>
					</td></tr></tbody></table>
				</div>
			</div>
			<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div id="grid-TextI" class="grid-item-width grid-item-width-2 grid-item-height">
				<div class="grid-item-text">
					<table class="TableP"><tbody><tr><td>
						<p class="titulo text-center">{{e($titulopri)}}</p>
						<p id="ParrafoHabitaciones">

						</p>
					</td></tr></tbody></table>
				</div>
			</div>
			<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
		@else
			@if($habitaciones[0]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[0]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
				<div id="1-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[0]->rooms_thumbnail)}});">
					<p class="Thabi">{{$json[1]->titulo}}</p>
					<div class="Dhabi">
						<table class="TableP"><tbody><tr><td>
							<p class="tituloH">{{$json[1]->titulo}}</p>
							<button class="btn-Detalles" value="{{$habitaciones[0]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[0]->rooms_positions}}">Detalles</button>
						</td></tr></tbody></table>
					</div>
				</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[0]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[1]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[1]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="2-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[1]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[1]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[1]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[1]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[2]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[2]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="3-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[2]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[2]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[2]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[2]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[3]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[3]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
				<div id="4-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[3]->rooms_thumbnail)}});">
					<p class="Thabi">{{$json[1]->titulo}}</p>
					<div class="Dhabi">
						<table class="TableP"><tbody><tr><td>
							<p class="tituloH">{{$json[1]->titulo}}</p>
							<button class="btn-Detalles" value="{{$habitaciones[3]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[3]->rooms_positions}}">Detalles</button>
						</td></tr></tbody></table>
					</div>
				</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[3]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[4]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[4]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="5-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[4]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[4]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[4]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[4]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			<div id="grid-Text" class="grid-item-width grid-item-width-2 grid-item-height">
				<input type="hidden" value="{{$textopri}}" id="textonuevoevento"/>
				<div class="grid-item-text">
					<table class="TableP"><tbody><tr><td>
						<p class="titulo text-center">{{e($titulopri)}}</p>
						<p id="ParrafoHabitaciones">

						</p>
					</td></tr></tbody></table>
				</div>
			</div>

			@if($habitaciones[5]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[5]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="6-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[5]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[5]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[5]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[5]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			<div id="grid-TextI" class="grid-item-width grid-item-width-2 grid-item-height">
				<div class="grid-item-text">
					<table class="TableP"><tbody><tr><td>
						<p class="titulo text-center">{{e($titulopri)}}</p>
						<p id="ParrafoHabitaciones">

						</p>
					</td></tr></tbody></table>
				</div>
			</div>

			@if($habitaciones[6]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[6]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="7-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[6]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[6]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[6]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[6]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[7]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[7]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="8-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[7]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[7]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[7]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[7]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[8]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[8]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="9-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[8]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[8]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[8]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[8]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif

			@if($habitaciones[9]->rooms_codigo=="default.jpg")
				<div class="grid-item-width grid-item-height" style="background-image:url({{URL('img/habdefault.jpg')}});"></div>
			@else
				<?php $json = json_decode($habitaciones[9]->rooms_especificaciones); ?>
				@if($json[0]->activo==1)
					<div id="10-grid" class="grid-item-width grid-item-height habiDis" style="background-image:url({{URL($habitaciones[9]->rooms_thumbnail)}});">
						<p class="Thabi">{{$json[1]->titulo}}</p>
						<div class="Dhabi">
							<table class="TableP"><tbody><tr><td>
								<p class="tituloH">{{$json[1]->titulo}}</p>
								<button class="btn-Detalles" value="{{$habitaciones[9]->rooms_positions}}" data-titulo="{{$json[1]->titulo}}" data-thumbnails="{{$habitaciones[9]->rooms_positions}}">Detalles</button>
							</td></tr></tbody></table>
						</div>
					</div>
				@else
					<div class="grid-item-width grid-item-height" style="background-image:url({{URL($habitaciones[9]->rooms_thumbnail)}});"><p class="proximamente">PRÓXIMAMENTE</p></div>
				@endif
			@endif
		@endif
	</div>
	<!-- Detalles -->
	<div id="Detalles">
		<div id="HabitacionDetalles" class="row row-DetHab">
			<div class="col-sm-5 col-xs-12 Contlista-HabDet" style="height:400px;">
				<table class="TableP"><tbody><tr><td>
					<p id="TituloHabitacionDetalles" class="titulo text-right"></p>
					<p class="text-right"><em id="ChekCap" class="ChekCap"></em></p>
					<p id="ListaHabitacionDetalles" class="lista-HabDet"></p>
					<div class="container-btnHabDet">
						<button id="btnRegresar" class="btn-HabDet btnRegresar" value="SingleStandar-grid">Regresar</button>
						<button id="btnGaleria" class="btn-HabDet btnGaleria">Galeria</button>
					</div>
				</td></tr></tbody></table>
			</div>
			<div id="gridHabiDetails" class="col-sm-7 col-xs-12 gridHabDet" style="padding:0px;height:400px;"></div>
		</div>
	</div>
	<!-- Reserva -->
	<div id="Reserva">
		<div id="bannerReserva" class="row" style="background-image:url({{url($urlinferior)}});" alt="{{$nombreinferior}}">
			<table class="TableP"><tbody><tr><td>
				<p id="titulo-SliderRes" class="titulo">Vive toda una experiencia<br/>y <b>reserva online</b> con nosotros<br/><em><b>¡Te invitamos!</b></em></p>
				<button id="btn-SliderRes">Reserva</button>
			</td></tr></tbody></table>
		</div>
	</div>
	<!-- Arriba -->
	<div id="Arriba">
		<i class="glyphicon glyphicon-menu-up"></i>
	</div>
@endsection
@section("modal-page")
	@include("modales.galleryHab")
@endsection
@section("Myscript")
	{!!Html::script('js/plugins/jquery.masonry.min.js')!!}
	{!!Html::script('js/roomsWaldorf.js')!!}
<script>
jQuery(function ($) {
	$(document).ready(function (){
		$("#btn-SliderRes").on("click",function(){
			var pictureurl = "";
			var pos = window.location.href.search("Habitaciones");
			if(pos > 0){
				pictureurl = window.location.href.substr(0,pos-1);
			}

			window.location.href =  pictureurl + "/waldorf/reservar";
		});

		if($("#textonuevoevento").val()=="<TEXTO>"){
  		$("#ParrafoHabitaciones").text($("#textonuevoevento").val());
  	}else{
  		$("#ParrafoHabitaciones").html($("#textonuevoevento").val());
  	}

		var id="<?php echo $id;?>";
		if(id!=""){
			$.fn.AnimateScroll(id);
		}
		$('.grid').masonry({
		  itemSelector: '.grid-item-width',
		  percentPosition: true
		});
	});
	$('.btnGaleria').click(function(){
		var titulo = $(this).data('titulo');
		$.ajax({
			type:'GET',
			url:'{{url('/habitacionType')}}',
			data:{'hab':$(this).data('thumbnails')},
			success:function(res){
				$('#Modal-Titulo, #contentLigbox').empty();
				$('#Modal-Titulo').append(titulo);
				$('#contentLigbox').html(res);
				$('#ModalGallHab').modal('toggle');
			},
			error:function(xhr,status){
				alert('Problemas...');
			},
		})
	});
	$('.btn-Detalles').click(function(){
		var titulo = $(this).data('titulo');
		var thumbnails = $(this).data('thumbnails');
		$.fn.AnimateScroll(thumbnails);
	});
	$.fn.AnimateScroll = (function(thumbnails){
		$.ajax({
			type:'GET',
			url:'{{url('/habitacionTypeDetails')}}',
			data:{'hab': thumbnails},
			success:function(res){
				$('#TituloHabitacionDetalles,#ListaHabitacionDetalles,#ChekCap,#gridHabiDetails').empty();
				$('#btnGaleria').removeData(['titulo','thumbnails']);
				$('#btnRegresar').removeData('prev');
				$('#TituloHabitacionDetalles').html(res.titulo);
				$('#ListaHabitacionDetalles').html(res.lista);
				$('#ChekCap').html("(Cap.M&aacute;x "+res.capMax+")<br/>Check In "+res.chkIN+" Hrs - Check Out "+res.chkOUT	+" Hrs");
				$('#btnGaleria').attr({"data-titulo":res.titulo,"data-thumbnails":thumbnails})
				$('#btnRegresar').attr("data-prev",thumbnails+"-grid")
				$('#gridHabiDetails').html(res.grid);
				$('#Detalles, #HabitacionDetalles').show();
				$('html, body').animate({scrollTop: ($("#HabitacionDetalles").offset().top)-150}, 1500);
			},
			error:function(xhr,status){
				alert('Problemas...');
			},
		})
	});
	$.fn.ModalToggle = (function(route,clave){
		//$('#ModalImgBox').attr('src',route);
		$.ajax({
			type:'GET',
			url:'{{url('/habitacionTypeGallery')}}',
			data:{'hab':route},
			success:function(res){
				$('#BootstrapCarouse-innerlLightBox').empty();
				$('#BootstrapCarouse-innerlLightBox').html(res);
				$('#'+clave).addClass('active');
				$('#BootstrapModalImgBox').modal('toggle');
				$('#ModalGallHab').modal('toggle');
			},
			error:function(xhr,status){
				alert('Problemas...');
			},
		})
	});
});
</script>
@endsection
