<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!-- Meta, title, CSS, favicons, etc. -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>{{Lang::get('message.titulo')}}</title>
      <!-- Bootstrap -->
      <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
      <!-- NProgress -->
      <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
      <!-- Animate.css -->
      <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
	  <link href="{{asset('css/backend.css')}}" rel="stylesheet">
      <link rel="shortcut icon" type="image/png" href="{{url('img/favicon.png')}}"/>
    </head>
    <body class="login">
      @if($bloqueado=="1")
        <input type="hidden" id="backendlock" value="1"/>
	  @elseif($bloqueado=="0")
		<input type="hidden" id="backendlock" value="0"/>
	  @endif
	  <input type="hidden" id="upslock" value="{{Session::get('ups')}}"/>
      <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>
        <div class="login_wrapper">
          <div class="animate form login_form">
            <section class="login_content">
              <form id="form-locklogin" action="{{url('admin/unlock')}}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <h1 class="title-header">Hotel Waldorf</h1>
                <div>
                  <input type="text" name="blocklogin" id="blocklogin" class="input-style form-control" placeholder="* Login"/>
                </div>
                <label id="campologin" class="alerta-lock float-left">El campo es obligatorio</label>
                <div>
                  <input type="password" name="blockpass" id="blockpass" class="input-style form-control" placeholder="* Password"/>
                </div>
                <label id="campopassword" class="alerta-lock float-left">El campo es obligatorio</label>
                <div>
                  <button type="button" id="btnEnviar">Entrar</button>
                  <a class="reset_pass color-header" href="{{url('/')}}">Ir al Sitio</a>
                </div>
                <div class="clearfix"></div>
                <div class="separator">
                  <div class="clearfix"></div>
                  <br />
                  <div>
                    <p class="title-header width-header">&copy; 2016 Hotel Waldorf V1.6 Powered By <a href="https://www.haciendacreativa.com.ve">Hacienda Creativa C.A.</a></p>
                  </div>
                </div>
              </form>
            </section>
          </div>
        </div>
      </div>
	  <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
	  <!-- Bootstrap -->
	  <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	  @include("modales.alerta")
	  <script src="{{asset('js/lock.js')}}"></script>
    </body>
</html>
