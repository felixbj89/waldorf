@extends("layout.panel")
@section("NuestroHotel-nav")
	active
@endsection
@section("Mycss")
	{!!Html::style('css/indexWaldorf.css')!!}
	{!!Html::style('css/elhotel.css')!!}
@endsection
@section("SliderWaldorf")
	<div id="Carousel-Waldorf" class="carousel slide carousel-fade" data-ride="carousel">
		<div id="Inner-Waldorf" class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="item-topage" style=" background-image:url({{URL($bannersuperior)}})"></div>
			</div>
		</div>
	</div>
@endsection
@section("tolinks")
	{{url('Home/Contacto')}}
@endsection
@section("body-page")
	<!-- historia -->
	<div id="historia">
		<div class="row" style="margin:0px">
			<div class="col-sm-5 col-xs-12">
				<table><tr><td>	
					<img class="imghistory" src="{{url()}}/img/hotel/principal_hotel.jpg">	
				</td></tr></table>
			</div>
			<div class="col-sm-7 col-xs-12 grid-gl" style="padding-right: 50px !important;">
				<table><tr><td>
					<h2 class="title">
						Nuestra Historia
					</h2>
					<p>
						En los años 40 y en atención a la dinámica socio económica del momento, producto de boom petrolero, la hotelería en Venezuela comienza a ergirse bajo los esquemas de la época que pevalecian para dicho sector, no es por casualidad el uso de nombres con una connotación comercial para estos establecimientos. Nace en 1944el Hotel Waldorf, edificación especialmente diseñada como hotel tal y como se aprecia al analizar su diseño. 
					</p>
					<p>
						Nuestro hotel recibe el nombre de Waldorf en honor al afamado Waldorf of Astodia ubicado en la ciudad de Nueva York, rascacielos que esta actualmente en funcionamiento con 47 pisos y destacado por su exelencia en el servico.
					</p>
					<p>
						Emulando estemodelo, Waldorf Caracas se caracterizo por observar gran distinción en sus instalaciones, se destacó por la adquisición de grandes obras de maestros de la alta pintura europea del siglo XIXy comienzos del XX las cuales exponian el el área de su sobrio restaurant, éstemismo, gozo de amplia fama, no solo de su exelente comida y noches bailables al compás de las mejores orquestas del momento sino tambien por el profecionalismo y esmero en la atención del personal que alli se desempeñaba.
					</p>
				</td></tr></table>
			</div>
		</div>
	</div>
	<!-- Galeria -->
	<div id="Galeria" style="margin-bottom:40px;">
		<div class="row" style="margin:0px">
			<div class="col-sm-5 col-xs-12">
				<table><tr><td>	
					<p>
						A solo 25 minutos del Aeropuerto Iinternacional de Maiquetia se encuentra el Hotel Waldorf, en Caracas, puerta de entrada de America del Sur. Tenemos accesos inmediatos a la estación de Metro Bellas Artes, lo que permite ir a los museos y a las atracciones culturales de la zonay en el corazón del centro finaciero y económico del país.
					</p>
					<a id="btn-galeria" class="titulo" data-toggle="modal" href="#modalgalhitoria" style="font-size:17px !important;">ver galeria</a>		
				</td></tr></table>
			</div>
			<div class="col-sm-7 col-xs-12 grid-gl">
				<div class="col-sm-3 col-xs-12 grid-galeria"><div class="grid-imgaleria" style="background-image:url('img/hotel/hotel1.jpg')"></div></div>
				<div class="col-sm-5 col-xs-12 grid-galeria grid-media"><div class="grid-imgaleria"  style="background-image:url('img/hotel/hotel2.jpg')"></div></div>
				<div class="col-sm-4 col-xs-12 grid-galeria"><div class="grid-imgaleria"  style="background-image:url('img/hotel/hotel3.jpg')"></div></div>
			</div>
		</div>
	</div>
	<!-- Contacto -->
	<div id="Contacto" style="background-image:url({{URL($urlcontacto)}})" alt="{{$picturecontacto}}">
		<table style="height:100%;width:100%;padding:32.5px auto;"><tr><td>
			<p class="titulo text-center" style="color:#FFF;">CONTACTO</p>
			<form id="form-contactus" class="col-xs-10 col-xs-offset-1" method="post">
				<div class="form-group col-xs-12">
					<select class="form-control inputWldorf destino" id="destino" name="destino">
						<option value="NULL" class="color">Seleccione un destino válido.</option>
						<option value="recepción" class="color">Recepción.</option>
						<option value="grupos" class="color">Grupos y Convenciones</option>
						<option value="reservaciones" class="color">Reservaciones</option>
						<option value="restaurante" class="color">Restaurante</option>
						<option value="promociones" class="color">Promociones</option>
					</select>
                    <label id="formatochecked" class="error">
                        Debe seleccionar una opción (Eventos/Restaurante/Mercadeo), intente nuevamente.
                    </label>
				</div>
				<div class="form-group col-xs-12 col-sm-6">
					<input type="text" class="inputWldorf form-control" name="nombre" id="nombre" placeholder="Nombre">
                    <label id="nombrelength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-6">
					<input type="email" class="inputWldorf form-control" name="correo" id="correo" placeholder="Correo Electr&oacute;nico">
                    <label id="formatoincorrecto" class="error">Formato de correo incorrecto, intente nuevamente.</label>
                    <label id="formatoespacio" class="error">En este campo no puede ingresar espacios, intente nuevamente.</label>
                    <label id="formatolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6">
					<input type="text" class="inputWldorf form-control" name="asunto" id="asunto" placeholder="Asunto">
                    <label id="asuntolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6">
					<input type="text" class="inputWldorf form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono">
                    <label id="telefonolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
                    <label id="telefonoshort" class="error">Debe Ingresar un teléfono válido, intente nuevamente.</label>
                    <label id="telefonoformato" class="error">Formato de teléfono incorrecto, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12">
					<textarea class="inputWldorf form-control" name="Mensaje" id="Mensaje" rows="4" placeholder="Mensaje"></textarea>
                    <label id="mensajelength" class="error">Este campo es obligatorio, intente nuevamente.</label>
                    <label id="mensajeshort" class="error">Debe Ingresar un mensaje válido, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12">
					<button type="button" id="btn-env" name="btn-env">ENVIAR</button>
				</div>
			</form>
		</td></tr></table>
		<!-- Arriba -->
		<div id="ArribaHome">
			<i class="glyphicon glyphicon-menu-up"></i>
		</div>
	</div>
@endsection
@section("modal-page")
 	@include("modales.modalgalhitoria")
@endsection
@section("Myscript")
<script>
jQuery(function ($) {
	$(document).ready(function (){
		var id="<?php echo $id;?>";
		if(id!=""){
			$('html, body').animate({scrollTop: ($("#"+id).offset().top)-150}, 1500);
		};
	});
	$('#arrowLightBoxNext,.controlLightBox-right').click(function() {
		$('#BootstrapCarouselLightBox').carousel('next');
	});
	$('#arrowLightBoxPrev,.controlLightBox-left').click(function() {
		$('#BootstrapCarouselLightBox').carousel('prev');
	});	
});
</script>
@endsection