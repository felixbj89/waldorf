<form id="form-slider" action="{{url('admin/addseculider')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" id="editando" name="editando" value="0"/>
	<input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
	<input type="hidden" id="url" value="{{url('/')}}"/>
	<div class="row form-group">
		<h3 class="fixed-position-header">Agregue el texto de su agrado</h3>
	</div>
	<div class="row form-group">
		<label class="alerta">Título</label>
		<input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título Principal" value="{{e($texto->titulo)}}"/>
	</div>
	<div class="row form-group">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<label class="alerta">Texto a ingresar</label>
			<textarea name="descr" id="descr" class="summernote">{{e($texto->texto)}}</textarea>
		</div>
	</div>
	<div class="row form-group">
		<h3 class="fixed-position-header">Agregue las imágenes en miniatura de su agrado </h3>
	</div>
	<div class="row form-group">
		<div class="col-md-3 col-sm-6 col-xs-6">
			<hr>
			<div class="">
				<input type="hidden" id="defaultimagen" value="{{url($imagenes[0]->img)}}"/>
				<div class="fileother">
					<input type="file" name="filethumb1" id="filethumb1" class="input-style form-control file fixed-file center" onchange='javascript:openThum1(event)' value="Cargar.."/>
					<img src="{{url($imagenes[0]->img)}}" id="preview1" class="fixed-picture-thumb center border-over">
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-6">
			<hr>
			<div class="">
				<input type="hidden" id="defaultimagen" value="{{url($imagenes[1]->img)}}"/>
				<div class="fileother">
					<input type="file" name="filethumb2" id="filethumb2" class="input-style form-control file fixed-file center" onchange='javascript:openThum2(event)' value="Cargar.."/>
					<img src="{{url($imagenes[1]->img)}}" id="preview2" class="fixed-picture-thumb center border-over">
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-6">
			<hr>
			<div class="">
				<input type="hidden" id="defaultimagen" value="{{url($imagenes[2]->img)}}"/>
				<div class="fileother">
					<input type="file" name="filethumb3" id="filethumb3" class="input-style form-control file fixed-file center" onchange='javascript:openThum3(event)' value="Cargar.."/>
					<img src="{{url($imagenes[2]->img)}}" id="preview3" class="fixed-picture-thumb center border-over">
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-6">
			<hr>
			<div class="">
				<input type="hidden" id="defaultimagen" value="{{url($imagenes[3]->img)}}"/>
				<div class="fileother">
					<input type="file" name="filethumb4" id="filethumb4" class="input-style form-control file fixed-file center" onchange='javascript:openThum4(event)' value="Cargar.."/>
					<img src="{{url($imagenes[3]->img)}}" id="preview4" class="fixed-picture-thumb center border-over">
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-md-3 col-sm-6 col-xs-6">
			<hr>
			<div class="">
				<input type="hidden" id="defaultimagen" value="{{url($imagenes[4]->img)}}"/>
				<div class="fileother">
					<input type="file" name="filethumb5" id="filethumb5" class="input-style form-control file fixed-file center" onchange='javascript:openThum5(event)' value="Cargar.."/>
					<img src="{{url($imagenes[4]->img)}}" id="preview5" class="fixed-picture-thumb center border-over">
				</div>
			</div>
		</div>
	</div>
	<div class="row form-group">
		<h3 class="fixed-position-header">Imágenes Asociadas / Click para subir una imagen</h3>
	</div>
	<div class="row form-group">
		<div class="col-xs-12 col-md-4 fixd-col-md-4v2">
			<div class="fileother">
				<input type="file" name="fileasociado1" id="fileasociado1" class="input-style form-control file fixed-file center" onchange='javascript:openFile(event)' value="Cargar.."/>
				<img src="{{url($imagenes[0]->slider)}}" id="asociado1" class="fixed-picture-background center border-over" data-confirmar1="0">
			</div>
		</div>
		<div class="col-xs-12 col-md-4 fixd-col-md-4v2">
			<div class="fileother">
				<input type="file" name="fileasociado2" id="fileasociado2" class="input-style form-control file fixed-file center" onchange='javascript:openFile(event)' value="Cargar.."/>
				<img src="{{url($imagenes[1]->slider)}}" id="asociado2" class="fixed-picture-background center border-over" data-confirmar2="0">
			</div>
		</div>
		<div class="col-xs-12 col-md-4 fixd-col-md-4v2">
			<div class="fileother">
				<input type="file" name="fileasociado3" id="fileasociado3" class="input-style form-control file fixed-file center" onchange='javascript:openFile(event)' value="Cargar.."/>
				<img src="{{url($imagenes[2]->slider)}}" id="asociado3" class="fixed-picture-background center border-over" data-confirmar3="0">
			</div>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-xs-12 col-md-4 fixd-col-md-4v2">
			<div class="fileother">
				<input type="file" name="fileasociado4" id="fileasociado4" class="input-style form-control file fixed-file center" onchange='javascript:openFile(event)' value="Cargar.."/>
				<img src="{{url($imagenes[3]->slider)}}" id="asociado4" class="fixed-picture-background center border-over" data-confirmar4="0">
			</div>
		</div>
		<div class="col-xs-12 col-md-4 fixd-col-md-4v2">
			<div class="fileother">
				<input type="file" name="fileasociado5" id="fileasociado5" class="input-style form-control file fixed-file center" onchange='javascript:openFile(event)' value="Cargar.."/>
				<img src="{{url($imagenes[4]->slider)}}" id="asociado5" class="fixed-picture-background center border-over" data-confirmar5="0">
			</div>
		</div>
	</div>
	<div class="row form-group" style="height:100px;">
		<div id="add_button" class="col-xs-12 col-md-6">
			<button type="button" id="btnslider" class="center">Modificar</button>
		</div>
		<div id="addcancel" class="col-xs-12 col-md-6">
			<a href="{{url('admin/crearshslider')}}" id="btnregresar" class="center cerrarregresar">Regresar</a>
		</div>
	</div>
</form>
<script src="{{asset('js/editsecuslider.js')}}"></script>