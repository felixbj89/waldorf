@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
	<link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/flatpickr-master/dist/flatpickr.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
	<link href="{{asset('css/history.css')}}" rel="stylesheet">
	<link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
	<div class="right_col" role="main">
		<input type="hidden" value="" id="picturelist"/>
		<input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
		<div class="">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="x_panel">
						<h2>Agregue una promoción</h2>
						@if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
						<h3 class="clienteerror"></h3>
					</div>
					<div class="x_panel">
						<form id="form-promociones" action="{{url('admin/addpromo')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
							<div class="row form-group">
								<h3 class="fixed-position-header pull-left"><b><i>Agregue una imagen para su promoción</i></b></h3>
								<h3 class="fixed-position-header pull-right"><b><i>Agregue la información de su agrado</i></b></h3>
								<hr>
							</div>
							<div class="row form-group">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
									<img src="{{url('img/default.jpg')}}" id="preview1" class="fixed-picture-galeria border-over">
									<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
								</div>
								<div class="col-xs-12 col-sm-8 col-md-8">
									<div class="row form-group">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<label class="alerta">Título</label>
											<input type="text" name="titulopromo" id="titulopromo" class="input-style form-control" placeholder="* Título a usar"/>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<label class="alerta">Asunto</label>
											<input type="text" name="asuntopromo" id="asuntopromo" class="input-style form-control" placeholder="* Asunto a usar"/>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-12 col-md-6">
											<label class="alerta">Estado</label>
											<select id="estado" name="estado" class="form-control">
												<option value="NULL">Seleccione el estado de su agrado</option>
												<option value="1">Activa</option>
												<option value="0">Inactiva</option>
											</select>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
											<label class="alerta">Favorito</label>
											<select id="favorito" name="favorito" class="form-control">
												<option value="NULL">Seleccione si es favorito su promoción</option>
												<option value="1">Favorito</option>
												<option value="0">No es Favorito</option>
											</select>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-12 col-md-12">
											<label class="alerta">Costo</label>
											<input type="number" name="costopromo" id="costopromo" min="1" class="input-style form-control fixed-input" placeholder="* Costo a usar"/>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-md-6">
											<label class="control-label clearfix alerta">Inicio</label>
											<input type="text" id="inicio_promociones" class="form-control input-style" name="inicio_promociones" placeholder="*INICIO DE LA PROMOCIÓN"/>
										</div>
										<div class="col-xs-12 col-md-6">
											<label class="control-label clearfix alerta">Fin</label>
											<input type="text" id="fin_promociones" class="form-control input-style" name="fin_promociones" placeholder="*FIN DE LA PROMOCIÓN"/>
										</div>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<h3 class="fixed-position-header"><b><i>Descripción de su promoción</i></b></h3>
								<hr>
							</div>
							<div class="row form-group">
								<div class="col-xs-12 col-md-12">
									<label class="control-label clearfix alerta">Descripción a Ingresar</label>
									<textarea name="descr" id="descr" cols="10" rows="5" class="input-style form-control summernote"></textarea>
								</div>
							</div>
							<div class="row form-group">
								<div id="add_button" class="col-xs-12 col-md-6">
									<button type="button" id="btnAgregarPromo">Agregar</button>
								</div>
								<div id="addcancel" class="col-xs-12 col-md-6">
									<button type="reset" id="btnclose">Cancelar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("modales")
	
@endsection
@section("mi-script")
	<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
	<script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/flatpickr.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/es.js')}}"></script>
	<script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
	<script src="{{asset('js/promociones.js')}}"></script>
@endsection