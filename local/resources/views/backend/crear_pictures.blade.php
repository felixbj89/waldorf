@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("priactive")
  active
@endsection
@section("subactive")
  active
@endsection
@section("displaypri")
display:block;
@endsection
@section("displaysub")
display:block;
@endsection
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/jquery-timepicker/jquery.timepicker.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/superior.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>{{e($texto)}}</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-md-8">
                <label>Recuerde que: </label>
                <ul>
                    <li>
                      Resoluciones permitidas
                      <ul>
                        <li>Para el Thumbnail: 295x250 px</li>
                        <li>Para la Galería: 1080x500 px</li>
                      </ul>
                    </li>
                    <li>El peso por imagen debe ser inferior a los 500K.</li>
                </ul>
            </div>
          </div>
        </div>
        <div class="x_panel">
          <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
              <input type="hidden" value="{{url('/')}}" id="url_portada"/>
			  <input type="hidden" value="{{url('img/habitaciones/habitaciones/')}}" id="url"/>
              <form id="form-rooms" action="{{url('admin/modifypictures')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                  <input type="hidden" id="galeria1" name="galeria1"/>
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                  <input type="hidden" name="tiposec" id="tiposec" value="{{$modo}}" />
                  <input type="hidden" name="propilist" id="propilist" />
                  <div class="row">
                    <h3 class="fixed-position-header">Agregar/Modificar Imágenes a la Habitación</h3>
    								<hr>
                  </div>
                  <div class="row">
                    @if(Session::has("message"))
                      <h3 class="errormensaje">{{Session::get("message")}}</h3>
                    @endif
                    <h3 class="modalmensaje"></h3>
                    <div id="" class="col-xs-12 col-md-8">
                      <label class="alerta">Habitaciones</label>
                      <select id="selectcuartospicture" name="selectcuartospicture" class="input-style form-control">

                      </select>
                      <label class="control-label alerta" id="selectvalidopic">Seleccione una habitación válida</label>
                      <label class="control-label alerta" id="disabledsearchpic">Recuerde buscar una habitación válida</label>
                    </div>
                    <div class="col-xs-12 col-md-4">
                      <button type="button" id="btnseepicturehab" class="center">Buscar</button>
                    </div>
                  </div>
                  <div class="row">
                    <div id="file_picture" class="col-xs-12 col-md-7">
                      <div class="form-group">
                        <label class="alerta">Cargar Imagen</label>
                        <input type="file" name="picture_rooms" id="picture_rooms" class="input-style form-control" onchange='javascript:openFile(event)'/>
                        <label class="alerta" id="fileobligatorio">El campo es obligatorio, intente nuevamente</label>
                        <label class="alerta" id="fileformato">Solo se permiten imágenes JPG/PNG, intente nuevamente</label>
                        <label class="alerta" id="filesize">Peso máximo por imagen 500K, intente nuevamente</label>
                        <label class="alerta" id="fileresol">No cumple con la resolución, intente nuevamente</label>
        								<label class="alerta" id="filerror">Debe cargar una habitación, intente nuevamente</label>
                        <label class="alerta" id="todosobligatorio2">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                    </div>
                    <div id="file_picture" class="col-xs-12 col-md-5">
                      <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
                        <h3 class="fixed-position-header">Thumbnails</h3>
                        <hr>
                        <div class="">
                            <img src="{{url('img/default.jpg')}}" id="preview" class="fixed-picture-config">
                            <label class="alerta" id="todosobligatorio3">Los campos son obligatorio, intente nuevamente</label>
                        </div>
                      </div>
                    </div>
        		      </div>
                  <div class="row">
                      <h3 class="fixed-position-header">Galería: Debe ingresar 6 imágenes para su galería</h3>
                      <hr>
                      <div id="file_picture" class="col-xs-12 col-md-12">
                        <div class="form-group">
                          <label class="alerta">Cargar Imagen</label>
                          <input type="file" name="picture_galeria[]" id="picture_galeria" class="input-style form-control" onchange='javascript:openGaleria(event)' multiple="true"/>
                          <label class="alerta" id="fileobligatorio2">El campo es obligatorio, intente nuevamente</label>
                          <label class="alerta" id="fileformato2">Solo se permiten imágenes JPG/PNG, intente nuevamente</label>
                          <label class="alerta" id="filesize2">Peso máximo por imagen 500K, intente nuevamente</label>
                          <label class="alerta" id="fileresol2">No cumple con la resolución, intente nuevamente</label>
                          <label class="alerta" id="todosobligatorio4">Los campos son obligatorio, intente nuevamente</label>
                        </div>
                      </div>
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview1" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio5">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview2" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio6">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview3" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio7">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview4" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio8">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview5" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio9">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                      <div id="file_picture" class="col-xs-12 col-md-4">
                        <img src="{{url('img/default.jpg')}}" id="preview6" class="fixed-picture-config">
                        <label class="alerta" id="todosobligatorio10">Los campos son obligatorio, intente nuevamente</label>
                      </div>
                    </div>
                    <hr>
                    <div class="row fixed-row">
                      <hr>
                      <div id="add_button" class="col-xs-12 col-md-6">
                          <button type="reset" id="btnclose">Cancelar</button>
                      </div>
                      <div id="add_button" class="col-xs-12 col-md-6">
                          <button type="button" id="btnadd">Agregar</button>
                      </div>
                  </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.tablalength")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('vendors/jquery-timepicker/jquery.timepicker.min.js')}}"></script>
  <script src="{{asset('vendors/Datepair.js/dist/jquery.datepair.min.js')}}"></script>
  <script src="{{asset('js/crearpics.js')}}"></script>
@endsection
