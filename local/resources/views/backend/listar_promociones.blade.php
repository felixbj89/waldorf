@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
	<link href="{{asset('vendors/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/flatpickr-master/dist/flatpickr.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
	<link href="{{asset('css/superior.css')}}" rel="stylesheet">
	<link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
	<div class="right_col" role="main">
		<input type="hidden" value="{{url('/')}}" id="url" name="url"/>
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="x_panel">
					<h2>Listar: Promociones Registradas</h2>
					<h3 class="mensajeserver"></h3>
					@if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
					<h3 class="clienteerror"></h3>
				</div>
				<div id="panel" class="x_panel">
					<div class="col-md-12 col-sm-12 col-xs-12">
						@if(count($size) == 0)
						  <h3 class="errormensaje">{{Lang::get('message.notsuperior')}}</h3>
						@else
							<table id="promociones_list" class="table table-striped table-bordered">
								<thead>
								  <tr>
									<th>Promoción</th>
									<th>Opciones</th>
								  </tr>
								</thead>
								<tbody>
									@foreach($imagenes as $img)
										<tr data-promociones="{{e(base64_encode($img->id))}}">
											<td><img id="loading_superior" src="{{url(e($img->promociones_path))}}" class="img-responsive center" width="600" height="500"></td>
											<td>
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-12">
														@if($img->promociones_status==1)
															<button id="btnEPromocion" class="btn center btn-table-wal">
																<i class="fa fa-power-off cenico"></i> Activa
															</button>
														@else
															<button id="btnEPromocion" class="btn center btn-table-wal">
																<i class="fa fa-power-off cenico"></i> Inactiva
															</button>
														@endif
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12">
														<button id="btnModificarPromo" class="btn center btn-table-wal"><i class="fa fa-pencil cenico"></i> Modificar</button>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-12">
														<button id="btnRemover" class="btn center btn-table-wal"><i class="fa fa-times cenico"></i> Remover</button>
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
				</div>
			</div>
			<h3 class="mensajeserver"></h3>
		</div>
	</div>
@endsection
@section("modales")
	@include("modales.wait")
@endsection
@section("mi-script")
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/flatpickr.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/es.js')}}"></script>
	<script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
	<script src="{{asset('js/listar_promociones.js')}}"></script>
@endsection