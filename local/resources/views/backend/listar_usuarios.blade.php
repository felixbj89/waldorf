@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/datatables.net/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="{{route('admin/list')}}" id="rutalist"/>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>Listar Usuarios</h2>
          <div class="x_panel">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h2>Seleccione la opción de su agrado</h2>
  						<div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
  							<div class="tile-stats">
  								<div class="icon"><i class="fa fa-save"></i></div>
  								<div class="count corregir-font">Activar Opciones</div>
  								<button type="button" id="btnRespaldarUsuario" class="btnRespaldarUsuario">Desactivar</button>
  							</div>
  						</div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
  							<div class="tile-stats">
  								<div class="icon"><i class="fa fa-edit"></i></div>
  								<div class="count corregir-font">Activar Respaldo</div>
  								<button type="button" id="btnEditarUsuariotool" class="btnEditarUsuariotool" disabled="true">Activar</button>
  							</div>
  						</div>
            </div>
          </div>
          <div class="x_panel">
            <div class="col-md-12 col-sm-12 col-xs-12">
                @if(count($usuarios) == 0)
                  <h3 class="errormensaje">{{Lang::get('message.noexiste')}}</h3>
                @else
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                     Click si desea respaldar
                   </p>
                   <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>E-mail</th>
                            <th>Login/Username</th>
                            <th>Estado</th>
                            <th>Perfil</th>
                            <th id="editarth">Editar usuario</th>
                            <th id="eliminarth">Eliminar usuario</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usu)
                              <tr data-email="{{e($usu->user_email)}}">
                                  <td>{{e($usu->user_name)}}</td>
                                  <td>{{e($usu->user_email)}}</td>
                                  <td>{{e($usu->user_login)}}</td>
                                  <td>
                                    @if($usu->user_status==1)
                                      	<i class='fa fa-power-off on'></i> Activo
                                    @else
                                      	<i class='fa fa-power-off off'></i> Inactivo
                                    @endif
                                  </td>
                                  <td>
                                    @if($usu->user_role==1)
                                      Administrador
                                    @elseif($usu->user_role==2)
                                      Roperador
                                    @elseif($usu->user_role==3)
                                        Operador
                                    @endif
                                  </td>
                                  <td><button type="button" id="btnEditarUsuario" class="btnEditarUsuario">Modificar</button></td>
                                  <td><button type="button" id="btnEliminarUsuario" class="btnEliminarUsuario">Eliminar</button></td>
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net/Responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
  <script src="{{asset('vendors/switchery/dist/switchery.min.js')}}"></script>
  <script src="{{asset('js/listar.js')}}"></script>
@endsection
@section("modales")
  @include("modales.edit")
  @include("modales.confirm")
@endsection
