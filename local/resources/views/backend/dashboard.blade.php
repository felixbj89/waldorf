@extends("layout.master")
@section("mi-css")
	<link href="{{asset('vendors/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/flatpickr-master/dist/flatpickr.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
	<link href="{{asset('css/superior.css')}}" rel="stylesheet">
	<link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("mi-script")
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/flatpickr.js')}}"></script>
	<script src="{{asset('vendors/flatpickr-master/dist/es.js')}}"></script>
	<script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
	<script src="{{asset('js/tablehistorial.js')}}"></script>
	<script src="{{asset('js/promociones.js')}}"></script>
@endsection
@section("notificaciones-rangos")
	<li role="presentation" class="dropdown">							
		<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
			<i class="fa fa-tags"></i>
			<span class="badge bg-red">{{count($rangos)}}</span>
		</a>
		<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
			<?php
				$size = count($rangos);
				if($size==0){
			?>
					<li>
						<a>
							<span class="center texto-center">
								<span>No hay promociones disponibles</span>
							</span>
						</a>
					</li>
			<?php
				}else{
			?>
					<li>
						<a>
							<span class="center texto-center">
								<span class="">Próximas activas</span>
							</span>
						</a>
					</li>
			<?php
					for($i = 0; $i < $size; $i++){
			?>
						<li>
							<a>
								<span class="image"><i class="fa fa-tags"></i></span>
								<span>
									<span>{{e($rangos[$i]["title"])}}</span>
									<span id="verpromo" class="time" data-promo="{{e($rangos[$i]['id'])}}">Ver <i class="fa fa-angle-right"></i></span>
								</span>
							</a>
						</li>
			<?php 
					}
					
			?>
					<li>
						<div class="text-center">
							<a href="{{url('admin/verpromos')}}">
								<strong>Ver Promociones</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</li>
			<?php
				}
			?>
		</ul>
	</li>
@endsection
@section("notificaciones-proximas")
	<li role="presentation" class="dropdown">							
		<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
			<i class="fa fa-clock-o"></i>
			<span class="badge bg-red">{{count($proximas)}}</span>
		</a>
		<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
			<?php
				$size = count($proximas);
				if($size==0){
			?>
					<li>
						<a>
							<span class="center texto-center">
								<span>No hay promociones disponibles</span>
							</span>
						</a>
					</li>
			<?php
				}else{
			?>
					<li>
						<a>
							<span class="center texto-center">
								<span class="">Próximas promociones</span>
							</span>
						</a>
					</li>
			<?php
					for($i = 0; $i < $size; $i++){
			?>
						<li>
							<a>
								<span class="image"><i class="fa fa-tags"></i></span>
								<span>
									<span>{{e($proximas[$i]["title"])}}</span>
									<span id="verpromo" class="time" data-promo="{{e($proximas[$i]['id'])}}">Ver <i class="fa fa-angle-right"></i></span>
								</span>
							</a>
						</li>
			<?php 
					}
					
			?>
					<li>
						<div class="text-center">
							<a href="{{url('admin/verpromos')}}">
								<strong>Ver Promociones</strong>
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</li>
			<?php
				}
			?>
		</ul>
	</li>
@endsection
@section("dashboard-fotosprincipales")
	<div class="tile-stats">
		<div class="icon"><i class="fa fa-picture-o"></i></div>
		<div class="count">{{e($npicture)}}</div>
		<h3>Fotos Principales</h3>
		<a id="picturesuperiores" class="fixed-link-p">Crear</a>
	</div>
@endsection
@section("go-rooms")
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats card-dashboard">
			<div class="icon"><i class="fa fa-hotel"></i></div>
			<div class="count">{{e($size_rooms)}}</div>
			<h5>Habitaciones</h5>
			<a href="{{url('admin/crearhab')}}" class="pull-right">Registrar.</a>
		</div>
	</div>
@endsection
@section("go-principal")
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats card-dashboard">
			<div class="icon"><i class="fa fa-picture-o"></i></div>
			<div class="count">{{e($npicture)}}</div>
			<h5>Foto Principal: Habitaciones</h5>
			<a href="{{url('admin/hrincipal')}}" class="pull-right">Registrar.</a>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats card-dashboard">
			<div class="icon"><i class="fa fa-picture-o"></i></div>
			<div class="count">{{e($nrestaurante)}}</div>
			<h5>Foto Principal: Restaurante</h5>
			<a href="{{url('admin/crearsrprincipal')}}" class="pull-right">Registrar.</a>
		</div>
	</div>
@endsection
@section("go-promociones")
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats card-dashboard">
			<div class="icon"><i class="fa fa-tags"></i></div>
			<div class="count">{{e($npicture)}}</div>
			<h5>Promociones</h5>
			<a href="{{url('admin/crearpromociones')}}" class="pull-right">Registrar.</a>
		</div>
	</div>
@endsection
@section("go-visitas")
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
			<input type="hidden" id="browser" value="{{$browser}}"/>
			<canvas id="lineChart" style="width: 722px; height: 361px;" width="722" height="241"></canvas>
		</div>
	</div>
</div>
@endsection
@section("go-navegadores")
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Navegadores</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<input type="hidden" id="contador" value="{{$contador}}"/>
			<canvas id="mybarChart"></canvas>
		</div>
	</div>
</div>
@endsection
@section("go-dispositivos")
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Dispositivos</h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<input type="hidden" id="devices" value="{{$devices}}"/>
			<canvas id="chartdispositivos"></canvas>
		</div>
	</div>
</div>
@endsection
@section("go-registros")
	<div class="row">
		<div class="col-xs-12 col-md-7">
			<div class="x_panel">
				<div class="x_title">
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="col-xs-9">
						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane active" id="usuariosregistrados">
								<p class="lead pull-left">Ultimos Usuarios</p>
								<table id="usuarios_list" class="table table-striped table-bordered">
									<thead>
									  <tr>
										<th>Nombre</th>
										<th>E-mail</th>
										<th>Login/Username</th>
										<th>Estado</th>
										<th>Perfil</th>
									  </tr>
									</thead>
									<tbody>
										@foreach($usuarios as $usu)
										  <tr data-email="{{e($usu->user_email)}}">
											  <td>{{e($usu->user_name)}}</td>
											  <td>{{e($usu->user_email)}}</td>
											  <td>{{e($usu->user_login)}}</td>
											  <td>
												@if($usu->user_status==1)
													<i class='fa fa-power-off on'></i> Activo
												@else
													<i class='fa fa-power-off off'></i> Inactivo
												@endif
											  </td>
											  <td>
												@if($usu->user_role==1)
												  Administrador
												@elseif($usu->user_role==2)
												  Roperador
												@elseif($usu->user_role==3)
													Operador
												@endif
											  </td>
										  </tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="tab-pane" id="habitaciones">
								<p class="lead pull-left">Habitaciones Registradas</p>
								<table id="rooms_list" class="table table-striped table-bordered">
									<thead>
									  <tr>
										<th>Portada</th>
										<th>Título</th>
										<th>Posición</th>
									  </tr>
									</thead>
									<tbody>
										@foreach($rooms as $r)
											<?php $json = json_decode($r->rooms_especificaciones) ?>
											<tr>
											  <td><img src="{{url($r->rooms_thumbnail)}}" class="img-responsive img-thumbnail box-dashboard"/></td>
											  <td>{{$json[1]->titulo}}</td>
											  <td>#{{$r->rooms_positions}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-3">
						<!-- required for floating -->
						<!-- Nav tabs -->
						<ul class="nav nav-tabs tabs-right">
							<li class="active"><a href="#usuariosregistrados" data-toggle="tab">Usuarios Registrados</a></li>
							<li><a href="#habitaciones" data-toggle="tab">Habitaciones</a></li>
						</ul>
					</div>	
				</div>
			</div>
		</div>
		<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Secciones</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<input type="hidden" id="secciones" value="{{$secciones}}"/>
					<canvas id="chartsecciones"></canvas>
				</div>
			</div>
		</div>
	</div>
@endsection