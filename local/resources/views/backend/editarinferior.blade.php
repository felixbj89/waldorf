<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
  <form id="form-picture" action="{{url('admin/editinferior')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	  <input type="hidden" name="tiposec" id="tiposec" value="{{$estado}}" />
	  <input type="hidden" name="idpicture" id="idpicture" value="{{$imagenes['id']}}" />
	  <div class="row">
		  <h3 class="modalmensaje"></h3>
		  <div id="file_picture" class="col-xs-12 col-md-12">					  
			<div class="form-group fileother">
				<input name="banner_inferior" id="banner_inferior" class="input-style form-control file fixed-file center" onchange="javascript:openFile(event)" value="Cargar.." type="file">
				<img src="{{url($imagenes['picture_path'])}}" id="loading_inferior" class="fixed-picture-config zoom">
				<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
			</div>
		  </div>
	  </div>
	  <div class="row fixed-row">
		  <hr>
		<div id="add_button" class="col-xs-6 col-md-6">
			<a href="{{url($ruta)}}" type="button" id="btnDashboard" class="btn">Regresar</a>
		</div>
		<div id="add_button" class="col-xs-12 col-md-6">
			  <button type="button" id="btnadd" class="btn">Agregar</button>
		  </div>
	  </div>
  </form>
</div>
<script src="{{asset('js/editar_inferior.js')}}"></script>