@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
	<div class="right_col" role="main">
		<div class="">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="x_panel">
						<h2>Crear Usuarios</h2>
						<div class="animated flipInY col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-users"></i></div>
								<div class="count">{{e($size)}}</div>
								<h3>Usuarios</h3>
								<a id="listarUsuarios" class="fixed-link-p">Listar Usuario Registrados.</a>
							</div>
						</div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <label>Recuerde que: </label>
              <ul>
                <li>El Login/Username no se admiten números</li>
              </ul>
            </div>
					</div>
					<div class="x_panel">
						<h2>Una vez ingresada la información deberá pulsar el botón crear</h2>
						<h3 class="errormensaje">{{Lang::get('message.errorcreando')}}</h3>
						<form id="form-create" method="post">
							<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
							<input type="hidden" value="{{route('admin/create')}}" id="ruta"/>
							<div class="row form-group">
								<h3>Datos Personales</h3>
								<hr>
							</div>
							<div class="row form-group">
								<div id="nombre_user" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">Nombre</label>
										<input type="text" name="nombre" id="nombre" class="input-style form-control" placeholder="* Nombre de usuario"/>
										<label class="alerta" id="nombreobligatorio">El campo es obligatorio, intente nuevamente</label>
										<label class="alerta" id="todosobligatorio1">Los campos son obligatorio, intente nuevamente</label>
									</div>
								</div>
								<div id="correo_user" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">E-mail</label>
										<input type="email" name="correo" id="correo" class="input-style form-control" placeholder="* E-mail"/>
										<label class="alerta" id="correoobligatorio">El campo es obligatorio, intente nuevamente</label>
										<label class="alerta" id="correoformato">Formato de correo incorrecto, intente nuevamente</label>
										<label class="alerta" id="todosobligatorio2">Los campos son obligatorio, intente nuevamente</label>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<h3>Estado en el sistema</h3>
								<hr>
							</div>
							<div class="row form-group">
									<div id="perfil_user" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">Perfil</label>
										<select id="perfil" name="perfil" class="input-style form-control">
											<option value="-1" class="fuente">Seleccione el perfil deseado</option>
											<option value="1" class="fuente">Administrador</option>
											<option value="2" class="fuente">Roperador</option>
											<option value="3" class="fuente">Operador</option>
										</select>
										<label class="alerta" id="todosobligatorio3">Los campos son obligatorio, intente nuevamente</label>
										<label class="alerta" id="perfiltipo">Seleccione un perfil correcto, intente nuevamente</label>
									</div>
								</div>
								<div id="correo_user" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">Estado</label>
										<select id="estado" name="estado" class="input-style form-control">
											<option value="-1" class="fuente">Seleccione el estado deseado</option>
											<option value="1" class="fuente">Activo</option>
											<option value="2" class="fuente">Inactivo</option>
										</select>
										<label class="alerta" id="todosobligatorio4">Los campos son obligatorio, intente nuevamente</label>
										<label class="alerta" id="estadotipo">Seleccione un estado correcto, intente nuevamente</label>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<h3>Credenciales en el sistema</h3>
								<hr>
							</div>
							<div class="row form-group">
								<div id="login_cliente" class="col-xs-12 col-md-12">
									<div class="form-group">
										<label class="alerta">Login/Username</label>
										<input type="text" name="userlogin" id="userlogin" class="input-style form-control" placeholder="* Login/Username"/>
										<label class="alerta" id="loginobligatorio">El campo es obligatorio, intente nuevamente</label>
										<label class="alerta" id="loginformato">Introduzca un login válido, intente nuevamente</label>
										<label class="alerta" id="todosobligatorio5">Los campos son obligatorio, intente nuevamente</label>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div id="password_cliente" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">Password</label>
										<input type="password" name="userpassword" id="userpassword" class="input-style form-control" placeholder="* Password"/>
										<label class="alerta" id="todosobligatorio6">Los campos son obligatorio, intente nuevamente</label>
										<label class="alerta" id="passwordobligatorio">El campo es obligatorio, intente nuevamente</label>
									</div>
								</div>
								<div id="password_cliente" class="col-xs-12 col-md-6">
									<div class="form-group">
										<label class="alerta">Confirmar</label>
										<input type="password" name="userconfirmar" id="userconfirmar" class="input-style form-control" placeholder="* Confirme su Password"/>
										<label class="alerta" id="todosobligatorio7">Los campos son obligatorio, intente nuevamente</label>
										<label class="alerta" id="confirmarpassword">No coinciden las password ingresada, intente nuevamente</label>
									</div>
								</div>
							</div>
							<div class="row">
								<button type="button" id="btnCrear" class="btnCrear">Crear</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("modales")
	@include("modales.beforecrear")
	@include("modales.aftercrear")
@endsection
@section("mi-script")
<script src="{{asset('js/user.js')}}"></script>
@endsection
