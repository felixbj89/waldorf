@extends("layout.master")
@section("page-body")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/history.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("mi-dashboard")
@stop
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <input type="hidden" name="tiposec" id="tiposec" value="4" />
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>Historial Nuestra Historia</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-sm-12 col-md-4">
                <label>Recuerde que: </label>
                <ul>
                    <li>Podrá remover imágenes anteriormente usadas.</li>
                    <li>Podrá usar imágenes anteriormente usadas.</li>
                    <li>El máximo total es de 6 imágenes.</li>
                </ul>
            </div>
          </div>
          <div class="x_panel" id="bloque_principal">
            <div class="row">
              <h2>Use una imagen del historial</h2>
              <hr>
              <div class="col-xs-12 col-md-12 fixed-border">
				<div id="picturering" class="ocultar">
					<label class="center alerta fixed-wait fixed-wait-aux">Espere un momento</label>
					<img src="{{url('img/ring.svg')}}" class="img-responsive center"/>
				</div>
				<div id="resultadoprofile">
					<label id="successchange" class="center alert-success fixed-wait fixed-po">Modificación Sastifactoria</label>
				</div>
				<div id="removesastifactorio" class="">
					<label id="removerchange" class="center alert-success fixed-wait fixed-po">Imagen removida Sastifactoriamente</label>
				</div>
				<table id="datatable-history" class="table table-striped table-bordered">
					 <thead>
						 <tr>
							 @if(count($pictures)==0)
							 <th>Imagen</th>
							 @else
								<th>Imagen</th>
								<th>Opción/Cambiar</th>
								<th>Opción/Remover</th>
							 @endif
						 </tr>
					 </thead>
					 <tbody>
						 @if(count($pictures)==0)
							<tr>
								<td>
									<label class="alerta">No posee imágenes en el historial</label>
									<img src="{{url('img/default.jpg')}}" class="img-responsive">
								</td>
							</tr>
						 @else
								@foreach($pictures as $pic)
									<tr data-date="{{$pic->picture_date}}" data-status="{{$pic->id}}">
										<td><img src="{{url($pic->picture_path)}}" id="newimg_{{$pic->id}}" class="img-responsive"></td>
										<td><button type="button" id="historialbtn" class="fixed-button-historial">Cambiar</button></td>
										<td><button type="button" id="removerhistorial" class="fixed-button-historial">Remover</button></td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</thead>
				</table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.afterhistorialsuperior")
  @include("modales.notmatch")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('js/history.js')}}"></script>
@endsection
