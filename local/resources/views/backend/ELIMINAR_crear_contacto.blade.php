@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/superior.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>Agregar/Cambiar la Imagen de Contacto</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-sm-12 col-md-8">
                <label>Recuerde que: </label>
                <ul>
                    <li>Resolución permitida. 1080x400 px.</li>
                    <li>El campo Nombre no exceder los 20 carácteres</li>
                    <li>El peso por imagen debe ser inferior a los 500K</li>
                    <li>Solo pueden existir un máximo de 6 imágenes en el historial</li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <!--<button id="btneditar" type="button" class="fixed-button center" data-toggle="modal" data-target="#historialcontactus">
                    Historial
                    <i class="fa fa-book"></i>
                </button>-->
            </div>
          </div>
        </div>
        <div class="x_panel">
          <div class="col-md-6 col-sm-12 col-xs-12 fixed-colmd4v4">
              <input type="hidden" id="historial" value="{{$pictures}}">
              <input type="hidden" id="size" value="{{count($pictures)}}">
              <form id="form-contactus" action="{{url('admin/addcontactus')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                  <input type="hidden" name="tiposec" id="tiposec" value="5" />
                  <div class="row">
                    <h3 class="fixed-position-header">Agregue la imagen de su agrado</h3>
    								<hr>
                  </div>
                  <div class="row">
                      @if(Session::has("message"))
                        <h3 class="errormensaje">{{Session::get("message")}}</h3>
                      @endif
                      <div id="file_picture" class="col-xs-12 col-md-12">
                        <div class="form-group">
                          <label class="alerta">Cargar Imagen</label>
                          <input type="file" name="file_contactus" id="file_contactus" class="input-style form-control" onchange='javascript:openFile(event)'/>
                          <label class="alerta" id="fileobligatorio">El campo es obligatorio, intente nuevamente</label>
                          <label class="alerta" id="fileformato">Solo se permiten imágenes JPG/PNG, intente nuevamente</label>
                          <label class="alerta" id="filesize">Peso máximo por imagen 500K, intente nuevamente</label>
                          <label class="alerta" id="fileresol">No cumple con la resolución, intente nuevamente</label>
                          <label class="alerta" id="todosobligatorio2">Los campos son obligatorio, intente nuevamente</label>
                        </div>
                      </div>
                      <div id="nombre_picture" class="col-xs-12 col-md-12">
                        <div class="form-group">
                          <label class="alerta">Nombre</label>
                          <input type="text" name="pictureName" id="pictureName" class="input-style form-control" placeholder="* Nombre de la Imagen"/>
                          <label class="alerta" id="pictureobligatorio">El campo es obligatorio, intente nuevamente</label>
                          <label class="alerta" id="todosobligatorio1">Los campos son obligatorio, intente nuevamente</label>
                        </div>
                      </div>
                  </div>
                  <div class="row fixed-row">
                      <hr>
                      <div id="add_button" class="col-xs-12 col-md-6">
                          <button type="reset" id="btnclose">Cancelar</button>
                      </div>
                      <div id="add_button" class="col-xs-12 col-md-6">
                          <button type="button" id="btnadd">Agregar</button>
                      </div>
                  </div>
              </form>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12 fixed-colmd4v4">
            <h3 class="fixed-position-header">Imagen a subir</h3>
            <hr>
            <div class="fixed-background-preview">
                <img src="{{url('img/default.jpg')}}" id="preview" class="fixed-picture-config">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.afterhistorialsuperior")
	@include("modales.historialcontactus")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('js/contacto.js')}}"></script>
@endsection
