<div class="x_panel">
	<form id="form-promociones" action="{{url('admin/editarpromo')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
		<input type="hidden" name="promo_id" id="promo_id" value="{{ base64_encode($promocion->id) }}" />
		<div class="row form-group">
			<h3 class="fixed-position-header pull-left"><b><i>Agregue una imagen para su promoción</i></b></h3>
			<h3 class="fixed-position-header pull-right"><b><i>Agregue la información de su agrado</i></b></h3>
			<hr>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($promocion->promociones_path)}}"/>
				<img src="{{url($promocion->promociones_path)}}" id="preview1" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8">
				<div class="row form-group">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<label class="alerta">Título</label>
						<input type="text" name="titulopromo" id="titulopromo" class="input-style form-control" placeholder="* Título a usar" value="{{e($promocion->promociones_title)}}"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<label class="alerta">Asunto</label>
						<input type="text" name="asuntopromo" id="asuntopromo" class="input-style form-control" placeholder="* Asunto a usar" value="{{e($promocion->promociones_asunto)}}"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-sm-12 col-md-6">
						<label class="alerta">Estado</label>
						<select id="estado" name="estado" class="form-control">
							<option value="NULL">Seleccione el estado de su agrado</option>
							@if($promocion->promociones_status==1)
								<option value="1" selected>Activa</option>
								<option value="0">Inactiva</option>
							@else
								<option value="1">Activa</option>
								<option value="0" selected>Inactiva</option>
							@endif
						</select>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6">
						<label class="alerta">Favorito</label>
						<select id="favorito" name="favorito" class="form-control">
							<option value="NULL">Seleccione si es favorito su promoción</option>
							@if($promocion->promociones_favorito==1)
								<option value="1" selected>Favorito</option>
								<option value="0">No es Favorito</option>
							@else
								<option value="1">Favorito</option>
								<option value="0" selected>No es Favorito</option>
							@endif
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<label class="alerta">Costo</label>
						<input type="number" name="costopromo" id="costopromo" min="1" class="input-style form-control fixed-input" placeholder="* Costo a usar" value="{{e($promocion->promociones_costo)}}"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-6">
						<label class="control-label clearfix alerta">Inicio</label>
						<input type="text" id="inicio_promociones" class="form-control input-style" name="inicio_promociones" placeholder="*INICIO DE LA PROMOCIÓN" value="{{e($promocion->promociones_dateini)}}"/>
					</div>
					<div class="col-xs-12 col-md-6">
						<label class="control-label clearfix alerta">Fin</label>
						<input type="text" id="fin_promociones" class="form-control input-style" name="fin_promociones" placeholder="*FIN DE LA PROMOCIÓN" value="{{e($promocion->promociones_datefin)}}"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row form-group">
			<h3 class="fixed-position-header"><b><i>Descripción de su promoción</i></b></h3>
			<hr>
		</div>
		<div class="row form-group">
			<div class="col-xs-12 col-md-12">
				<label class="control-label clearfix alerta">Descripción a Ingresar</label>
				<textarea name="descr" id="descr" cols="10" rows="5" class="input-style form-control summernote">
					{{e($promocion->promociones_descripcion)}}
				</textarea>
			</div>
		</div>
		<div class="row form-group">
			<div id="add_button" class="col-xs-12 col-md-6">
				<button type="button" id="btnModificarPromocion">Aceptar</button>
			</div>
			<div id="addcancel" class="col-xs-12 col-md-6">
				<button type="reset" id="btnclose">Cancelar</button>
			</div>
		</div>
	</form>
</div>
<script src="{{asset('js/promociones.js')}}"></script>