@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/jquery-timepicker/jquery.timepicker.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
  <link href="{{asset('css/habibackend.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('/')}}" id="url"/>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>{{e($texto)}}</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-sm-12 col-md-8">
                <label>Recuerde que: </label>
                <ul>
                  <li>Podrá
                      <ul>
                        <li>Crear una Habitación</li>
                        <li>Agregar/Modificar Especificación de una habitación</li>
                        <li>Agregar/Modificar El Texto Principal de la sección</li>
                        <li>Agregar/Modificar Los Thumbnails de las habitaciones.</li>
                      </ul>
                  </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <i class="fa fa-cog fixed-cog"></i>
            </div>
          </div>
        </div>
        <div class="x_panel">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="fixed-habground">
                        <div id="botones" class="row">
                            <div class="col-xs-12 col-md-12">
                              <button id="btnHabCrear" type="button" class="fixed-button center no-float not-width">
                                  Crear
                                  <i class="fa fa-pencil"></i>
                              </button>
                            </div>
                            <div class="col-xs-12 col-md-12">
                              <button id="" type="button" class="fixed-button center no-float not-width" data-toggle="modal" data-target="#textoprincipal">
                                  Texto
                                  <i class="fa fa-file-text"></i>
                              </button>
                            </div>
                            <div class="col-xs-12 col-md-12">
                              <button id="" type="button" class="fixed-button center no-float not-width" onclick="javascript:verrooms()">
                                  Propiedades
                                  <i class="fa fa-cog"></i>
                              </button>
                            </div>
                            <div class="col-xs-12 col-md-12">
                              <button id="btnmedia" type="button" class="fixed-button center no-float not-width">
                                  Media
                                  <i class="fa fa-file-image-o"></i>
                              </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
	@include("modales.editarrow")
	@include("modales.textoprincipal")
	@include("modales.beforecrear")
	@include("modales.notmatch")
	@include("modales.tablalength")
	@include("modales.propiedadesrooms")
	@include("modales.picturerooms")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('vendors/jquery-timepicker/jquery.timepicker.min.js')}}"></script>
  <script src="{{asset('vendors/Datepair.js/dist/jquery.datepair.min.js')}}"></script>
  <script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
  <script src="{{asset('js/summernote-es-ES.js')}}"></script>
  <script src="{{asset('js/administrar.js')}}"></script>
@endsection
