@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/w3/w3.css')}}" rel="stylesheet">
  <link href="{{asset('css/superior.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>{{e($texto)}}</h2>
		  @if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
		  <h3 class="clienteerror"></h3>
		  <span>Recuerde que:</span>
		  <ul class="lista_ul">
			<li><a class="config-icons" data-toggle="modal" data-target="#documentacion"><i class="fa fa-book" title="Instrucciones"></i> Click para ver las instrucciones</a></li>
		  </ul>
        </div>
        <div class="x_panel">
          <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
              <input type="hidden" id="historial" value="{{$pictures}}">
              <input type="hidden" id="size" value="{{count($pictures)}}">
              <form id="form-picture" action="{{url('admin/addsuperior')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                  <input type="hidden" name="tiposec" id="tiposec" value="{{$modo}}" />
                  <div class="row">
                      <h3 class="modalmensaje"></h3>
					  <div id="file_picture" class="col-xs-12 col-md-12">					  
						<div class="form-group fileother">
							<input name="banner_superior" id="banner_superior" class="input-style form-control file fixed-file center" onchange="javascript:openFile(event)" value="Cargar.." type="file">
							<img src="{{url('img/default.jpg')}}" id="loading_superior" class="fixed-picture-config zoom">
							<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
						</div>
					  </div>
                  </div>
                  <div class="row fixed-row">
                      <hr>
                      <div id="add_button" class="col-xs-6 col-md-4">
						<a href="{{url('admin/verhfotos')}}" type="button" id="btnclose" class="btn">Listar</a>
					</div>
					<div id="add_button" class="col-xs-6 col-md-4">
						<a href="{{url('admin/dashboard')}}" type="button" id="btnDashboard" class="btn">Dashboard</a>
					</div>
					<div id="add_button" class="col-xs-12 col-md-4">
                          <button type="button" id="btnadd" class="btn">Agregar</button>
                      </div>
                  </div>
              </form>
          </div>
		  <h3 class="clienteerror"></h3>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.afterhistorialsuperior")
  @include("modales.historialsuperior")
  @include("modales.documentacion")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('js/superior.js')}}"></script>
@endsection
