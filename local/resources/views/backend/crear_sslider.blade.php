@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
  <link href="{{asset('css/spliderppal.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
	<input type="hidden" value="" id="picturelist"/>
    <div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="x_panel">
				<h2>{{e($texto)}}</h2>
				<div class="x_panel">
					<div id="warning_picture" class="col-xs-12 col-sm-12 col-md-6">
						<label>Recuerde que: </label>
						<ul>
							<li>Resolución permitida. {{e($medidas)}}</li>
							@if($modo==1)
								<li>Los campos de texto no deben exceder los 40 carácteres.</li>
							@endif
							<li>El peso por imagen debe ser inferior a los 500K.</li>
							<li>Pasos
								<ul>
									<li>Ingrese el nombre de la imagen a subir</li>
									<li>Cargue en el sistema la imagen</li>
								</ul>
							</li>
							<li>Pulse Agregar, Para finalizar.</li>
						</ul>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						@if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
						<h3 class="clienteerror"></h3>
					</div>
				</div>
			</div>
			<div id="panel" class="x_panel">
				<form id="form-slider" action="{{url('admin/addslider')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
					<input type="hidden" id="editando" name="editando" value="0"/>
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
					<input type="hidden" name="origen" id="origen" value="0"/>
					<input type="hidden" name="modo" id="modo" value="{{e($modo)}}"/>
					<input type="hidden" id="url" value="{{url('/')}}"/>
					<input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
					@if($modo==1)
						<div class="row form-group">
							<div id="" class="col-xs-12 col-md-12">
								<h3 class="fixed-position-header"><b><i>Agregue la información de su agrado</b></i></h3>
							</div>
						</div>
					@else
						<div class="row form-group">
							<div id="" class="col-xs-12 col-md-12">
								<h3 class="fixed-position-header"><b><i>Agregue una Galería al sistema</b></i></h3>
							</div>
						</div>
					@endif
					<div class="row form-group">
						<div id="nombre_picture" class="col-xs-12 col-md-6">
							@if($modo==1)
								<div class="row form-group">
									<div id="nombre_picture" class="col-xs-12 col-md-12">
										<label class="alerta clearfix control-label">Título</label>
										<input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título a usar"/>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-md-12">
										<label class="alerta clearfix control-label">Sub Título</label>
										<input type="text" name="subtitulohistory" id="subtitulohistory" class="input-style form-control" placeholder="* Sub Título a usar"/>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-xs-12 col-md-12">
										<label class="alerta">Autor</label>
										<input type="text" name="autor" id="autor" class="input-style form-control" placeholder="* Autor"/>
									</div>
								</div>
							@else
							@endif
						</div>
						@if($modo==1)
							<div id="nombre_picture" class="col-xs-12 col-md-6">
								<label class="alerta clearfix control-label">Texto a ingresar</label>
								<textarea name="descripcion" id="descripcion" class="form-control summernote"></textarea>
							</div>
						@endif
					</div>
					@if($modo==1)
						<div class="row form-group">
							<h3 class="fixed-position-header">Seleccione la posición de su agrado.</h3>
						</div>
						<div class="row form-group">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview1" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview2" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture2" id="file_picture2" class="input-style form-control file" onchange='javascript:openFile2(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview3" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture3" id="file_picture3" class="input-style form-control file" onchange='javascript:openFile3(event)' value="Cargar.."/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview4" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture4" id="file_picture4" class="input-style form-control file" onchange='javascript:openFile4(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview5" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture5" id="file_picture5" class="input-style form-control file" onchange='javascript:openFile5(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview6" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture6" id="file_picture6" class="input-style form-control file" onchange='javascript:openFile6(event)' value="Cargar.."/>
							</div>
						</div>
						<div class="row form-group">
							<div id="add_button" class="col-xs-12 col-md-4">
								<button type="button" id="btnaddslider">Agregar</button>
							</div>
							<div id="add_button" class="col-xs-12 col-md-4">
								<button type="button" id="btneditar">Modificar</button>
							</div>
							<div id="addcancel" class="col-xs-12 col-md-4">
								<button type="reset" id="btnclose">Cancelar</button>
							</div>
						</div>
					@else
						<div class="row form-group">
							<h3 class="fixed-position-header">Seleccione la posición de su agrado.</h3>
						</div>
						<div class="row form-group">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview1" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview2" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture2" id="file_picture2" class="input-style form-control file" onchange='javascript:openFile2(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview3" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture3" id="file_picture3" class="input-style form-control file" onchange='javascript:openFile3(event)' value="Cargar.."/>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview4" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture4" id="file_picture4" class="input-style form-control file" onchange='javascript:openFile4(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview5" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture5" id="file_picture5" class="input-style form-control file" onchange='javascript:openFile5(event)' value="Cargar.."/>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
								<img src="{{url('img/default.jpg')}}" id="preview6" class="fixed-picture-galeria border-over">
								<input type="file" name="file_picture6" id="file_picture6" class="input-style form-control file" onchange='javascript:openFile6(event)' value="Cargar.."/>
							</div>
						</div>
						<div class="row form-group">
							<div id="add_button" class="col-xs-12 col-md-4">
								<button type="button" id="btnaddslider">Agregar</button>
							</div>
							<div id="add_button" class="col-xs-12 col-md-4">
								<button type="button" id="btneditar">Modificar</button>
							</div>
							<div id="addcancel" class="col-xs-12 col-md-4">
								<button type="reset" id="btnclose">Cancelar</button>
							</div>
						</div>
					@endif
				</form>
			</div>
		</div>
    </div>
</div>
@endsection
@section("modales")
  @include("modales.notmatch")
  @include("modales.meditar")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
  <script src="{{asset('js/summernote-es-ES.js')}}"></script>
  <script src="{{asset('js/sliderprincipal.js')}}"></script>
@endsection
