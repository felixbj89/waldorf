<form id="form-slider" action="{{url('admin/editslider')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	<input type="hidden" id="editando" name="editando" value="1"/>
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
	<input type="hidden" name="origen" id="origen" value="0"/>
	<input type="hidden" name="modo" id="modo" value="{{e($modo)}}"/>
	<input type="hidden" id="url" value="{{url('/')}}"/>
	<input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
	<div class="row form-group">
		<h3 class="fixed-position-header">Agregue la información de su agrado</h3>
	</div>
	<div class="row form-group">
		<div id="nombre_picture" class="col-xs-12 col-md-6">
			@if($modo==1)
				<div class="row form-group">
					<div id="nombre_picture" class="col-xs-12 col-md-12">
						<label class="alerta clearfix control-label">Título</label>
						<input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título a usar" value="{{e($texto[0]->titulo)}}"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12">
						<label class="alerta clearfix control-label">Sub Título</label>
						<input type="text" name="subtitulohistory" id="subtitulohistory" class="input-style form-control" placeholder="* Sub Título a usar" value="{{e($texto[0]->subtitulo)}}"/>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12">
						<label class="alerta">Autor</label>
						<input type="text" name="autor" id="autor" class="input-style form-control" placeholder="* Autor" value="{{e($texto[0]->autor)}}"/>
					</div>
				</div>
			@else
			@endif
		</div>
		@if($modo==1)
			<div id="nombre_picture" class="col-xs-12 col-md-6">
				<label class="alerta clearfix control-label">Texto a ingresar</label>
				<textarea name="descripcion" id="descripcion" class="form-control summernote">{{e($texto[0]->texto)}}</textarea>
			</div>
		@endif
	</div>
	@if($modo==1)
		<div class="row form-group">
			<h3 class="fixed-position-header">Seleccione la posición de su agrado.</h3>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[0])}}"/>
				<img src="{{url($slider[0])}}" id="preview1" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[1])}}"/>
				<img src="{{url($slider[1])}}" id="preview2" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture2" id="file_picture2" class="input-style form-control file" onchange='javascript:openFile2(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[2])}}"/>
				<img src="{{url($slider[2])}}" id="preview3" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture3" id="file_picture3" class="input-style form-control file" onchange='javascript:openFile3(event)' value="Cargar.."/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[3])}}"/>
				<img src="{{url($slider[3])}}" id="preview4" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture4" id="file_picture4" class="input-style form-control file" onchange='javascript:openFile4(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[4])}}"/>
				<img src="{{url($slider[4])}}" id="preview5" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture5" id="file_picture5" class="input-style form-control file" onchange='javascript:openFile5(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($slider[5])}}"/>
				<img src="{{url($slider[5])}}" id="preview6" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture6" id="file_picture6" class="input-style form-control file" onchange='javascript:openFile6(event)' value="Cargar.."/>
			</div>
		</div>
		<div class="row form-group">
			<div id="add_button" class="col-xs-12 col-md-6">
				<button type="button" id="btnaddslider">Modificar</button>
			</div>
			<div id="addcancel" class="col-xs-12 col-md-6">
				<a href="{{url('admin/rslider')}}" class="btn btnwaldorf">Regresar</a>
			</div>
		</div>
	@else
		<div class="row form-group">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[0])}}"/>
				<img src="{{url($sliderprincipal[0])}}" id="preview1" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture1" id="file_picture1" class="input-style form-control file" onchange='javascript:openFile1(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[1])}}"/>
				<img src="{{url($sliderprincipal[1])}}" id="preview2" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture2" id="file_picture2" class="input-style form-control file" onchange='javascript:openFile2(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[2])}}"/>
				<img src="{{url($sliderprincipal[2])}}" id="preview3" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture3" id="file_picture3" class="input-style form-control file" onchange='javascript:openFile3(event)' value="Cargar.."/>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[3])}}"/>
				<img src="{{url($sliderprincipal[3])}}" id="preview4" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture4" id="file_picture4" class="input-style form-control file" onchange='javascript:openFile4(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[4])}}"/>
				<img src="{{url($sliderprincipal[4])}}" id="preview5" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture5" id="file_picture5" class="input-style form-control file" onchange='javascript:openFile5(event)' value="Cargar.."/>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<input type="hidden" id="defaultimagen" value="{{url($sliderprincipal[5])}}"/>
				<img src="{{url($sliderprincipal[5])}}" id="preview6" class="fixed-picture-galeria border-over">
				<input type="file" name="file_picture6" id="file_picture6" class="input-style form-control file" onchange='javascript:openFile6(event)' value="Cargar.."/>
			</div>
		</div>
		<div class="row form-group">
			<div id="add_button" class="col-xs-12 col-md-6">
				<button type="button" id="btnaddslider">Modificar</button>
			</div>
			<div id="addcancel" class="col-xs-12 col-md-6">
				@if($modo==1)
					<a href="{{url('admin/rslider')}}" class="btn btnwaldorf">Regresar</a>
				@elseif($modo==2)
					<a href="{{url('admin/crearhotelgaleria')}}" class="btn btnwaldorf">Regresar</a>
				@else
					<a href="{{url('admin/crearhslider')}}" class="btn btnwaldorf">Regresar</a>
				@endif
			</div>
		</div>
	@endif
</form>
<script src="{{asset('js/editarslider.js')}}"></script>