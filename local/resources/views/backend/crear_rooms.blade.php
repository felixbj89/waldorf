@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("priactive")
	active
@endsection
@section("subactive")
	active
@endsection
@section("displaypri")
display:block;
@endsection
@section("displaysub")
display:block;
@endsection
@section("mi-css")
	<!--DATATABLE-->
	<link href="{{asset('vendors/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
	<!--FIN-->
	
	<link href="{{asset('vendors/jquery-timepicker/jquery.timepicker.css')}}" rel="stylesheet">
	<link href="{{asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/superior.css')}}" rel="stylesheet">
	<link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>{{e($texto)}}</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-md-6">
                <label>Recuerde que: </label>
                <ul>
					<li>Para la creación de la habitación debe ingresar 6 imágenes.</li>
					<li>La terminación para la hora 12:00PM equivaldrá a 12:00M.</li>
                </ul>
            </div>
			<div id="warning_picture" class="col-xs-12 col-md-6">
				@if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
				<h3 class="clienteerror" id="messagevalidacion"></h3>
			</div>
          </div>
        </div>
        <div class="x_panel">
          <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 col-xl-12">
              <input type="hidden" id="historial" value="{{$pictures}}">
              <input type="hidden" id="size" value="{{count($pictures)}}">
              <form id="form-rooms" class="form-horizontal" action="{{url('admin/addrooms')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                  <input type="hidden" id="galeria1" name="galeria1"/>
                  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                  <input type="hidden" name="tiposec" id="tiposec" value="{{$modo}}" />
                  <input type="hidden" name="propilist" id="propilist" />
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h3 class="fixed-position-header"><b><i>Portada y Propiedades de la Habitación</i></b></h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-4">
							<label class="control-label clearfix">Portada de la Habitación: Arrastre o Click para subir su imagen</label>
							<div class="fileother">
								<input name="imagen_portada" id="imagen_portada" class="input-style form-control file fixed-file center" onchange="javascript:openFile(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="loading_portada" class="fixed-picture-portada zoom">
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-8">
							<label class="control-label clearfix">Propiedades de la Habitación</label>
							<input type="text" name="propiedades" id="propiedades" class="time end input-style form-control" placeholder="* Propiedades"/>
							<button type="button" id="btnAddproperties" class="float-right">Añadir</button>
							<table id="datatable-propiedades" class="table table-striped table-bordered table-position">
								<thead>
									<tr>
										<th>Característica</th>
										<th>Opciones</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
                            </table>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h3 class="fixed-position-header"><b><i>Especificaciones de la Habitación</i></b></h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Título a usar</label>
							<input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Titulo"/>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Capacidad Máxima</label>
							<input type="number" name="capcidadmax" id="capcidadmax" class="input-style form-control" placeholder="* Capacidad Máxima" min="1" max="4"/>
						</div>
					</div>
					<div id="checkinout" class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Check In</label>
							<input type="text" name="checkin" id="checkin" class="time start input-style form-control" placeholder="* Check In"/>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Check Out</label>
							<input type="text" name="checkout" id="checkout" class="time end input-style form-control" placeholder="* Check Out"/>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Posición</label>
							<input type="number" name="posicion" id="posicion" class="input-style form-control" placeholder="* Posición" min="1" max="10"/>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6">
							<label class="control-label clearfix">Activo</label>
							<select id="estado" name="estado" class="input-style form-control">
							  <option value="NULL" class="input-style">Seleccione una opción de su agrado</option>
							  <option value="1" class="input-style">Activo</option>
							  <option value="0" class="input-style">Inactivo</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h3 class="fixed-position-header"><b><i>Galería de la Habitación</i></b></h3>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms1" id="imagen_rooms1" class="input-style form-control file fixed-file center" onchange="javascript:openRooms1(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms1" name="rooms1" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms2" id="imagen_rooms2" class="input-style form-control file fixed-file center" onchange="javascript:openRooms2(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms2" name="rooms2" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms3" id="imagen_rooms3" class="input-style form-control file fixed-file center" onchange="javascript:openRooms3(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms3" name="rooms3" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms4" id="imagen_rooms4" class="input-style form-control file fixed-file center" onchange="javascript:openRooms4(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms4" name="rooms4" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms5" id="imagen_rooms5" class="input-style form-control file fixed-file center" onchange="javascript:openRooms5(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms5" name="rooms5" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="fileother">
								<input name="imagen_rooms6" id="imagen_rooms6" class="input-style form-control file fixed-file center" onchange="javascript:openRooms6(event)" value="Cargar.." type="file">
								<img src="{{url('img/default.jpg')}}" id="rooms6" name="rooms6" class="fixed-picture-room zoom border-rooms">
								<div class="zoom_title">CLICK PARA AGREGAR UNA IMAGEN</div>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div id="" class="col-xs-12 col-md-6">
							<button type="button" id="btnadd">Agregar</button>
						</div>
						<div id="" class="col-xs-12 col-md-6">
							<button type="reset" id="btnclose">Cancelar</button>
						</div>
					</div>
				</form>
			</div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
	@include("modales.editarrow")
	@include("modales.tablalength")
@endsection
@section("mi-script")
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
	<script src="{{asset('vendors/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('vendors/jquery-timepicker/jquery.timepicker.min.js')}}"></script>
	<script src="{{asset('vendors/Datepair.js/dist/jquery.datepair.min.js')}}"></script>
	<script src="{{asset('js/createrooms.js')}}"></script>
@endsection
