@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <!-- SUMMERNOTE -->
  <link rel="stylesheet" href="{{asset('vendors/summernote-0.8.9-dist/dist/summernote.css')}}">
  <link href="{{asset('css/history.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
	<div class="">
		<div class="row">
			<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="tile-stats">
					<div class="icon"><i class="fa fa-edit"></i></div>
					<div id="" class="count"><a id="btneditarHistory" href="#">Modificar</a></div>
					<p>Click para modificar su historia</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="x_panel">
					<h2>Agregue/Modifique Nuestra Historia</h2>
					<div class="x_panel">
						<div id="warning_picture" class="col-xs-12 col-sm-12 col-md-12">
							<label>Recuerde que: </label>
							<ul>
								<li>Resolución a un rango de 300x200 px.</li>
								<li>El campo Nombre no exceder los 20 carácteres</li>
								<li>El peso por imagen debe ser inferior a los 500K</li>
								<li>Solo pueden existir un máximo de 6 imágenes en el historial</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="x_panel">
					<form id="form-history" action="{{url('admin/addhistory')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
						<input type="hidden" id="historial" value="{{$pictures}}">
					  <input type="hidden" id="size" value="{{count($pictures)}}">
					  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
					  <input type="hidden" name="origen" id="origen" value="0"/>
					  <input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
					  <input type="hidden" name="texto" id="texto"/>
					  <input type="hidden" name="tiposec" id="tiposec" value="4" />
					  <input type="hidden" id="url" value="{{url('/')}}"/>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
								<h3 class="fixed-position-header">Agregue la información de su agrado </h3>
								@if (count($errors) > 0)
									<div class="errormensaje">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
								<h3 class="errormensaje">{{Session::get("message")}}</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
								<div class="form-group">
									<label class="alerta">Título</label>
									<input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título a usar"/>
									<label class="alerta" id="priobligatorio">El campo es obligatorio, intente nuevamente</label>
									<label class="alerta" id="todosobligatorio1">Los campos son obligatorio, intente nuevamente</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
								<div class="form-group">
									<label class="alerta">Sub Título</label>
									<input type="text" name="subtitulohistory" id="subtitulohistory" class="input-style form-control" placeholder="* Sub Título a usar"/>
									<label class="alerta" id="subobligatorio">El campo es obligatorio, intente nuevamente</label>
									<label class="alerta" id="todosobligatorio2">Los campos son obligatorio, intente nuevamente</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
								<div id="bloquetexto" class="form-group has-feedback">
									<label for="descripcionnotificacion">TEXTO</label>
									<div id="descripcion"></div>
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
								<h3 class="fixed-position-header">Imagen a subir</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
								<div class="form-group">
									<label class="alerta">Cargar Imagen</label>
									<input type="file" name="file_picture" id="file_picture" class="input-style form-control" onchange='javascript:openFile(event)'/>
									<label class="alerta" id="fileobligatorio">El campo es obligatorio, intente nuevamente</label>
									<label class="alerta" id="fileformato">Solo se permiten imágenes JPG/PNG, intente nuevamente</label>
									<label class="alerta" id="filesize">Peso máximo por imagen 500K, intente nuevamente</label>
									<label class="alerta" id="fileresol">No cumple con la resolución, intente nuevamente</label>
									<label class="alerta" id="todosobligatorio3">Los campos son obligatorio, intente nuevamente</label>
								</div>
								<div class="">
									<img src="{{url('img/default.jpg')}}" id="preview" class="fixed-picture-config">
								</div>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 container-button">
								<button type="button" id="btnaddhistory" class="btn btn-default"><i class="fa fa-check" aria-hidden="true"></i> Agregar</button>
								<button type="reset" id="btnclose" class="btn btn-default"><i class="fa fa-undo" aria-hidden="true"></i> Cancelar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_alerta" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i id="profileclose" class="titulo" data-dismiss="modal">X</i>
				<h4 class="modal-title text-center alerta">HOTEL WALDORF/AVISO</h4>
			</div>
			<div class="modal-body">
        <div class="row">
          <hr>
          <div class="col-xs-12 col-md-12 fixed-border">
				<div id="picturering" class="">
					<label id="message_advertencia" class="center alerta fixed-wait fixed-wait-aux"></label>
				</div>
          </div>
        </div>
        <div class="row fixed-row-btn">
            <hr class="colorhr">
            <div class="col-xs-12 col-md-12">
                <button type="reset" id="btnclose" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
			</div>
		</div>
	</div>
</div>

@endsection
@section("modales")
  @include("modales.afterhistorialsuperior")
	@include("modales.historialhistory")
	@include("modales.meditar")
  @include("modales.notmatch")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
	<!-- SUMMERNOTE -->
	<script src="{{asset('vendors/summernote-0.8.9-dist/dist/summernote.js')}}"></script>
	<script src="{{asset('vendors/summernote-0.8.9-dist/dist/es-ES.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('js/history.js')}}"></script>
@endsection
