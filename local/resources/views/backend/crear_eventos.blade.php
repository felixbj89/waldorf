@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/eventos.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <input type="hidden" value="{{url('img/default.jpg')}}" id="defaultimagen">
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>Agregue/Modifique Eventos Gastronomicos</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-sm-12 col-md-8">
                <label>Recuerde que: </label>
                <ul>
                    <li>Resolución a un rango de 160x300 px.</li>
                    <li>El campo Título y el de SubTitulo no deben superar los 20 carácteres</li>
                    <li>El peso por imagen debe ser inferior a los 500K</li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <button id="btnModificar" type="button" class="fixed-button center">
                    Modificar
                    <i class="fa fa-edit"></i>
                </button>
            </div>
          </div>
        </div>
        <div class="x_panel">
          <form id="form-gastronomia" action="{{url('admin/addeventos')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
              <input type="hidden" name="origen" id="origen" value="2"/>
              <input type="hidden" name="texto" id="texto"/>
              <input type="hidden" name="tiposec" id="tiposec" value="5" />
              <input type="hidden" id="url" value="{{url('/')}}"/>
              <div class="row">
                <h3 class="fixed-position-header">Agregue la información de su agrado </h3>
                @if (count($errors) > 0)
                <div class="errormensaje">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <hr>
                @if(Session::has("message"))
                <h3 class="errormensaje">{{Session::get("message")}}</h3>
                @endif
              </div>
              <div class="row">
                <div id="nombre_picture" class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label class="alerta">Título</label>
                    <input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título a usar"/>
                    <label class="alerta" id="priobligatorio">El campo es obligatorio</label>
                    <label class="alerta" id="todosobligatorio5">Los campos son obligatorio</label>
                  </div>
                </div>
                <div id="nombre_picture" class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label class="alerta">Sub Título</label>
                    <input type="text" name="subtitulohistory" id="subtitulohistory" class="input-style form-control" placeholder="* Sub Título a usar"/>
                    <label class="alerta" id="subobligatorio">El campo es obligatorio</label>
                    <label class="alerta" id="todosobligatorio6">Los campos son obligatorio</label>
                  </div>
                </div>
                <div class="col-xs-12 col-md-12">
                  <label class="alerta">Texto a ingresar</label>
                  <div id="alerts"></div>
                  <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                      <ul class="dropdown-menu">
                      </ul>
                    </div>
                    <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li>
                          <a data-edit="fontSize 5">
                            <p style="font-size:17px">Grande</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 3">
                            <p style="font-size:14px">Normal</p>
                          </a>
                        </li>
                        <li>
                          <a data-edit="fontSize 1">
                            <p style="font-size:11px">Pequeña</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="btn-group">
                      <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                      <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                      <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                      <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                    </div>
                    <div class="btn-group">
                      <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                      <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                      <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                      <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                    </div>
                    <div class="btn-group">
                      <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                      <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                      <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                      <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a></div>

                      <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                      </div>
                    </div>
                    <div id="editor" class="editor-wrapper input-style form-control"></div>
                    <textarea name="descr" id="descr" style="display:none;"></textarea>
					<label class="alerta" id="errorMensaje">{{Lang::get('message.eventosex')}}</label>
                  <!--<textarea name="descr" id="descr" cols="10" rows="5" class="input-style form-control"></textarea>-->
                </div>
              </div>
              <div class="row fixed-row">
                <hr>
                <div id="addcancel" class="col-xs-12 col-md-6">
                  <button type="reset" id="btnclose">Cancelar</button>
                </div>
                <div id="add_button" class="col-xs-12 col-md-6">
                  <button type="button" id="btnaddeventos">Agregar</button>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
              <input type="hidden" id="editando" name="editando" value="0"/>
			  <input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
			  <div class="col-md-6 col-sm-12 col-xs-12">
                <h3 class="fixed-position-header">Imagen a subir</h3>
                <hr>
                <div class="form-group">
                  <label class="alerta">Cargar Imagen</label>
                  <input type="file" name="file_picture1" id="file_picture1" class="input-style form-control" onchange='javascript:openFile(event)'/>
                  <label class="alerta" id="fileobligatorio1">El campo es obligatorio</label>
                  <label class="alerta" id="fileformato1">Solo se permiten imágenes JPG/PNG</label>
                  <label class="alerta" id="fileresol1">No cumple con la resolución</label>
                  <label class="alerta" id="filesize1">Peso máximo por imagen 500K</label>
                  <label class="alerta" id="todosobligatorio1">Los campos son obligatorio</label>
                </div>
                <div class="">
                  <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                  <img src="{{url('img/restaurante/eventos/default.jpg')}}" id="preview1" class="fixed-picture-config EventosGastronomicos">
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <h3 class="fixed-position-header">Imagen a subir</h3>
                <hr>
                <div class="form-group">
                  <label class="alerta">Cargar Imagen</label>
                  <input type="file" name="file_picture2" id="file_picture2" class="input-style form-control" onchange='javascript:openFile(event)'/>
                  <label class="alerta" id="fileobligatorio2">El campo es obligatorio</label>
                  <label class="alerta" id="fileformato2">Solo se permiten imágenes JPG/PNG</label>
                  <label class="alerta" id="filesize2">Peso máximo por imagen 500K</label>
                  <label class="alerta" id="fileresol2">No cumple con la resolución</label>
                  <label class="alerta" id="todosobligatorio2">Los campos son obligatorio</label>
                </div>
                <div class="">
                  <img src="{{url('img/restaurante/eventos/default.jpg')}}" id="preview2" class="fixed-picture-config EventosGastronomicos">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.alerta")
  @include("modales.meditar")
  @include("modales.notmatch")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
  <script src="{{asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('js/eventos.js')}}"></script>
@endsection
