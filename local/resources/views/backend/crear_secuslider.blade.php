@extends("layout.master")
@section("page-body")
@stop
@section("mi-dashboard")
@stop
@section("mi-css")
  <link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/summernote-master/dist/summernote.css')}}" rel="stylesheet">
  <link href="{{asset('vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/secuslider.css')}}" rel="stylesheet">
  <link href="{{asset('css/master.css')}}" rel="stylesheet">
@endsection
@section("body")
<div class="right_col" role="main">
  <input type="hidden" value="" id="picturelist"/>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="x_panel">
          <h2>{{e($texto)}}</h2>
          <div class="x_panel">
            <div id="warning_picture" class="col-xs-12 col-sm-12 col-md-6">
                <label>Recuerde que: </label>
                <ul>
                  <li>Resolución permitida. {{e($medidas)}}</li>
                  <li>Los campos de texto no deben exceder los 20 carácteres.</li>
                  <li>El peso por imagen debe ser inferior a los 500K.</li>
                  <li>Deberá ingresar 5 imágenes thumbnails y de imágenes grandes, para poder continuar.</li>
                  <li>Podrá Cambiar el Thumbnail luego de ser cargado.</li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
				@if(Session::has("message")) <h3 class="errormensaje">{{Session::get("message")}}</h3> @endif
				<h3 class="clienteerror"></h3>
			</div>
          </div>
        </div>
        <div id="panel" class="x_panel">
          <form id="form-slider" action="{{url('admin/addseculider')}}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
            <input type="hidden" id="editando" name="editando" value="0"/>
			<input type="hidden" id="modoEditar" name="modoEditar" value="{{Lang::get('message.meditar')}}"/>
            <input type="hidden" name="modo" id="modo" value="{{e($modo)}}"/>
            <input type="hidden" id="url" value="{{url('/')}}"/>
            <div class="row form-group">
              <h3 class="fixed-position-header">Agregue el texto de su agrado</h3>
              <hr>
              <div id="nombre_picture" class="col-xs-12 col-md-12 fixd-col-md-4v3">
                <div class="form-group">
                  <label class="alerta">Título</label>
                  <input type="text" name="titulohistory" id="titulohistory" class="input-style form-control" placeholder="* Título Principal"/>
                </div>
              </div>
			 </div>
			 <div class="row form-group">
              <div class="col-md-12 col-sm-12 col-xs-12">
					<label class="alerta">Texto a ingresar</label>
					<textarea name="descr" id="descr" class="summernote"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="fixed-position-header">Agregue las imágenes en miniatura de su agrado </h3>
                <div class="col-md-12 col-sm-12 col-xs-12">
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <hr>
                  <div class="">
                    <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                    <div class="fileother">
                      <input type="file" name="filethumb1" id="filethumb1" class="input-style form-control file fixed-file center" onchange='javascript:openThum1(event)' value="Cargar.."/>
                      <img src="{{url('img/default.jpg')}}" id="preview1" class="fixed-picture-thumb center border-over">
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <hr>
                  <div class="">
                    <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                    <div class="fileother">
                      <input type="file" name="filethumb2" id="filethumb2" class="input-style form-control file fixed-file center" onchange='javascript:openThum2(event)' value="Cargar.."/>
                      <img src="{{url('img/default.jpg')}}" id="preview2" class="fixed-picture-thumb center border-over">
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <hr>
                  <div class="">
                    <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                    <div class="fileother">
                      <input type="file" name="filethumb3" id="filethumb3" class="input-style form-control file fixed-file center" onchange='javascript:openThum3(event)' value="Cargar.."/>
                      <img src="{{url('img/default.jpg')}}" id="preview3" class="fixed-picture-thumb center border-over">
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <hr>
                  <div class="">
                    <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                    <div class="fileother">
                      <input type="file" name="filethumb4" id="filethumb4" class="input-style form-control file fixed-file center" onchange='javascript:openThum4(event)' value="Cargar.."/>
                      <img src="{{url('img/default.jpg')}}" id="preview4" class="fixed-picture-thumb center border-over">
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                  <hr>
                  <div class="">
                    <input type="hidden" id="defaultimagen" value="{{url('img/default.jpg')}}"/>
                    <div class="fileother">
                      <input type="file" name="filethumb5" id="filethumb5" class="input-style form-control file fixed-file center" onchange='javascript:openThum5(event)' value="Cargar.."/>
                      <img src="{{url('img/default.jpg')}}" id="preview5" class="fixed-picture-thumb center border-over">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <h3 class="fixed-position-header">Imágenes Asociadas / Click para subir una imagen</h3>
              <hr>
              <div class="row form-group">
                <div class="col-xs-12 col-md-4 fixd-col-md-4v2">
                  <div class="fileother">
                    <input type="file" name="fileasociado1" id="fileasociado1" class="input-style form-control file fixed-file center" onchange='javascript:openFile1(event)' value="Cargar.."/>
                    <img src="{{url('img/default.jpg')}}" id="asociado1" class="fixed-picture-background center border-over" data-confirmar1="0">
                  </div>
                </div>
                <div class="col-xs-12 col-md-4 fixd-col-md-4v2">
                  <div class="fileother">
                    <input type="file" name="fileasociado2" id="fileasociado2" class="input-style form-control file fixed-file center" onchange='javascript:openFile2(event)' value="Cargar.."/>
                    <img src="{{url('img/default.jpg')}}" id="asociado2" class="fixed-picture-background center border-over" data-confirmar2="0">
                  </div>
                </div>
                <div class="col-xs-12 col-md-4 fixd-col-md-4v2">
                  <div class="fileother">
                    <input type="file" name="fileasociado3" id="fileasociado3" class="input-style form-control file fixed-file center" onchange='javascript:openFile3(event)' value="Cargar.."/>
                    <img src="{{url('img/default.jpg')}}" id="asociado3" class="fixed-picture-background center border-over" data-confirmar3="0">
                  </div>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-xs-12 col-md-4 fixd-col-md-4v2">
                  <div class="fileother">
                    <input type="file" name="fileasociado4" id="fileasociado4" class="input-style form-control file fixed-file center" onchange='javascript:openFile4(event)' value="Cargar.."/>
                    <img src="{{url('img/default.jpg')}}" id="asociado4" class="fixed-picture-background center border-over" data-confirmar4="0">
                  </div>
                </div>
                <div class="col-xs-12 col-md-4 fixd-col-md-4v2">
                  <div class="fileother">
                    <input type="file" name="fileasociado5" id="fileasociado5" class="input-style form-control file fixed-file center" onchange='javascript:openFile5(event)' value="Cargar.."/>
                    <img src="{{url('img/default.jpg')}}" id="asociado5" class="fixed-picture-background center border-over" data-confirmar5="0">
                  </div>
                </div>
              </div>
            </div>
            <div class="row form-group" style="height:100px;">
              <div id="add_button" class="col-xs-12 col-md-6">
                <button type="button" id="btnslider" class="center">Agregar</button>
              </div>
              <div id="addcancel" class="col-xs-12 col-md-6">
                <button type="button" id="btneditar" class="center">Modificar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section("modales")
  @include("modales.notmatch")
  @include("modales.meditar")
  @include("modales.tablalength")
@endsection
@section("mi-script")
  <script src="{{asset('vendors/summernote-master/dist/summernote.min.js')}}"></script>
  <script src="{{asset('js/summernote-es-ES.js')}}"></script>
  <script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
  <script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>
  <script src="{{asset('js/secuslider.js')}}"></script>
@endsection
