@extends("layout.panel")
@section("Restaurante-nav")
	active
@endsection
@section("Mycss")
	{!!Html::style('css/restauratWaldorf.css')!!}
	{!!Html::style('vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css')!!}
@endsection
@section("SliderWaldorf")
	<div id="slider-topage" style="background-image:url({{url($urlsuperior)}});" alt="{{$nombresuperior}}"></div>
@endsection
@section("tolinks")
	{{url('Home/Rontacto')}}
@endsection
@section("body-page")
	<!-- Galeria -->
	<div id="Galeria" class="row">
		<div class="col-xs-10 col-xs-offset-1" style="padding:0px;">
			<div class="col-sm-6 col-xs-12 h400">
				<table class="TableP"><tbody><tr><td class="pos-td">
					<p class="tituloDeco text-center">{{e($titulogaleria)}}</p>
					<p class="titulo text-center">{{e($subtitulogaleria)}}</p>
					<p id="parrafogaleria" class="text-center">

					</p>
					<p class="text-center"><b>{{e($autorgaleria)}}</b></p>
				</td></tr></tbody></table>
			</div>
			<div class="col-sm-6 col-xs-12 h400 hd400">
				<div class="col-xs-11 h400 hd400" style="padding:0px;">
					<table class="TableP"><tbody><tr><td>
						<div id="Carousel-Restaurat" style="position: relative;" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
							<div id="inner-Restaurat" class="carousel-inner" role="listbox">
								<div class="item active"><img style="margin:0px auto;" src="{{url('img/default.jpg')}}"></div>
							</div>
						</div>
					</td></tr></tbody></table>
				</div>
				<div class="col-xs-1 h400 hd400" style="padding:0px;left:10px;">
					<div class="control control-left"><img src="{{url('img/right-arrow.svg')}}" class="img-responsive glyphicon-prev flecha-prev"><!--i class="glyphicon glyphicon-menu-left glyphicon-prev"></i--></div>
					<div class="control control-right"><img src="{{url('img/left-arrow.svg')}}" class="img-responsive glyphicon-next"><!--i class="glyphicon glyphicon-menu-right glyphicon-next"></i--></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Reserva -->
	<div id="Reserva" style="background-image:url({{url($urlinferior)}});" alt="{{$nombreinferior}}">
		<div id="container-RR">
			<div id="logo-Restaurant"></div>
			<p id="titulo-Reserva" class="sub-titulo">Abierto de Lunes a Domingo<br/><b>De 11:30 Hrs a 23:00 Hrs</b></p>
			<button type="button" id="btn-restaurant"  data-toggle="modal" data-target="#ModalReservaRes">Para Reservar</button>
		</div>
	</div>
	<!-- Eventos -->
	<div id="Eventos" class="row">
		<div class="col-xs-10 col-xs-offset-1" style="padding:0px;">
			<input type="hidden" value="{{$textoseventos}}" id="textonuevoevento"/>
			<input type="hidden" value="{{$textogaleria}}" id="textonuevogaleria"/>
			<div class="col-sm-6 col-xs-12 h400">
				<table class="TableP"><tbody><tr><td>
					<p class="tituloDeco text-center">{{e($tituloeventos)}}</p>
					<p id="parrafoeventos" class="text-center">

					</p>
					<p class="text-center"><a href="{{url('Home/Contacto')}}" class="titulo-res link-T" style="margin-bottom:0px">{{e($substituloeventos)}} >></a></p>
				</td></tr></tbody></table>
			</div>
			<div class="col-sm-6 col-xs-12 h400" style="padding:0px;">
				<div class="row" style="margin:0px;">
					<div class="col-sm-6 h400" style="padding: 0px 5px;" id="EventosGastronomicos-I"><div class="EventosGastronomicos" style="background-image:url({{url('img/default.jpg')}});"></div></div>
					<div class="col-sm-6 h400" style="padding: 0px 5px;" id="EventosGastronomicos-II"><div class="EventosGastronomicos" style="background-image:url({{url('img/default.jpg')}});"></div></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Arriba -->
	<div id="Arriba">
		<i class="glyphicon glyphicon-menu-up"></i>
	</div>
@endsection
@section("modal-page")
	<!-- Modal -->
	<div class="modal fade" id="ModalReservaRes" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<i id="Iclose" class="titulo" data-dismiss="modal">X</i>
					<h4 class="tituloDeco modal-title text-center">Reserva</h4>
				</div>
					<div class="modal-body">
						<p class="text-center">Para procesar su solicitud es necesario completar los siguiente campos</p>
						<form id="form-reserva">
							<div class="row">
								<div id="nombre_cliente" class="col-xs-12">
									<div class="form-group">
										<input type="text" name="name" id="name" class="input-style form-control" placeholder="* Nombre"/>
                                        <label id="namelength" class="error error-p">Este dato es obligatorio, intente nuevamente.</label>
									</div>
								</div>
								<div id="telefo_cliente" class="col-xs-12">
									<div class="form-group">
										<input type="text" name="phone" id="phone" class="input-style form-control" placeholder="* Tel&eacute;fono"/>
                                        <label id="telefonolength" class="error error-p">
                                            Este dato es obligatorio, intente nuevamente.</label>
                                        <label id="telefonoshort" class="error error-p">
                                            Debe Ingresar un teléfono válido, intente nuevamente.</label>
                                        <label id="telefonoformato" class="error error-p">
                                            Formato de teléfono incorrecto, intente nuevamente.</label>
									</div>
								</div>
								<div id="hora" class="col-xs-6">
									<div class="form-group">
										<input type="text" name="horareserva" id="horareserva" class="input-style form-control timepicker" placeholder="* Hora"/>
									</div>
								</div>
								<div id="hora" class="col-xs-6">
									<div class="form-group">
										<input type="number" min="1" name="npersonas" id="npersonas" class="input-style form-control" placeholder="* Cantidad de personas"/>
                                        <label id="cantidadlength" class="error error-p">
                                            Este dato es obligatorio, intente nuevamente.</label>
									</div>
								</div>
							</div>
							<div class="row">
								<button type="button" id="btn-REnviar">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section("Myscript")
	{!!Html::script('js/plugins/jquery.masonry.min.js')!!}
	{!!Html::script('js/restauratWaldorf.js')!!}
<script>
jQuery(function ($) {
	$(document).ready(function (){
		if($("#textonuevoevento").val()=="<TEXTO>"){
  		$("#parrafoeventos").text($("#textonuevoevento").val());
  	}else{
  		$("#parrafoeventos").html($("#textonuevoevento").val());
  	}

		if($("#textonuevogaleria").val()=="<TEXTO>"){
  		$("#parrafogaleria").text($("#textonuevogaleria").val());
  	}else{
  		$("#parrafogaleria").html($("#textonuevogaleria").val());
  	}

		$.ajax({
			type:'GET',
			url:'{{url('/RestaurantImgContent')}}',
			success:function(res){
				$('#inner-Restaurat,#EventosGastronomicos-I,EventosGastronomicos-II').empty();
				$('#inner-Restaurat').html(res.Gallery);
				$('#EventosGastronomicos-I').html(res.Eventos.Eventos0);
				$('#EventosGastronomicos-II').html(res.Eventos.Eventos1);
			},
			error:function(xhr,status){
				alert('Problemas...');
			},
		});
		$('.grid').masonry({
		  itemSelector: '.grid-item-width',
		  percentPosition: true
		});

		//$.fn.AnimateScroll(id);
	});
});
</script>
@endsection
