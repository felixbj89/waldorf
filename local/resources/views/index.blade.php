@extends("layout.panel")
@section("Home-nav")
	active
@endsection
@section("Mycss")
	{!!Html::style('css/indexWaldorf.css')!!}
	{!!Html::style('css/modalpromo.css')!!}
	<link href="{{asset('vendors/shadowbox-3.0.3/shadowbox.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section("SliderWaldorf")
	<div id="Carousel-Waldorf" class="carousel slide carousel-fade" data-ride="carousel">
		<div id="Inner-Waldorf" class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="item-topage" style=" background-image:url({{URL('img/default.jpg')}})"></div>
			</div>
		</div>
	</div>
@endsection
@section("tolinks")
	{{url('Home/Contacto')}}
@endsection
@section("body-page")
	<!-- Habitaciones -->
	<div id="habitaciones">
		<div class="row" style="margin:0px;">
			<div class="col-md-7 col-xs-12" id="Waldorf-ImgH">
				<div id="Carousel-WaldrofHab" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
					<ol id="WaldrofHab-indicators" class="">
						<li id="indicero" data-target="#Carousel-WaldrofHab" data-slide-to="0" style=" background-image:url({{URL('img/default.jpg')}})" class="idicator-Hwaldorf"></li>
						<li data-target="#Carousel-WaldrofHab" data-slide-to="1" style=" background-image:url({{URL('img/default.jpg')}})" class="idicator-Hwaldorf"></li>
						<li data-target="#Carousel-WaldrofHab" data-slide-to="2" style=" background-image:url({{URL('img/default.jpg')}})" class="idicator-Hwaldorf"></li>
						<li data-target="#Carousel-WaldrofHab" data-slide-to="3" style=" background-image:url({{URL('img/default.jpg')}})" class="idicator-Hwaldorf"></li>
						<li data-target="#Carousel-WaldrofHab" data-slide-to="4" style=" background-image:url({{URL('img/default.jpg')}})" class="idicator-Hwaldorf"></li>
					</ol>
					<div id="Inner-WaldrofHab" class="carousel-inner" role="listbox">
						<div class="item-WaldrofHab item active" style=" background-image:url({{URL('img/default.jpg')}})"></div>
						<div class="item-WaldrofHab item" style=" background-image:url({{URL('img/default.jpg')}})"></div>
						<div class="item-WaldrofHab item" style=" background-image:url({{URL('img/default.jpg')}})"></div>
						<div class="item-WaldrofHab item" style=" background-image:url({{URL('img/default.jpg')}})"></div>
						<div class="item-WaldrofHab item" style=" background-image:url({{URL('img/default.jpg')}})"></div>
					</div>
				</div>
			</div>
			<div class="col-md-5 col-xs-12" id="Waldorf-DesH">
				<table class="TableP" style="max-width:320px;margin:0px auto;"><tbody><tr><td>
					<p class="titulo text-center">{{e($tituloslidersecu)}}</p>
					<input type="hidden" id="segmentohab" value="{{e($textoslidersecu)}}"/>
					<p id="ParrafoHab">
						<!--Dentro del Hotel Waldorf contamos con las mejores habitaciones de la ciudad ubicadas en una zona de alto inter&eacute;s comercial para aquellos visitantes con altos niveles de exigencia y est&aacute;ndares &uacute;nicos dentro de la capital del pa&iacute;s.
					<br/><br/>
						Sin duda alguna ponemos a tu disposición la excelencia que nos ha distinguido por de seis d&eacute;cadas.-->
					</p>
					<button id="btn-walhab">ver m&aacute;s</button>
				</td></tr></tbody></table>
			</div>
		</div>
	</div>
	<!-- Historia -->
	<div id="Historia">
		<input type="hidden" value="{{$textohistory}}" id="textonuevo"/>
		<div class="row" style="padding:0px;margin:0px;">
			<div class="col-xs-10 col-xs-offset-1" style="padding:0px;">
				<div id="imgHistoria" style="background-image:url({{URL($urlhistory)}});" class="col-sm-4"></div>
				<div id="ContainrTP" class="col-sm-8" style="padding:0px;">
					<table class="TableP"><tbody><tr><td>
						<p class="titulo text-center">{{e($titulohistory)}}</p>
						<p id="ParrafoHistoria">

						</p>
						<p class="text-center"><a href="" class="titulo link-T" style="margin-bottom:0px;font-size:17px !important;">{{e($subtitulohistory)}} >></a></p>
					</td></tr></tbody></table>
				</div>
			</div>
		</div>
	</div>
	<hr style="margin-top:40px;margin-bottom:40px;border:0;border-top:1px solid #333;width:83.33333%;background-color:transparent;" align="center">
	<!-- Restaurante -->
	<div id="Restaurante" style="margin-bottom:40px;">
		<input type="hidden" value="{{$textogastronomia}}" id="textogastro"/>
		<div class="row" style="margin:0px;">
			<div class="grid col-xs-10 col-xs-offset-1" style="padding:0px;">
				<div class="grid-item-width grid-item-height item-text">
					<div class="grid-item-text">
						<table class="TableP"><tbody><tr><td>
							<p class="titulo text-center">{{e($titulogastronomia)}}</p>
							<p id="ParrafoGastro">

							</p>
							<p class="text-right"><a href="{{url('Restaurante')}}" class="titulo text-right link-T" style="margin-bottom:0px;font-size:17px !important;">{{e($subtitulogastronomia)}} >></a></p>
						</td></tr></tbody></table>
					</div>
				</div>
				<div class="gridItemImg grid-item-width grid-item-height2" id="gridItemImg1"><div class="grid-item-img" style="background-image:url({{URL('img/default.jpg')}})"></div></div>
				<div class="gridItemImg grid-item-width grid-item-height" id="gridItemImg2"><div class="grid-item-img" style="background-image:url({{URL('img/default.jpg')}})"></div></div>
				<div class="gridItemImg grid-item-width grid-item-height" id="gridItemImg0"><div class="grid-item-img" style="background-image:url({{URL('img/default.jpg')}})"></div></div>
				<div class="gridItemImg grid-item-width grid-item-height" id="gridItemImg3"><div class="grid-item-img" style="background-image:url({{URL('img/default.jpg')}})"></div></div>
			</div>
		</div>
	</div>
	<!-- Promociones -->
	@if($promociones["promociones"] !="")
		<div id="promociones" style="margin-bottom:40px;">
			<p class="titulo text-center">Promociones Vigentes</p>
			<div id="content-promociones" style="margin-bottom:40px;">
				<div id="CarouselPromo" class="carousel fdi-Carousel slide">
				 <!-- Carousel items -->
					<div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
						<div class="carousel-inner onebyone-carosel">
							{!!$promociones["promociones"]!!}
						</div>
						<div class="left carousel-control prueba"><a href="#eventCarousel" data-slide="prev"></a></div>
						<div class="right carousel-control prueba"><a href="#eventCarousel" data-slide="next"></a></div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<!-- Contacto -->
	<div id="Contacto" style="background-image:url({{URL($urlcontacto)}})" alt="{{$picturecontacto}}">
		<table style="height:100%;width:100%;padding:32.5px auto;"><tr><td>
			<p class="titulo text-center" style="color:#FFF;">CONTACTO</p>
			<form id="form-contactus" class="col-xs-10 col-xs-offset-1" method="post">
				<div class="form-group col-xs-12">
					<select class="form-control inputWldorf destino grey_color" id="destino" name="destino">
						<option value="NULL" class="color">Seleccione un destino válido.</option>
						<option value="recepción" class="color">Banquetes y Eventos Sociales.</option>
						<option value="grupos" class="color">Grupos y Convenciones</option>
						<option value="reservaciones" class="color">Reservaciones</option>
						<option value="restaurante" class="color">Restaurante</option>
						<option value="promociones" class="color">Promociones</option>
					</select>
                    <label id="formatochecked" class="error">
                        Debe seleccionar una opción (Eventos/Restaurante/Mercadeo), intente nuevamente.
                    </label>
				</div>
				<div class="form-group col-xs-12 col-sm-6">
					<input type="text" class="inputWldorf form-control" name="nombre" id="nombre" placeholder="Nombre">
                    <label id="nombrelength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-6">
					<input type="email" class="inputWldorf form-control" name="correo" id="correo" placeholder="Correo Electr&oacute;nico">
                    <label id="formatoincorrecto" class="error">Formato de correo incorrecto, intente nuevamente.</label>
                    <label id="formatoespacio" class="error">En este campo no puede ingresar espacios, intente nuevamente.</label>
                    <label id="formatolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6">
					<input type="text" class="inputWldorf form-control" name="asunto" id="asunto" placeholder="Asunto">
                    <label id="asuntolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12 col-sm-12 col-md-6">
					<input type="text" class="inputWldorf form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono">
                    <label id="telefonolength" class="error">Este campo es obligatorio, intente nuevamente.</label>
                    <label id="telefonoshort" class="error">Debe Ingresar un teléfono válido, intente nuevamente.</label>
                    <label id="telefonoformato" class="error">Formato de teléfono incorrecto, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12">
					<textarea class="inputWldorf form-control" name="Mensaje" id="Mensaje" rows="4" placeholder="Mensaje"></textarea>
                    <label id="mensajelength" class="error">Este campo es obligatorio, intente nuevamente.</label>
                    <label id="mensajeshort" class="error">Debe Ingresar un mensaje válido, intente nuevamente.</label>
				</div>
				<div class="form-group col-xs-12">
					<button type="button" id="btn-env" name="btn-env">ENVIAR</button>
				</div>
			</form>
		</td></tr></table>
		<!-- Arriba -->
		<div id="ArribaHome">
			<i class="glyphicon glyphicon-menu-up"></i>
		</div>
	</div>
@endsection
@section("modal-page")
	@include("modales.modalPromo")
@endsection
@section("Myscript")
	{!!Html::script('js/plugins/jquery.masonry.min.js')!!}
	<script type="text/javascript" src="{{asset('vendors/shadowbox-3.0.3/shadowbox.js')}}"></script>
	<script type="text/javascripT">	Shadowbox.init({ language: "es", players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv'] });</script>
	{!!Html::script('js/indexWaldorf.js')!!}
<script>
jQuery(function ($) {
	$(document).ready(function (){
		var modalp = {{e($modalpromo)}};
		if(modalp){
			setTimeout(function() {
				Shadowbox.open({
					content:    '<div class="oferta"><img src="{{$ruta}}"></div>',
					player:     "html",
					title:      "",
					width:      501,
					height:     502
				});
			}, 50);
		}	
		if($("#segmentohab").val()=="<TEXTO>"){
			$("#ParrafoHab").text($("#segmentohab").val());
		}else{
			$("#ParrafoHab").html($("#segmentohab").val());
		}
		
		$("#alerta").on("hidden.bs.modal",function(){
			$("#acceder").modal("show");
		});

		if($("#error").val()!=undefined){
			$("#errormensajelogin").text($("#error").val());
			$("#acceder").modal("hide");
			$("#alerta").modal("show");
		}

		$("#btn-walhab").on("click",function(){
			var pictureurl = "";
			var pos = window.location.href.search("Home");
			if(pos > 0){
				pictureurl = window.location.href.substr(0,pos-1);
			}else{
				pos = window.location.href.search("/");
				if(pos > 0){
					pictureurl = window.location.href.substr(0,pos-1);
				}
			}

			if(pictureurl=="http"){
				window.location.href = (window.location.href + "/Habitaciones");
			}else{
				window.location.href = (pictureurl + "/Habitaciones");
			}
		});

		var id="<?php echo $id;?>";
		$.ajax({
			type:'GET',
			url:'{{url('/IndexImgContent')}}',
			success:function(res){
				$('#Inner-Waldorf,#WaldrofHab-indicators,#Inner-WaldrofHab,.gridItemImg').empty();
				$('#Inner-Waldorf').html(res.Slider);
				$('#WaldrofHab-indicators').html(res.thumbnailsHabi);
				$('#Inner-WaldrofHab').html(res.imgHabi);
				$('#gridItemImg0').html(res.imgRes.imgR0);
				$('#gridItemImg1').html(res.imgRes.imgR1);
				$('#gridItemImg2').html(res.imgRes.imgR2);
				$('#gridItemImg3').html(res.imgRes.imgR3);
			},
			error:function(xhr,status){
				alert('Problemas...');
			},
		});
		$('.grid').masonry({
		  itemSelector: '.grid-item-width',
		  percentPosition: true
		});
		
		if(id=="Rontacto"){
			$("#destino").val("restaurante");
			$('html, body').animate({scrollTop: ($("#Contacto").offset().top)-150}, 1500);
		}else{
			$.fn.AnimateScroll(id);
		}
		

		$('#CarouselPromo').carousel({
			interval: false
		})
		
		if($(window).width() > 767){
			var promos = {{e($countpromo)}};
			if(promos > 1){
				$('.fdi-Carousel .item').each(function () {
					var next = $(this).next();
					if (!next.length) {
						next = $(this).siblings(':first');
					}
					next.children(':first-child').clone().appendTo($(this));

					if (next.next().length > 0) {
						next.next().children(':first-child').clone().appendTo($(this));
					}
					else {
						$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
					}
				});
				var url = $(".onebyone-carosel>.item.active").next().data('imgpromo');
				var name = $(".onebyone-carosel>.item.active").next().data('namepromo');
				$('#modalPromo .content-imgPromo>img').attr("src",url);
				$('#btn-promo').data("namepromo",name);	
			}else{
				$('#eventCarousel .item.active').css({"margin":"0 auto","width":"300px"});
			}
			
			if(promos == 2){
				$("#eventCarousel").carousel("next");
			}
			
		}else{
			$('#CarouselPromo').addClass('carousel-fade');
			$('#btn-promo').data("namepromo",name);
		}
		
		$("#btn-promo").on("click",function(){
			var info = $(".onebyone-carosel>.item.active").data('info');
			$("#asunto").val(info["asunto"]);
			$("#destino").val("promociones");
			$("#Mensaje").html("Requiero/Solicitud la promoción: " + info["asunto"] + ", de costo: " + info["costo"] + ", Descripción: " + info["descripcion"].replace(/<\/?[^>]+(>|$)/g, ""));
		});
		
		//var url = $(".onebyone-carosel>.item.active").data('imgpromo');
		
		$(document).on("click","#picture",function(){
		   $('#modalPromo .content-imgPromo>img').attr("src",$(this).data("picture"));
		});
		
		
		//$('#modalPromo .content-imgPromo>img').attr("src",url);
		
	});
});
</script>
@endsection
