<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'backendlogin' => 'Ingrese sus credenciales para poder continuar.',
    "backendprofile" => "Edite sus datos",
    "backenduserdata" => "Datos del Usuario",
    "titulo" => "HOTEL WALDORF",
    "antes" => "Espere un momento, su mensaje se está enviando",
    "antesreserva" => "Espere un momento, su reservación se está enviando",
    "antescrear" => "Espere un momento, el usuario se esta creando",
    "despues" => "Mensaje enviado satisfactoriamente",
    "despuesreserva" => "Reserva enviada satisfactoriamente",
    "despuescrear" => "Creación de usuario sastifactoria",
    "errorcreando" => "No se pudo crear el usuario, intente nuevamente",
    "noexiste" => "No existen usuarios registrados en el sistema.",
	"notsuperior" => "No existen Fotos Principales registradas.",
    "seguro" => "Confirme para remover el usuario del sistema",
	"eventosex" => "Ha alcanzado el límite máximo permitido 290/290 caracteres",
	"sslidermex" => "Ha alcanzado el límite máximo permitido 268/268 caracteres",
	"gastromex" => "Ha alcanzado el límite máximo permitido 193/193 caracteres",
	"historymex" => "Ha alcanzado el límite máximo permitido 455/455 caracteres",
	"secumex" => "Ha alcanzado el límite máximo permitido 229/229 caracteres",
	"confirmado" => "Guardado satisfactorio",
	"meditar" => "Usted ha iniciado el modo de edición de esta sección"
];
