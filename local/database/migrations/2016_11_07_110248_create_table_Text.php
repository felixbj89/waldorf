<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Text;

class CreateTableText extends Migration
{
    //0 home
    //1 nuestra historia
    //2 habitaciones
    //3 restaurant
    //------------------------
    //0 = Historia/Home / Galeria/Restaurante
    //1 = gastronomia/Home
    //2 = Eventos/Restaurante

    public function up(){
      Schema::create('text', function (Blueprint $table) {
        $table->increments('id');
        $table->enum('text_section',['0','1','2','3']);
        $table->enum('text_origen',['0','1','2']);
        $table->enum('text_status',['0','1','2']);
        $table->text('text_json');
        $table->integer('user_id');
        $table->timestamps();
      });

      Text::create([
        "text_section" => "0",
        "text_origen" => "0",
        "text_json" => '[{"titulo":"<TITULO>","subtitulo":"<SUB TITULO>","texto":"<TEXTO>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Valores de nuestra História","subtitulo":"CONOCE MÁS","texto":"Schlesinger se concentró en El Encantado y el Waldorf: a mediodía estaba en restaurant de La Electricidad y de noche despachaba hasta altas horas en el Waldorf, en cuyas paredes del comedor colgaban obras de maestros de la alta pintura europea del siglo XIX y comienzos del XX pues invertía en arte parte de las ganancias como hostelero profesional."}]',
        "user_id" => "1"
      ]);

      Text::create([
        "text_section" => "0",
        "text_origen" => "1",
        "text_json" => '[{"titulo":"<TITULO>","subtitulo":"<SUB TITULO>","texto":"<TEXTO>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Exelencia Gastronómica","subtitulo":"TE INVITAMOS","texto":"Contamos con el mejor restaurante de pastas de la zona oeste de la Ciudad de Caracas, no puedes perderte de las exquisiteces de nuestra cocina."}]',
        "user_id" => "1"
      ]);

      Text::create([
        "text_section" => "3",
        "text_origen" => "2",
        "text_json" => '[{"titulo":"<TITULO>","subtitulo":"<SUB TITULO>","texto":"<TEXTO>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Eventos Gastronomicos","subtitulo":"CONTÁCTANOS","texto":"En un ambiente único rodeado de TV digitales disfrutarás los grandes eventos deportivos y música en vivo a partir de los jueves. Ven, te esperamos con el mejor toque al estilo ¨ Il Pappardelle¨"}]',
        "user_id" => "1"
      ]);

      Text::create([
        "text_section" => "3",
        "text_origen" => "0",
        "text_json" => '[{"titulo":"<TITULO>","subtitulo":"<SUB TITULO>","texto":"<TEXTO>","autor":"<AUTOR>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Restaurante","subtitulo":"Italo & Mediterraneo","texto":"Il Pappardelle es un restaurant de comida italiana que representa un oasis en el centro de Caracas, por su excelente ambiente y atención de primera para el disfrute de las mejores pastas y pizzas. Junto con los acompañantes perfectos cuenta con un gran stock de vinos."}]',
        "user_id" => "1"
      ]);

      Text::create([
        "text_section" => "2",
        "text_origen" => "0",
        "text_json" => '[{"titulo":"<TITULO>","texto":"<TEXTO>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Habitaciones","subtitulo":"Dentro del Hotel Waldorf contamos con las mejores habitaciones de la ciudad ubicadas en una zona de alto interés comercial para aquellos visitantes con altos niveles de exigencia y estándares únicos dentro de la capital del país."}]',
        "user_id" => "1"
      ]);

      Text::create([
        "text_section" => "0",
        "text_origen" => "2",
        "text_json" => '[{"titulo":"<TITULO>","texto":"<TEXTO>"}]',
        "text_status" => "0",
        //"text_json" => '[{"titulo":"Habitaciones","subtitulo":"Dentro del Hotel Waldorf contamos con las mejores habitaciones de la ciudad ubicadas en una zona de alto interés comercial para aquellos visitantes con altos niveles de exigencia y estándares únicos dentro de la capital del país."}]',
        "user_id" => "1"
      ]);
    }

    public function down()  {
      Schema::drop('text');
    }
}
