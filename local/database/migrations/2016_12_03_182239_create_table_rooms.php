<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Rooms;

class CreateTableRooms extends Migration{

     public function up(){
       Schema::create('rooms', function (Blueprint $table) {
         $table->increments('id');
         $table->string('rooms_thumbnail');
         $table->text('rooms_especificaciones');
         $table->integer('rooms_positions');
         $table->string('rooms_codigo');
         $table->integer('rooms_galeria');
         $table->timestamps();
       });

       Rooms::create([
   			"rooms_thumbnail" => "img/habdefault.jpg",
        "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
   			"rooms_positions" => "1",
        "rooms_codigo" => "default.jpg",
   			"rooms_galeria" => "19"
   		]);

      Rooms::create([
         "rooms_thumbnail" => "img/habdefault.jpg",
         "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
         "rooms_positions" => "2",
         "rooms_codigo" => "default.jpg",
         "rooms_galeria" => "19"
       ]);

       Rooms::create([
     			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
     			"rooms_positions" => "3",
          "rooms_codigo" => "default.jpg",
     			"rooms_galeria" => "19"
     		]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "4",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
    		]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "5",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
    		]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "6",
          "rooms_codigo" => "default.jpg",
  			  "rooms_galeria" => "19"
    		]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "7",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
    		]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "8",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
    		]);


        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "9",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
      	]);

        Rooms::create([
    			"rooms_thumbnail" => "img/habdefault.jpg",
          "rooms_especificaciones" => '[{"cuarto":"0","activo":"0"},{"nombre":"<NOMBRE>","titulo":"<TITULO>","capacidad":"<CAPACIDAD>","in":"<IN>","out":"<OUT>","especificaciones":[],"thumbnails":["galeria1.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria3.jpg","galeria2.jpg"]}]',
    			"rooms_positions" => "10",
          "rooms_codigo" => "default.jpg",
    			"rooms_galeria" => "19"
    		]);
     }

    public function down(){
        Schema::drop('rooms');
    }
}
