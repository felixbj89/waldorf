<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBookingUrl extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::connection("BOOKING")->create('booking_url', function (Blueprint $table) {
      $table->increments('id');
      $table->text('booking_urlDesarrollo');
      $table->text('booking_urlProduccion');
      $table->text('booking_keyDesarrollo');
	  $table->text('booking_keyProduccion');
      $table->enum('booking_status',['0','1']);
	  $table->enum('booking_hotel',['0','1']);
      $table->integer('user_id');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('booking_url');
  }
}
