<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PromocioneS;

class CreateTablePromociones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('promociones', function (Blueprint $table) {
			$table->increments('id');
			$table->string('promociones_title');
			$table->string('promociones_asunto');
			$table->enum('promociones_status',['0','1']);
			$table->enum('promociones_favoritos',['0','1']);
			$table->integer('promociones_costo');
			$table->string('promociones_dateini');
			$table->string('promociones_datefin');
			$table->text('promociones_descripcion');
			$table->text('promociones_path');
			$table->integer('user_id');
			$table->timestamps();
		});
		
		Promociones::create([
			"promociones_title" => "TITLE",
			"promociones_asunto" => "ASUNTO",
			"promociones_costo" => "0",
			"promociones_status" => "0",
			"promociones_favoritos" => "0",
			"promociones_dateini" => "0000-00-00 00:00:00",
			"promociones_datefin" => "0000-00-00 00:00:00",
			"promociones_descripcion" => "DESCRIPCIÓN",
			"promociones_path" => "img/default.jpg",
			"user_id" => "1"
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('picture');
    }
}
