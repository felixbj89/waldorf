<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateTableUser extends Migration{

    public function up(){
        Schema::create('user', function (Blueprint $table) {
          $table->increments('id');
          $table->string('user_name');
          $table->string('user_email')->unique();
          $table->text('user_login');
          $table->string('user_password');
          $table->enum('user_role',['0','1','2','3']);
          $table->enum('user_status',['0','1']);
          $table->text('user_token');
          $table->enum('user_token_status',['0','1']);
          $table->timestamps();
	      });

  		User::create([
  			"user_name" => "waldorf",
  			"user_email" => "haciendacreativa@gmail.com",
  			"user_login" => "wal",
  			"user_password" => hash('ripemd320', "wal"),
  			"user_role" => "0",
  			"user_status" => "1",
  			"user_token" => str_random(255),
  			"user_token_status" => "0"
  		]);
    }

    public function down(){
         Schema::drop('user');
    }
}
