<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Booking;

class CreateTableBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection("BOOKING")->create('booking_user', function (Blueprint $table) {
        $table->increments('id');
        $table->string('user_name');
        $table->string('user_email')->unique();
        $table->text('user_login');
        $table->string('user_password');
        $table->enum('user_role',['0','1','2','3']);
        $table->enum('user_status',['0','1']);
        $table->text('user_token');
        $table->enum('user_token_status',['0','1']);
        $table->timestamps();
      });

      Booking::create([
  			"user_name" => "HCRED",
  			"user_email" => "haciendacreativa@gmail.com",
  			"user_login" => "hcred",
  			"user_password" => hash('ripemd320', "hcred"),
  			"user_role" => "0",
  			"user_status" => "1",
  			"user_token" => str_random(255),
  			"user_token_status" => "0"
  		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_user');
    }
}
