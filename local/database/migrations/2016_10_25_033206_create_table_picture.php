<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Picture;
use Carbon\Carbon;

class CreateTablePicture extends Migration{

  //0 = slider
  /*****************************************/ //superior
  //1 = habitaciones ( banner superior )
  //2 = restaurant ( banner superior )
  //3 = historial superior habitaciones.
  //4 = historial superior restaurante
  /****************************************/ //inferior
  //5 = banner inferior ( habitaciones )
  //6 = banner inferior ( restaurant )
  //7 = historial inferior habitaciones.
  //8 = historial inferior restaurante.
  /****************************************/
  //9 = picture history
  //10 = historial de history
  //11 = Galería en el hotel
  /****************************************/ //contacto
  //12 = picture actual del hotel
  //13 = picture contacto
  //14 = historial contacto
  /***************************************/ //gastronomia
  //15 = picture gastronomia1-4
  /***************************************/ //eventos
  //16 = picture eventos 1-2
  /***************************************/ //slider home
  //17 = picture slider principal del home
  //18 = picture slider del restaurante
  /***************************************/ //habitaciones
  //19 = picture galeria-1 hab-1
  /***************************************/ //slider secundario home
  //20 = picture slider secundario
  //21 = historial picture hotel
  
    public function up(){
      Schema::create('picture', function (Blueprint $table) {
        $table->increments('id');
        $table->string('picture_nombre');
        $table->text('picture_path');
        $table->string('picture_date');
        $table->enum('picture_status',['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21']);
        $table->integer('user_id');
        $table->timestamps();
      });

		//FOTO PRINCIPAL / HABITACIONES & RESTAURANTE 
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "3",
  			"user_id" => "1"
  		]);
		
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "4",
  			"user_id" => "1"
  		]);
		
		//FOTO SECUNDARIA / HABITACIONES & RESTAURANTE 
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "7",
  			"user_id" => "1"
  		]);
		
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "8",
  			"user_id" => "1"
  		]);
		
		//FOTO HISTORY / HOME
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "10",
  			"user_id" => "1"
  		]);
	
		//FOTO CONTACTO / HOME
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => "img/default.jpg",
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "14",
  			"user_id" => "1"
  		]);
		
		//FOTO GASTRONOMIA / HOME
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => '[{"img":"img/default.jpg","tipo":"1"},{"img":"img/default.jpg","tipo":"2"},{"img":"img/default.jpg","tipo":"3"},{"img":"img/default.jpg","tipo":"4"}]',
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "15",
  			"user_id" => "1"
  		]);
		
		//SLIDER / HOME
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => '{"cantidad":"0","posicion":["img/home/slider/default.jpg","img/home/slider/default.jpg","img/home/slider/default.jpg","img/home/slider/default.jpg","img/home/slider/default.jpg","img/home/slider/default.jpg"]}',
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "17",
  			"user_id" => "1"
  		]);
		
		//SLIDER / RESTAURANTE
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => '{"cantidad":"0","posicion":["img/restaurante/galeria/thumbnails/default.jpg","img/restaurante/galeria/thumbnails/default.jpg","img/restaurante/galeria/thumbnails/default.jpg","img/restaurante/galeria/thumbnails/default.jpg","img/restaurante/galeria/thumbnails/default.jpg","img/restaurante/galeria/thumbnails/default.jpg"]}',
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "18",
  			"user_id" => "1"
  		]);
		
		//SLIDER / RESTAURANTE
		Picture::create([
  			"picture_nombre" => "Waldorf Caracas",
			"picture_path" => '{"cantidad":"0","posicion":["img/hotel/galeria/default.jpg","img/hotel/galeria/default.jpg","img/hotel/galeria/default.jpg","img/hotel/galeria/default.jpg","img/hotel/galeria/default.jpg","img/hotel/galeria/default.jpg"]}',
  			"picture_date" => Carbon::now(-4),
  			"picture_status" => "11",
  			"user_id" => "1"
  		]);
		
		/*************************************************/
		Picture::create([
  			"picture_nombre" => "Hotel Waldorf",
			"picture_path" => '[{"img":"img/restaurante/eventos/default.jpg","tipo":"1"},{"img":"img/restaurante/eventos/default.jpg","tipo":"2"}]',
  			"picture_date" => Carbon::now(-8),
  			"picture_status" => "16",
  			"user_id" => "1"
  		]);

		Picture::create([
  			"picture_nombre" => "Hotel Waldorf",
			"picture_path" => '[{"cantidad":"0","tipo":"0"},{"img":"img/default.jpg","tipo":"1"},{"img":"img/default.jpg","tipo":"2"},{"img":"img/default.jpg","tipo":"3"},{"img":"img/default.jpg","tipo":"4"},{"img":"img/default.jpg","tipo":"5"},{"img":"img/default.jpg","tipo":"6"}]',
  			"picture_date" => Carbon::now(-11),
  			"picture_status" => "19",
  			"user_id" => "1"
  		]);

		Picture::create([
  			"picture_nombre" => "Hotel Waldorf",
			"picture_path" => '[{"img":"img/default.jpg","slider":"img/default.jpg","tipo":"1"},{"img":"img/default.jpg","slider":"img/default.jpg","tipo":"2"},{"img":"img/default.jpg","slider":"img/default.jpg","tipo":"3"},{"img":"img/default.jpg","slider":"img/default.jpg","tipo":"4"},{"img":"img/default.jpg","slider":"img/default.jpg","tipo":"5"}]',
  			"picture_date" => Carbon::now(-12),
  			"picture_status" => "20",
  			"user_id" => "1"
  		]);
    }

    public function down()  {
      Schema::drop('picture');
    }
}
