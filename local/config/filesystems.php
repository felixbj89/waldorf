<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app'),
        ],

        'restaurante' => [
            'driver' => 'local',
            'root' => 'img/restaurante/slider',
			      'visibility' => 'public',
        ],

        'thumbnails' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones',
			      'visibility' => 'public',
        ],

        'thumbnails_uno' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/1/thumbnails',
			      'visibility' => 'public',
        ],
		
		'promociones' => [
            'driver' => 'local',
            'root' => 'img/promociones',
			      'visibility' => 'public',
        ],

        'thumbnails_dos' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/2/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_tres' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/3/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_cuatro' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/4/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_cinco' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/5/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_seis' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/6/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_siete' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/7/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_ocho' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/8/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_nueve' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/9/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_diez' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/habitaciones/10/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_slider' => [
            'driver' => 'local',
            'root' => 'img/home/habitaciones/thumbnails',
			      'visibility' => 'public',
        ],

        'thumbnails_bigslider' => [
            'driver' => 'local',
            'root' => 'img/home/habitaciones',
			      'visibility' => 'public',
        ],

        'habitaciones' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/slider',
			      'visibility' => 'public',
        ],

        'superior' => [
            'driver' => 'local',
            'root' => 'img/superior',
			      'visibility' => 'public',
        ],
		
		'inferior' => [
            'driver' => 'local',
            'root' => 'img/inferior',
			      'visibility' => 'public',
        ],

        'reserva_restaurante' => [
            'driver' => 'local',
            'root' => 'img/restaurante/slider_reserva',
			      'visibility' => 'public',
        ],

        'slider_ppal' => [
            'driver' => 'local',
            'root' => 'img/home/slider',
			      'visibility' => 'public',
        ],
		
		'hotelbannersuperior' => [
            'driver' => 'local',
            'root' => 'img/hotel/bannersuperior/',
			      'visibility' => 'public',
        ],
		
		'galeria_hotel' => [
            'driver' => 'local',
            'root' => 'img/hotel/galeria',
			      'visibility' => 'public',
        ],
		
        'slider_rsecu' => [
            'driver' => 'local',
            'root' => 'img/restaurante/galeria/thumbnails',
			      'visibility' => 'public',
        ],

        'restaurante_gastronomia' => [
            'driver' => 'local',
            'root' => 'img/home/restaurate',
			      'visibility' => 'public',
        ],

        'restaurante_eventos' => [
            'driver' => 'local',
            'root' => 'img/restaurante/eventos',
			      'visibility' => 'public',
        ],

        'valores_history' => [
            'driver' => 'local',
            'root' => 'img/home/historia',
			      'visibility' => 'public',
        ],

        'habitacion_reserva' => [
            'driver' => 'local',
            'root' => 'img/habitaciones/slider_reserva',
			'visibility' => 'public',
        ],

        'contactus' => [
            'driver' => 'local',
            'root' => 'img/home/contacto',
			      'visibility' => 'public',
        ],
		
		'historial_contacto' => [
            'driver' => 'local',
            'root' => 'img/contacto',
			      'visibility' => 'public',
        ],

        'ftp' => [
            'driver'   => 'ftp',
            'host'     => 'ftp.example.com',
            'username' => 'your-username',
            'password' => 'your-password',

            // Optional FTP Settings...
            // 'port'     => 21,
            // 'root'     => '',
            // 'passive'  => true,
            // 'ssl'      => true,
            // 'timeout'  => 30,
        ],

        's3' => [
            'driver' => 's3',
            'key'    => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

        'rackspace' => [
            'driver'    => 'rackspace',
            'username'  => 'your-username',
            'key'       => 'your-key',
            'container' => 'your-container',
            'endpoint'  => 'https://identity.api.rackspacecloud.com/v2.0/',
            'region'    => 'IAD',
            'url_type'  => 'publicURL',
        ],

    ],

];
