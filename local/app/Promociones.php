<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promociones extends Model
{
	protected $table = "promociones";

	protected $fillable = [
		"promociones_title",
		"promociones_asunto",
		"promociones_costo",
		"promociones_status",
		"promociones_favorito",
		"promociones_dateini",
		"promociones_datefin",
		"promociones_descripcion",
		"promociones_path",
		"user_id"

	];
	
	protected $hidden = [];
	
	public static function construirpromocion(){
		$activaspromociones = Promociones::promocionesrango();
		$activa = "";
		for($i = 0; $i < count($activaspromociones); $i++){
			if($i==0){
				$activa .="<div data-info='".json_encode($activaspromociones[$i])."'\"data-namepromo=\"".$activaspromociones[$i]["title"]."\" data-imgpromo='".url($activaspromociones[$i]["path"])."' class=\"item active\">".
							"<a id=\"picture\" data-toggle=\"modal\" href=\"#modalPromo\" data-picture='".url($activaspromociones[$i]["path"])."'><img src=\"".url($activaspromociones[$i]["path"])."\" class=\"img-responsive center-block\"></a>".
						"</div>";
			}else{
				$activa .="<div data-info='".json_encode($activaspromociones[$i])."'\"data-namepromo=\"".$activaspromociones[$i]["title"]."\" data-imgpromo='".url($activaspromociones[$i]["path"])."' class=\"item\">".
							"<a id=\"picture\" data-toggle=\"modal\" href=\"#modalPromo\" data-picture='".url($activaspromociones[$i]["path"])."'><img src=\"".url($activaspromociones[$i]["path"])."\" class=\"img-responsive center-block\"></a>".
						"</div>";
			}
		}
		
		return array("promociones" => $activa);
	}
	
	public static function promocionesrango(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromosAll());
		$rangos = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			if($promociones[$i]->promociones_status=="1"){
				$ini = $promociones[$i]->promociones_dateini;
				$date_ini = explode("-",explode(" ",$ini)[0]);
				$hora_ini = explode(":",explode(" ",$ini)[1]);
				$init_ini = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
				$end_ini = Carbon::create($date_ini[0], $date_ini[1], $date_ini[2], $hora_ini[0], $hora_ini[1], $hora_ini[2], 'America/Asuncion');

				$fin = $promociones[$i]->promociones_datefin;
				$date_fin = explode("-",explode(" ",$fin)[0]);
				$hora_fin = explode(":",explode(" ",$fin)[1]);
				$end_fin = Carbon::create($date_fin[0], $date_fin[1], $date_fin[2], $hora_fin[0], $hora_fin[1], $hora_fin[2], 'America/Asuncion');

				if($init_ini->gte($end_ini) && $init_ini->lte($end_fin)){
					$rangos[$j] = [
						"id" => base64_encode ( $promociones[$i]->id ),
						"title" => $promociones[$i]->promociones_title,
						"path" => $promociones[$i]->promociones_path,
						"costo" => $promociones[$i]->promociones_costo,
						"asunto" => $promociones[$i]->promociones_asunto,
						"descripcion" => $promociones[$i]->promociones_descripcion,
						"rango" => ($init_ini->gte($end_ini) && $init_ini->lte($end_fin))
					]; $j++;
				}
			}
		}

		return $rangos;
	}
	
	public static function getPromos($status){
		return Promociones::where("promociones_status",$status)->get()->toJson();
	}
	
	public static function getPromoID($id){
		return Promociones::where("id",$id)->first();
	}
	
	public static function getFavorito(){
		return Promociones::where("promociones_favorito","1")->where("promociones_status","1")->first();
	}
	
	public static function getPromosAll(){
		return Promociones::whereNotIn("promociones_path",["img/default.jpg"])->get();
	}
	
	public static function getPromosAllActive(){
		return Promociones::whereNotIn("promociones_path",["img/default.jpg"])->where("promociones_status","1")->get();
	}
	
	public static function removerpromo($id){
		$finder = Promociones::where("id",$id)->first();
		if(count($finder)==1){
			if($finder->promociones_status=="1")return 2;
			if($finder->promociones_path!="img/default.jpg" && file_exists($finder->promociones_path))unlink($finder->promociones_path);
			$finder->delete();
			return 1;
		}return 0;
	}
	
	public static function changepromo($id){
		$finder = Promociones::where("id",$id)->first();
		if(count($finder)==1){
			if($finder->promociones_status=="1"){
				$finder->update([
					"promociones_status" => "0"
				]);
				
				return 1;
			}else{
				$finder->update([
					"promociones_status" => "1"
				]);
				
				return 1;
			}
		}return 0;
	}
	
	public static function activar($id){
		$finder = Promociones::where("id",$id)->first();
		if(count($finder)==1){
			$finder->update([
				"promociones_status" => "1"
			]);
			
			return 1;
		}return 0;
	}
	
	public static function desactivar($id){
		$finder = Promociones::where("id",$id)->first();
		if(count($finder)==1){
			$finder->update([
				"promociones_status" => "0"
			]);
			
			return 1;
		}return 0;
	}
	
    public static function editpromo($inputs){
		$finder = Promociones::where("id",base64_decode($inputs["promo_id"]))->first();
		if(count($finder)==1){
			if($finder->promociones_favorito=="0" && array_key_exists("favorito",$inputs) && $inputs["favorito"]=="1"){
				return 4;
			}else{
				$date = Carbon::now(-4);
				$titulo = $finder->promociones_title;
				if(array_key_exists("titulopromo",$inputs)){
					$titulo = $inputs["titulopromo"];
				}
				
				$asunto = $finder->promociones_asunto;
				if(array_key_exists("asuntopromo",$inputs)){
					$asunto = $inputs["asuntopromo"];
				}
				
				$costo = $finder->promociones_costo;
				if(array_key_exists("costopromo",$inputs)){
					$costo = $inputs["costopromo"];
				}
			
				$status = "";
				if(array_key_exists("estado",$inputs)){
					$status = $inputs["estado"];
				}else{
					$status = $finder->promociones_status;
				}
				
				$favorito = "";
				if(array_key_exists("favorito",$inputs)){
					$favorito = $inputs["favorito"];
				}else{
					$favorito = $finder->promociones_favorito;
				}
				
				$init = "";
				if(array_key_exists("inicio_promociones",$inputs) && $inputs["inicio_promociones"]!=""){
					$init = $inputs["inicio_promociones"];
				}else{
					$init = $finder->promociones_dateini;
				}
				
				$fin = "";
				if(array_key_exists("fin_promociones",$inputs) && $inputs["fin_promociones"]!=""){
					$fin = $inputs["fin_promociones"];
				}else{
					$fin = $finder->promociones_datefin;
				}
			
				$descripcion = $finder->promociones_descripcion;
				if(array_key_exists("descr",$inputs)){
					$buscar = "\"";
					$reemplazar   = '\\"';
					$inputs["descr"]  = (str_replace($buscar, $reemplazar,$inputs["descr"]));
					$inputs["descr"] = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$inputs["descr"]));
					$descripcion = str_replace("\r\n",'', $inputs["descr"]);
				}
				
				$imagen = $finder->promociones_path;
				if(array_key_exists("file_picture1",$inputs)){
					if($imagen!="img/default.jpg" && file_exists($imagen))unlink($imagen);
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_picture1"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$imagen = str_replace($search, $replace, $name);
					\Storage::disk('promociones')->put($imagen,  \File::get($inputs["file_picture1"]));
					$imagen = ("img/promociones/".$imagen);
				}
			
				$finder->update([
					"promociones_title" => $titulo,
					"promociones_asunto" => $asunto,
					"promociones_costo" => $costo,
					"promociones_status" => $status,
					"promociones_favorito" => $favorito,
					"promociones_dateini" => $init,
					"promociones_datefin" => $fin,
					"promociones_descripcion" => $descripcion,
					"promociones_path" => $imagen,
					"user_id" => "1"
				]);
				
				return 1;
			}
		}return 0;
	}
	
	public static function addpromo($inputs){
		$favorito_finder = Promociones::where("promociones_favorito","1")->whereNotIn("promociones_path",["img/default.jpg"])->get();
		if(count($favorito_finder)==1 && array_key_exists("favorito",$inputs) && $inputs["favorito"]=="1")return 3;
		$finder = Promociones::where("promociones_path","img/default.jpg")->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){
			$titulo = $finder->promociones_title;
			if(array_key_exists("titulopromo",$inputs)){
				$titulo = $inputs["titulopromo"];
			}
			
			$asunto = $finder->promociones_asunto;
			if(array_key_exists("asuntopromo",$inputs)){
				$asunto = $inputs["asuntopromo"];
			}
			
			$costo = $finder->promociones_costo;
			if(array_key_exists("costopromo",$inputs)){
				$costo = $inputs["costopromo"];
			}
			
			$status = "";
			if(array_key_exists("estado",$inputs)){
				$status = $inputs["estado"];
			}else{
				$status = $finder->promociones_status;
			}
			
			$favorito = "";
			if(array_key_exists("favorito",$inputs)){
				$favorito = $inputs["favorito"];
			}else{
				$favorito = $finder->promociones_favorito;
			}
			
			$init = "";
			if(array_key_exists("inicio_promociones",$inputs)){
				$init = $inputs["inicio_promociones"];
			}else{
				$init = $finder->promociones_dateini;
			}
			
			
			$fin = "";
			if(array_key_exists("fin_promociones",$inputs)){
				$fin = $inputs["fin_promociones"];
			}else{
				$fin = $finder->promociones_datefin;
			}
			
			$descripcion = $finder->promociones_descripcion;
			if(array_key_exists("descr",$inputs)){
				$buscar = "\"";
				$reemplazar   = '\\"';
				$inputs["descr"]  = (str_replace($buscar, $reemplazar,$inputs["descr"]));
				$inputs["descr"] = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$inputs["descr"]));
				$descripcion = str_replace("\r\n",'', $inputs["descr"]);
			}
			
			$imagen = $finder->promociones_path;
			if(array_key_exists("file_picture1",$inputs)){
				if($imagen!="img/default.jpg" && file_exists($imagen))unlink($imagen);
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["file_picture1"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$imagen = str_replace($search, $replace, $name);
				\Storage::disk('promociones')->put($imagen,  \File::get($inputs["file_picture1"]));
				$imagen = ("img/promociones/".$imagen);
			}
			
			$finder->update([
				"promociones_title" => $titulo,
				"promociones_asunto" => $asunto,
				"promociones_costo" => $costo,
				"promociones_status" => $status,
				"promociones_favorito" => $favorito,
				"promociones_dateini" => $init,
				"promociones_datefin" => $fin,
				"promociones_descripcion" => $descripcion,
				"promociones_path" => $imagen,
				"user_id" => "1"
			]);
			
			Promociones::create([
				"promociones_title" => "TITLE",
				"promociones_asunto" => "ASUNTO",
				"promociones_costo" => "0",
				"promociones_status" => "0",
				"promociones_favorito" => "0",
				"promociones_dateini" => "0000-00-00 00:00:00",
				"promociones_datefin" => "0000-00-00 00:00:00",
				"promociones_descripcion" => "DESCRIPCIÓN",
				"promociones_path" => "img/default.jpg",
				"user_id" => "1"
			]);
		
			return 1;
		}return 0;
	}
}
