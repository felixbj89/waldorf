<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rooms extends Model{

  protected $table = "rooms";

  protected $fillable = [
      "rooms_thumbnail",
      "rooms_especificaciones",
      "rooms_codigo",
      "rooms_positions",
      "rooms_galeria",
  ];

  public function picture(){
    return $this->hasMany('App\Picture',"picture_status","rooms_galeria");
  }

  public function seerooms($rooms, $requests){
    $encontrado = $rooms->where("rooms_positions", $requests["posicion"])->first();
    if(count($encontrado)==1){
      return $encontrado;
    }

    return NULL;
  }

  public function modifyroomspicture($rooms, $inputs){
    if($inputs["picture_galeria"][0]==null && !array_key_exists("picture_rooms",$inputs)){
      return 2;
    }else{
      $encontrado = $rooms->where("rooms_positions", $inputs["selectcuartospicture"])->first();
      $listGaleria = "";
      if(count($encontrado)==1){
        $json = json_decode($encontrado->rooms_especificaciones);
        if($inputs["picture_galeria"][0]!=null){//SI AGREGO IMAGEN AL SLIDER BORRO
          if(count($inputs["picture_galeria"])==6){
            foreach($json[1]->thumbnails as $picture){
              if($picture!="galeria1.jpg" || $picture!="galeria3.jpg"
              || $picture!="galeria3.jpg" || $picture!="galeria3.jpg"
              || $picture!="galeria3.jpg" || $picture!="galeria2.jpg"){
                if(file_exists($picture)){
                  unlink($picture);
                }
              }
            }

            $size = count($inputs["picture_galeria"]);
            $dt = Carbon::now(-4);
            $thumbnails = "";

            for($i = 0; $i < $size; $i++){
              $namePicture = $dt->year.$dt->month.$dt->day.$dt->hour.$dt->minute.$dt->second.$inputs["picture_galeria"][$i]->getClientOriginalName();
        			$search  = array(' ', '(', ')','ñ');
        			$replace = array('', '', '','n');
        			$namePicture = str_replace($search, $replace, $namePicture);
              if($inputs["selectcuartospicture"]=="1")
                \Storage::disk('thumbnails_uno')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="2")
                \Storage::disk('thumbnails_dos')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="3")
                \Storage::disk('thumbnails_tres')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="4")
                \Storage::disk('thumbnails_cuatro')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="5")
                \Storage::disk('thumbnails_cinco')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="6")
                \Storage::disk('thumbnails_seis')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="7")
                \Storage::disk('thumbnails_siete')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="8")
                \Storage::disk('thumbnails_ocho')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="9")
                \Storage::disk('thumbnails_nueve')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              else if($inputs["selectcuartospicture"]=="10")
                \Storage::disk('thumbnails_diez')->put($namePicture,  \File::get($inputs["picture_galeria"][$i]));
              $thumbnails .= "\"img/habitaciones/habitaciones/".$inputs["selectcuartospicture"]."/thumbnails/".$namePicture."\",";
            }

            $listGaleria = substr($thumbnails,0,strlen($thumbnails)-1);
          }
        }else{//no estoy agregando sliders a la modificación
          $galeria = "";
          $size = count($json[1]->thumbnails);
          for($i = 0; $i < $size; $i++){
            $galeria .= "\"".$json[1]->thumbnails[$i]."\",";
          }

          $listGaleria = substr($galeria,0,strlen($galeria)-1);
        }

        $propiedades = "";//PROPIEDADES DE LAS HABITACIONES
        $size = count($json[1]->especificaciones);
        for($i = 0; $i < $size; $i++){
          $propiedades .= "\"".$json[1]->especificaciones[$i]."\",";
        }

        $listaPropiedades = substr($propiedades,0,strlen($propiedades)-1);
        $default = '[{"cuarto":"'.$json[0]->cuarto.'","activo":"'.$json[0]->activo.'"},{"nombre":"'.$json[1]->nombre.'","titulo":"'.$json[1]->titulo.'","capacidad":"'.$json[1]->capacidad.'","in":"'.$json[1]->in.'","out":"'.$json[1]->out.'","especificaciones":['.$listaPropiedades.'],"thumbnails":['.$listGaleria.']}]';

        //NOMBRE DEL THUMBNAIL
        $thumb = "";
        if(array_key_exists("picture_rooms",$inputs)){
          if($encontrado->rooms_thumbnail!="img/habdefault.jpg" && file_exists($encontrado->rooms_thumbnail)){
            unlink($encontrado->rooms_thumbnail);
          }

          $dt = Carbon::now(-4);
          $thumb = $dt->year.$dt->month.$dt->day.$dt->hour.$dt->minute.$dt->second.$inputs["picture_rooms"]->getClientOriginalName();
          $search  = array(' ', '(', ')','ñ');
          $replace = array('', '', '','n');
          $thumb = str_replace($search, $replace, $thumb);
          $aux = "img/habitaciones/habitaciones/".$thumb;
          \Storage::disk('thumbnails')->put($thumb,  \File::get($inputs["picture_rooms"]));
        }else{
          $aux = $encontrado->rooms_thumbnail;
        }

        $rooms->where("rooms_positions",$inputs["selectcuartospicture"])->update([
          "rooms_thumbnail" => $aux,
          "rooms_especificaciones" => $default,
          "rooms_positions" => $inputs["selectcuartospicture"]
        ]);

        return 1;
      }return 0;
    }
  }

  public function modifyrooms($rooms, $default, $posicion){
    $encontrado = $rooms->where("rooms_positions", $posicion)->first();
    if(count($encontrado)==1){
      $encontrado->update([
        "rooms_especificaciones" => $default
      ]);

      return 1;
    }

    return 0;
  }

	public static function storerooms($requests){
		if(array_key_exists("imagen_rooms1",$requests) && array_key_exists("imagen_rooms2",$requests) && array_key_exists("imagen_rooms3",$requests)
		&& array_key_exists("imagen_rooms4",$requests) && array_key_exists("imagen_rooms5",$requests) && array_key_exists("imagen_rooms6",$requests)){
			if(array_key_exists("imagen_portada",$requests)){
				$cuarto = Rooms::where("rooms_positions",$requests["posicion"])->where("rooms_codigo","default.jpg")->first();//POSICIÓN VACIA
				if(count($cuarto)==1){//UBICO LA POSICIÓN A INSERTAR
					$name = ""; $dt = Carbon::now(-4);
					$portada = $dt->year.$dt->month.$dt->day.$dt->hour.$dt->minute.$dt->second.$requests["imagen_portada"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$portada = (str_replace($search, $replace, $portada));
					//ELIMINO UNA IMAGEN DE PORTADA VIEJA
					if($cuarto->rooms_thumbnail!="img/habdefault.jpg" && file_exists($cuarto->rooms_thumbnail)) unlink($cuarto->rooms_thumbnail);
					\Storage::disk('thumbnails')->put($portada,  \File::get($requests["imagen_portada"]));
					$especificaciones = json_decode($cuarto->rooms_especificaciones);
					for($j = 0; $j < count($especificaciones[1]->thumbnails); $j++){
						if($especificaciones[1]->thumbnails[$j]!="galeria1.jpg" && $especificaciones[1]->thumbnails[$j]!="galeria2.jpg" 
						&& $especificaciones[1]->thumbnails[$j]!="galeria3.jpg" && file_exists($especificaciones[1]->thumbnails[$j])){
							unlink($especificaciones[1]->thumbnails[$j]);
						}
					}
					
					$dat = Carbon::now(-4);
					
					//1
					$rooms1 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms1"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms1 = (str_replace($search, $replace, $rooms1));
				
					//2
					$rooms2 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms2"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms2 = (str_replace($search, $replace, $rooms2));
					
					//3
					$rooms3 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms3"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms3 = (str_replace($search, $replace, $rooms3));
					
					//4
					$rooms4 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms4"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms4 = (str_replace($search, $replace, $rooms4));
					
					//5
					$rooms5 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms5"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms5 = (str_replace($search, $replace, $rooms5));
					
					//6
					$rooms6 = $dat->year.$dat->month.$dat->day.$dat->hour.$dat->minute.$dat->second.$requests["imagen_rooms6"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$rooms6 = (str_replace($search, $replace, $rooms6));
					
					
					if($requests["posicion"]=="1"){
						\Storage::disk('thumbnails_uno')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_uno')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_uno')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_uno')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_uno')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_uno')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="2"){
						\Storage::disk('thumbnails_dos')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_dos')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_dos')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_dos')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_dos')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_dos')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="3"){
						\Storage::disk('thumbnails_tres')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_tres')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_tres')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_tres')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_tres')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_tres')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="4"){
						\Storage::disk('thumbnails_cuatro')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_cuatro')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_cuatro')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_cuatro')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_cuatro')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_cuatro')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="5"){
						\Storage::disk('thumbnails_cinco')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_cinco')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_cinco')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_cinco')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_cinco')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_cinco')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="6"){
						\Storage::disk('thumbnails_seis')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_seis')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_seis')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_seis')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_seis')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_seis')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="7"){
						\Storage::disk('thumbnails_siete')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_siete')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_siete')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_siete')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_siete')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_siete')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="8"){
						\Storage::disk('thumbnails_ocho')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_ocho')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_ocho')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_ocho')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_ocho')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_ocho')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="9"){
						\Storage::disk('thumbnails_nueve')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_nueve')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_nueve')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_nueve')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_nueve')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_nueve')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}else if($requests["posicion"]=="10"){
						\Storage::disk('thumbnails_diez')->put($rooms1,  \File::get($requests["imagen_rooms1"]));
						\Storage::disk('thumbnails_diez')->put($rooms2,  \File::get($requests["imagen_rooms2"]));
						\Storage::disk('thumbnails_diez')->put($rooms3,  \File::get($requests["imagen_rooms3"]));
						\Storage::disk('thumbnails_diez')->put($rooms4,  \File::get($requests["imagen_rooms4"]));
						\Storage::disk('thumbnails_diez')->put($rooms5,  \File::get($requests["imagen_rooms5"]));
						\Storage::disk('thumbnails_diez')->put($rooms6,  \File::get($requests["imagen_rooms6"]));
					}
					
					$lista = substr($requests["propilist"],0,strlen($requests["propilist"])-1);//LISTA DE PROPIEDADES
					$codigo = substr($portada,0,strlen($portada)-4);//CODIGO DE LA HABITACIÓN
					$in = $requests["checkin"]; $out = $requests["checkout"];
					if($requests["checkin"]=="12:00pm"){
						$in = substr($requests["checkin"],0,strlen($requests["checkin"])-2)."M";
					}else if($requests["checkout"]=="12:00pm"){
						$out = substr($requests["checkout"],0,strlen($requests["checkout"])-2)."M";
					}
					
					$default = '[{"cuarto":"1","activo":"'.$requests["estado"].'"},{"nombre":"'.$codigo.'","titulo":"'.$requests["titulohistory"].'","capacidad":"'.$requests["capcidadmax"].'","in":"'.$in.'","out":"'.$out.'","especificaciones":['.$lista.'],"thumbnails":["'.$rooms1.'","'.$rooms2.'","'.$rooms3.'","'.$rooms4.'","'.$rooms5.'","'.$rooms6.'"]}]';
					$cuarto->where("rooms_positions",$requests["posicion"])->update([
						"rooms_thumbnail" => "img/habitaciones/habitaciones/".$portada,
						"rooms_especificaciones" => $default,
						"rooms_codigo"  => $codigo,
						"rooms_positions" => $requests["posicion"],
						"rooms_galeria" => "19"
					]);
					
					return 1;
				}return 4;
			}return 3;
		}return 2;
	}
}
