<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Session;
use Storage;

class Picture extends Model{

  protected $table = "picture";

  protected $fillable = [
      "picture_nombre",
      "picture_date",
      "picture_status",
      "picture_path",
      "user_id"

  ];

	public function user(){
		return $this->hasMany('App\User',"id","user_id");
	}

	public function rooms(){
		return $this->belongsTo('App\Rooms');
	}

  /*public function storesliderslhome($picture, $nthumbnail, $asociados,$editando,$posicion){
    $encontrado = $picture->where("picture_status","20")->first();
    if(count($encontrado)==1){
		$json = json_decode($encontrado->picture_path);
		$posicion = json_decode($posicion);
		
		$default = "[";
		$i = 0;
	  
		if(count($asociados) == 5){
			for($i = 0, $j = 0; $i < count($json); $i++){
				$slisave = substr($json[$i]->slider,strripos($json[$i]->slider,"/")+1,strlen($json[$i]->slider));
				if($slisave!="default.jpg" && file_exists($json[$i]->slider)){
					unlink($json[$i]->slider);
					$j++;
				}
			}	
		}
		
		$date = Carbon::now(-4);
		for($i = 0; $i < count($json); $i++){
			if($i==$posicion[0]->posicion){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$asociados[0]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$name = str_replace($search, $replace, $name);
				\Storage::disk('thumbnails_bigslider')->put($name,  \File::get($asociados[0]));
				$default.= '{"img":"'.$json[$i]->img.'","slider":"img/home/habitaciones/'.$name.'","tipo":"'.($posicion[0]->posicion+1).'"},';
			}else{
				$default.= '{"img":"'.$json[$i]->img.'","slider":"'.$json[$i]->slider.'","tipo":"'.$json[$i]->tipo.'"},';
			}
		}
	
		Picture::where("id",$encontrado->id)->update([
			"picture_nombre" => $nthumbnail,
			"picture_path" => substr($default,0,strlen($default)-1)."]"
		]);
		
		return 1;
    }return 0;
  }

  public function storesliderthumbhome($picture, $nthumbnail, $thumbnails, $newthumbnails, $editando){
	$encontrado = $picture->where("picture_status","20")->first();
    if(count($encontrado)==1){
      $json = json_decode($encontrado->picture_path);
      $default = "[";
      $i = 0;
      foreach($json as $ppicture){
        $thumbsave = substr($ppicture->img,strripos($ppicture->img,"/")+1,strlen($ppicture->img));
        if($thumbsave!=$thumbnails[$i]){//ELIMINO EL THUMB QUE SEA DIFERENTE
          if($thumbsave!="default.jpg" && file_exists($ppicture->img)){
            unlink($ppicture->img);
          }
        }$i++;
      }
	  
      $date = Carbon::now(-4); $i = 0;
      foreach($json as $ppicture){
        $name = "";
        $thumbsave = substr($ppicture->img,strripos($ppicture->img,"/")+1,strlen($ppicture->img));
        if($thumbsave!=$thumbnails[$i]){
          $name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$thumbnails[$i];
          $search  = array(' ', '(', ')','ñ');
          $replace = array('', '', '','n');
          $name = str_replace($search, $replace, $name);
          \Storage::disk('thumbnails_slider')->put($name,  \File::get($newthumbnails[$i]));
          $name = "img/home/habitaciones/thumbnails/".str_replace($search, $replace, $name);
        }else{
          $name = $ppicture->img;
        }

        $default.= '{"img":"'.$name.'","slider":"'.$ppicture->slider.'","tipo":"'.($i+1).'"},';
        $i++;
      }

      Picture::where("id",$encontrado->id)->update([
        "picture_nombre" => $nthumbnail,
        "picture_path" => substr($default,0,strlen($default)-1)."]"
      ]);

      return 1;
    }return 0;
  }*/

	public static function storesliderhome($inputs){
		$encontrado = Picture::where("picture_status","20")->first();
		if(count($encontrado)==1){
			$json = json_decode($encontrado->picture_path);
			if(!array_key_exists("fileasociado1",$inputs) && !array_key_exists("filethumb1",$inputs) && !array_key_exists("fileasociado2",$inputs)
			&& !array_key_exists("filethumb2",$inputs) && !array_key_exists("fileasociado3",$inputs) && !array_key_exists("filethumb3",$inputs)
			&& !array_key_exists("fileasociado4",$inputs) && !array_key_exists("filethumb4",$inputs) && !array_key_exists("fileasociado5",$inputs)
			&& !array_key_exists("filethumb5",$inputs)){
				return 2;
			}
						
			if($json!=NULL){
				$i = 0;
				$default = "[";
				$date = Carbon::now(-4);
				
				$unothumb = ""; $unoslider = "";
				$dosthumb = ""; $dosslider = "";
				$tresthumb = ""; $tresslider = "";
				$cuatrothumb = ""; $cuatroslider = "";
				$cincothumb = ""; $cincoslider = "";
				if(array_key_exists("fileasociado1",$inputs)){
					if($json[0]->slider!="img/default.jpg" && file_exists($json[0]->slider)){
						unlink($json[0]->slider);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["fileasociado1"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$unoslider = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_bigslider')->put($unoslider,  \File::get($inputs["fileasociado1"]));
					$unoslider = ("img/home/habitaciones/".$unoslider);
				}else{
					$unoslider = $json[0]->slider;
				}
				
				if(array_key_exists("filethumb1",$inputs)){
					if($json[0]->img!="img/default.jpg" && file_exists($json[0]->img)){
						unlink($json[0]->img);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["filethumb1"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$unothumb = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_slider')->put($unothumb,  \File::get($inputs["filethumb1"]));
					$unothumb = ("img/home/habitaciones/thumbnails/".$unothumb);
				}else{
					$unothumb = $json[0]->img;
				}
				
				if(array_key_exists("fileasociado2",$inputs)){
					if($json[1]->slider!="img/default.jpg" && file_exists($json[1]->slider)){
						unlink($json[1]->slider);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["fileasociado2"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$dosslider = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_bigslider')->put($dosslider,  \File::get($inputs["fileasociado2"]));
					$dosslider = ("img/home/habitaciones/".$dosslider);
				}else{
					$dosslider = $json[1]->slider;
				}
				
				if(array_key_exists("filethumb2",$inputs)){
					if($json[1]->img!="img/default.jpg" && file_exists($json[1]->img)){
						unlink($json[1]->img);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["filethumb2"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$dosthumb = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_slider')->put($dosthumb,  \File::get($inputs["filethumb2"]));
					$dosthumb = ("img/home/habitaciones/thumbnails/".$dosthumb);
				}else{
					$dosthumb = $json[1]->img;
				}
				
				if(array_key_exists("fileasociado3",$inputs)){
					if($json[2]->slider!="img/default.jpg" && file_exists($json[2]->slider)){
						unlink($json[2]->slider);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["fileasociado3"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$tresslider = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_bigslider')->put($tresslider,  \File::get($inputs["fileasociado3"]));
					$tresslider = ("img/home/habitaciones/".$tresslider);
				}else{
					$tresslider = $json[2]->slider;
				}
				
				if(array_key_exists("filethumb3",$inputs)){
					if($json[2]->img!="img/default.jpg" && file_exists($json[2]->img)){
						unlink($json[2]->img);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["filethumb3"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$tresthumb = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_slider')->put($tresthumb,  \File::get($inputs["filethumb3"]));
					$tresthumb = ("img/home/habitaciones/thumbnails/".$tresthumb);
				}else{
					$tresthumb = $json[2]->img;
				}
				
				if(array_key_exists("fileasociado4",$inputs)){
					if($json[3]->slider!="img/default.jpg" && file_exists($json[3]->slider)){
						unlink($json[3]->slider);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["fileasociado4"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$cuatroslider = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_bigslider')->put($cuatroslider,  \File::get($inputs["fileasociado4"]));
					$cuatroslider = ("img/home/habitaciones/".$cuatroslider);
				}else{
					$cuatroslider = $json[3]->slider;
				}
				
				if(array_key_exists("filethumb4",$inputs)){
					if($json[3]->img!="img/default.jpg" && file_exists($json[3]->img)){
						unlink($json[3]->img);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["filethumb4"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$cuatrothumb = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_slider')->put($cuatrothumb,  \File::get($inputs["filethumb4"]));
					$cuatrothumb = ("img/home/habitaciones/thumbnails/".$cuatrothumb);
				}else{
					$cuatrothumb = $json[3]->img;
				}
				
				if(array_key_exists("fileasociado5",$inputs)){
					if($json[4]->slider!="img/default.jpg" && file_exists($json[4]->slider)){
						unlink($json[4]->slider);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["fileasociado5"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$cincoslider = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_bigslider')->put($cincoslider,  \File::get($inputs["fileasociado5"]));
					$cincoslider = ("img/home/habitaciones/".$cincoslider);
				}else{
					$cincoslider = $json[4]->slider;
				}
				
				if(array_key_exists("filethumb5",$inputs)){
					if($json[4]->img!="img/default.jpg" && file_exists($json[4]->img)){
						unlink($json[4]->img);
					}
					
					$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["filethumb5"]->getClientOriginalName();
					$search  = array(' ', '(', ')','ñ');
					$replace = array('', '', '','n');
					$cincothumb = str_replace($search, $replace, $name);
					\Storage::disk('thumbnails_slider')->put($cincothumb,  \File::get($inputs["filethumb5"]));
					$cincothumb = ("img/home/habitaciones/thumbnails/".$cincothumb);
				}else{
					$cincothumb = $json[4]->img;
				}
																								
				if(array_key_exists("filethumb1",$inputs) && array_key_exists("fileasociado1",$inputs)){
					$default.= '{"img":"'.$unothumb.'","slider":"'.$unoslider.'","tipo":"1"},';
				}else{
					$default.= '{"img":"'.$unothumb.'","slider":"'.$unoslider.'","tipo":"1"},';
				}
				
				if(array_key_exists("filethumb2",$inputs) && array_key_exists("fileasociado2",$inputs)){
					$default.= '{"img":"'.$dosthumb.'","slider":"'.$dosslider.'","tipo":"2"},';
				}else{
					$default.= '{"img":"'.$dosthumb.'","slider":"'.$dosslider.'","tipo":"2"},';
				}
				
				if(array_key_exists("filethumb3",$inputs) && array_key_exists("fileasociado3",$inputs)){
					$default.= '{"img":"'.$tresthumb.'","slider":"'.$tresslider.'","tipo":"3"},';
				}else{
					$default.= '{"img":"'.$tresthumb.'","slider":"'.$tresslider.'","tipo":"3"},';
				}
				
				if(array_key_exists("filethumb4",$inputs) && array_key_exists("fileasociado4",$inputs)){
					$default.= '{"img":"'.$cuatrothumb.'","slider":"'.$cuatroslider.'","tipo":"4"},';
				}else{
					$default.= '{"img":"'.$cuatrothumb.'","slider":"'.$cuatroslider.'","tipo":"4"},';
				}
				
				if(array_key_exists("filethumb5",$inputs) && array_key_exists("fileasociado5",$inputs)){
					$default.= '{"img":"'.$cincothumb.'","slider":"'.$cincoslider.'","tipo":"5"},';
				}else{
					$default.= '{"img":"'.$cincothumb.'","slider":"'.$cincoslider.'","tipo":"5"},';
				}
				
				Picture::where("id",$encontrado->id)->update([
					"picture_nombre" => "Waldorf Caracas",
					"picture_path" => substr($default,0,strlen($default)-1)."]"
				]);
				
				return 1;
			}
		}return 0;
	}

  /*public function removerPicture($estado, $date, $picture){
    if($estado==1){//se esta removiendo los banners superiores que pertenezcan al historial
      if(file_exists($picture->where("picture_date",$date)->where("picture_status","3")->value("picture_path"))){
        unlink($picture->where("picture_date",$date)->where("picture_status","3")->value("picture_path"));
        $picture->where("picture_date",$date)->where("picture_status","3")->delete();
        return 1;
      }return 0;
    }else if($estado==2){
      if(file_exists($picture->where("picture_date",$date)->where("picture_status","7")->value("picture_path"))){
        unlink($picture->where("picture_date",$date)->where("picture_status","7")->value("picture_path"));
        $picture->where("picture_date",$date)->where("picture_status","7")->delete();
        return 1;
      }return 0;
    }else if($estado==3){//elimino una foto del historial de history
      if(file_exists($picture->where("picture_date",$date)->where("picture_status","9")->value("picture_path"))){
        unlink($picture->where("picture_date",$date)->where("picture_status","9")->value("picture_path"));
        $picture->where("picture_date",$date)->where("picture_status","9")->delete();
        return 1;
      }return 0;
    }else if($estado==5){//elimino una foto del historial de contactus
      if(file_exists($picture->where("picture_date",$date)->where("picture_status","14")->value("picture_path"))){
        unlink($picture->where("picture_date",$date)->where("picture_status","14")->value("picture_path"));
        $picture->where("picture_date",$date)->where("picture_status","14")->delete();
        return 1;
      }return 0;
    }return 0;
  }*

  public function search($picture,$estado){
    $encontrado = $picture->where("picture_status","=",$estado)->first();
    return (count($encontrado)==1?1:0);
  }

  public function offactivo($picture,$id,$estado){//MANDO AL HISTORIAL - SUPERIOR.. (DEBE HABER UNA IMAGEN ACTIVA)
    $copy = $picture->where("id",$id)->value("picture_path");
    $ruta = "";
    if($estado==3)//SUPERIOR
      $ruta = "img/superior/";
    else if($estado==9)//HISTORY
      $ruta = "img/history/";
    else if($estado==14)//CONTACTO
      $ruta = "img/contacto/";
    else//INFERIOR
      $ruta = "img/inferior/";

    $newlocation =  $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
    if(file_exists($copy)){
      if(copy($copy,$newlocation)){
        unlink($copy);
        Picture::where("id",$id)->update([
          "picture_status" => $estado,
          "picture_path" => $newlocation
        ]);

        return 1;
      }
    }return 0;
  }

  public function updatePicture($picture,$estado){
    $encontrado = $picture->where("picture_status","=",$estado)->first();
    if(count($encontrado)==1 && ( $estado==5 || $estado==6 ) ){//INFERIOR
      $copy = $encontrado->picture_path;
      $newlocation = 'img/inferior/'.substr($copy,strripos($copy,"/")+1,strlen($copy));
      if(file_exists($copy)){
        if(copy($copy,$newlocation)){
          unlink($copy);
          $encontrado->update([
              "picture_status" => "7",
              "picture_path" => $newlocation
          ]);

          return 1;
        }
      }return 0;
    }else if(count($encontrado)==1 && ( $estado==1 || $estado==2 ) ){//SUPERIOR
      $copy = $encontrado->picture_path;
      $newlocation = 'img/superior/'.substr($copy,strripos($copy,"/")+1,strlen($copy));
      if(file_exists($copy)){
        if(copy($copy,$newlocation)){
          unlink($copy);
          $encontrado->update([
              "picture_status" => "3",
              "picture_path" => $newlocation
          ]);

          return 1;
        }
      }return 0;
    }else if(count($encontrado)==1 && ( $estado==13 ) ){//CONTACTO
      $copy = $encontrado->picture_path;
      $newlocation = 'img/contacto/'.substr($copy,strripos($copy,"/")+1,strlen($copy));
      if(file_exists($copy)){
        if(copy($copy,$newlocation)){
          unlink($copy);
          $encontrado->update([
              "picture_status" => "14",
              "picture_path" => $newlocation
          ]);

          return 1;
        }
      }
    }return 0;
  }*/
	
	//ALMACENO A GASTRONOMIA
	public function storegastronomia($picture,$pic1, $pic2, $pic3, $pic4, $estado,$editando){
		$idAux = $picture->where("picture_status",$estado)->value("id");
		if(count($idAux)==1 && $estado=="15"){//SE ENCONTRO UNA IMAGEN YA EN EL SISTEMA.
			$gastronomia = json_decode($picture->where("id",$idAux)->value("picture_path"));
			$default = "["; $historial1 = ""; $historial2 = ""; $historial3 = ""; $historial4 = ""; $date = Carbon::now(-4);
			$confirmar = 0;
			foreach($gastronomia as $gas){//COMPRUEBO SI EXISTE YA UNA IMAGEN 1-4
				if($gas->tipo=="1" && $pic1!=NULL){//EXISTE IMAGEN TIPO 1..
					$historial1 = "\"".$gas->img."\"";
				}else if($gas->tipo=="2" && $pic2!=NULL){//EXISTE IMAGEN TIPO 2..
					$historial2 = "\"".$gas->img."\"";
				}else if($gas->tipo=="3" && $pic3!=NULL){//EXISTE IMAGEN TIPO 3..
					$historial3 = "\"".$gas->img."\"";
				}else if($gas->tipo=="4" && $pic4!=NULL){//EXISTE IMAGEN TIPO 4..
					$historial4 = "\"".$gas->img."\"";
				}else{
					if($gas->img=="img/default.jpg"  && $pic2==NULL && $pic3==NULL && $pic4==NULL && $pic1!=NULL && $gas->tipo!="1"){//EXISTE IMG 1
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else if($gas->img=="img/default.jpg" && $pic1==NULL && $pic3==NULL && $pic4==NULL && $pic2!=NULL && $gas->tipo!="2"){//EXISTE IMG 2
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else if($gas->img=="img/default.jpg" && $pic1==NULL && $pic2==NULL && $pic4==NULL && $pic3!=NULL && $gas->tipo!="3"){//EXISTE IMG 3
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else if($gas->img=="img/default.jpg" && $pic1==NULL && $pic2==NULL && $pic3==NULL && $pic4!=NULL && $gas->tipo!="4"){//EXISTE IMG 4
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else if($pic1==NULL && $pic2==NULL && $pic3==NULL && $pic4==NULL){//SI VOY A EDITAR PERO NO AGREGO UNA NUEVA IMAGEN
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
						$confirmar = 1;
					}else{
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}
				}
			}

			if(!$confirmar){
				if($pic4!=NULL && $pic3!=NULL && $pic2!=NULL && $pic1!=NULL){//1-2-3-4 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';

					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));
				}else if($pic1!=NULL && $pic2!=NULL && $pic3==NULL && $pic4==NULL){//1-2 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';
				}else if($pic1!=NULL && $pic3!=NULL && $pic2==NULL && $pic4==NULL){//1-3 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';
				}else if($pic1!=NULL && $pic4!=NULL && $pic2==NULL && $pic3==NULL){//1-4 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';
				}else if($pic1!=NULL && $pic2!=NULL && $pic3!=NULL && $pic4==NULL){//1-2-3 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';

					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));
				}else if($pic1!=NULL && $pic2!=NULL && $pic3==NULL && $pic4!=NULL){//1-2-4 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';

					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));
				}else if($pic1!=NULL && $pic3!=NULL && $pic4!=NULL && $pic2==NULL){//1-3-4 EXISTEN
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen1.'","tipo":"1"}]';

					\Storage::disk('restaurante_gastronomia')->put($imagen1,  \File::get($pic1));
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));
				}else if($pic1==NULL && $pic3!=NULL && $pic4!=NULL && $pic2!=NULL){//2-3-4 EXISTEN
					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"}]';

					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));
				}else if($pic1==NULL && $pic3!=NULL && $pic4==NULL && $pic2!=NULL){//2-3 EXISTEN
					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"}]';
				}else if($pic1==NULL && $pic3==NULL && $pic4!=NULL && $pic2!=NULL){//2-4 EXISTEN
					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen2,  \File::get($pic2));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen2.'","tipo":"2"}]';
				}else if($pic1==NULL && $pic3!=NULL && $pic4!=NULL && $pic2==NULL){//3-4 EXISTEN
					$imagen3 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
					$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen3,  \File::get($pic3));

					$imagen4 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
					$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
					if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					\Storage::disk('restaurante_gastronomia')->put($imagen4,  \File::get($pic4));

					$default .='{"img":"img/home/restaurate/'.$imagen4.'","tipo":"4"},';
					$default .='{"img":"img/home/restaurate/'.$imagen3.'","tipo":"3"}]';
				}else{
					$size = 0;
					if($pic1!=NULL){//IMG #1
						$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
						$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
						if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$search  = array(' ', '(', ')','ñ');
						$replace = array('', '', '','n');
						$name = str_replace($search, $replace, $name);
						$default .='{"img":"img/home/restaurate/'.$name.'","tipo":"1"}]';
						\Storage::disk('restaurante_gastronomia')->put($name,  \File::get($pic1));
					}else if($pic2!=NULL){//IMG #2
						$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
						$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
						if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$search  = array(' ', '(', ')','ñ');
						$replace = array('', '', '','n');
						$name = str_replace($search, $replace, $name);
						$default .='{"img":"img/home/restaurate/'.$name.'","tipo":"2"}]';
						\Storage::disk('restaurante_gastronomia')->put($name,  \File::get($pic2));
					}else if($pic3!=NULL){//IMG #3
						$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic3->getClientOriginalName();
						$eliminar = substr($historial3,1,strripos($historial3,"\"")-1);
						if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$search  = array(' ', '(', ')','ñ');
						$replace = array('', '', '','n');
						$name = str_replace($search, $replace, $name);
						$default .='{"img":"img/home/restaurate/'.$name.'","tipo":"3"}]';
						$picture->picture_path = $default;
						\Storage::disk('restaurante_gastronomia')->put($name,  \File::get($pic3));
					}else if($pic4!=NULL){//IMG #4
						$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic4->getClientOriginalName();
						$eliminar = substr($historial4,1,strripos($historial4,"\"")-1);
						if($eliminar!="img/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$search  = array(' ', '(', ')','ñ');
						$replace = array('', '', '','n');
						$name = str_replace($search, $replace, $name);
						$default .='{"img":"img/home/restaurate/'.$name.'","tipo":"4"}]';
						$picture->picture_path = $default;
						\Storage::disk('restaurante_gastronomia')->put($name,  \File::get($pic4));
					}
				}
			}else{
				$auxiliar = substr($default,0,strripos($default,","))."]";
				$default = $auxiliar;
			}

			$picture->where("id",$idAux)->update([
				"picture_nombre" => "Hotel Waldorf",
				"picture_date" => $date,
				"picture_status" => $estado,
				"user_id" => Session::get("usuario")->id,
				"picture_path" => $default
			]); return 1;
		}return 0;
	}
	
	//ALMACENO A HISTORY
	public function storeother($picture,$estado,$pic){//STORE PARA CUANDO ES TEXTO E IMAGEN ( MAX 1 )
		$idAux = $picture->where("picture_status",$estado)->value("id");
		if(count($idAux)==1){//SE ENCONTRO UNA IMAGEN YA EN EL SISTEMA.
			$copy = $picture->where("id",$idAux)->value("picture_path");
			$newlocation = 'img/history/'.substr($copy,strripos($copy,"/")+1,strlen($copy));
			if($copy!="img/default.jpg" && file_exists($copy)){
				if(copy($copy,$newlocation)){
					unlink($copy);
					$picture->where("id",$idAux)->update([
						"picture_status" => "10",
						"picture_path" => $newlocation
					]);
				}
			}
			
			$picture->picture_nombre = "Waldorf Caracas";
			$picture->picture_status = $estado;
			$picture->picture_date = Carbon::now(-4);
			$name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			$picture->picture_path = "img/home/historia/".$name;
			\Storage::disk('valores_history')->put($name,  \File::get($pic));

			$picture->user_id = Session::get("usuario")->id;
			$picture->save();
		}else{
			$picture->picture_nombre = $pic->getClientOriginalName();
			$picture->picture_status = $estado;
			$picture->picture_date = Carbon::now(-4);
			$name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			$picture->picture_path = "img/home/historia/".$name;
			\Storage::disk('valores_history')->put($name,  \File::get($pic));

			$picture->user_id = Session::get("usuario")->id;
			$picture->save();
		}
		
		return 1;
	}

  /*public function store($picture, $inputs,$estado,$pic){
	$picture->picture_nombre = $inputs["pictureName"]==""?$pic->getClientOriginalName():$inputs["pictureName"];
    $picture->picture_date = Carbon::now(-4);
    $picture->picture_status = $estado;
    if($estado==1){//SUPERIOR
      $name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
      $picture->picture_path = "img/habitaciones/slider/".$name;
      \Storage::disk('habitaciones')->put($name,  \File::get($pic));
    }else if($estado==5){//INFERIOR
      $name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
      $picture->picture_path = "img/habitaciones/slider_reserva/".$name;
      \Storage::disk('habitacion_reserva')->put($name,  \File::get($pic));
    }else if($estado==6){//INFERIOR
      $name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
		$search  = array(' ', '(', ')','ñ');
		$replace = array('', '', '','n');
		$name = str_replace($search, $replace, $name);
      $picture->picture_path = "img/restaurante/slider_reserva/".$name;
      \Storage::disk('reserva_restaurante')->put($name,  \File::get($pic));
    }else if($estado==2){//SUPERIOR
		//dd($inputs);
	
		$name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
		$search  = array(' ', '(', ')','ñ');
		$replace = array('', '', '','n');
		$name = str_replace($search, $replace, $name);
		$picture->picture_path = "img/restaurante/slider/".$name;
		\Storage::disk('restaurante')->put($name,  \File::get($pic));
    }else if($estado==13){//CONTACTO
      $name = $picture->picture_date->year.$picture->picture_date->month.$picture->picture_date->day.$picture->picture_date->hour.$picture->picture_date->minute.$picture->picture_date->second.$pic->getClientOriginalName();
		$search  = array(' ', '(', ')','ñ');
		$replace = array('', '', '','n');
		$name = str_replace($search, $replace, $name);
      $picture->picture_path = "img/home/contacto/".$name;
      \Storage::disk('contactus')->put($name,  \File::get($pic));
    }

    $picture->user_id = Session::get("usuario")->id;
    $picture->save();

    return 1;
  }*/

	//OBTENGO LA GALERIA DEL RESTAURANTE EN JSON
	public static function getRestauranteSlider(){
		return Picture::where("picture_status","18")->first()->toJson();
	}
	
	//OBTENGO LA GALERIA DEL HOTEL EN JSON
	public static function getHotelSlider(){
		return Picture::where("picture_status","11")->first()->toJson();
	}
	
	//OBTENGO LA GALERIA DEL HOME
	public static function getHomeSlider(){
		return Picture::where("picture_status","17")->first()->toJson();
	}
	
	//REMOVER EN EL HISTORIAL
	public function remover_fotoprincipal($inputs, $tipo){
		$name = "";
		$date = Carbon::now(-4);
		$historial = Picture::where("id",$inputs["pictureid"])->first();
		$ruta = $historial->picture_path;
		if(file_exists($ruta)){
			unlink($ruta);//ELIMINO DEL HISTORIAL.
			Picture::where("id",$inputs["pictureid"])->delete(); return 1;
			return 1;
		}return 0;
	}
	
	//EDITANDO EN EL HISTORIAL.
	public function edit_fotoprincipal($inputs, $tipo){
		$name = "";
		$date = Carbon::now(-4);
		$historial = Picture::where("id",$inputs["idpicture"])->first();
		$ruta = $historial->picture_path;
		if(file_exists($ruta)){
			unlink($ruta);//ELIMINO DEL HISTORIAL.
			
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_superior"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			\Storage::disk('superior')->put($name,  \File::get($inputs["banner_superior"]));
				
			Picture::where("id",$inputs["idpicture"])->update([
				"picture_path" => "img/superior/".$name
			]);
			
			return 1;
		}
		
		return 0;
	}
	
	public static function edit_fotosecundaria($inputs, $tipo){
		$name = "";
		$date = Carbon::now(-4);
		$historial = Picture::where("id",$inputs["idpicture"])->first();
		$ruta = $historial->picture_path;
		if(file_exists($ruta)){
			unlink($ruta);//ELIMINO DEL HISTORIAL.
			
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_inferior"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			
			$ruta = "";
			
			if($tipo=="13"){
				\Storage::disk('historial_contacto')->put($name,  \File::get($inputs["banner_inferior"]));
				$ruta = "img/contacto/";
			}else{
				\Storage::disk('inferior')->put($name,  \File::get($inputs["banner_inferior"]));
				$ruta = "img/inferior/";
			}
				
				
			Picture::where("id",$inputs["idpicture"])->update([
				"picture_path" => $ruta.$name
			]);
			
			return 1;
		}
		
		return 0;
	}
	
	//COMPRUEBO Y TRASLADO LA IMAGEN AL HISTORIAL
	public static function moverHistorial($estado,$origen,$newlocation){
		if(file_exists($origen)){
			if(copy($origen,$newlocation)){
				unlink($origen);
				Picture::create([//NUEVA IMAGEN DEL HISTORIAL
					"picture_nombre" => "Waldorf Caracas",
					"picture_status" => $estado,
					"picture_date" => Carbon::now(-4),
					"picture_path" => $newlocation,
					"user_id" => Session::get("usuario")->id
				]);
			}return 1;
		}return 0;
	}
	
	//CONTROLA INSERCIÓN DEL FOTO PRINCIPAL
	public static function store_fotoprincipal($inputs,$estado,$historial){
		$finder = Picture::where("picture_status",$estado)->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){//EXISTE REGISTRO PARA INSERCIÓN
			//CARGO LA DATA PARA LA CREACIÖN O ACTUALIZACIÓN DE LA FOTO PRINCIPAL
			//MUEVO LA IMAGEN ANTERIOR AL HISTORIAL
			$newlocation = 'img/superior/'.substr($finder->picture_path,strripos($finder->picture_path,"/")+1,strlen($finder->picture_path));
			if(Picture::moverHistorial($historial,$finder->picture_path,$newlocation)){
				//NUEVA IMAGEN
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_superior"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$name = str_replace($search, $replace, $name);
				$ruta = "img/restaurante/slider/";
				if($estado=="1"){
					\Storage::disk('habitaciones')->put($name,  \File::get($inputs["banner_superior"]));
					$ruta = "img/habitaciones/slider/";
				}else if($estado=="12"){
					\Storage::disk('hotelbannersuperior')->put($name,  \File::get($inputs["banner_superior"]));
					$ruta = "img/hotel/bannersuperior/";
				}else
					\Storage::disk('restaurante')->put($name,  \File::get($inputs["banner_superior"]));
				
				Picture::where("picture_status",$estado)->update([
					"picture_nombre" => "Waldorf Caracas",
					"picture_date" => Carbon::now(-4),
					"picture_status" => $estado,
					"picture_path" => $ruta.$name,
					"user_id" => Session::get("usuario")->id
				]);
				
			}
			
			return 2;
		}else{//NO EXISTE UNA FOTO PRINCIPAL REGISTRADA PARA LA HABITACIÓN
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_superior"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			$ruta = "img/restaurante/slider/";
			if($estado=="1"){
				\Storage::disk('habitaciones')->put($name,  \File::get($inputs["banner_superior"]));
				$ruta = "img/habitaciones/slider/";
			}else if($estado=="12"){
				\Storage::disk('hotelbannersuperior')->put($name,  \File::get($inputs["banner_superior"]));
				$ruta = "img/hotel/bannersuperior/";
			}else
				\Storage::disk('restaurante')->put($name,  \File::get($inputs["banner_superior"]));
				
			Picture::create([
				"picture_nombre" => "Waldorf Caracas",
				"picture_date" => $date,
				"picture_status" => $estado,
				"picture_path" => $ruta.$name,
				"user_id" => Session::get("usuario")->id
			]);
			
			return 1;
		}return 0;
	}
	
	//CONTROLA LA INSERCIÓN DE LA FOTO SECUNDARIA
	public static function store_fotosecundaria($inputs,$estado,$historial){
		$finder = Picture::where("picture_status",$estado)->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){//EXISTE REGISTRO PARA INSERCIÓN
			//CARGO LA DATA PARA LA CREACIÖN O ACTUALIZACIÓN DE LA FOTO PRINCIPAL
			//MUEVO LA IMAGEN ANTERIOR AL HISTORIAL
			$newlocation = 'img/inferior/'.substr($finder->picture_path,strripos($finder->picture_path,"/")+1,strlen($finder->picture_path));
			if(Picture::moverHistorial($historial,$finder->picture_path,$newlocation)){
				//NUEVA IMAGEN
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_inferior"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$name = str_replace($search, $replace, $name);
				$ruta = "img/restaurante/slider_reserva/";
				if($estado=="5"){
					\Storage::disk('habitacion_reserva')->put($name,  \File::get($inputs["banner_inferior"]));
					$ruta = "img/habitaciones/slider_reserva/";
				}else
					\Storage::disk('reserva_restaurante')->put($name,  \File::get($inputs["banner_inferior"]));
				
				Picture::where("picture_status",$estado)->update([
					"picture_nombre" => "Waldorf Caracas",
					"picture_date" => Carbon::now(-4),
					"picture_status" => $estado,
					"picture_path" => $ruta.$name,
					"user_id" => Session::get("usuario")->id
				]);
				
			}
			
			return 2;
		}else{
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_inferior"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			$ruta = "img/restaurante/slider_reserva/";
			if($estado=="5"){
				\Storage::disk('habitacion_reserva')->put($name,  \File::get($inputs["banner_inferior"]));
				$ruta = "img/habitaciones/slider_reserva/";
			}else
				\Storage::disk('reserva_restaurante')->put($name,  \File::get($inputs["banner_inferior"]));
				
			Picture::create([
				"picture_nombre" => "Waldorf Caracas",
				"picture_date" => $date,
				"picture_status" => $estado,
				"picture_path" => $ruta.$name,
				"user_id" => Session::get("usuario")->id
			]);
			
			return 1;
		}
	}
	
	//INTERCAMBIO FOTO PRINCIPAL VS LA SELECCIONADA DEL HISTORIAL
	public function changestate($inputs, $estado, $idactivo){
		$finder = Picture::where("picture_status",$estado)->where("id",$inputs["pictureid"])->first();
		if(count($finder)==1){
			//FOTO PRINCIPAL QUE PASARA AL HISTORIAL
			$activo = Picture::where("picture_status",$idactivo)->first();
			
			$imagen = substr($activo->picture_path,strripos($activo->picture_path,"/")+1,strlen($activo->picture_path));
			$newlocation = "";
			
			if($estado=="7" || $estado=="8"){
				$newlocation = "img/inferior/".$imagen;
			}else if($estado=="3" || $estado=="4" || $estado=="21"){
				$newlocation = "img/superior/".$imagen;
			}else if($estado=="14"){
				$newlocation = "img/contacto/".$imagen;
			}
			
			if(file_exists($activo->picture_path)){//EXISTE LA IMAGEN ACTUAL
				if(copy($activo->picture_path,$newlocation)){//PASO LA IMAGEN ACTUAL A LA CARPETA SUPERIOR/HISTORIAL
					unlink($activo->picture_path);
					
					//ACTUAL: SE PASA AL HISTORIAL
					Picture::where("id",$activo->id)->update([
						"picture_status" => $estado,
						"picture_path" => $newlocation
					]);
					
					//HISTORIAL: SE PASA A MODO: ACTUAL
					$finder = Picture::where("picture_status",$estado)->where("id",$inputs["pictureid"])->first();
					$imagen = substr($finder->picture_path,strripos($finder->picture_path,"/")+1,strlen($finder->picture_path));
					if($estado=="7")
						$locationpri = "img/habitaciones/slider_reserva/".$imagen;
					else if($estado=="8")
						$locationpri = "img/restaurante/slider_reserva/".$imagen;
					else if($estado=="3")
						$locationpri = "img/habitaciones/slider/".$imagen;
					else if($estado=="4")
						$locationpri = "img/restaurante/slider/".$imagen;
					else if($estado=="14")
						$locationpri = "img/home/contacto/".$imagen;
					else if($estado=="21")
						$locationpri = "img/hotel/bannersuperior/".$imagen;
								
					if(copy($finder->picture_path,$locationpri)){
						unlink($finder->picture_path);
						Picture::where("id",$inputs["pictureid"])->update([
							"picture_status" => $idactivo,
							"picture_path" => $locationpri
						]);
				
						return 1;
					}
				}
			}
		}return 0;
	}
	
	//VER EL LIMITE DE UNA IMAGEN
	public function limite($picture,$estado){
		$total = $picture->where("picture_status","=",$estado)->get();
		return count($total);
	}

	//ALMACENO EL SLIDER DEL RESTAURANTE
	public static function storeslider($request,$status){
		$finder = Picture::where("picture_status",$status)->first();
		if(count($finder)==1){
			$date = Carbon::now(-4);
			$default = "";
			$json = json_decode($finder->picture_path);
			
			$uno = $json->posicion[0]; 
			$dos = $json->posicion[1];
			$tres = $json->posicion[2];
			$cuatro = $json->posicion[3];
			$cinco = $json->posicion[4];
			$seis = $json->posicion[5];
			
			$size = 0;
			if($request["editando"]==0 && $json->cantidad==6){
				$size = 0;
			}else{
				$size = $json->cantidad;
			}
						
			if(array_key_exists("file_picture1",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture1"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$uno = str_replace($search, $replace, $name);
				if(($json->posicion[0]!="img/home/slider/default.jpg"  && $json->posicion[0]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[0]!="img/hotel/galeria/default.jpg") && $json->posicion[0]!="img/default.jpg") && file_exists($json->posicion[0])){
					unlink($json->posicion[0]);
					$size-=1;
				}
					
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($uno,  \File::get($request["file_picture1"]));
					$uno = ("img/home/slider/".$uno);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($uno,  \File::get($request["file_picture1"]));
					$uno = ("img/hotel/galeria/".$uno);
				}else{
					\Storage::disk('slider_rsecu')->put($uno,  \File::get($request["file_picture1"]));
					$uno = ("img/restaurante/galeria/thumbnails/".$uno);
				}
				
				$size+=1;
			}
			
			if(array_key_exists("file_picture2",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture2"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$dos = str_replace($search, $replace, $name);
				if(($json->posicion[1]!="img/home/slider/default.jpg"  && $json->posicion[1]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[1]!="img/hotel/galeria/default.jpg") && $json->posicion[1]!="img/default.jpg") && file_exists($json->posicion[1])){
					unlink($json->posicion[1]);
					$size-=1;
				}
				
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($dos,  \File::get($request["file_picture2"]));
					$dos = ("img/home/slider/".$dos);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($dos,  \File::get($request["file_picture2"]));
					$dos = ("img/hotel/galeria/".$dos);
				}else{
					\Storage::disk('slider_rsecu')->put($dos,  \File::get($request["file_picture2"]));
					$dos = ("img/restaurante/galeria/thumbnails/".$dos);
				}
				
				$size+=1;
			}
			
			if(array_key_exists("file_picture3",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture3"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$tres = str_replace($search, $replace, $name);
				if(($json->posicion[2]!="img/home/slider/default.jpg"  && $json->posicion[2]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[2]!="img/hotel/galeria/default.jpg") && $json->posicion[2]!="img/default.jpg") && file_exists($json->posicion[2])){
					unlink($json->posicion[2]);
					$size-=1;
				}
				
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($tres,  \File::get($request["file_picture3"]));
					$tres = ("img/home/slider/".$tres);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($tres,  \File::get($request["file_picture3"]));
					$tres = ("img/hotel/galeria/".$tres);
				}else{
					\Storage::disk('slider_rsecu')->put($tres,  \File::get($request["file_picture3"]));
					$tres = ("img/restaurante/galeria/thumbnails/".$tres);
				}
				
				$size+=1;
			}
			
			if(array_key_exists("file_picture4",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture4"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$cuatro = str_replace($search, $replace, $name);
				if(($json->posicion[3]!="img/home/slider/default.jpg"  && $json->posicion[3]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[3]!="img/hotel/galeria/default.jpg") && $json->posicion[3]!="img/default.jpg") && file_exists($json->posicion[3])){
					unlink($json->posicion[3]);
					$size-=1;
				}
				
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($cuatro,  \File::get($request["file_picture4"]));
					$cuatro = ("img/home/slider/".$cuatro);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($cuatro,  \File::get($request["file_picture4"]));
					$cuatro = ("img/hotel/galeria/".$cuatro);
				}else{
					\Storage::disk('slider_rsecu')->put($cuatro,  \File::get($request["file_picture4"]));
					$cuatro = ("img/restaurante/galeria/thumbnails/".$cuatro);
				}
				
				$size+=1;
			}
			
			if(array_key_exists("file_picture5",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture5"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$cinco = str_replace($search, $replace, $name);
				if(($json->posicion[4]!="img/home/slider/default.jpg"  && $json->posicion[4]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[4]!="img/hotel/galeria/default.jpg") && $json->posicion[4]!="img/default.jpg") && file_exists($json->posicion[4])){
					unlink($json->posicion[4]);
					$size-=1;
				}
				
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($cinco,  \File::get($request["file_picture5"]));
					$cinco = ("img/home/slider/".$cinco);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($cinco,  \File::get($request["file_picture5"]));
					$cinco = ("img/hotel/galeria/".$cinco);
				}else{
					\Storage::disk('slider_rsecu')->put($cinco,  \File::get($request["file_picture5"]));
					$cinco = ("img/restaurante/galeria/thumbnails/".$cinco);
				}
				
				$size+=1;
			}
			
			if(array_key_exists("file_picture6",$request)){
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$request["file_picture6"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$seis = str_replace($search, $replace, $name);
				if(($json->posicion[5]!="img/home/slider/default.jpg"  && $json->posicion[5]!="img/home/restaurante/galeria/thumbnails/default.jpg" 
				&& ($json->posicion[5]!="img/hotel/galeria/default.jpg") && $json->posicion[5]!="img/default.jpg") && file_exists($json->posicion[5])){
					unlink($json->posicion[5]);
					$size-=1;
				}
				
				if($request["modo"]=="0"){
					\Storage::disk('slider_ppal')->put($seis,  \File::get($request["file_picture6"]));
					$seis = ("img/home/slider/".$seis);
				}else if($request["modo"]=="2"){
					\Storage::disk('galeria_hotel')->put($seis,  \File::get($request["file_picture6"]));
					$seis = ("img/hotel/galeria/".$seis);
				}else{
					\Storage::disk('slider_rsecu')->put($seis,  \File::get($request["file_picture6"]));
					$seis = ("img/restaurante/galeria/thumbnails/".$seis);
				}
				
				$size+=1;
			}
			
			if(($size + $json->cantidad) <= 6) $size +=  $json->cantidad; else $size = 6;
			if($request["modo"]=="0"){
				$default = '{"cantidad":"'.$size.'","posicion":["'.$uno.'","'.$dos.'","'.$tres.'","'.$cuatro.'","'.$cinco.'","'.$seis.'"]}';
			}else if($request["modo"]=="2"){
				$default = '{"cantidad":"'.$size.'","posicion":["'.$uno.'","'.$dos.'","'.$tres.'","'.$cuatro.'","'.$cinco.'","'.$seis.'"]}';
			}else{
				$default = '{"cantidad":"'.$size.'","posicion":["'.$uno.'","'.$dos.'","'.$tres.'","'.$cuatro.'","'.$cinco.'","'.$seis.'"]}';
			}
			
			Picture::where("id",$finder->id)->update([
				"picture_nombre" => "Waldor Caracas",
				"picture_date" => $date,
				"picture_status" => $status,
				"user_id" => Session::get("usuario")->id,
				"picture_path" => $default
			]);
				
			return 1;
		}return 0;
	}

	//ALMACENO EVENTOS
	public function storeeventos($picture,$pic1, $pic2, $estado,$editando){
		$idAux = $picture->where("picture_status",$estado)->value("id");
		if(count($idAux)==1 && $estado=="16"){//SE ENCONTRO UNA IMAGEN YA EN EL SISTEMA.
			$eventos = json_decode($picture->where("id",$idAux)->value("picture_path"));
			$default = "["; $historial1 = ""; $historial2 = ""; $date = Carbon::now(-4);
			$confirmar = 0;
			foreach($eventos as $gas){//COMPRUEBO SI EXISTE YA UNA IMAGEN 1-4
				if($gas->tipo=="1" && $pic1!=NULL){//EXISTE IMAGEN TIPO 1..
					$historial1 = "\"".$gas->img."\"";
				}else if($gas->tipo=="2" && $pic2!=NULL){//EXISTE IMAGEN TIPO 2..
					$historial2 = "\"".$gas->img."\"";
				}else{
					if($gas->img=="img/restaurante/eventos/default.jpg"  && $pic2==NULL && $pic1!=NULL && $gas->tipo!="1"){//EXISTE IMG 1
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else if($gas->img=="img/restaurante/eventos/default.jpg" && $pic1==NULL && $pic2!=NULL && $gas->tipo!="2"){//EXISTE IMG 2
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}else{
						$default.='{"img":"'.$gas->img.'","tipo":"'.$gas->tipo.'"},';
					}
				}
			}

			if(!$confirmar){
				if($pic1!=NULL && $pic2!=NULL){//1-2
					$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
					$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
					if($eliminar!="img/restaurante/eventos/default.jpg" && file_exists($eliminar)) unlink($eliminar);
					
					$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
					$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
					if($eliminar!="img/restaurante/eventos/default.jpg" && file_exists($eliminar)) unlink($eliminar);

					$default .='{"img":"img/restaurante/eventos/'.$imagen2.'","tipo":"2"},';
					$default .='{"img":"img/restaurante/eventos/'.$imagen1.'","tipo":"1"}]';

					\Storage::disk('restaurante_eventos')->put($imagen1,  \File::get($pic1));
					\Storage::disk('restaurante_eventos')->put($imagen2,  \File::get($pic2));
				}else{
					if($pic1!=NULL){//1
						$imagen1 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic1->getClientOriginalName();
						$eliminar = substr($historial1,1,strripos($historial1,"\"")-1);
						if($eliminar!="img/restaurante/eventos/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$default .='{"img":"img/restaurante/eventos/'.$imagen1.'","tipo":"1"}]';
						\Storage::disk('restaurante_eventos')->put($imagen1,  \File::get($pic1));
					}else if($pic2!=NULL){//2
						$imagen2 = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$pic2->getClientOriginalName();
						$eliminar = substr($historial2,1,strripos($historial2,"\"")-1);
						if($eliminar!="img/restaurante/eventos/default.jpg" && file_exists($eliminar)) unlink($eliminar);
						$default .='{"img":"img/restaurante/eventos/'.$imagen2.'","tipo":"2"}]';
						\Storage::disk('restaurante_eventos')->put($imagen2,  \File::get($pic2));
					}
				}
			}

			if($pic1==NULL && $pic2==NULL){
				$auxiliar = substr($default,0,strripos($default,","))."]";
				$default = $auxiliar;
			}

			$picture->where("id",$idAux)->update([
				"picture_nombre" => "Hotel Waldorf",
				"picture_date" => $date,
				"picture_status" => $estado,
				"user_id" => Session::get("usuario")->id,
				"picture_path" => $default
			]); return 1;
		}return 0;
	}
	
	public static function store_fotocontacto($inputs,$estado,$historial){
		$finder = Picture::where("picture_status",$estado)->first();
		$date = Carbon::now(-4);
		if(count($finder)==1){//EXISTE REGISTRO PARA INSERCIÓN
			//CARGO LA DATA PARA LA CREACIÖN O ACTUALIZACIÓN DE LA FOTO PRINCIPAL
			//MUEVO LA IMAGEN ANTERIOR AL HISTORIAL
			$newlocation = 'img/contacto/'.substr($finder->picture_path,strripos($finder->picture_path,"/")+1,strlen($finder->picture_path));
			if(Picture::moverHistorial($historial,$finder->picture_path,$newlocation)){
				//NUEVA IMAGEN
				$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_inferior"]->getClientOriginalName();
				$search  = array(' ', '(', ')','ñ');
				$replace = array('', '', '','n');
				$name = str_replace($search, $replace, $name);
				$ruta = "img/home/contacto/";
				\Storage::disk('contactus')->put($name,  \File::get($inputs["banner_inferior"]));
				
				Picture::where("picture_status",$estado)->update([
					"picture_nombre" => "Waldorf Caracas",
					"picture_date" => Carbon::now(-4),
					"picture_status" => $estado,
					"picture_path" => $ruta.$name,
					"user_id" => Session::get("usuario")->id
				]);
				
			}
			
			return 2;
		}else{
			$name = $date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$inputs["banner_inferior"]->getClientOriginalName();
			$search  = array(' ', '(', ')','ñ');
			$replace = array('', '', '','n');
			$name = str_replace($search, $replace, $name);
			$ruta = "img/home/contacto/";
			\Storage::disk('contactus')->put($name,  \File::get($inputs["banner_inferior"]));
				
			Picture::create([
				"picture_nombre" => "Waldorf Caracas",
				"picture_date" => $date,
				"picture_status" => $estado,
				"picture_path" => $ruta.$name,
				"user_id" => Session::get("usuario")->id
			]);
			
			return 1;
		}
	}
}