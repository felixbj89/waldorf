<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TextRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
        'titulohistory' => 'required',
        'subtitulohistory' => 'required',
      ];
  }

  public function messages()
  {
      return [
          'titulohistory.required' => '1 El campo es obligatorio, intente nuevamente',
          'subtitulohistory.required' => '2 El campo es obligatorio, intente nuevamente',
      ];
  }
}
