<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventosRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
        'titulohistory' => 'required',
        'subtitulohistory' => 'required',
      ];
  }

  public function messages()
  {
      return [
          'titulohistory.required' => 'El campo es obligatorio, intente nuevamente',
          'subtitulohistory.required' => 'El campo es obligatorio, intente nuevamente',
          'file_picture1.required'  => 'El campo es obligatorio, intente nuevamente',
          'file_picture2.required'  => 'El campo es obligatorio, intente nuevamente',
      ];
  }
}
