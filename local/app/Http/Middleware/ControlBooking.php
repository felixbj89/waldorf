<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use \Illuminate\Session\Store;
use App\Booking;

class ControlBooking
{
  private $time = 3600;
  private $session;

	public function __construct(Store $session){
		$this->session = $session;
	}

  public function handle($request, Closure $next){
  		if(Session::has("bookuser")){
  			if(!Session::has("lastactividad")){
  				Session::set("lastactividad",time());
  			}else if((time()-Session::get("lastactividad")) > $this->time){
  				Session::forget("lastactividad");
  				Session::forget("bookuser");
          Booking::where("user_email",Session::get("bookuser")->user_email)->update([
            "user_token_status" => "0"
          ]);

          Session::flush();
  				Session::flash("volatile","Su tiempo de sesión ha terminado");
  				return redirect()->to("/");
  			}

  			Session::set("lastactividad",time());
  			return $next($request);
  		}return redirect()->to("/");
    }
}
