<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\User;
use App\Text;
class Lock{

    public function handle($request, Closure $next){
      if(!Session::has("usuario")){
        User::where("user_email",Session::get("usuario")["user_email"])->update([
          "user_token_status" => "0"
        ]);

        Text::where("text_status",1)->update([
          "text_status" => "0"
        ]);

        return view("lock.lock")->with(["bloqueado" => "0"]);
      }return $next($request);
    }
}
