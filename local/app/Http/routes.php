<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*==================================VIEWS===================================*/
Route::get('exe', 'FrotController@exe');
Route::get('/','FrotController@index');
Route::get('admin/login',"FrotController@backendlogin");
Route::get('Habitaciones/admin/login',"FrotController@backendhablogin");
Route::get('Restaurante/admin/login',"FrotController@backendrestlogin");
Route::get('Home/{id}','FrotController@index_');
Route::get('Home/Restaurante/{id}','FrotController@index_');
Route::get('Home/{id}/admin/login','FrotController@backendconlogin');
Route::get('hotel/{id?}','FrotController@hotel');
Route::get('Habitaciones','FrotController@habitaciones');
Route::get('Habitacion/{id}','FrotController@habitaciones_');
Route::get('Restaurante','FrotController@restaurantes');
Route::get('waldorf/reservar','BookingController@reservar');
Route::get('Restaurante/waldorf/reservar','BookingController@reservar');
Route::get('Habitaciones/waldorf/reservar','BookingController@reservar');
Route::get('hotel/waldorf/reservar','BookingController@reservar');
Route::post('enviarcontacto','EmailController@send');
Route::post('Restaurante/enviareserva','EmailController@reserva');
/*===================================GET====================================*/
Route::get('IndexImgContent','FrotController@IndexImgContent');
Route::get('habitacionType','FrotController@habitacionGallery');
Route::get('habitacionTypeDetails','FrotController@habitacionDetails');
Route::get('habitacionTypeGallery','FrotController@habitacionTypeGallery');
Route::get('RestaurantImgContent','FrotController@RestaurantImgContent');
/*==================================BOOKING==================================*/
Route::group(['prefix' => 'booking'], function(){
	Route::get('setup','BookingController@index');
	Route::post('autentificar','BookingController@autentificar');
	Route::get('salir/{modo}', ["uses" => 'BookingController@salir', 'as' => 'booking/salir']);
	Route::group(['middleware' => 'chequear'], function(){
		Route::get('configbook', ["uses" => 'BookingController@booking', 'as' => 'booking/configbook']);
		Route::post('savesetup', ["uses" => 'BookingController@savesetup', 'as' => 'booking/savesetup']);
	});
});
/*==================================BACKEND==================================*/
Route::group(['prefix' => 'admin'], function(){
	Route::post('dashboard', ["uses" => 'LoginController@index', 'as' => 'admin/dashboard']);
	Route::post('unlock', ["uses" => 'LoginController@reLogin', 'as' => 'admin/unlock']);
	Route::get('salir', ["uses" => 'DashboardController@salir', 'as' => 'admin/salir']);

	Route::group(['middleware' => 'lock'], function(){
		Route::get('dashboard', ["uses" => 'DashboardController@dashboard', 'as' => 'admin/dashboard']);
		Route::get('bloqueado', ["uses" => 'DashboardController@bloqueado', 'as' => 'admin/bloqueado']);
		Route::get('fbloqueado', ["uses" => 'DashboardController@fbloqueado', 'as' => 'admin/fbloqueado']);
		Route::get('autenticado', ["uses" => 'DashboardController@gofrontend', 'as' => 'admin/autenticado']);
		//GET-CREAR
		Route::get('usuarios', ["uses" => 'DashboardController@usuarios', 'as' => 'admin/usuarios']);
		Route::get('history', ["uses" => 'DashboardController@history', 'as' => 'admin/history']);
		Route::get('rslider', ["uses" => 'DashboardController@rslider', 'as' => 'admin/rslider']);
		Route::get('eventos', ["uses" => 'DashboardController@eventos', 'as' => 'admin/eventos']);
		Route::get('habitaciones', ["uses" => 'DashboardController@habitaciones', 'as' => 'admin/habitaciones']);
		Route::get('gastronomia', ["uses" => 'DashboardController@gastronomia', 'as' => 'admin/gastronomia']);
		Route::get('crearhslider', ["uses" => 'DashboardController@crearhslider', 'as' => 'admin/crearhslider']);
		Route::get('crearhab', ["uses" => 'DashboardController@crearhab', 'as' => 'admin/crearhab']);
		Route::get('crearpics', ["uses" => 'DashboardController@crearpics', 'as' => 'admin/crearpics']);
		Route::get('crearrinferior', ["uses" => 'DashboardController@crearrinferior', 'as' => 'admin/crearrinferior']);
		Route::get('crearhinferior', ["uses" => 'DashboardController@crearhinferior', 'as' => 'admin/crearhinferior']);
		Route::get('crearicontacto', ["uses" => 'DashboardController@crearicontacto', 'as' => 'admin/crearicontacto']);
		Route::get('crearshprincipal', ["uses" => 'DashboardController@crearshprincipal', 'as' => 'admin/crearshprincipal']);
		Route::get('crearsehprincipal', ["uses" => 'DashboardController@crearsehprincipal', 'as' => 'admin/crearsehprincipal']);
		Route::get('crearshslider', ["uses" => 'DashboardController@crearshslider', 'as' => 'admin/crearshslider']);
		Route::get('crearhotelgaleria', ["uses" => 'DashboardController@crearhotelgaleria', 'as' => 'admin/crearhotelgaleria']);
		Route::get('crearpromociones', ["uses" => 'DashboardController@crearpromociones', 'as' => 'admin/crearpromociones']);
		//GET-LISTAR
		Route::get('hishome', ["uses" => 'DashboardController@hishome', 'as' => 'admin/hishome']);
		Route::get('vercfotos', ["uses" => 'DashboardController@vercfotos', 'as' => 'admin/vercfotos']);
		Route::get('verhfotos', ["uses" => 'DashboardController@verhfotos', 'as' => 'admin/verhfotos']);
		Route::get('verrfotos', ["uses" => 'DashboardController@verrfotos', 'as' => 'admin/verrfotos']);
		Route::get('verhifotos', ["uses" => 'DashboardController@verhifotos', 'as' => 'admin/verhifotos']);
		Route::get('verrifotos', ["uses" => 'DashboardController@verrifotos', 'as' => 'admin/verrifotos']);
		Route::get('verehfotos', ["uses" => 'DashboardController@verehfotos', 'as' => 'admin/verehfotos']);
		Route::get('listar', ["uses" => 'DashboardController@listar', 'as' => 'admin/listar']);
		Route::get('verpromos', ["uses" => 'DashboardController@verpromos', 'as' => 'admin/verpromos']);
		Route::post('verhabitacion', ["uses" => 'TextController@verhabitacion', 'as' => 'admin/verhabitacion']);
		Route::post('verpropiedades', ["uses" => 'TextController@verpropiedades', 'as' => 'admin/verpropiedades']);
		Route::post('verhabpicture', ["uses" => 'PictureController@verhabpicture', 'as' => 'admin/verhabpicture']);
		Route::post('list', ["uses" => 'UserController@editobj', 'as' => 'admin/list']);
		//POST-CREAR
		Route::post('addsuperior', ["uses" => 'PictureController@addsuperior', 'as' => 'admin/addsuperior']);
		Route::post('addinferior', ["uses" => 'PictureController@addinferior', 'as' => 'admin/addinferior']);
		Route::post('addgastronomia', ["uses" => 'TextController@addgastronomia', 'as' => 'admin/addgastronomia']);
		Route::post('addhistory', ["uses" => 'TextController@addhistory', 'as' => 'admin/addhistory']);
		Route::post('addslider', ["uses" => 'PictureController@addslider', 'as' => 'admin/addslider']);
		Route::post('addeventos', ["uses" => 'TextController@addeventos', 'as' => 'admin/addeventos']);
		Route::post('addseculider', ["uses" => 'TextController@addseculider', 'as' => 'admin/addseculider']);
		Route::post('addtextprincipal', ["uses" => 'TextController@addtextprincipal', 'as' => 'admin/addtextprincipal']);
		Route::post('addrooms', ["uses" => 'TextController@addrooms', 'as' => 'admin/addrooms']);
		Route::post('addpromo', ["uses" => 'PromocionesController@addpromo', 'as' => 'admin/addpromo']);
		Route::post('create', ["uses" => 'UserController@create', 'as' => 'admin/create']);
		//POST-ELIMINAR
		Route::post('removepicture', ["uses" => 'PictureController@removepicture', 'as' => 'admin/removepicture']);
		Route::post('removerpromo', ["uses" => 'PromocionesController@removerpromo', 'as' => 'admin/removerpromo']);
		Route::post('remove', ["uses" => 'UserController@remove', 'as' => 'admin/remove']);
		//POST-STATE
		Route::post('changestate', ["uses" => 'PictureController@changestate', 'as' => 'admin/changestate']);
		Route::post('changepromo', ["uses" => 'PromocionesController@changepromo', 'as' => 'admin/changepromo']);
		//POST-EDIT
		Route::post('editbannersuperior', ["uses" => 'DashboardController@editbannersuperior', 'as' => 'admin/editbannersuperior']);
		Route::post('editbannerinferior', ["uses" => 'DashboardController@editbannerinferior', 'as' => 'admin/editbannerinferior']);
		Route::post('profile', ["uses" => 'UserController@edit', 'as' => 'admin/profile']);
		Route::post('editregistrado', ["uses" => 'UserController@editregistrado', 'as' => 'admin/editregistrado']);
		Route::post('editsuperior', ["uses" => 'PictureController@editsuperior', 'as' => 'admin/editsuperior']);
		Route::post('editinferior', ["uses" => 'PictureController@editinferior', 'as' => 'admin/editinferior']);
		Route::post('editgastronomia', ["uses" => 'TextController@editgastronomia', 'as' => 'admin/editgastronomia']);
		Route::post('edithistory', ["uses" => 'TextController@edithistory', 'as' => 'admin/edithistory']);
		Route::post('editarslider', ["uses" => 'DashboardController@editarslider', 'as' => 'admin/editarslider']);
		Route::post('editslider', ["uses" => 'PictureController@editslider', 'as' => 'admin/editslider']);
		Route::post('editareventos', ["uses" => 'TextController@editareventos', 'as' => 'admin/editareventos']);
		Route::post('editarsecuslider', ["uses" => 'DashboardController@editarsecuslider', 'as' => 'admin/editarsecuslider']);
		Route::post('editpromo', ["uses" => 'DashboardController@editpromo', 'as' => 'admin/editpromo']);
		Route::post('editarpromo', ["uses" => 'PromocionesController@editarpromo', 'as' => 'admin/editarpromo']);
		/*Route::get('resuperior', ["uses" => 'DashboardController@resuperior', 'as' => 'admin/resuperior']);*/
		/*Route::get('secundario', ["uses" => 'DashboardController@binferior', 'as' => 'admin/secundario']);
		/*Route::get('resecundario', ["uses" => 'DashboardController@resecundario', 'as' => 'admin/resecundario']);*
		/*Route::get('hissuperior', ["uses" => 'DashboardController@hissuperior', 'as' => 'admin/hissuperior']);*/
		/*Route::get('hisinferior', ["uses" => 'DashboardController@hisinferior', 'as' => 'admin/hisinferior']);*
		Route::get('hiscontacto', ["uses" => 'DashboardController@hiscontacto', 'as' => 'admin/hiscontacto']);
		/*Route::get('hisrsuperior', ["uses" => 'DashboardController@hisrsuperior', 'as' => 'admin/hisrsuperior']);*/
		/*Route::get('hisrinferior', ["uses" => 'DashboardController@hisrinferior', 'as' => 'admin/hisrinferior']);*
		Route::post('verificar', ["uses" => 'DashboardController@verificar', 'as' => 'admin/verificar']);
		Route::post('changepicture', ["uses" => 'PictureController@changepicture', 'as' => 'admin/changepicture']);
		/*Route::post('addcontactus', ["uses" => 'PictureController@addcontactus', 'as' => 'admin/addcontactus']);
		Route::post('modifyrooms', ["uses" => 'TextController@modifyrooms', 'as' => 'admin/modifyrooms']);
		Route::post('modifypictures', ["uses" => 'PictureController@modifypictures', 'as' => 'admin/modifypictures']);*/
	});
});
