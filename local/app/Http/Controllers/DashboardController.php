<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Picture;
use App\Text;
use App\Rooms;
use App\Promociones;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller{

	private $usuario;

	public function salir(){
		User::where("user_email",Session::get("usuario")["user_email"])->update([
		  "user_token_status" => "0"
		]);

		Text::where("text_status",1)->update([
			"text_status" => "0"
		]);

		Session::flush();
		return redirect()->to("/");
	}

	private function obtener_json($section,$estado){
		return Text::where("text_origen",$estado)->where("text_section",$section)->value("text_json");
	}

	private function picture_active($tipo){
		$picture = Picture::where("picture_status","=",$tipo)->first();
		if(count($picture)==0){
			return 4;
		}else if(count($picture)==1){
			return $tipo;
		}
	}

	private function obtener_rooms(){
		return Rooms::whereNotIn("rooms_thumbnail",["img/habdefault.jpg"])->take(5)->get();
	}

	public function gofrontend(){
		$this->usuario = Session::get("usuario");
		$text = new Text();
		$json = json_decode($this->obtener_json("0","0"));
		$jsongastronomia =  json_decode($this->obtener_json("0","1"));
		$jsonslidersecu = json_decode($this->obtener_json("0","2"));
		$id=""; $ruta = "";

		$superior = $this->picture_active(9);
		$contacto = $this->picture_active(13);
		$habcu = $this->obtener_rooms();
		$promociones = Promociones::construirpromocion();
		$favorito = Promociones::getFavorito();
		if($favorito!=NULL)
			$ruta = url($favorito->promociones_path);
		return view('index')->with([
			"id" => $id,
			"modallogin" => "0",
			"titulohistory" => $json[0]->titulo,
			"subtitulohistory" => $json[0]->subtitulo,
			"textohistory" => $json[0]->texto,
			"usuario" => $this->usuario,
			"cuartos" => $habcu,
			"tituloslidersecu" => $jsonslidersecu[0]->titulo,
			"textoslidersecu" => $jsonslidersecu[0]->texto,
			"titulogastronomia" => $jsongastronomia[0]->titulo,
			"subtitulogastronomia" => $jsongastronomia[0]->subtitulo,
			"textogastronomia" => $jsongastronomia[0]->texto,
			"urlhistory" => Picture::where("picture_status","=",$superior)->value("picture_path"),
			"nombrehistory" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"ruta" => $ruta,
			"promociones" => $promociones,
			"modalpromo" => 0,
			"countpromo" => count(Promociones::getPromosAllActive()),
		]);
	}

	//CREATE
	public function crearpics(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pictures")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","14")->get(),
			"npicture" => Picture::where("picture_status","14")->count(),
			"texto" => "Crear Habitación",
			"nottexto" => "Imagen de fondo no encontrada",
			"tipo" => "HABITACIÓN",
			"modo" => "0"
		]);
	}

	public function crearhab(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_rooms")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","14")->get(),
			"npicture" => Picture::where("picture_status","14")->count(),
			"texto" => "Crear Habitación",
			"nottexto" => "Imagen de fondo no encontrada",
			"tipo" => "HABITACIÓN",
			"modo" => "0"
		]);
	}

	public function habitaciones(){
		$this->usuario = Session::get("usuario");
		return view("backend.administrarhab")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"texto" => "Administrar Habitaciones",
			"nottexto" => "Propiedades no encontradas",
			"tipo" => "PROPIEDADES",
		]);
	}

	public function crearicontacto(){//HOME-INFERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pInferiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","14")->get(),
			"npicture" => Picture::where("picture_status","14")->count(),
			"modo" => "4",
			"texto" => "Agregar una Fondo a contacto",
			"recuerde" => "Resolución Permitida: 1080x400 px",
			"title_modal" => "CONTACTO",
			"ruta" => ""
		]);
	}

	public function crearshprincipal(){//HABITACIÓN-SUPERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pSuperiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","3")->get(),
			"npicture" => Picture::where("picture_status","3")->count(),
			"modo" => "1",
			"texto" => "Agregar una Foto Principal",
			"recuerde" => "Resolución Permitida: 1080x500 px",
			"title_modal" => "FOTO PRINCIPAL",
		]);
	}

	public function crearsehprincipal(){//HOTEL-SUPERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pSuperiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","12")->get(),
			"npicture" => Picture::where("picture_status","12")->count(),
			"modo" => "2",
			"texto" => "Agregar una Foto Principal",
			"recuerde" => "Resolución Permitida: 1080x500 px",
			"title_modal" => "FOTO PRINCIPAL",
		]);
	}

	public function crearsrprincipal(){//RESTAURANTE-SUPERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pSuperiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","4")->get(),
			"npicture" => Picture::where("picture_status","4")->count(),
			"ususystem" => NULL,
			"modo" => "0",
			"texto" => "Agregar una Foto Principal",
			"recuerde" => "Resolución Permitida: 1080x500 px",
			"title_modal" => "FOTO PRINCIPAL"
		]);
	}

	public function crearhinferior(){//HABITACIÓN-INFERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pInferiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","7")->get(),
			"npicture" => Picture::where("picture_status","7")->count(),
			"modo" => "3",
			"texto" => "Agregar una Foto Secundaria",
			"recuerde" => "Resolución Permitida: 871x300 px",
			"title_modal" => "FOTO SECUNDARIA",
			"ruta" => ""
		]);
	}

	public function crearrinferior(){//RESTAURANTE-INFERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.crear_pInferiores")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","7")->get(),
			"npicture" => Picture::where("picture_status","7")->count(),
			"modo" => "2",
			"texto" => "Agregar una Foto Secundaria",
			"recuerde" => "Resolución Permitida: 1180x400 px",
			"title_modal" => "FOTO SECUNDARIA",
			"ruta" => ""
		]);
	}

	public function gastronomia(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_gastronomia")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"npicture" => Picture::where("picture_status","15")->count(),
			"texto" => "Agregar/Cambiar Gastronomía",
			"nottexto" => "Gastronomía no encontrada",
			"tipo" => "GASTRONOMIA",
			"datos" => NULL
		]);
	}

	public function crearhslider(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_sslider")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"texto" => "Agregar/Cambiar Slider Principal",
			"nottexto" => "Sliders no encontrados",
			"medidas" => "1080x500 px.",
			"tipo" => "SLIDERS",
			"modo" => "0",
		]);
	}

	public function crearhotelgaleria(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_sslider")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"texto" => "Agregar/Cambiar Slider Secundario",
			"nottexto" => "Sliders no encontrados",
			"medidas" => "1080x500 px.",
			"tipo" => "SLIDERS",
			"modo" => "2",
		]);
	}

	public function crearshslider(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_secuslider")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"medidas" => "750x450 px / 100x70 px",
			"texto" => "Crear Galería",
			"nottexto" => "Galería no encontrada.",
			"tipo" => "Galería",
			"modo" => "9"
		]);
	}

	public function history(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_history")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::where("picture_status","9")->get(),
			"npicture" => Picture::where("picture_status","9")->count(),
			"texto" => "Agregar/Cambiar Valores de Nuestra Historia",
			"nottexto" => "Historia no encontrada",
			"tipo" => "HISTORIA",
		]);
	}

	public function rslider(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_sslider")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"texto" => "Agregar/Cambiar Galería",
			"nottexto" => "Galería no encontrada",
			"tipo" => "GALERÍA: RESTAURANTE",
			"medidas" => "350x250 px.",
			"modo" => "1"
		]);
	}

	public function eventos(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_eventos")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"npicture" => Picture::where("picture_status","15")->count(),
			"nottexto" => "Eventos no encontrados",
			"tipo" => "EVENTOS",
		]);
	}

	public function crearpromociones(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_promocion")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"nottexto" => "Promociones no encontradas",
			"tipo" => "PROMOCIONES",
		]);
	}

	//LIST
	public function vercfotos(){//LISTAR-CONTACTO
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","14")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_inferior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "13",//CONTACTO
			"imagenes" => $imagenes,
			"size" => count($imagenes),
			"title" => "Contacto"
		]);
	}
	
	public function verpromos(){
		$this->usuario = Session::get("usuario");
		$imagenes = Promociones::getPromosAll();
		return view("backend.listar_promociones",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"imagenes" => $imagenes,
			"size" => count($imagenes),
			"title" => "Promociones"
		]);
	}
	
	public function verhfotos(){//LISTAR-SUPERIOR
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","3")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_superior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "1",//HABITACIÓN
			"imagenes" => $imagenes,
			"size" => count($imagenes)
		]);
	}

	public function verrfotos(){//LISTAR-SUPERIOR
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","4")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_superior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "2",//RESTAURANTE
			"imagenes" => $imagenes,
			"size" => count($imagenes)
		]);
	}

	public function verhifotos(){//LISTAR INFERIOR
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","7")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_inferior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "5",//HABITACIÓN
			"imagenes" => $imagenes,
			"size" => count($imagenes),
			"title" => "Habitación"
		]);
	}

	public function verrifotos(){//LISTAR-INFERIOR
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","8")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_inferior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "6",//RESTAURANTE
			"imagenes" => $imagenes,
			"size" => count($imagenes),
			"title" => "Restaurante"
		]);
	}

	public function verehfotos(){
		$this->usuario = Session::get("usuario");
		$imagenes = Picture::where("picture_status","21")->whereNotIn("picture_path",["img/default.jpg"])->get();
		return view("backend.listar_superior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => "12",//HOTEL
			"imagenes" => $imagenes,
			"size" => count($imagenes)
		]);
	}

	public function hishome(){//LISTAR-HISTORIA
		$this->usuario = Session::get("usuario");
		return view("backend.historial_history")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"pictures" => Picture::whereNotIn("picture_path",["img/default.jpg"])->where("picture_status","10")->get(),
			"npicture" => Picture::where("picture_status","10")->count(),
			"texto" => "Crear Habitación",
			"nottexto" => "Imagen de fondo no encontrada",
			"tipo" => "HABITACIÓN",
			"modo" => "3"
		]);
	}

    public function listar(){
		$this->usuario = Session::get("usuario");
		return view("backend.listar_usuarios")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"usuarios" => User::where("user_role",">",$this->usuario->user_role)->get(),
		]);
	}

	public function usuarios(){
		$this->usuario = Session::get("usuario");
		return view("backend.crear_usuarios")->with([
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
		]);
	}

	/*public function verificar(){
		return redirect()->action("DashboardController@dashboard");
	}*/

	private function visitas($mes,$registro){
		$enero = 0; $febrero = 0; $marzo = 0; $abril = 0; $mayo = 0; $junio = 0; $julio = 0; $agosto = 0; $septiembre = 0; $octubre = 0; $noviembre = 0; $diciembre = 0;
		foreach ($registro as $value) {
			for($i = 0; $i < 12; $i++){
				if($mes+($i+1)==1 && substr($value->created_at,5,2)==1){
					$enero++;
				}else if($mes+($i+1)==2 && substr($value->created_at,5,2)==2){
					$febrero++;
				}else if($mes+($i+1)==3 && substr($value->created_at,5,2)==3){
					$marzo++;
				}else if($mes+($i+1)==4 && substr($value->created_at,5,2)==4){
					$abril++;
				}else if($mes+($i+1)==5 && substr($value->created_at,5,2)==5){
					$mayo++;
				}else if($mes+($i+1)==6 && substr($value->created_at,5,2)==6){
					$junio++;
				}else if($mes+($i+1)==7 && substr($value->created_at,5,2)==7){
					$julio++;
				}else if($mes+($i+1)==8 && substr($value->created_at,5,2)==8){
					$agosto++;
				}else if($mes+($i+1)==9 && substr($value->created_at,5,2)==9){
					$septiembre++;
				}else if($mes+($i+1)==10 && substr($value->created_at,5,2)==10){
					$octubre++;
				}else if($mes+($i+1)==11 && substr($value->created_at,5,2)==11){
					$noviembre++;
				}else if($mes+($i+1)==12 && substr($value->created_at,5,2)==12){
					$diciembre++;
				}
			}
		}

		return [$enero,$febrero,$marzo,$abril,$mayo,$junio,$julio,$agosto,$septiembre,$octubre,$noviembre,$diciembre];
	}
	
	private function promosactivar(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromos("1"));
		$activas = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			$ini = $promociones[$i]->promociones_dateini;
			$date_ini = explode("-",explode(" ",$ini)[0]);
			$hora_ini = explode(":",explode(" ",$ini)[1]);
			$init = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
			$end = Carbon::create($date_ini[0], $date_ini[1], $date_ini[2], $hora_ini[0], $hora_ini[1], $hora_ini[2], 'America/Asuncion');
			if($init->lte($end)){
				$activas[$j] = [
					"id" => base64_encode ( $promociones[$i]->id ),
					"title" => $promociones[$i]->promociones_title,
					"vencida" => $init->lte($end)
				]; $j++;
			}
		}

		return $activas;
	}

	private function promovencidas(){
		$actual = Carbon::now("America/Asuncion",-4);
		$promociones = json_decode(Promociones::getPromos("1"));
		$vencidas = [];
		for($i = 0, $j = 0; $i < count($promociones); $i++){
			$fin = $promociones[$i]->promociones_datefin;
			$date_fin = explode("-",explode(" ",$fin)[0]);
			$hora_fin = explode(":",explode(" ",$fin)[1]);
			$init = Carbon::create($actual->year, $actual->month, $actual->day, $actual->hour, $actual->minute, $actual->second, 'America/Asuncion');
			$end = Carbon::create($date_fin[0], $date_fin[1], $date_fin[2], $hora_fin[0], $hora_fin[1], $hora_fin[2], 'America/Asuncion');
			if($init->gte($end)){
				$vencidas[$j] = [
					"id" => base64_encode ( $promociones[$i]->id ),
					"title" => $promociones[$i]->promociones_title,
					"vencida" => $init->gte($end)
				]; $j++;

				Promociones::desactivar( $promociones[$i]->id );
			}
		}

		return $vencidas;
	}
	
	public function dashboard(){
		$this->usuario = Session::get("usuario");
		//ESTADÍSTICA: NAVEGADORES
		$firefox =  count(DB::table("tracker_agents")->where("browser","Firefox")->orderBy("browser","desc")->select("browser")->get());
		$android =  count(DB::table("tracker_agents")->where("browser","Chrome Mobile")->orderBy("browser","desc")->select("browser")->get());
		$edge =  count(DB::table("tracker_agents")->where("browser","Edge")->orderBy("browser","desc")->select("browser")->get());
		$chrome =  count(DB::table("tracker_agents")->where("browser","Chrome")->orderBy("browser","desc")->select("browser")->get());
		$browser = array($firefox,$chrome,$android,$edge);
		$navegadores = 0;
		if($browser[0]=="0" && $browser[1]=="0" && $browser[2]=="0" && $browser[3]=="0" && $browser[4]=="0") $navegadores = 1;

		$home =  count(DB::table("tracker_route_paths")->where("path","/")->orderBy("path","desc")->select("path")->get());
		$historia =  count(DB::table("tracker_route_paths")->where("path","Historia")->orderBy("path","desc")->select("path")->get());
		$habitaciones =  count(DB::table("tracker_route_paths")->where("path","Habitaciones")->orderBy("path","desc")->select("path")->get());
		$restaurante =  count(DB::table("tracker_route_paths")->where("path","Restaurante")->orderBy("path","desc")->select("path")->get());
		$contacto = count(DB::table("tracker_route_paths")->where("path","Home/Contacto")->orderBy("path","desc")->select("path")->get());
		$secbrowser = array($home,$historia,$habitaciones,$restaurante,$contacto);
		$secciones = 0;
		if($secbrowser[0]=="0" && $secbrowser[1]=="0" && $secbrowser[2]=="0" && $secbrowser[3]=="0" && $secbrowser[4]=="0") $secciones = 1;

		$cpu =  count(DB::table("tracker_devices")->where("kind","Computer")->orderBy("kind","desc")->select("kind")->get());
		$phone =  count(DB::table("tracker_devices")->where("kind","Phone")->orderBy("kind","desc")->select("kind")->get());
		$otrosdevice =  count(DB::table("tracker_devices")->whereNotIn("kind",["Computer","Phone"])->orderBy("kind","desc")->select("kind")->get());
		$devices = array($cpu,$phone,$otrosdevice);
		$dispositivos = 0;
		if($devices[0]=="0" && $devices[1]=="0" && $devices[2]=="0") $dispositivos = 1;

		/*$slider = json_decode(Picture::where("picture_status","17")->value("picture_path"));
		$datos = NULL;
		for($i = 1, $j = 0; $i <= $slider[0]->cantidad; $i++){
			$datos[$j++] = array(substr($slider[$i]->img,strripos($slider[$i]->img,"/")+1,strlen($slider[$i]->img)),$slider[$i]->tipo,url($slider[$i]->img));
		}*/

		$mes = substr(Carbon::now(-4),5,2);
		$mes = ($mes==12?($mes-7):($mes==1?$mes:$mes-7));
		$contador_visitas = $this->visitas($mes,DB::table("tracker_sessions")->select("uuid")->select("uuid","created_at")->orderBy("created_at","desc")->get());
		$this->promovencidas();
		return view("backend.dashboard")->with([
			"usuario" => $this->usuario,
			"usuarios" => User::where("user_role",">",$this->usuario->user_role)->take(10)->get(),
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"size" => User::where("user_role",">","0")->count(),
			"npicture" => Picture::where("picture_status","3")->whereNotIn("picture_path",["img/default.jpg"])->count(),
			"nrestaurante" => Picture::where("picture_status","2")->whereNotIn("picture_path",["img/default.jpg"])->count(),
			"size_rooms" => Rooms::whereNotIn("rooms_codigo",["default.jpg"])->count(),
			"rooms" => Rooms::whereNotIn("rooms_codigo",["default.jpg"])->orderby("rooms_positions","asc")->get(),
			"browser" => json_encode($browser),
			"secciones" => json_encode($secbrowser),
			"devices" => json_encode($devices),
			/*"sconfirmar" => $secciones,
			"dconfirmar" => $dispositivos,
			"nconfirmar" => $navegadores,*/
			"proximas" => $this->promosactivar(),
			"rangos" => Promociones::promocionesrango(),
			"contador" => json_encode($contador_visitas),
			/*"ususystem" => User::where("user_role",">","0")->orderBy('created_at', 'desc')->take(5)->get(),
			"textossystem" => Text::where("text_origen",">=","0")->where("text_origen","<=","2")->where("text_status","=","1")->orderBy('created_at', 'desc')->take(6)->get(),
			"datos" => $datos*/
		]);
	}

	public function fbloqueado(){
		User::where("user_email",Session::get("usuario")["user_email"])->update([
		  "user_token_status" => "1"
		]);

		Text::where("text_status",1)->update([
			"text_status" => "0"
		]);

		return view("lock.lock")->with(["bloqueado" => "0"]);
	}

	public function bloqueado(){
		User::where("user_email",Session::get("usuario")["user_email"])->update([
		  "user_token_status" => "1"
		]);

		Text::where("text_status",1)->update([
			"text_status" => "0"
		]);

		return view("lock.lock")->with(["bloqueado" => "1"]);
	}

	//EDIT
	public function editbannersuperior(Request $request){//EDITAR-BANNER SUPERIOR
		$this->usuario = Session::get("usuario");
		return view("backend.editarsuperior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => $request->input("estado"),//HABITACIÓN
			"imagenes" => $request->all()["pictureid"],
			"ruta" => $request->all()["ruta"],
			"size" => count($request->all()["pictureid"])
		]);
	}

	public function editbannerinferior(Request $request){
		$this->usuario = Session::get("usuario");
		return view("backend.editarinferior",[
			"usuario" => $this->usuario,
			"email" => $this->usuario->user_email,
			"nombre" => $this->usuario->user_name,
			"login" => $this->usuario->user_login,
			"estado" => $request->input("estado"),//HABITACIÓN
			"imagenes" => $request->all()["pictureid"],
			"ruta" => $request->all()["ruta"],
			"size" => count($request->all()["pictureid"])
		]);
	}
	
	public function editpromo(Request $request){
		return view("backend.editar_promo",[
			"promocion" => Promociones::getPromoID(base64_decode($request->input("promo_id"))),
			"ruta" => url('admin/verpromos')
		]);
	}
	
	public function editarsecuslider(Request $request){
		$texto = new  Text();
		$texto_finder = json_decode($texto->searchtext($texto,"2","0")->text_json);
		$imagenes_finder = json_decode(Picture::where("picture_status","20")->value("picture_path"));

		return view("backend.editar_secuslider",[
			"texto" => $texto_finder[0],
			"imagenes" => $imagenes_finder
		]);
	}

	public function editarslider(Request $request){//EDITAR-SLIDER
		$jsonGaleria = json_decode(Picture::getRestauranteSlider());
		$jsonGaleriaprincipal = json_decode(Picture::getHomeSlider());

		if($request->input("modo")=="2"){
			$jsonGaleriaprincipal = json_decode(Picture::getHotelSlider());

		}

		$galeria = json_decode($jsonGaleria->picture_path);
		$galeriaprincipal = json_decode($jsonGaleriaprincipal->picture_path);
		return view("backend.editar_slider",[
			"slider" => $galeria->posicion,
			"texto" => json_decode(Text::where("text_origen","0")->where("text_section","3")->value("text_json")),
			"sliderprincipal" => $galeriaprincipal->posicion,
			"modo" => $request->input("modo")
		]);
	}
}
