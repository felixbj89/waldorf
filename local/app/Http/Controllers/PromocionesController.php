<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Promociones;

class PromocionesController extends Controller
{
    public function addpromo(Request $request){
		$estado = Promociones::addpromo($request->all());
		if($estado==1){//SE INSERTO SIN PROBLEMAS.
			return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
		}else if($estado==3){
			return redirect()->back()->with(["message" => "No puede agregar más de una promoción como favorito."]);
		}else{//NO SE INSERTO
			return redirect()->back()->with(["message" => "No se pudo crear su promoción."]);
		}
	}
	
	public function removerpromo(Request $request){
		$estado = Promociones::removerpromo(base64_decode($request->input("promo_id")));
		return response()->json([
			"estado" => $estado
		]);
	}
	
	public function changepromo(Request $request){
		$estado = Promociones::changepromo(base64_decode($request->input("promo_id")));
		return response()->json([
			"estado" => $estado,
			"status" => Promociones::where("id",base64_decode($request->input("promo_id")))->value("promociones_status")
		]);
	}
	
	public function editarpromo(Request $request){
		$estado = Promociones::editpromo($request->all());
		if($estado==1){
			return redirect()->back()->with(["message" => "Los cambios han sido guardados satisfactoriamente"]);
		}else if($estado==3){
			return redirect()->back()->with(["message" => "No puede agregar más de una promoción como favorito."]);
		}else if($estado==4){
			return redirect()->back()->with(["message" => "Ya existe una promoción señalada como favorito."]);
		}else{//NO SE INSERTO
			return redirect()->back()->with(["message" => "No se pudo modificar su promoción."]);
		}
	}
}
