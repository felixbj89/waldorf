<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DashboardController;
use App\User;
use Session;

class LoginController extends Controller{

	private $usuario, $dash;

	public function reLogin(Request $request){
		$this->usuario = new User();
		$this->usuario = ($this->usuario->recheck($this->usuario,$request->all()));
		if($this->usuario!=NULL){
			Session::set("usuario",$this->usuario);
			return redirect()->action("DashboardController@dashboard");
		}else{
			return redirect()->back();
		}
	}

	public function index(Request $request){
		$this->usuario = new User();
		$this->usuario = ($this->usuario->check($this->usuario,$request->all()));
		if($this->usuario!=NULL){
			Session::set("usuario",$this->usuario);
			return redirect()->action("DashboardController@dashboard");
		}else{
			Session::flash("error","Credenciales Incorrectas, Intente nuevamente.");
			return redirect()->back();
		}
	}
}
