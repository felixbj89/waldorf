<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller{

    public function remove(Request $request){
      $usuario = new User();
      if($usuario->deleteUser($usuario,$request->input("email"))){
        return response()->json([
          "resultado" => "1"
        ]);
      }else{
        return response()->json([
          "resultado" => "0"
        ]);
      }
    }

    public function editregistrado(Request $request){
      $usuario = new User();
      return $usuario->editprofile($usuario,$request->input("formulario"),$request->input("estado"));

      if($usuario->editprofile($usuario,$request->input("formulario"),$request->input("estado"))){
        return response()->json([
    			"resultado" => "1"
    		]);
      }else{
        return response()->json([
    			"resultado" => "0"
    		]);
      }
    }

    public function editobj(Request $request){
      $usuario = User::where("user_role",">","0")->where("user_email","=",$request->input("email"))->first();
      return response()->json([
          "usuario" => $usuario,
          "resultado" => "1"
      ]);
    }

    public function edit(Request $request){
  		$usuario = new User();
      $estado = $usuario->editUser($request->input("formulario"));
  		if($estado==1){
  			return response()->json([
      			"resultado" => "1",
  				  "usuario" => User::where("user_email",$request->input("formulario")[3]["value"])->first()
  			]);
  		}else if($estado==2){
        return response()->json([
            "resultado" => "2",
        ]);
      }
    }

    public function create(Request $request){
      $usuario = new User();
      if($usuario->createUser($usuario,$request->input("formulario"))){

        return response()->json([
          "resultado" => "1",
          "size" => User::where("user_role",">","0")->count()
        ]);

      }else{
        return response()->json([
    			"resultado" => "0"
    		]);
      }
    }
}
