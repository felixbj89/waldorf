<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\SuperiorRequest;
use App\Http\Requests\InferiorRequest;
use App\Http\Requests\ContactImageRequest;
use App\Http\Requests\SliderRequest;
use App\Picture;
use App\Text;
use App\Rooms;
use Lang;
use Carbon\Carbon;
use Session;

class PictureController extends Controller{

    /*private function update($picture,$estado){//actualizo la imagen activa para que vaya al historial.
      if($picture->updatePicture($picture,$estado)){
        return 1;
      }return 0;
    }*/

    public function verhabpicture(Request $request){
		$rooms = new Rooms();
		if($rooms = ($rooms->seerooms($rooms,$request->all()))){
			return response()->json([
				"respuesta" => "1",
				"cuarto" => $rooms
			]);
		}

		return response()->json([
          "respuesta" => "0"
		]);
    }

    /*public function modifypictures(Request $request){
      $cuartos = Rooms::where("rooms_positions",$request->input("selectcuartospicture"))->first();
      if(count($cuartos)=="1"){
        $estado = $cuartos->modifyroomspicture($cuartos,$request->all());
        if($estado==1){
          return redirect()->back()->with(["message" => "Se pudo modificar la habitación"]);
        }else if($estado==2){
          return redirect()->back()->with(["message" => "No hubo cambios en la habitación"]);
        }
      }else{
        return redirect()->back()->with(["message" => "No se pudo modificar la habitación"]);
      }
    }*/

    /*public function remover(Request $request){
      $picture = new Picture();
      if($picture->removerPicture($request->input("tipo"),$request->input("picture"),$picture)){
        return response()->json([
            "resultado" => "1",
            "message" => "Se pudo remover la imagen"
        ]);
      }else{
        return response()->json([
            "resultado" => "0",
            "message" => "No se pudo remover la imagen"
        ]);
      }
    }*/

    /*private function searchpicture($estado){//si existe más de una imagen con el mismo estado
      $picture = new Picture();
      if($this->update($picture,$estado)){
        return $picture->search($picture,$estado);//BUSCO SUPERIORES O INFERIORES
      }return 0;
    }*/

    /*private function obtener_propiedades($imagen,$opc){
  		list($ancho, $alto, $tipo, $atributos) = getimagesize($imagen);
  		switch($opc){
  			case 1:
  				return $ancho;
  			break;
  			case 2:
  				return $alto;
  			break;
  		}
  	}*/

    /*public function changepicture(Request $request){
      $id = Picture::where("picture_date",$request->input("picture"))->value("id");//LA QUE SE ACTIVARA
      if($request->input("tipo")=="0" || $request->input("tipo")=="1"){//superior
        $idinactivo = Picture::where("picture_status",($request->input("tipo")=="0"?"2":"1"))->value("id");//PASARA A ESTAR INACTIVO EN EL HISTORIAL
        $picture = new Picture();
        if($picture->offactivo($picture,$idinactivo,3)){
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = ($request->input("tipo")=="0"?"img/restaurante/slider/":"img/habitaciones/slider/");
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);
              Picture::where("id",$id)->update([//ACTIVO UNA IMAGEN DEL HISTORIAL SUPERIOR
                "picture_status" => ($request->input("tipo")=="0"?"2":"1"),
                "picture_path" => $newlocation
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url(Picture::where("id",$idinactivo)->value("picture_path")),
                "status" => Picture::where("id",$idinactivo)->value("id"),
                "date" => Picture::where("id",$idinactivo)->value("picture_date")
              ]);
            }
          }

          return response()->json([
            "resultado" => "0"
          ]);
        }else{//EN CASO DE SE USE EL HISTORIAL PARA UNA SECCIÓN QUE NO ESTE INICIALIZADA
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = ($request->input("tipo")=="0"?"img/restaurante/slider/":"img/habitaciones/slider/");
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);

              Picture::where("id",$id)->update([//ACTIVO UNA IMAGEN DEL HISTORIAL SUPERIOR
                "picture_status" => ($request->input("tipo")=="0"?"2":"1"),
                "picture_path" => $newlocation,
                "picture_nombre" => "Hotel Waldorf",
                "picture_date" => Carbon::now(-4),
                "user_id" => Session::get("usuario")->id
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url("img/default.jpg"),
                "status" => Picture::where("id",$picture->picture_status)->value("id"),
                "date" => Picture::where("id",$picture->picture_date)->value("picture_date")
              ]);
            }
          }
        }
      }else if($request->input("tipo")=="2" || $request->input("tipo")=="3"){//inferior
        $idinactivo = Picture::where("picture_status",($request->input("tipo")=="2"?"6":"5"))->value("id");//PASARA A ESTAR INACTIVO EN EL HISTORIAL
        $picture = new Picture();
        if($picture->offactivo($picture,$idinactivo,7)){
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = ($request->input("tipo")=="2"?"img/restaurante/slider_reserva/":"img/habitaciones/slider_reserva/");
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);
              Picture::where("id",$id)->update([//ACTIVO UNA IMAGEN DEL HISTORIAL INFERIOR
                "picture_status" => ($request->input("tipo")=="2"?"6":"5"),
                "picture_path" => $newlocation
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url(Picture::where("id",$idinactivo)->value("picture_path")),
                "status" => Picture::where("id",$idinactivo)->value("id"),
                "date" => Picture::where("id",$idinactivo)->value("picture_date")
              ]);
            }
          }

          return response()->json([
            "resultado" => "0"
          ]);
        }else{//EN CASO DE SE USE EL HISTORIAL PARA UNA SECCIÓN QUE NO ESTE INICIALIZADA
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = ($request->input("tipo")=="2"?"img/restaurante/slider_reserva/":"img/habitaciones/slider_reserva/");
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);

              Picture::where("id",$id)->update([//ACTIVO UNA IMAGEN DEL HISTORIAL SUPERIOR
                "picture_status" => ($request->input("tipo")=="2"?"6":"5"),
                "picture_path" => $newlocation,
                "picture_nombre" => "Hotel Waldorf",
                "picture_date" => Carbon::now(-4),
                "user_id" => Session::get("usuario")->id
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url("img/default.jpg"),
                "status" => Picture::where("id",$picture->picture_status)->value("id"),
                "date" => Picture::where("id",$picture->picture_date)->value("picture_date")
              ]);
            }
          }
        }
      }else if($request->input("tipo")=="4"){//historia
        $idinactivo = Picture::where("picture_status",("8"))->value("id");//PASARA A ESTAR INACTIVO EN EL HISTORIAL
        $picture = new Picture();
        if($picture->offactivo($picture,$idinactivo,9)){//ACTIVO UNA IMAGEN DEL HISTORIAL EN HISTORIA
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = "img/home/historia/";
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);

              Picture::where("id",$id)->update([
                "picture_status" => "8",
                "picture_path" => $newlocation
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url(Picture::where("id",$idinactivo)->value("picture_path")),
                "status" => Picture::where("id",$idinactivo)->value("id"),
                "date" => Picture::where("id",$idinactivo)->value("picture_date")
              ]);
            }
          }
        }
      }else if($request->input("tipo")=="5"){//contacto
        $idinactivo = Picture::where("picture_status",("13"))->value("id");//PASARA A ESTAR INACTIVO EN EL HISTORIAL

        $picture = new Picture();
        if($picture->offactivo($picture,$idinactivo,14)){//ACTIVO UNA IMAGEN DEL HISTORIAL EN HISTORIA
          $copy = $picture->where("id",$id)->value("picture_path");
          $ruta = "img/home/contacto/";
          $newlocation = $ruta.substr($copy,strripos($copy,"/")+1,strlen($copy));;
          if(file_exists($copy)){
            if(copy($copy,$newlocation)){
              unlink($copy);

              Picture::where("id",$id)->update([
                "picture_status" => "13",
                "picture_path" => $newlocation
              ]);

              return response()->json([
                "resultado" => "1",
                "imagen" => url(Picture::where("id",$idinactivo)->value("picture_path")),
                "status" => Picture::where("id",$idinactivo)->value("id"),
                "date" => Picture::where("id",$idinactivo)->value("picture_date")
              ]);
            }
          }
        }
      }

      return response()->json([
        "resultado" => "0"
      ]);
    }*/

    /*public function editarslider(Request $request){
      $picture = new Picture(); $pic = NULL;
      if($request->input("modo")=="0" && $picture->search($picture,17) && $request->input("origen")=="2"){//SPLIDER PPAL
        $slider = Picture::where("picture_status",17)->first();
        $encontrado = 17;
        if(count($slider)==1){
            $encontrado = 17;
        }

        return response()->json([
            "resultado" => "1",
            "nombre" => $slider->picture_nombre,
            "slider" => Picture::where("picture_status",$encontrado)->value("picture_path")
        ]);
      }else if($request->input("modo")=="1" && $picture->search($picture,18)){
        $slider = Picture::where("picture_status",18)->first();
        $encontrado = 18;
        $texto = NULL;
        if(count($slider)==1){
            $encontrado = 18;
            $texto = Text::where("text_section","3")->where("text_origen","0")->value("text_json");
        }

        return response()->json([
            "resultado" => "1",
            "nombre" => $slider->picture_nombre,
            "slider" => Picture::where("picture_status",$encontrado)->value("picture_path"),
            "texto" => $texto
        ]);
      }else if($request->input("modo")=="9" && $picture->search($picture,20)){
          $slider = Picture::where("picture_status",20)->first();
          $encontrado = 20;
          $texto = NULL;
          if(count($slider)==1){
              $encontrado = 20;
              $texto = Text::where("text_section","0")->where("text_origen","2")->where("text_status","2")->value("text_json");
          }

          return response()->json([
            "resultado" => "1",
            "nombre" => $slider->picture_nombre,
            "slider" => Picture::where("picture_status",$encontrado)->value("picture_path"),
            "texto" => $texto
        ]);
      }

      return response()->json([
          "resultado" => "0",
      ]);
    }*/
   
    /*public function addcontactus(ContactImageRequest $request){
      $filepic = $request->file('file_contactus');
      if($this->obtener_propiedades($filepic,1)!=1080 && $this->obtener_propiedades($filepic,2)!=400){
        return redirect()->back()->with(["message" => "La imagen no cumple con las medidas requeridas"]);
      }else{
        $picture = new Picture();
        if($this->limit(14)){
          if(!$this->searchpicture(13)){
            if($picture->store($picture,$request->all(),13,$filepic)){
              return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
            }else{
              return redirect()->back()->with(["message" => "No se pudo agregar la imagen"]);
            }
          }
        }return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
      }
    }*/

	//REMVER UNA FOTO PRINCIPAL
	public function removepicture(Request $request){
		$picture = new Picture();
		$estado = $picture->remover_fotoprincipal($request->all(),$request->input("tiposec"));
		return response()->json([
			"estado" => $estado
		]);
	}
	
	//AGREGO UNA FOTO PRINCIPAL
    public function addsuperior(Request $request){
	  $filepic = $request->file('banner_superior');
		if($request->input("tiposec")=="0"){//RESTAURANTE
		  $picture = new Picture();
		  if($picture->limite($picture,"4") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
			$estado = Picture::store_fotoprincipal($request->all(),"2","4");
			if($estado==1){//SE INSERTO SIN PROBLEMAS.
				return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
			}else if($estado==2){
				return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
			}else{//NO SE INSERTO
				return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
			}
		  }else{
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		  }
        }else if($request->input("tiposec")=="2"){//EL HOTEL
			$picture = new Picture();
			if($picture->limite($picture,"12") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
			$estado = Picture::store_fotoprincipal($request->all(),"12","21");
			if($estado==1){//SE INSERTO SIN PROBLEMAS.
				return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
			}else if($estado==2){
				return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
			}else{//NO SE INSERTO
				return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
			}
		  }else{
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		  }
		}else{//HABITACIÓN
          $picture = new Picture();
		  if($picture->limite($picture,"3") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
			$estado = $picture->store_fotoprincipal($request->all(),"1","3");
			if($estado==1){//SE INSERTO SIN PROBLEMAS.
				return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
			}else if($estado==2){
				return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
			}else{//NO SE INSERTO
				return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
			}
		  }else{
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		  }
        }
    }
	
	//AGREGO UNA FOTO SECUNDARIA
	public function addinferior(Request $request){
		if($request->input("tiposec")=="2"){//RESTAURANTE
			$picture = new Picture();
			if($picture->limite($picture,"6") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
				$estado = Picture::store_fotosecundaria($request->all(),"6","8");
				if($estado==1){//SE INSERTO SIN PROBLEMAS.
					return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
				}else if($estado==2){
					return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
				}else{//NO SE INSERTO
					return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
				}
			}else{
				return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
			}
		}else if($request->input("tiposec")=="4"){
			$picture = new Picture();
			if($picture->limite($picture,"14") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
				$estado = Picture::store_fotocontacto($request->all(),"13","14");
				if($estado==1){//SE INSERTO SIN PROBLEMAS.
					return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
				}else if($estado==2){
					return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
				}else{//NO SE INSERTO
					return redirect()->back()->with(["message" => "No se altero el Fondo de Contacto."]);
				}
			}
		}else{
			$picture = new Picture();
			if($picture->limite($picture,"7") <= 6){//EXISTE AÚN ESPACIO PARA ALMACENAR IMÁGENES.
				$estado = Picture::store_fotosecundaria($request->all(),"5","7");
				if($estado==1){//SE INSERTO SIN PROBLEMAS.
					return redirect()->back()->with(["message" => "Registro Satisfactorio"]);
				}else if($estado==2){
					return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
				}else{//NO SE INSERTO
					return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
				}
			}else{
				return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
			}
		}
    }
	
	//EDITA Y ELIMINA A IMAGEN QUE ESTE EN EL HISTORIAL
	public function editsuperior(Request $request){
		$picture = new Picture();
		$estado = $picture->edit_fotoprincipal($request->all(),$request->input("tiposec"));
		if($estado==1){//SE INSERTO SIN PROBLEMAS.
			return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
		}else{//NO SE INSERTO
			return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
		}
	}
	
	//EDITO LAS IMÁGENES INFERIORES
	public function editinferior(Request $request){
		$estado = Picture::edit_fotosecundaria($request->all(),$request->input("tiposec"));
		if($estado==1){//SE INSERTO SIN PROBLEMAS.
			return redirect()->back()->with(["message" => "Guardado Satisfactorio"]);
		}else{//NO SE INSERTO
			return redirect()->back()->with(["message" => "No se altero la Foto Principal."]);
		}
	}
	
	//CAMBIO EL ESTADO A LAS FOTOS PRINCIPALES
	public function changestate(Request $request){
		$picture = new Picture();
		$estado = $picture->changestate($request->all(),$request->input("estado"),$request->input("activo")); 
		return response()->json([
			"estado" => $estado
		]);
	}

	//AGREGO EL SLIDER AL SISTEMA
	public function addslider(Request $request){
		if($request->input("modo")=="0"){
			$estado = Picture::storeslider($request->all(),17);
			if($estado==1){
				return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en la galeria principal."]);
			}
		}else if($request->input("modo")=="2"){
			$estado = Picture::storeslider($request->all(),11);
			if($estado==1){
				return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en la galeria del restaurante."]);
			}
		}else{
			$estado = Text::savegaleria($request->all(),"0","3");
			if($estado==1){
				$estado = Picture::storeslider($request->all(),18);
				if($estado==1){
					return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
				}else{
					return redirect()->back()->with(["message" => "No hubo cambios en la galeria del restaurante."]);
				}
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en el área de la galeria del restaurante."]);
			}
		}
    }

	//CAMBIO LA DATA DEL ÁREA DEL SLIDER EN RESTAURANTE
	public function editslider(Request $request){
		if($request->input("modo")=="0"){
			$estado = Picture::storeslider($request->all(),17);
			if($estado==1){
				return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en la galeria del restaurante."]);
			}
		}else if($request->input("modo")=="2"){
			$estado = Picture::storeslider($request->all(),11);
			if($estado==1){
				return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en la galeria del restaurante."]);
			}
		}else{
			$estado = Text::savegaleria($request->all(),"0","3");
			if($estado==1){
				$estado = Picture::storeslider($request->all(),18);
				if($estado==1){
					return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
				}else{
					return redirect()->back()->with(["message" => "No hubo cambios en la galeria del restaurante."]);
				}
			}else{
				return redirect()->back()->with(["message" => "No hubo cambios en el área de la galeria del restaurante."]);
			}
		}
	}
}
