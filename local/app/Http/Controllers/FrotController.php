<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use App\Picture;
use App\Text;
use App\Promociones;
use File;
use App\Http\Controllers\Controller;
use Session;
use Tracker;
use App\Rooms;
use Artisan;
use Redirect;

class FrotController extends Controller{

	private function picture_active($tipo,$opc){
		$picture = Picture::where("picture_status","=",$tipo)->first();
		if(count($picture)==0 && $opc==0){
			return 4;
		}else if(count($picture)==0 && $opc==1){
			return 10;
		}else{
			return $tipo;
		}
	}
	
	public function exe(){
		\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        ini_set('max_execution_time', 180);
        Artisan::call('config:clear');
        Artisan::call('view:clear');
		Artisan::call('cache:clear');
		Artisan::call('route:clear');
        Session::flash('message-success', 'Clear');
        ini_set('max_execution_time', 60);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('/');
	}
	
	public function backendlogin(){
		$id=""; $ruta = "";
		$text = new Text();
		$json = json_decode($this->obtener_json("0","0"));
		$valores = $this->picture_active(9,1);
        $habcu = $this->obtener_rooms();
		$contacto = $this->picture_active(13,0);
		$jsongastronomia =  json_decode($this->obtener_json("0","1"));
		$jsonslidersecu = json_decode($this->obtener_json("0","2"));
		$promociones = Promociones::construirpromocion();
		$favorito = Promociones::getFavorito();
		$switch = 0;
		if($favorito!=NULL){
			$ruta = url($favorito->promociones_path);
			$switch = 1;
		}
		return view('index')->with([
			"id" => $id,
			"modallogin" => "1",
			"titulohistory" => $json[0]->titulo,
			"subtitulohistory" => $json[0]->subtitulo,
			"textohistory" => $json[0]->texto,
			"usuario" => NULL,
            "cuartos" => $habcu,
			"promociones" => $promociones,
			"titulogastronomia" => $jsongastronomia[0]->titulo,
			"subtitulogastronomia" => $jsongastronomia[0]->subtitulo,
			"textogastronomia" => $jsongastronomia[0]->texto,
			"tituloslidersecu" => $jsonslidersecu[0]->titulo,
			"textoslidersecu" => $jsonslidersecu[0]->texto,
			"urlhistory" => Picture::where("picture_status","=",$valores)->value("picture_path"),
			"nombrehistory" => Picture::where("picture_status","=",$valores)->value("picture_nombre"),
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"ruta" => $ruta,
			"countpromo" => count(Promociones::getPromosAllActive()),
			"modalpromo" => 0
		]);
	}

	public function backendhablogin(){
		$id = "";
		$superior = $this->picture_active(1,0);
		$inferior = $this->picture_active(5,1);
		$jsontextoprincipal =  json_decode($this->obtener_json("2","0"));
		$jsonhabitaciones = json_decode($this->json(5,19));
		$habcu = $this->obtener_rooms();
		$rooms = Rooms::all(); $total = 0;
		foreach($rooms as $cuartos){
			if($cuartos->rooms_codigo=="default.jpg"){
				$total++;
			}
		}
	
		return view('rooms')->with([
				"id" => $id,
				"modallogin" => "1",
				"usuario" => NULL,
				"titulopri" => $jsontextoprincipal[0]->titulo,
				"textopri" => $jsontextoprincipal[0]->texto,
				"notmach" => ($total==10?0:1),
				"habitaciones" => $rooms,
				"total" => $total,
				"cuartos" => $habcu,
				"rooms_codigo" => $rooms,
				"urlsuperior" => Picture::where("picture_status","=",$superior)->value("picture_path"),
				"nombresuperior" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
				"urlinferior" => Picture::where("picture_status","=",$inferior)->value("picture_path"),
				"nombreinferior" => Picture::where("picture_status","=",$inferior)->value("picture_nombre")
		]);
	}

	public function backendrestlogin(){
		$superior = $this->picture_active(2,0);
		$inferior = $this->picture_active(6,1);
		$jsoneventos =  json_decode($this->obtener_json("3","2"));
		$jsongaleria = json_decode($this->obtener_json("3","0"));
        $rooms = $this->obtener_rooms();
		
		return view('restaurat')->with([
			"modallogin" => "1",
			"usuario" => NULL,
            "cuartos" => $rooms,
			"tituloeventos" => $jsoneventos[0]->titulo,
			"substituloeventos" => $jsoneventos[0]->subtitulo,
			"textoseventos" => $jsoneventos[0]->texto,
			"urlsuperior" => Picture::where("picture_status","=",$superior)->value("picture_path"),
			"nombresuperior" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
			"urlinferior" => Picture::where("picture_status","=",$inferior)->value("picture_path"),
			"nombreinferior" => Picture::where("picture_status","=",$inferior)->value("picture_nombre"),
			"titulogaleria" => $jsongaleria[0]->titulo,
			"subtitulogaleria" => $jsongaleria[0]->subtitulo,
			"textogaleria" => $jsongaleria[0]->texto,
			"autorgaleria" => $jsongaleria[0]->autor
		]);
	}

	public function backendconlogin($id){
		$text = new Text();  $ruta = "";
		$json = json_decode($this->obtener_json("0","0"));
		$valores = $this->picture_active(9,1);
		$contacto = $this->picture_active(13,0);
		$jsongastronomia =  json_decode($this->obtener_json("0","1"));
		$jsonslidersecu = json_decode($this->obtener_json("0","2"));
		$rooms = $this->obtener_rooms();
		$promociones = Promociones::construirpromocion();
		$favorito = Promociones::getFavorito();
		$switch = 0;
		if($favorito!=NULL){
			$ruta = url($favorito->promociones_path);
			$switch = 1;
		}
		
		return view('index')->with([
			"id" => $id,
			"modallogin" => "1",
			"usuario" => (Session::has("usuario")?Session::get("usuario"):NULL),
			"titulohistory" => $json[0]->titulo,
			"subtitulohistory" => $json[0]->subtitulo,
			"textohistory" => $json[0]->texto,
			"titulogastronomia" => $jsongastronomia[0]->titulo,
			"subtitulogastronomia" => $jsongastronomia[0]->subtitulo,
			"textogastronomia" => $jsongastronomia[0]->texto,
			"tituloslidersecu" => $jsonslidersecu[0]->titulo,
			"textoslidersecu" => $jsonslidersecu[0]->texto,
            "cuartos" => $rooms,
			"promociones" => $promociones,
			"urlhistory" => Picture::where("picture_status","=",$valores)->value("picture_path"),
			"nombrehistory" => Picture::where("picture_status","=",$valores)->value("picture_nombre"),
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"ruta" => $ruta,
			"countpromo" => count(Promociones::getPromosAllActive())
		]);
	}

	private function obtener_json($section,$estado){
		return Text::where("text_origen",$estado)->where("text_section",$section)->value("text_json");
	}

	private function obtener_rooms(){
		return Rooms::whereNotIn("rooms_thumbnail",["img/habdefault.jpg"])->take(5)->get();
	}
		
	public function index (){
		$text = new Text();
		$json = json_decode($this->obtener_json("0","0"));
		$id=""; $ruta = "";
		$valores = $this->picture_active(9,1);
		$contacto = $this->picture_active(13,0);
		$picture = $this->picture_active(15,0);
		$jsongastronomia =  json_decode($this->obtener_json("0","1"));
		$jsonslidersecu = json_decode($this->obtener_json("0","2"));
		$rooms = $this->obtener_rooms();
		$promociones = Promociones::construirpromocion();
		$favorito = Promociones::getFavorito();
		$switch = 0;
		if($favorito!=NULL){
			$ruta = url($favorito->promociones_path);
			$switch = 1;
		}
		
		return view('index')->with([
			"id" => $id,
			"modallogin" => "0",
			"usuario" => (Session::has("usuario")?Session::get("usuario"):NULL),
			"titulohistory" => $json[0]->titulo,
			"subtitulohistory" => $json[0]->subtitulo,
			"textohistory" => $json[0]->texto,
			"tituloslidersecu" => $jsonslidersecu[0]->titulo,
			"textoslidersecu" => $jsonslidersecu[0]->texto,
			"titulogastronomia" => $jsongastronomia[0]->titulo,
			"subtitulogastronomia" => $jsongastronomia[0]->subtitulo,
			"textogastronomia" => $jsongastronomia[0]->texto,
			"urlhistory" => Picture::where("picture_status","=",$valores)->value("picture_path"),
			"nombrehistory" => Picture::where("picture_status","=",$valores)->value("picture_nombre"),
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"cuartos" => $rooms,
			"promociones" => $promociones,
			"ruta" => $ruta,
			"countpromo" => count(Promociones::getPromosAllActive()),
			"modalpromo" => $switch
		]);
	}

	public function index_ ($id){
		$text = new Text();  $ruta = "";
		$json = json_decode($this->obtener_json("0","0"));
		$valores = $this->picture_active(9,1);
		$contacto = $this->picture_active(13,0);
		$jsongastronomia =  json_decode($this->obtener_json("0","1"));
		$jsonslidersecu = json_decode($this->obtener_json("0","2"));
		$habitaciones = $this->obtener_rooms();
		$promociones = Promociones::construirpromocion();
		$favorito = Promociones::getFavorito();
		$switch = 0;
		if($favorito!=NULL){
			$ruta = url($favorito->promociones_path);
			$switch = 1;
		}
		return view('index')->with([
			"id" => $id,
			"modallogin" => "0",
			"usuario" => (Session::has("usuario")?Session::get("usuario"):NULL),
			"titulohistory" => $json[0]->titulo,
			"subtitulohistory" => $json[0]->subtitulo,
			"textohistory" => $json[0]->texto,
			"tituloslidersecu" => $jsonslidersecu[0]->titulo,
			"textoslidersecu" => $jsonslidersecu[0]->texto,
			"titulogastronomia" => $jsongastronomia[0]->titulo,
			"subtitulogastronomia" => $jsongastronomia[0]->subtitulo,
			"textogastronomia" => $jsongastronomia[0]->texto,
			"cuartos" => $habitaciones,
			"urlhistory" => Picture::where("picture_status","=",$valores)->value("picture_path"),
			"nombrehistory" => Picture::where("picture_status","=",$valores)->value("picture_nombre"),
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"promociones" => $promociones,
			"ruta" => $ruta,
			"countpromo" => count(Promociones::getPromosAllActive()),
			"modalpromo" => $switch
		]);
	}

	public function habitaciones (){
		$id="";
		$superior = $this->picture_active(1,0);
		$inferior = $this->picture_active(5,1);
		$jsontextoprincipal =  json_decode($this->obtener_json("2","0"));
		$jsonhabitaciones = json_decode($this->json(5,19));
		$cuhabitanciones = $this->obtener_rooms();
		$rooms = Rooms::all(); $total = 0;
		foreach($rooms as $cuartos){
			if($cuartos->rooms_codigo=="default.jpg"){
				$total++;
			}
		}
				
		return view('rooms')->with([
			"id" => $id,
			"usuario" => NULL,
			"modallogin" => "0",
			"titulopri" => $jsontextoprincipal[0]->titulo,
			"textopri" => $jsontextoprincipal[0]->texto,
			"notmach" => ($total==10?0:1),
			"habitaciones" => $rooms,
			"total" => $total,
			"cuartos" => $cuhabitanciones,
			"urlsuperior" => Picture::where("picture_status","=",$superior)->value("picture_path"),
			"nombresuperior" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
			"urlinferior" => Picture::where("picture_status","=",$inferior)->value("picture_path"),
			"nombreinferior" => Picture::where("picture_status","=",$inferior)->value("picture_nombre")
		]);
	}

	public function habitaciones_ ($id){
		$superior = $this->picture_active(1,0);
		$jsontextoprincipal =  json_decode($this->obtener_json("2","0"));
		$inferior = $this->picture_active(5,1);
		//$jsonhabitaciones = json_decode($this->json(5,19));
		$habitaciones = $this->obtener_rooms();
		$rooms = Rooms::all(); $total = 0;
		foreach($rooms as $cuartos){
			if($cuartos->rooms_codigo=="default.jpg"){
				$total++;
			}
		}
		
		$hab = Rooms::all();
		
		return view('rooms')->with([
			"id" => $id,
			"usuario" => NULL,
			"modallogin" => "0",
			"titulopri" => $jsontextoprincipal[0]->titulo,
			"textopri" => $jsontextoprincipal[0]->texto,
			"notmach" => ($total==10?0:1),
			"habitaciones" => $hab,
			"total" => $total,
			"cuartos" => $habitaciones,
			"urlsuperior" => Picture::where("picture_status","=",$superior)->value("picture_path"),
			"nombresuperior" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
			"urlinferior" => Picture::where("picture_status","=",$inferior)->value("picture_path"),
			"nombreinferior" => Picture::where("picture_status","=",$inferior)->value("picture_nombre")
		]);
	}

    public function restaurantes (){
		$superior = $this->picture_active(2,0);
		$inferior = $this->picture_active(6,1);
		$jsoneventos = json_decode($this->obtener_json("3","2"));
		$jsongaleria = json_decode($this->obtener_json("3","0"));
		$habitaciones = $this->obtener_rooms();
		return view('restaurat')->with([
			"usuario" => NULL,
			"modallogin" => "0",
			"tituloeventos" => $jsoneventos[0]->titulo,
			"substituloeventos" => $jsoneventos[0]->subtitulo,
			"textoseventos" => $jsoneventos[0]->texto,
			"titulogaleria" => $jsongaleria[0]->titulo,
			"subtitulogaleria" => $jsongaleria[0]->subtitulo,
			"textogaleria" => $jsongaleria[0]->texto,
			"autorgaleria" => $jsongaleria[0]->autor,
			"cuartos" => $habitaciones,
			"urlsuperior" => Picture::where("picture_status","=",$superior)->value("picture_path"),
			"nombresuperior" => Picture::where("picture_status","=",$superior)->value("picture_nombre"),
			"urlinferior" => Picture::where("picture_status","=",$inferior)->value("picture_path"),
			"nombreinferior" => Picture::where("picture_status","=",$inferior)->value("picture_nombre")
		]);
	}

	public function habitacionGallery(){//MODAL-THUMB
		$cuartos = Rooms::where("rooms_positions",$_GET['hab'])->first();
		if(count($cuartos)==1){
			$habType =  $_GET['hab'];
			$habitaciones = json_decode($cuartos->rooms_especificaciones);
			$res = "";
			for($i=0;$i<6;$i++){
				$res .= "<a class=\"imageGalligbox\" href=\"javascript:$.fn.ModalToggle('".$_GET['hab']."','".$i."')\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$habitaciones[1]->thumbnails[$i].'').");\"></a>";
			}return $res;
		}return NULL;
	}

    public function habitacionDetails(){
		$cuartos = Rooms::where("rooms_positions",$_GET['hab'])->first();
		if(count($cuartos)==1){
			$habType =  $_GET['hab'];
			$habitaciones = json_decode($cuartos->rooms_especificaciones);
			$grid = $chkIN = $chkOUT = $capMax = $Ldetails = $titulo = "";
			$res = NULL;
			$titulo = $habitaciones[1]->titulo;
			$res = array('titulo' => $titulo);
			$capMax = $habitaciones[1]->capacidad." pax";
			$res = array_add($res,'capMax',$capMax);
			$chkIN = $habitaciones[1]->in;
			$res = array_add($res,'chkIN',$chkIN);
			$chkOUT = $habitaciones[1]->out;
			$res = array_add($res,'chkOUT',$chkOUT);
			$Ldetails = "";
			foreach($habitaciones[1]->especificaciones as $habi){
				$Ldetails .= $habi."<br/>";
			}

			$img = array();
			for($i=0;$i<6;$i++){
				$img = array_add($img,'img'.$i,$habitaciones[1]->thumbnails[$i]);
			}

			$res = array_add($res,'lista',$Ldetails);
			$grid = "<div class=\"gridHabDet-item-width-2 gridHabDet-item-height-2 gridHabDetG1\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img0"].'').");\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width-3 gridHabDet-item-height gridHabDetG1\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img1"].'').");\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width-5 gridHabDet-item-height gridHabDetG2\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img3"].'').");\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width gridHabDet-item-height\" style=\"position:absolute;top:0px;right:0px;\"><div class=\"gridHabDet-glyphicon\"><img src=\"".url('img/left-arrow.svg')."\" class=\"img-responsive img-flechas glyphicon-next\"></div><div class=\"gridHabDet-glyphicon\"><img src=\"".url('img/right-arrow.svg')."\" class=\"img-responsive img-flechas glyphicon-next\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width-4 gridHabDet-item-height gridHabDetG1\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img2"].'').");\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width-6 gridHabDet-item-height gridHabDetG2\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img4"].'').");\"></div></div>";
			$grid .= "<div class=\"gridHabDet-item-width-3 gridHabDet-item-height gridHabDetG2\"><div class=\"gridHabDet-img\" style=\"background-image:url(".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$img["img5"].'').");\"></div></div>";
			$res = array_add($res,'grid',$grid);

			return $res;
		}return NULL;
	}

	public function habitacionTypeGallery(){
		$cuartos = Rooms::where("rooms_positions",$_GET['hab'])->first();
		if(count($cuartos)==1){
			$habType =  $_GET['hab'];
			$habitaciones = json_decode($cuartos->rooms_especificaciones);
			$res = "";
			for($i=0;$i<6;$i++){
				$res .= "<div class=\"item\" id=\"".$i."\"><table class=\"TitemLightBox\"><tr><td><img class=\"modal-content img-responsive img-rounded ModalImgBox\" src=\"".url('img/habitaciones/habitaciones/'.$_GET['hab'].'/thumbnails/'.$habitaciones[1]->thumbnails[$i].'')."\"></td></tr></table></div>";
			}
			return $res;
		}return NULL;
	}

	public function json($opc,$tipo){
		if($opc==1 && $tipo == 15 ){
			$picture = Picture::where("picture_status",$tipo)->value("picture_path");
			return $picture;
		}else if($opc==2 && $tipo==16){
			$picture = Picture::where("picture_status",$tipo)->value("picture_path");
			return $picture;
		}else if($opc==3 && $tipo==17){
			$picture = Picture::where("picture_status",$tipo)->value("picture_path");
			return $picture;
		}else if($opc==4 && $tipo==18){
			$picture = Picture::where("picture_status",$tipo)->value("picture_path");
			return $picture;
		}else if($opc==5 && $tipo==19){
			$picture = Rooms::where("rooms_galeria",$tipo)->value("rooms_especificaciones");
			return $picture;
		}else if($opc==6 && $tipo==20){
			$picture = Picture::where("picture_status",$tipo)->value("picture_path");
			return $picture;
		}
	}

	private function buscarpos($restaurante,$tipo,$opc){
		$size = ($opc==0?4:2);
		for($i = 0; $i < $size; $i++){
			if($restaurante[$i]->tipo==$tipo){
				return $i;
			}
		}return -1;
	}

	private function insertarpos($imgR,$restaurante,$pos,$opc){
		$posjson = $this->buscarpos($restaurante,($pos==0?1:($pos==1?2:($pos==2?3:4))),$opc);
		foreach($imgR as $key => $value){
			$posicion = substr($key,strlen($key)-1,strlen($key));
			if($posicion==$pos){
				if($opc==0){
					array_set($imgR,$key,"<div class=\"grid-item-img\" style=\"background-image:url('".url($restaurante[$posjson]->img)."')\"></div>");
				}else if($opc==1)
					array_set($imgR,$key,"<div class=\"EventosGastronomicos\" style=\"background-image:url('".url($restaurante[$posjson]->img)."')\"></div>");
			}
		}return $imgR;
	}
	
	public function IndexImgContent(){
		$habitaciones = json_decode($this->json(6,20));
		$restaurante = json_decode($this->json(1,15));
		$jsonGaleria = json_decode(Picture::getHomeSlider());
		$galeria = json_decode($jsonGaleria->picture_path);
		
		$Slider = $imgR = $imgH = $thumbnailsH = $res = $cadena = "";

		for($i = 0; $i < 5; $i++){//INCIAILIZAR GASTRONOMIA
			if($i < 4){
				if($i==0){
					$imgR = array('imgR'.$i => "<div class=\"grid-item-img\" style=\"background-image:url('img/default.jpg')\"></div>");
				}else{
					$imgR = array_add($imgR,'imgR'.$i,"<div class=\"grid-item-img\" style=\"background-image:url('img/default.jpg')\"></div>");
				}
			}
		}

		$imgR = $this->insertarpos($imgR,$restaurante,0,0);
		$imgR = $this->insertarpos($imgR,$restaurante,1,0);
		$imgR = $this->insertarpos($imgR,$restaurante,2,0);
		$imgR = $this->insertarpos($imgR,$restaurante,3,0);
	
		if($galeria->cantidad==0){
			$cadena .= "<div class=\"item active\"><img style=\"margin:0px auto;\" src=\"".url($galeria->posicion[0])."\" alt=\"\"></div>";
			$cadena .= "<div class=\"item\"><div class=\"item-topage\" style=\" background-image:url(".url($galeria->posicion[0]).")\"></div></div>";
		}else{
			for($i = 0; $i < count($galeria->posicion); $i++){
				if($galeria->posicion[$i]!="img/home/slider/default.jpg"){
					if($i==0){
						$cadena .= "<div class=\"item active\"><img style=\"margin:0px auto;\" src=\"".url($galeria->posicion[$i])."\" alt=\"\"></div>";
					}else{
						$cadena .= "<div class=\"item\"><div class=\"item-topage\" style=\" background-image:url(".url($galeria->posicion[$i]).")\"></div></div>";
					}
				}
			}
		}

		$fileJ = File::get("./data/IndexImg/Habitaciones.json");
		$sliderhab = json_decode($fileJ,true);
		for($clave=0;$clave<5;$clave++){
			if($clave == 0){
				$thumbnailsH .= "<li id=\"indicero\" data-target=\"#Carousel-WaldrofHab\" data-slide-to=\"".$clave."\" style=\" background-image:url('".url($habitaciones[$clave]->img)."')\" class=\"idicator-Hwaldorf\"></li>";
				$imgH .= "<div class=\"item-WaldrofHab item active\" style=\" background-image:url('".url($habitaciones[$clave]->slider)."')\"></div>";
			}else{
				$thumbnailsH .= "<li data-target=\"#Carousel-WaldrofHab\" data-slide-to=\"".$clave."\" style=\" background-image:url('".url($habitaciones[$clave]->img)."')\" class=\"idicator-Hwaldorf\"></li>";
				$imgH .= "<div class=\"item-WaldrofHab item\" style=\" background-image:url('".url($habitaciones[$clave]->slider)."')\"></div>";
			}
		}

		$res = array('thumbnailsHabi' => $thumbnailsH);
		$res = array_add($res,'imgHabi',$imgH);
		$res = array_add($res,'imgRes',$imgR);
		$res = array_add($res,'Slider',$cadena);
		return $res;
	}

	public function RestaurantImgContent(){
		$jsonGaleria = json_decode(Picture::getRestauranteSlider());
		$galeria = json_decode($jsonGaleria->picture_path);
		
		$eventos = json_decode($this->json(2,16));
		$EventosImg = $Gallery = $res = ""; $cadena = "";

		if($galeria->cantidad==0){
			$cadena .= "<div class=\"item active\"><img style=\"margin:0px auto;\" src=\"".url($galeria->posicion[0])."\" alt=\"\"></div>";
			$cadena .= "<div class=\"item\"><div class=\"item-topage\" style=\" background-image:url(".url($galeria->posicion[0]).")\"></div></div>";
		}else{
			for($i = 0; $i < count($galeria->posicion); $i++){
				if($galeria->posicion[$i]!="img/restaurante/default.jpg"){
					if($i==0){
						$cadena .= "<div class=\"item active\"><img style=\"margin:0px auto;\" src=\"".url($galeria->posicion[$i])."\" alt=\"\"></div>";
					}else{
						$cadena .= "<div class=\"item\"><div class=\"item-topage\" style=\" background-image:url(".url($galeria->posicion[$i]).")\"></div></div>";
					}
				}
			}
		}

		for($i = 0; $i < 2; $i++){//INCIAILIZAR EVENTOS
			if($i==0){
				$EventosImg = array('Eventos'.$i => "<div class=\"grid-item-img\" style=\"background-image:url('img/restaurante/default.jpg')\"></div>");
			}else{
				$EventosImg = array_add($EventosImg,'Eventos'.$i,"<div class=\"grid-item-img\" style=\"background-image:url('img/restaurante/default.jpg')\"></div>");
			}
		}

		$EventosImg = $this->insertarpos($EventosImg,$eventos,0,1);
		$EventosImg = $this->insertarpos($EventosImg,$eventos,1,1);

		$res = array('Gallery' => $cadena);
		$res = array_add($res,'Eventos',$EventosImg);
		return $res;
	}
	
	private function getGallery($id){
		$indicadores = "";
		$finder = Picture::where("picture_status",$id)->first();
		$imagenes = json_decode($finder->picture_path);
		$miniatura = ""; $galeria = "";
		for($i = 0; $i < $imagenes->cantidad; $i++){
			if($i==0){
				$miniatura .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"0\" class=\"thumbnails active\" style=\"background-image:url(".url($imagenes->posicion[$i]).")\"></li>";
				$galeria .= "<div class=\"item active\"><table class=\"TableP\"><tbody><tr><td><img src=\"".url($imagenes->posicion[$i])."\"></td></tr></tbody></table></div>";
			}else{
				$miniatura .= "<li data-target=\"#BootstrapCarouselLightBox\" data-slide-to=\"0\" class=\"thumbnails\" style=\"background-image:url(".url($imagenes->posicion[$i]).")\"></li>";
				$galeria .= "<div class=\"item\"><table class=\"TableP\"><tbody><tr><td><img src=\"".url($imagenes->posicion[$i])."\"></td></tr></tbody></table></div>";
			}
		}
		
		$galeria_construida = array("miniatura" => $miniatura);
		$galeria_construida = array_add($galeria_construida,"galeria",$galeria);
		return $galeria_construida;
	}
	
	private function getBannerSuperior($estado){
		$finder = Picture::where("picture_status","12")->first();
		if(count($finder)==1){
			return $finder->picture_path;
		}return "img/default.jpg";
	}
	
	public function hotel(Request $request){
		$contacto = $this->picture_active(13,0);
		$habcu = $this->obtener_rooms();
		$galeriaconstruida = $this->getGallery("11");
		
		return view('elhotel')->with([
			"id" => "",
			"usuario" => NULL,
			"modallogin" => "0",
			"cuartos" => $habcu,
			"galeria" => $galeriaconstruida,
			"urlcontacto" => Picture::where("picture_status","=",$contacto)->value("picture_path"),
			"picturecontacto" => Picture::where("picture_status","=",$contacto)->value("picture_nombre"),
			"bannersuperior" => $this->getBannerSuperior("12"),
			"id" => $request -> id
		]);
	}
}
