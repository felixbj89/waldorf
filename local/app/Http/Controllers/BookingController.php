<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Booking;
use App\ConfigBooking;
use Session;
use Crypt;

class BookingController extends Controller{

  public function salir($modo){
	Booking::where("user_email","haciendacreativa@gmail.com")->update([
      "user_token_status" => "0"
    ]);

	
    Session::flush();
    return redirect()->to(Crypt::decrypt($modo));
  }

  public function savesetup(Request $request){
	$booking = new ConfigBooking();
	$url = NULL;
	if($request->all()["produccion"]==""){
		$url = $request->all()["developer"];
	}else{
		$url = $request->all()["produccion"];
	}
	
	$inputs = [
		"link" => $url,
		"key" => $request->all()["key"],
		"estado" => $request->all()["tipo"]
	];
	
	$estado = $booking->store_url($booking,$inputs,"0");
	
    
    if($estado==1){
      return redirect()->back()->with([
        "respuesta" => "1",
        "mensaje" => "Cambios Guardados Sastifactoriamente",
        "desarrollo" => NULL,
        "produccion" => NULL,
        "tipo" => NULL,
        "usuario" =>  Session::get("bookuser"),
        "credenciales" => NULL,
        "key" => NULL,
      ]);
    }else{
      return redirect()->back()->with([
        "respuesta" => "0",
        "mensaje" => "No se pudo procesar los cambios.",
        "desarrollo" => NULL,
        "produccion" => NULL,
        "tipo" => NULL,
        "usuario" =>  Session::get("bookuser"),
        "credenciales" => NULL,
        "key" => NULL,
      ]);
    }
  }

  public function reservar(Request $request){
	$regreso = "/";
	if(explode("/",$request->path())[0]=="Restaurante"){
		$regreso = "Restaurante";
	}else if(explode("/",$request->path())[0]=="Habitaciones"){
		$regreso = "Habitaciones";
	}else if(explode("/",$request->path())[0]=="hotel"){
		$regreso = "hotel";
	}
	
    $configuracion = ConfigBooking::where("booking_hotel","0")->first();
    return view("booking.reservar")->with([
      "modal" => "0",
      "credenciales" => NULL,
      "desarrollo" => (count($configuracion)==0?NULL:$configuracion->booking_urlDesarrollo),
      "produccion" => (count($configuracion)==0?NULL:$configuracion->booking_urlProduccion),
      "tipo" => (count($configuracion)==0?NULL:$configuracion->booking_status),
      "keydev" => (count($configuracion)==0?NULL:$configuracion->booking_keyDesarrollo),
	  "keyprod" => (count($configuracion)==0?NULL:$configuracion->booking_keyProduccion),
	  "regreso" => $regreso,
	  "usuario" => NULL
    ]);
  }

  public function booking(){
    $configuracion = ConfigBooking::where("booking_hotel","0")->first();
    return view("booking.index")->with([
      "modal" => "0",
      "usuario" =>  Session::get("bookuser"),
      "credenciales" => NULL,
      "desarrollo" => (count($configuracion)==0?NULL:$configuracion->booking_urlDesarrollo),
      "produccion" => (count($configuracion)==0?NULL:$configuracion->booking_urlProduccion),
      "tipo" => (count($configuracion)==0?NULL:$configuracion->booking_status),
      "keydev" => (count($configuracion)==0?NULL:$configuracion->booking_keyDesarrollo),
	  "keyprod" => (count($configuracion)==0?NULL:$configuracion->booking_keyProduccion),
    ]);
  }

  public function autentificar(Request $request){
    $booking = new Booking();
	if(($booking = $booking->check($booking,$request->all()))!=NULL){
      Session::set("bookuser",$booking);
      return redirect()->action("BookingController@booking");
    }
	
    return redirect()->to("booking/setup")->with([
      "modal" => "1",
      "usuario" =>  NULL,
      "credenciales" => "usuario no existe, intente nuevamente",
      "desarrollo" => NULL,
      "produccion" => NULL,
      "tipo" => NULL,
      "keydev" => NULL,
	  "keyprod" => NULL,
    ]);
  }

  public function index(){
    return view("booking.index")->with([
      "modal" => "1",
      "usuario" =>  NULL,
      "credenciales" => NULL,
      "desarrollo" => NULL,
      "produccion" => NULL,
      "tipo" => NULL,
      "keydev" => NULL,
	  "keyprod" => NULL,
    ]);
  }
}
