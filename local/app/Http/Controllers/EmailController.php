<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class EmailController extends Controller{
    
     public function reserva(Request $request){
        
        $data = [
           "formulario" => $request->input("formulario")
        ];

        Mail::send('layout.emailII', $data, function ($message) use($request) {
            $message->from("hotelwaldorf89@gmail.com", "MENSAJE RECIBIDO DEL WEBSITE"); 
            $message->to("recepcion@hotelwaldorf.com.ve", "HOTEL WALDORF")->subject("HOTEL WALDORF: NUEVA RESERVA");
        });
        
        return response()->json([
           "success" => "1"
        ]);
    }
    
    public function send(Request $request){
		$correoto = "";
		if($request->input("formulario")[0]["value"]=="recepcion"){
			$correoto = "banquetes@hotelwaldorf.com.ve";
		}else if($request->input("formulario")[0]["value"]=="grupos"){
			$correoto = "ventas@hotelwaldorf.com.ve";
		}else if($request->input("formulario")[0]["value"]=="restaurante"){
			$correoto = "ilpappardelle@hotelwaldorf.com.ve";
		}else if($request->input("formulario")[0]["value"]=="promociones"){
			$correoto = "recepcion@hotelwaldorf.com.ve";
		}else if($request->input("formulario")[0]["value"]=="reservaciones"){
			$correoto = "reservaciones@hotelwaldorf.com.ve";
		}
		
        $data = [
           "formulario" => $request->input("formulario")
        ];

        Mail::send('layout.email', $data, function ($message) use($request,$correoto) {
            $message->from("hotelwaldorf89@gmail.com", "MENSAJE RECIBIDO DEL WEBSITE"); 
            $message->to($correoto, "HOTEL WALDORF")->subject("HOTEL WALDORF: MENSAJE DE CONTACTO");
        });
        
        return response()->json([
           "success" => "1"
        ]);
    }
}
