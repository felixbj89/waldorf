<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\TextRequest;
use App\Http\Requests\GastronomiaRequest;
use App\Http\Requests\EventosRequest;
use App\Text;
use Lang;
use App\Picture;
use App\Rooms;

class TextController extends Controller{

  /*public function modifyrooms(Request $request){
    $encontrado = Rooms::where("rooms_positions",$request->input("posicion"))->first();
    if(count($encontrado)==1){
      $json = json_decode($encontrado->rooms_especificaciones);
      $lista = substr($request->input("lista"),0,strlen($request->input("lista"))-1);
      $size = count($json[1]->thumbnails);
      $thumb = "";
      for($i = 0; $i < $size; $i++){
        $thumb .= "\"".$json[1]->thumbnails[$i]."\",";
      }

      $thumbnails = substr($thumb,0,strlen($thumb)-1);
      $rooms = new Rooms();
      $default = '[{"cuarto":"1","activo":"'.$request->input("estado").'"},{"nombre":"'.$json[1]->nombre.'","titulo":"'.$request->input("titulo").'","capacidad":"'.$request->input("capacidad").'","in":"'.$request->input("in").'","out":"'.$request->input("out").'","especificaciones":['.$lista.'],"thumbnails":['.$thumbnails.']}]';
      if($rooms->modifyrooms($rooms,$default,$request->input("posicion"))){
        return response()->json([
          "respuesta" => "1",
          "size" => count($rooms->whereNotIn("rooms_codigo",["default.jpg"])->get()),
          "cuartos" => $rooms->select("rooms_especificaciones","rooms_positions","rooms_codigo")->get()
        ]);
      }

      return response()->json([
        "respuesta" => "0"
      ]);
    }
  }*/

	public function verhabitacion(Request $request){
		return Rooms::where("rooms_positions",$request->input("posicion"))->first();
	}

	public function verpropiedades(){
		$size = 0;
		$cuartos = Rooms::all();
		foreach($cuartos as $cu){
			if($cu->rooms_codigo=="default.jpg"){
				$size++;
			}
		}
		
		if($size==10){
			return response()->json([
				"size" => $size,
				"respuesta" => "0",
			]);
		}else{
			return response()->json([
				"size" => count(Rooms::whereNotIn("rooms_codigo",["default.jpg"])->get()),
				"respuesta" => "1",
				"cuartos" => Rooms::whereNotIn("rooms_codigo",["default.jpg"])->select("rooms_especificaciones","rooms_positions","rooms_codigo")->get()
			]);
		}
	}

	public function addseculider(Request $request){
		$estado = Text::savestextoslider($request->all());
		if($estado==1){
			return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "No habido cambios en la Galería de Habitaciones."]);
		}else if($estado==3){
			return redirect()->back()->with(["message" => "Modificación Satisfactoria."]);
		}else{
			return redirect()->back()->with(["message" => "No se pudo guardar la habitación"]);
		}
	}

	public function addrooms(Request $request){
		$estado = Rooms::storerooms($request->all());
		if($estado==1){
			return redirect()->back()->with(["message" => "Guardado sastifactorio"]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "Debe ingresar 6 imágenes para la galería."]);
		}else if($estado==3){
			return redirect()->back()->with(["message" => "Debe ingresar la imagen de portada."]);
		}else if($estado==4){
			return redirect()->back()->with(["message" => "La posición seleccionada ya está en uso."]);
		}else{
			return redirect()->back()->with(["message" => "No se pudo guardar la habitación"]);
		}
	}

	public function editgastronomia(Request $request){
		$texto = new Text(); $textoedit;
		if(($textoedit = $texto->searchtext($texto,$request->input("origen"),"0"))!=NULL && $request->input("origen")=="1"){//GASTRONOMIA
			$imagen = Picture::where("picture_status",15)->first();
			$encontrado = 15;
			if(count($imagen)==1){
				$encontrado = 15;
			}

			return response()->json([
				"resultado" => "1",
				"texto" => $textoedit,
				"imagen" => Picture::where("picture_status",$encontrado)->value("picture_path")
			]);
		}
	}

	public function editareventos(Request $request){
		$texto = new Text(); $textoedit;
	
		if(($textoedit = $texto->searchtext($texto,$request->input("origen"),"3"))!=NULL && $request->input("origen")=="2"){//HISTORIA
			$imagen = Picture::where("picture_status",16)->first();
			$encontrado = 16;
			if(count($imagen)==1){
				$encontrado = 16;
			}

			return response()->json([
				"resultado" => "1",
				"texto" => $textoedit,
				"imagen" => Picture::where("picture_status",$encontrado)->value("picture_path")
			]);
		}

		return response()->json([
			"resultado" => "0",
			"message" => "No se encontro alguna data en el sistema"
		]);
	}

	public function edithistory(Request $request){
		$texto = new Text(); $textoedit;
		if(($textoedit = $texto->searchtext($texto,$request->input("origen"),"0"))!=NULL && $request->input("origen")=="0"){//HISTORIA
			$imagen = Picture::whereNotIn("picture_path",["img/default.jpg"])->where("picture_status",9)->first();
			if(count($imagen)==1){
				$encontrado = 9;

				return response()->json([
					"resultado" => "1",
					"texto" => $textoedit,
					"imagen" => Picture::where("picture_status",$encontrado)->value("picture_path")
				]);
			}
		}

		return response()->json([
			"resultado" => "0",
			"message" => "No se encontro alguna historia en el sistema"
		]);
	}

	public function addtextprincipal(Request $request){
		$texto = new Text();
		if($texto->savetextprincipal($request->all(),$texto)){
			return response()->json([
				"respuesta" => "1",
				"message" => "Los Cambios han sido guardados satisfactoriamente"
			]);
		}else{
				return response()->json([
				"respuesta" => "0",
				"message" => "No se pudo guardar el texto principal"
			]);
		}
	}
	
	public function addeventos(EventosRequest $request){
		$file_picture1 = $request->file("file_picture1");//PRIMERA IMAGEN - GASTRONOMIA
		$file_picture2 = $request->file("file_picture2");//SEGUNDA IMAGEN - GASTRONOMIA
		$texto = new Text();
		$estado = $texto->saveeventos($request->all(),$file_picture1, $file_picture2,$texto,"2","3");
		if($estado==1){
			return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		}else{
			return redirect()->back()->with(["message" => "No se pudo guardar la historia"]);
		}
	}
	
	public function addgastronomia(GastronomiaRequest $request){
		$file_picture1 = $request->file("file_picture1");//PRIMERA IMAGEN - GASTRONOMIA
		$file_picture2 = $request->file("file_picture2");//SEGUNDA IMAGEN - GASTRONOMIA
		$file_picture3 = $request->file("file_picture3");//TERCERA IMAGEN - GASTRONOMIA
		$file_picture4 = $request->file("file_picture4");//CUARTA IMAGEN - GASTRONOMIA

		$texto = new Text();
		$estado = $texto->savegastronomia($request->all(),$file_picture1, $file_picture2, $file_picture3, $file_picture4,$texto,"1");
		if($estado==1){
			return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		}else{
			return redirect()->back()->with(["message" => "No se altero la gastronomía"]);
		}
	}
	

	public function addhistory(TextRequest $request){
		$file_picture = $request->file("file_picture");
		$texto = new Text();
		$estado = $texto->savehistory($request->all(),$file_picture,$texto,"0");
		if($estado==1){
			return redirect()->back()->with(["message" => Lang::get("message.confirmado")]);
		}else if($estado==2){
			return redirect()->back()->with(["message" => "Debe remover una imagen del historial antes de continuar"]);
		}else if($estado==0){
			return redirect()->back()->with(["message" => "No se pudo guardar la historia"]);
		}
	}
}
