<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;
use App\Text;
use Session;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = "user";

    protected $fillable = [
        "user_name",
    		"user_email",
    		"user_login",
    		"user_password",
    		"user_role",
    		"user_status",
    		"user_token",
    		"user_token_status"
    ];

    protected $hidden = [];

    public function text(){
      return $this->belongsTo('App\Text');
    }

    public function picture(){
      return $this->belongsTo('App\Picture');
    }

    public function deleteUser($usuario,$correo){
      $usuario->where("user_email",$correo)->delete();

      return 1;
    }

    public function editprofile($usuario,$formulario,$status){
      if($formulario[6]["value"]=="" || $formulario[7]["value"]=="" || $formulario[8]["value"]==""){
        return 0;
      }else{
        $usuario = User::where("user_email",$formulario[4]["value"])->first();
        $confirmar = 0;
        if($usuario->user_password==$formulario[7]["value"]
        || $usuario->user_login==$formulario[6]["value"]){//NO HUBO MODIFICACIÓN
          $confirmar = 1;
        }

        $usuario->where("user_email",$formulario[4]["value"])->update([
          "user_role" => $formulario[5]["value"],
          "user_status" => $status,
          "user_login" =>  ($confirmar==1?$usuario->user_login:$formulario[6]["value"]),
          "user_password" => ($confirmar==1?$usuario->user_password:hash('ripemd320',$formulario[7]["value"]))
        ]);

        return 1;
      }
    }

    public function editUser($formulario){
      $usuario = User::where("user_email",$formulario[3]["value"])->first();
      if($usuario->user_login==$formulario[4]["value"] && $usuario->user_password==$formulario[1]["value"]){
        return 2;
      }else{
  		  User::where("user_email",$formulario[3]["value"])->update([
  			   "user_login" => $formulario[4]["value"],
  			   "user_password" => ($formulario[6]["value"]==""?$formulario[1]["value"]:hash('ripemd320',$formulario[6]["value"]))
  		  ]);

        Session::set("usuario",User::where("user_email",$formulario[3]["value"])->first());
  		  return 1;
      }
    }

    public function checkEmail($correo){
      return (User::where("user_email",$correo)->first()==NULL?0:1);
    }

    public function createUser($usuario,$formulario){
      if($this->checkEmail($formulario[2]["value"])){
        return 0;
      }else if($formulario[6]["value"]!=$formulario[7]["value"]){
        return 0;
      }else{
        $usuario->user_name = $formulario[1]["value"];
        $usuario->user_email = $formulario[2]["value"];
        $usuario->user_password = hash('ripemd320',$formulario[6]["value"]);
        $usuario->user_role = $formulario[3]["value"];
        $usuario->user_status = $formulario[4]["value"];
        $usuario->user_login = $formulario[5]["value"];
        $usuario->user_token = str_random(255);
        $usuario->user_token_status = "0";

        $usuario->save();

        return 1;
      }
  }

  public function recheck($usuario,$request){
    $usuario = ($usuario->where("user_password",hash('ripemd320', $request["blockpass"]))->where("user_login",$request["blocklogin"])->where("user_token_status","1")->where("user_status","1")->first());
		if(count($usuario) > 0){//LE DIO A LOCK
		  User::where("user_password",hash('ripemd320', $request["blockpass"]))->where("user_login",$request["blocklogin"])->where("user_token_status","1")->where("user_status","1")->update([
			  "user_token_status" => "1"
		  ]);

			return $usuario;
		}else{//FINALIZO SU SESSION
      $usuario = (User::where("user_password",hash('ripemd320', $request["blockpass"]))->where("user_login",$request["blocklogin"])->where("user_token_status","0")->where("user_status","1")->first());
      if(count($usuario) > 0){
        User::where("user_password",hash('ripemd320', $request["blockpass"]))->where("user_login",$request["blocklogin"])->where("user_token_status","1")->where("user_status","1")->update([
  			  "user_token_status" => "1"
  		  ]);

  			return $usuario;
      }
    }return NULL;
	}

	public function check($usuario,$request){
    $session = (User::where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_token_status","1")->where("user_status","1")->first());
    if(count($session)==1){//SI DEJE LA SESSION ABIERTA
      User::where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_token_status","1")->where("user_status","1")->update([
			  "user_token_status" => "1"
		  ]);

      return $session;
    }else{//AUTENTIFICACIÓN POR PRIMERA VEZ
      $usuario = ($usuario->where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_token_status","0")->where("user_status","1")->first());
      if(count($usuario) == 1){
        User::where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_token_status","0")->where("user_status","1")->update([
          "user_token_status" => "1"
        ]);

        return $usuario;
      }
    }return NULL;
	}
}
