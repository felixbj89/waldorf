<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use App\Picture;

class Text extends Model{

  protected $table = "text";

  protected $fillable = [
      "text_section",
      "text_json",
      "text_origen",
      "text_status",
      "user_id"
  ];

	public function savetextprincipal($request,$texto){
		$buscar = "\"";
		$reemplazar   = '\\"';
		$request["texto"]  = str_replace($buscar, $reemplazar,$request["texto"]);
		$request["texto"] = str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$request["texto"]);
		$texhab = str_replace("\n",'', $request["texto"]);
		$texhabaux = str_replace("\r\n",'', $texhab);
	
		$encontrado = $texto->where("text_origen",$request["origen"])->where("text_section",$request["section"])->first();
		if(count($encontrado)==1){
			$texto->where("text_origen",$request["origen"])->where("text_section",$request["section"])->update([
				"text_origen" => $request["origen"],
				"user_id" => Session::get("usuario")->id,
				"text_json" => '[{"titulo":"'.$request["titulo"].'","texto":"'.$texhabaux.'"}]',
				"text_status" => "2"
			]);

			return 1;
		}return 0;
	}

	public static function savestextoslider($requets){
		$encontrado = Text::where("text_origen","2")->where("text_section","0")->first();
		if(count($encontrado)==1){
			$json = json_decode($encontrado["text_json"]);
			if($json!=NULL){
				$titulo = ""; $texto = "";
				if(array_key_exists("titulohistory",$requets) && $requets["titulohistory"]!=""){
					$titulo = $requets["titulohistory"];
				}else{
					$titulo = $json[0]->titulo;
				}
				
				if(array_key_exists("descr",$requets) && $requets["descr"]!=""){
					$buscar = "\"";
					$reemplazar   = '\\"';
					$texto  = str_replace($buscar, $reemplazar,$requets["descr"]);
					$texto = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$texto));
					$texto = (str_replace("\r\n",'', $texto));
				}else{
					$buscar = "\"";
					$reemplazar   = '\\"';
					$texto  = str_replace($buscar, $reemplazar,$json[0]->texto);
				}
				
				$encontrado->update([
					"text_origen" => "2",
					"user_id" => Session::get("usuario")->id,
					"text_json" => '[{"titulo":"'.$titulo.'","texto":"'.$texto.'"}]',
					"text_status" => "2"
				]);
				
				$estado = Picture::storesliderhome($requets);
				if($estado==1) return 1;
				else
					return 2;
			}
		}return 0;
	}

	public function searchtext($texto, $origen,$section){
		return $texto->where("text_section",$section)->where("text_origen",$origen)->where("text_status","2")->first();
	}

	public static function savegaleria($request,$estado,$section){
		$buscar = "\"";
		$reemplazar   = '\\"';
		$request["descripcion"]  = (str_replace($buscar, $reemplazar,$request["descripcion"]));
		$request["descripcion"] = (str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$request["descripcion"]));
		$descripcion = str_replace("\r\n",'', $request["descripcion"]);
		
		$finder = Text::where("text_origen",$estado)->where("text_section",$section)->first();
		
		if(count($finder)==1){
			
			Text::where("text_origen",$estado)->where("text_section",$section)->update([
				"text_origen" => $request["origen"],
				"user_id" => Session::get("usuario")->id,
				"text_json" => '[{"titulo":"'.$request["titulohistory"].'","subtitulo":"'.$request["subtitulohistory"].'","texto":"'.$descripcion.'","autor":"'.$request["autor"].'"}]',
				"text_status" => "2"
			]);

			return 1;
		}return 0;
	}

	public function saveeventos($inputs, $picture1, $picture2, $text,$estado,$section){
		$buscar = "\"";
		$reemplazar   = '\\"';
		$inputs["texto"]  = str_replace($buscar, $reemplazar,$inputs["texto"]);
		$inputs["texto"] = str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$inputs["texto"]);
		$encontrado = $text->where("text_origen",$estado)->where("text_section",$section)->first();
		$texto = str_replace("\r\n",'', $inputs["texto"]);
		if(count($encontrado)==1){
			$text->where("text_origen",$estado)->where("text_section",$section)->update([
				"text_origen" => $inputs["origen"],
				"user_id" => Session::get("usuario")->id,
				"text_json" => '[{"titulo":"'.$inputs["titulohistory"].'","subtitulo":"'.$inputs["subtitulohistory"].'","texto":"'.$texto.'"}]',
				"text_status" => "2"
			]);

			$piobj = new Picture();//ACTUALIZO PARA EL HISTORIAL E INSERTO LA NUEVA IMAGEN 1-2
			if($piobj->storeeventos($piobj,$picture1,$picture2,16,$inputs["editando"])==1){//IMG 1-4
				return 1;
			}else if($piobj->storeeventos($piobj,$picture1,$picture2,16,$inputs["editando"])==2){
				return 2;
			}
		}return 0;
	}
	
	public function savegastronomia($inputs, $picture1, $picture2, $picture3, $picture4, $text,$estado){
		$buscar = "\"";
		$reemplazar   = '\\"';
		$inputs["texto"]  = str_replace($buscar, $reemplazar,$inputs["texto"]);
		$inputs["texto"] = str_replace("[\n|\r|\n\r|\t|\0|\x0B]", "",$inputs["texto"]);
		$texto = str_replace("\r\n",'', $inputs["texto"]);

		$encontrado = $text->where("text_origen",$estado)->first();
		if(count($encontrado)==1){
			$text->where("text_origen",$estado)->update([
				"text_origen" => $inputs["origen"],
				"user_id" => Session::get("usuario")->id,
				"text_json" => '[{"titulo":"'.$inputs["titulohistory"].'","subtitulo":"'.$inputs["subtitulohistory"].'","texto":"'.$texto.'"}]',
				"text_status" => "2"
			]);

			$piobj = new Picture();//ACTUALIZO PARA EL HISTORIAL E INSERTO LA NUEVA IMAGEN 1-4
			if($piobj->storegastronomia($piobj,$picture1,$picture2, $picture3, $picture4,15,$inputs["editando"])==1){//IMG 1-4
				return 1;
			}else if($piobj->storegastronomia($piobj,$picture1,$picture2, $picture3, $picture4,15,$inputs["editando"])==2){
				return 2;
			}
		}return 0;
	}

	public function savehistory($inputs, $picture, $text,$estado){
		$buscar = "\"";
		$reemplazar   = '\\"';
		$inputs["texto"]  = str_replace($buscar, $reemplazar,$inputs["texto"]);
		$inputs["texto"] = str_replace("[\n|\r|\n\r|\r\n|\t|\0|\x0B]", "",$inputs["texto"]);
		$texto = str_replace("\r\n",'', $inputs["texto"]);

		$encontrado = $text->where("text_origen",$estado)->where("text_section","0")->first();
		if(count($encontrado)==1){
			$text->where("text_origen",$estado)->where("text_section","0")->update([
				"text_origen" => $inputs["origen"],
				"user_id" => Session::get("usuario")->id,
				"text_json" => '[{"titulo":"'.$inputs["titulohistory"].'","subtitulo":"'.$inputs["subtitulohistory"].'","texto":"'.$texto.'"}]',
				"text_status" => "2"
			]);

			if($picture!=NULL){
				$piobj = new Picture();//ACTUALIZO PARA EL HISTORIAL E INSERTO LA NUEVA IMAGEN
				if($piobj->limite($piobj,9) < 6){
					$piobj->storeother($piobj,9,$picture);
				}else{
					return 2;
				}
			}return 1;
		}return 0;
	}

	public function user(){
		return $this->hasMany('App\User',"id","user_id");
	}
}
