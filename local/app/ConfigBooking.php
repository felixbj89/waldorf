<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class ConfigBooking extends Model
{
  protected $connection = 'BOOKING';

  protected $table = "booking_url";

  protected $fillable = [
      "booking_urlDesarrollo",
      "booking_urlProduccion",
      "booking_status",
      "booking_keyProduccion",
	  "booking_keyDesarrollo",
      "user_id"
  ];

  protected $hidden = [];

  public function booking(){
    return $this->hasMany('App\Booking',"id","user_id");
  }

  public function store_url($config, $inputs, $typehotel){
		$hotel = $config->where("booking_hotel",$typehotel)->first();
		if(count($hotel)==0){//DESARROLLO
			if($inputs["estado"]=="0"){
				$config->create([
					"booking_urlDesarrollo" => $inputs["link"],
					"booking_urlProduccion" => "",
					"booking_status" => "1",
					"booking_keyDesarrollo" => $inputs["key"],
					"booking_keyProduccion" => "",
					"booking_hotel" => $typehotel
				]);
			}else{//PRODUCCIÓN
				$config->create([
					"booking_urlDesarrollo" => "",
					"booking_urlProduccion" => $inputs["link"],
					"booking_status" => "0",
					"booking_keyDesarrollo" => "",
					"booking_keyProduccion" => $inputs["key"],
					"booking_hotel" => $typehotel
				]);
			}
			
			return 1;
		}else{
			
			if($inputs["estado"]=="0"){
				$config->where("booking_hotel",$typehotel)->update([
					"booking_urlDesarrollo" => $inputs["link"],
					"booking_status" => "1",
					"booking_keyDesarrollo" => $inputs["key"]
				]);
			}else{//PRODUCCIÓN
				$config->where("booking_hotel",$typehotel)->update([
					"booking_urlProduccion" => $inputs["link"],
					"booking_status" => "0",
					"booking_keyProduccion" => $inputs["key"],
				]);
			}
			
			return 1;
		}
		
		return 0;
	}
}
