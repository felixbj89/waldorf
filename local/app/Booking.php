<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model{

  protected $connection = 'BOOKING';

  protected $table = "booking_user";

  protected $fillable = [
      "user_name",
      "user_email",
      "user_login",
      "user_password",
      "user_role",
      "user_status",
      "user_token",
      "user_token_status"
  ];

  protected $hidden = [];

  public function configbooking(){
    return $this->belongsTo('App\ConfigBooking');
  }

  public function check($usuario,$request){
		$usuario = Booking::where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_status","1")->first();
		if(count($usuario) > 0){
			Booking::where("user_password",hash('ripemd320', $request["password"]))->where("user_login",$request["login"])->where("user_token_status","0")->where("user_status","1")->update([
				"user_token_status" => "1"
			]);

			return $usuario;
		}return NULL;
	}
}
