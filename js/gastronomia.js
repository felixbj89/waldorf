var pic; var type; var size; var editar = 0;
function openFile(event){
  pic = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

$(document).ready(function() {

  $("#todosobligatorio1").hide();
  $("#todosobligatorio2").hide();
  $("#todosobligatorio3").hide();
  $("#todosobligatorio4").hide();
  $("#todosobligatorio5").hide();
  $("#todosobligatorio6").hide();
  $("#subobligatorio").hide();
  $("#priobligatorio").hide();
  $("#fileobligatorio1").hide();//IMG 1
  $("#fileformato1").hide();
  $("#filesize1").hide();
  $("#fileresol1").hide();
  $("#fileobligatorio2").hide();//IMG 2
  $("#fileformato2").hide();
  $("#filesize2").hide();
  $("#fileresol2").hide();
  $("#fileobligatorio3").hide();//IMG 3
  $("#fileformato3").hide();
  $("#filesize3").hide();
  $("#fileresol3").hide();
  $("#fileobligatorio4").hide();//IMG 4
  $("#fileformato4").hide();
  $("#filesize4").hide();
  $("#fileresol4").hide();
  $("#removerchange").hide();
  $("#successchange").hide();

  var tablaHistorial = $("#datatable-gastronomia").DataTable({
    "searching": false,
    "bInfo": false,
    "Info":false,
    "bLengthChange": false,
    "iDisplayLength": 1,
    "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },

    "fixedHeader": {
       "header": true,
       "footer": true
     },
    "bPaginate": true,
    "bAutoWidth": false,
  });

   $("#filtroselect").on("change",function(){
     $("#filtrar").attr("disabled",false);
   });
    /*********************************IMAGEN 4****************************************/
    $("#file_picture4").on("change",function(){
        var input = pic.target;
        var reader = new FileReader();
        reader.onload = function(){
          var dataURL = reader.result;
          $("#preview4").attr("src",dataURL);
        };

        type = input.files[0].type;
        size = input.files[0].size;

        reader.readAsDataURL(input.files[0]);
    });

    var _URL = window.URL || window.webkitURL;

    $("#file_picture4").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onload = function() {
            if(this.width ==369 && this.height == 215){
              $("#fileresol4").hide();
              $("#preview4").attr("style","width:100% !important");
            }else{
              $("#fileresol4").show();
              $("#preview4").attr("src",$("#defaultimagen").val());
            }
          };
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = _URL.createObjectURL(file);
      }
  });
  /*********************************************************************************/
  /*********************************IMAGEN 3****************************************/
  $("#file_picture3").on("change",function(){
      var input = pic.target;
      var reader = new FileReader();
      reader.onload = function(){
        var dataURL = reader.result;
        $("#preview3").attr("src",dataURL);
      };

      type = input.files[0].type;
      size = input.files[0].size;

      reader.readAsDataURL(input.files[0]);
  });

  var _URL = window.URL || window.webkitURL;

  $("#file_picture3").change(function(e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
          if(this.width ==369 && this.height == 151){
            $("#fileresol3").hide();
            $("#preview3").attr("style","width:100% !important");
          }else{
            $("#fileresol3").show();
            $("#preview3").attr("src",$("#defaultimagen").val());
          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }
});
  /*********************************************************************************/
  /*********************************IMAGEN 2****************************************/
  $("#file_picture2").on("change",function(){
      var input = pic.target;
      var reader = new FileReader();
      reader.onload = function(){
        var dataURL = reader.result;
        $("#preview2").attr("src",dataURL);
      };

      type = input.files[0].type;
      size = input.files[0].size;

      reader.readAsDataURL(input.files[0]);
  });

  var _URL = window.URL || window.webkitURL;

  $("#file_picture2").change(function(e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
          if(this.width ==245 && this.height == 370){
            $("#fileresol2").hide();
            $("#preview2").attr("style","width:100% !important");
          }else{
            $("#fileresol2").show();
            $("#preview2").attr("src",$("#defaultimagen").val());
          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }
});
/*********************************************************************************/
/*********************************IMAGEN 1****************************************/
  $("#file_picture1").on("change",function(){
      var input = pic.target;
      var reader = new FileReader();
      reader.onload = function(){
        var dataURL = reader.result;
        $("#preview1").attr("src",dataURL);
      };

      type = input.files[0].type;
      size = input.files[0].size;

      reader.readAsDataURL(input.files[0]);
  });

  var _URL = window.URL || window.webkitURL;

  $("#file_picture1").change(function(e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
          if(this.width == 355 && this.height == 200){
            $("#fileresol1").hide();
            $("#preview1").attr("style","width:100% !important");
          }else{
            $("#fileresol1").show();
            $("#preview1").attr("src",$("#defaultimagen").val());
          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }
});
/**********************************************************************************/

  $("#subtitulohistory, #titulohistory, #file_picture").bind("focusin",function(){
    if($("#todosobligatorio1").is(":visible")){
      $("#todosobligatorio1").hide();
    }

    if($("#todosobligatorio2").is(":visible")){
      $("#todosobligatorio2").hide();
    }

    if($("#todosobligatorio3").is(":visible")){
      $("#todosobligatorio3").hide();
    }

    if($("#todosobligatorio4").is(":visible")){
      $("#todosobligatorio4").hide();
    }

    if($("#todosobligatorio5").is(":visible")){
      $("#todosobligatorio5").hide();
    }

    if($("#todosobligatorio6").is(":visible")){
      $("#todosobligatorio6").hide();
    }

    if($("#subobligatorio").is(":visible")){
      $("#subobligatorio").hide();
    }

    if($("#priobligatorio").is(":visible")){
      $("#priobligatorio").hide();
    }

    if($("#fileobligatorio1").is(":visible")){
      $("#fileobligatorio1").hide();
    }

    if($("#fileformato1").is(":visible")){
      $("#fileformato1").hide();
    }

    if($("#filesize1").is(":visible")){
      $("#filesize1").hide();
    }

    if($("#fileobligatorio2").is(":visible")){
      $("#fileobligatorio2").hide();
    }

    if($("#fileformato2").is(":visible")){
      $("#fileformato2").hide();
    }

    if($("#filesize2").is(":visible")){
      $("#filesize2").hide();
    }

    if($("#fileobligatorio3").is(":visible")){
      $("#fileobligatorio3").hide();
    }

    if($("#fileformato3").is(":visible")){
      $("#fileformato3").hide();
    }

    if($("#filesize3").is(":visible")){
      $("#filesize3").hide();
    }

    if($("#fileobligatorio4").is(":visible")){
      $("#fileobligatorio4").hide();
    }

    if($("#fileformato4").is(":visible")){
      $("#fileformato4").hide();
    }

    if($("#filesize4").is(":visible")){
      $("#filesize4").hide();
    }

    if($("#fileresol1").is(":visible")){
      $("#fileresol1").hide();
    }

    if($("#fileresol2").is(":visible")){
      $("#fileresol2").hide();
    }

    if($("#fileresol3").is(":visible")){
      $("#fileresol3").hide();
    }

    if($("#fileresol4").is(":visible")){
      $("#fileresol4").hide();
    }

    if($(".errormensaje").is(":visible")){
      $(".errormensaje").hide();
    }
  });

  $("#titulohistory, #subtitulohistory").bind("keypress", function (event) {
      if (event.charCode!=0) {
       var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
       var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

       if (!regex.test(key) || ($(this).val()).length > 20) {
          event.preventDefault();
          return false;
       }
    }
  });

  $("#btneditarGastronomia").on("click",function(){
    //ajax..
	var textoeditar = $("#modoEditar").val();
    var editurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			editurl = window.location.href.substr(0,pos-1);
		}

    $("#editando").attr("value","1");
    $.ajax({
        url: editurl + "/admin/editgastronomia",
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{origen:"1"},
        success:function(respuesta){//RETORNARA UN OBJETO Y CARGA EL FORM
          if(respuesta.resultado=="1"){
            $("#notmach").modal("hide");
            var json = JSON.parse(respuesta.texto["text_json"]);
            $("#titulohistory").attr("value",json[0]["titulo"]);
            $("#subtitulohistory").attr("value",json[0]["subtitulo"]);
            $("#descr").text(json[0]["texto"]);
            $("#editor").html(json[0]["texto"]);
			$("#mensajeeditar").text(textoeditar);
			$("#modaleditar").modal("show");
			
            var jsonimagen = JSON.parse(respuesta.imagen);
            for(i = 0; i < jsonimagen.length; ++i){
              if(jsonimagen[i].tipo=="1"){
                $("#preview1").attr("style","width:100% !important");
                $("#preview1").attr("src",$("#url").val() + "/" + jsonimagen[i].img);
              }else if(jsonimagen[i].tipo=="2"){
                $("#preview2").attr("style","width:100% !important");
                $("#preview2").attr("src",$("#url").val() + "/" + jsonimagen[i].img);
              }else if(jsonimagen[i].tipo=="3"){
                $("#preview3").attr("style","width:100% !important");
                $("#preview3").attr("src",$("#url").val() + "/" + jsonimagen[i].img);
              }else if(jsonimagen[i].tipo=="4"){
                $("#preview4").attr("style","width:100% !important");
                $("#preview4").attr("src",$("#url").val() + "/" + jsonimagen[i].img);
              }
            }
            editar = 1;
          }else{
            $("#notmach").modal("show");
          }
        }
    });
  });

  $(document).on("click","#removerhistorial",function(){
    var picture = $(".fixed-button-historial").parents("tr").data("date");
    var seleccion = $(".fixed-button-historial").parents("tr");
    var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    $.ajax({
        url: urlfiltrada + '/admin/remover',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"picture":picture,"tipo":"4"},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
              $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");

              tablaHistorial.row(seleccion).remove().draw();
              $("#removerchange").show();
            }
        }
    });
  });

  $("#btncrearclose").on("click",function(){
    window.location.reload();
  });

  $("#historialgastronomia").on("show.bs.modal",function(){
    $("#removerchange").hide();
    $("#successchange").hide();
  });

  $(document).on("click","#historialbtn",function(){
    var picture = $(".fixed-button-historial").parents("tr").data("date");
    var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    $.ajax({
        url: urlfiltrada + '/admin/changepicture',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"picture":picture,"tipo":$("#tiposec").val()},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
              $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");
              $("#successchange").show();
            }
        }
    }).done(function(respuesta){
        if(respuesta.resultado==1){
          $("#historialgastronomia").modal("hide");
          $("#despueshistorial").modal("show");
        }
    });
  });

  $("#btnaddgastronomia").on("click",function(){
    if(pic==undefined && $("#titulohistory").val().length==0 && $("#subtitulohistory").val().length==0){
      $("#todosobligatorio1").show();
      $("#todosobligatorio2").show();
      $("#todosobligatorio3").show();
      $("#todosobligatorio4").show();
      $("#todosobligatorio5").show();
      $("#todosobligatorio6").show();
    }else if(editar==0 && pic==undefined){
      $("#fileobligatorio").show();
    }else if(editar==0 && type!="image/jpeg" && type!="image/png"){
      $("#fileformato").show();
    }else if(editar==0 && size >= 500000){
      $("#filesize").show();
    }else if($("#titulohistory").val().length == 0){
      $("#fileobligatorio").hide();
      $("#priobligatorio").show();
    }else if($("#subtitulohistory").val().length == 0){
      $("#fileobligatorio").hide();
      $("#subobligatorio").show();
    }else{
      $("#texto").attr("value",$("#editor").html());
      $("#fileobligatorio").hide();
      $("#fileformato").hide();
      $("#filesize").hide();
      $("#priobligatorio").hide();
      $("#subobligatorio").hide();
      $("#form-gastronomia").submit();
    }
  });

  /***************************************************************/
  function initToolbarBootstrapBindings() {
    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
    'Times New Roman', 'Verdana', 'Roboto-Bold'
  ],
  fontTarget = $('[title=Font]').siblings('.dropdown-menu');
  $.each(fonts, function(idx, fontName) {
    if(fontName=="Roboto-Bold"){
      fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:Roboto-Bold">' + fontName + '</a></li>'));
    }else{
      fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
    }
  });
  $('a[title]').tooltip({
    container: 'body'
  });
  $('.dropdown-menu input').click(function() {
    return false;
  })
  .change(function() {
    $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
  })
  .keydown('esc', function() {
    this.value = '';
    $(this).change();
  });

  $('[data-role=magic-overlay]').each(function() {
    var overlay = $(this),
    target = $(overlay.data('target'));
    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
  });

  if ("onwebkitspeechchange" in document.createElement("input")) {
    var editorOffset = $('#editor').offset();

    $('.voiceBtn').css('position', 'absolute').offset({
      top: editorOffset.top,
      left: editorOffset.left + $('#editor').innerWidth() - 35
    });
  } else {
    $('.voiceBtn').hide();
  }
  }

  function showErrorAlert(reason, detail) {
    var msg = '';
    if (reason === 'unsupported-file-type') {
      msg = "Unsupported format " + detail;
    } else {
      console.log("error uploading file", reason, detail);
    }
    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
  }

  initToolbarBootstrapBindings();
  $('#editor').wysiwyg({
    fileUploadError: showErrorAlert
  });
  window.prettyPrint;
  prettyPrint();
  
  $("#errorMensaje").hide();
  $("#editor").on("input",function(){
	var longitud = $("#editor").html();
	if(longitud.length > 193){
		$("#errorMensaje").show();
	}else{
		$("#errorMensaje").hide();
	}
  }).on("paste",function(event){
	var elementoTitle = event.originalEvent.clipboardData.getData('Text');
	if(elementoTitle.length > 193){
		$("#errorMensaje").show();
	}
  }).on("keypress",function(event){
	if (event.charCode!=0) {
	   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
	   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	   if (!regex.test(key) || ($(this).val()).length > 193) {
		  event.preventDefault();
		  return false;
	   }
	}
		
	if(event.which==8){
		var longitud = $("#editor").html();
		if(longitud.length < 193){
			$("#errorMensaje").hide();
		}
	}
  });
});
