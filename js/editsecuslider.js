var thumb1 = undefined;
var thumb2 = undefined;
var thumb3 = undefined;
var thumb4 = undefined;
var thumb5 = undefined;

function openThum1(event){
	thumb1 = event;
}

function openThum2(event){
	thumb2 = event;
}

function openThum3(event){
	thumb3 = event;
}

function openThum4(event){
	thumb4 = event;
}

function openThum5(event){
	thumb5 = event;
}

$(document).ready(function(){
		//CARGA LAS IMÁGENES.
	var _URL = window.URL || window.webkitURL;
	//IMAGEN-1
	$("#filethumb1").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview1").attr("src",img.src);
		}
	});
	
	$("#fileasociado1").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#asociado1").attr("src",img.src);
		}
	});
	
	//IMAGEN-2
	$("#filethumb2").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview2").attr("src",img.src);
		}
	});
	
	$("#fileasociado2").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#asociado2").attr("src",img.src);
		}
	});
	
	//IMAGEN-3
	$("#filethumb3").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview3").attr("src",img.src);
		}
	});
	
	$("#fileasociado3").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#asociado3").attr("src",img.src);
		}
	});
	
	//IMAGEN-4
	$("#filethumb4").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview4").attr("src",img.src);
		}
	});
	
	$("#fileasociado4").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#asociado4").attr("src",img.src);
		}
	});
	
	//IMAGEN-5
	$("#filethumb5").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview5").attr("src",img.src);
		}
	});
	
	$("#fileasociado5").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#asociado5").attr("src",img.src);
		}
	});
	
	//SUBMIT
	$("#pictureName, #filethumb1, #fileasociado1, #filethumb2, #fileasociado2, #filethumb3, #fileasociado3, #filethumb4, #fileasociado4, #filethumb5, #fileasociado5").bind("focusin",function(){
		$(".clienteerror").text("");
	});
	
	$("#btnslider").on("click",function(){
		$("#form-slider").submit();
	});
	
	$("#titulohistory").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 20) {
				event.preventDefault();
				return false;
			}	
		}
	});
	
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			}
		},
		placeholder: 'Ingrese su texto principal',
		height: 400,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: true 
	});
});