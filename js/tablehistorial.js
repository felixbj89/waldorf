$(document).ready(function(){
	
	var tablaHistorial = $("#usuarios_list").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 5,
		"language": {
		  "sProcessing":     "Procesando...",
		  "sLengthMenu":     "Mostrar _MENU_ registros",
		  "sZeroRecords":    "No se encontraron resultados",
		  "sEmptyTable":     "Ningún dato disponible en esta tabla",
		  "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		  "sInfoPostFix":    "",
		  "sSearch":         "Buscar:",
		  "sUrl":            "",
		  "sInfoThousands":  ",",
		  "sLoadingRecords": "Cargando...",
		  "oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		  },
		  "oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		  }
		},

		"fixedHeader": {
		   "header": true,
		   "footer": true
		 },
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	tablaHistorial = $("#rooms_list").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
		  "sProcessing":     "Procesando...",
		  "sLengthMenu":     "Mostrar _MENU_ registros",
		  "sZeroRecords":    "No se encontraron resultados",
		  "sEmptyTable":     "Ningún dato disponible en esta tabla",
		  "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		  "sInfoPostFix":    "",
		  "sSearch":         "Buscar:",
		  "sUrl":            "",
		  "sInfoThousands":  ",",
		  "sLoadingRecords": "Cargando...",
		  "oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		  },
		  "oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		  }
		},

		"fixedHeader": {
		   "header": true,
		   "footer": true
		 },
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	
	// Bar chart
	var navegadores = JSON.parse($("#browser").val());
	if(navegadores!=undefined){
		var ctx = document.getElementById("mybarChart");
		var mybarChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Navegadores Usados"],
				datasets: [{
					label: 'Firefox',
					backgroundColor: "#ff8300",
					data: [navegadores[0]]
				},
				{
					label: 'Chrome',
					backgroundColor: "#ffc99A",
					data: [navegadores[1]]
				},
				{
					label: 'Android',
					backgroundColor: "#74cbfc",
					data: [navegadores[2]]
				},
				{
					label: 'Edge',
					backgroundColor: "#26B99A",
					data: [navegadores[3]]
				}]
			},

			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}
	
	var dispositivos = JSON.parse($("#devices").val());
	if(dispositivos!=undefined){
		var ctx = document.getElementById("chartdispositivos");
		var mybarChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Dispositivos Usados"],
				datasets: [{
					label: 'Computadora',
					backgroundColor: "#ff8300",
					data: [dispositivos[0]]
				},
				{
					label: 'Móvil',
					backgroundColor: "#ffc99A",
					data: [dispositivos[1]]
				}]
			},

			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}
	
	var secciones = JSON.parse($("#secciones").val());
	if(secciones!=undefined){
		var ctx = document.getElementById("chartsecciones");
		var mybarChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Secciones Visitadas"],
				datasets: [{
					label: 'Home',
					backgroundColor: "#ff8300",
					data: [secciones[0]]
				},
				{
					label: 'El Hotel',
					backgroundColor: "#ffc99A",
					data: [secciones[1]]
				},
				{
					label: 'Habitaciones',
					backgroundColor: "#74cbfc",
					data: [secciones[2]]
				},
				{
					label: 'Restaurante',
					backgroundColor: "#26B99A",
					data: [secciones[3]]
				},
				{
					label: 'Contacto',
					backgroundColor: "#FA98fc",
					data: [secciones[4]]
				}]
			},

			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}
	
	Chart.defaults.global.legend = {
		enabled: true
    }
	
	// Line chart
	var contador = JSON.parse($("#contador").val());
	console.log(contador);
	var ctx = document.getElementById("lineChart");
	var lineChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			datasets: [{
				label: "Visitas realizadas",
				backgroundColor: "rgba(38, 185, 154, 0.31)",
				borderColor: "rgba(38, 185, 154, 0.7)",
				pointBorderColor: "rgba(38, 185, 154, 0.7)",
				pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
				pointHoverBackgroundColor: "#fff",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointBorderWidth: 1,
				data: contador
			}]
		},
	});
	
	$(document).on("click","#verpromo",function(){
		var promociones = $(this).data("promo");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
            url: url + 'admin/editpromo',
			headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			method:"post",
            data:{"promo_id":promociones},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#panel").empty();
				$("#panel").html(respuesta);
				$("#panel").append("<footer>" + 
					"<div class=\"pull-right\">" + 
						"&copy; 2016 Hotel Waldorf V2.5, Powered By <a href=\"https://www.haciendacreativa.com.ve\">Hacienda Creativa C.A.</a>" + 
					"</div>" + 
					"<div class=\"clearfix\"></div>" + 
				"</footer>");
			}
        }).done(function(respuesta){
        });
	});
});