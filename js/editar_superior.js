var pic; var type; var size;
function openFile(event){
  pic = event;
}

$(document).ready(function() {
	$(".clienteerror").hide();
	
	var _URL = window.URL || window.webkitURL;
	$("#banner_superior").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				console.log(file);
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#loading_superior").attr("src",img.src);
		}
	});
	
	$("#btnadd").on("click",function(){
		if(pic==undefined){
			$(".clienteerror").text("Debe cargar una imagen antes de continuar");
			$(".clienteerror").show();
		}else{
			$("#form-picture").submit();
		}
	});
});