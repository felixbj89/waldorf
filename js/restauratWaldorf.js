jQuery(function ($) {
    $("#namelength").hide();
    $("#telefonolength").hide();
    $("#telefonoshort").hide();
    $("#telefonoformato").hide();
    $("#cantidadlength").hide();

    function longitud(campo){
        if(campo.val().length==0){
            return 0;
        }else{
            return 1;
        }
    }

    function shorlength(campo){
        if(campo.val().length < 3){
            return 0;
        }else{
            return 1;
        }
    }

    /****************************CANTIDAD*****************************/
     $("#npersonas").bind("focusout",function(){
        if($("#npersonas").val()==""){
             $("#cantidadlength").show();
         }else{
             $("#cantidadlength").hide();
         }
    });


     $("#npersonas").bind("focusin",function(){
        if($("#cantidadlength").is(":visible")){
            $("#cantidadlength").hide();
        }
    });
    /*******************************PHONE*******************************/
    $("#phone").bind("focusout",function(){
        if($("#phone").val()=="+(__) ___-_______"){
             $("#telefonoshort").hide();
             $("#telefonolength").show();
         }else{
             if($("#phone").val()=="+(00) 000-0000000"){
                $("#telefonolength").hide();
                $("#telefonoshort").hide();
                $("#telefonoformato").show();
            }else{
                $("#telefonolength").hide();
                $("#telefonoformato").hide();
                $("#telefonoshort").hide();
            }

         }
    });

    $("#phone").bind("focusin",function(){
        if($("#telefonolength").is(":visible")){
            $("#telefonolength").hide();
        }

        if($("#telefonoshort").is(":visible")){
            $("#telefonoshort").hide();
        }
    });

    /*******************************NOMBRE*******************************/
    $("#name").bind("focusout",function(){
        if(!longitud($("#name"))){
             $("#namelength").show();
         }else{
             $("#namelength").hide();
         }
    });

     $("#name").bind("focusin",function(){
        if($("#namelength").is(":visible")){
            $("#namelength").hide();
        }
    });

    var nombre;
    $("#name").bind("input", function (event) {
        nombre = $("#name").val();
    });

    $("#name").bind("keyup", function (event) {
         if(event.keyCode==13){
            $("#name").val(nombre);
         }
	});

    $("#name").bind("keydown", function (event) {
        if(event.keyCode==9){
            $("#name").val(nombre);
        }
	});

    $("#name").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 60) {
				event.preventDefault();
				return false;
			}
		}
	});

    /********************************************************************/
    /*ENVIAR*/
    $("#btn-REnviar").on("click",function(){
        if(!longitud($("#name"))){
             $("#namelength").show();
         }else if($("#phone").val()=="+(__) ___-_______" || $("#phone").val()==""){
             $("#namelength").hide();
             $("#telefonoshort").hide();
             $("#telefonolength").show();
         }else if($("#phone").val()=="+(00) 000-0000000"){
            $("#telefonolength").hide();
             $("#namelength").hide();
             $("#telefonoshort").hide();
             $("#telefonoformato").show();
         }else if($("#npersonas").val()==""){
             $("#namelength").hide();
             $("#telefonoshort").hide();
             $("#telefonolength").hide();
             $("#cantidadlength").show();
         }else{
             $("#namelength").hide();
             $("#telefonoshort").hide();
             $("#telefonolength").hide();
             $("#cantidadlength").hide();

             $.ajax({
                    url: window.location.href + "/enviareserva",
                    headers:{
					   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
                    method:"post",
                    data:{formulario:$("#form-reserva").serializeArray()},
                    datatype:"json",
                    beforeSend:function(respuesta){
                        $("#antesreserva").modal("show");
                    },
                    success:function(respuesta){
                        $("#horareserva").val("");
                        $("#name").val("");
                        $("#npersonas").val("");
                        $("#phone").val("");
                        if(respuesta.success){
                            $("#antesreserva").modal("hide");
                            $("#despuesreserva").modal("show");
                        }
                    }
                });
         }
    });

	$('#Arriba').click(function(){
		$('html, body').animate({scrollTop: ($('body').offset().top)}, 1500);
	});
	$('.glyphicon-prev').click(function() {
		$('#Carousel-Restaurat').carousel('prev');
	});
	$('.glyphicon-next').click(function() {
		$('#Carousel-Restaurat').carousel('next');
	});

    $('#horareserva').datetimepicker({
        format:'DD/MM/YYYY h:mm a',
        minDate: new Date("10/27/2016"),
        stepping:5,
        locale:moment.locale('es'),
        showClose:true,
        debug:true
    });
});
