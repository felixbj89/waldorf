function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

function addrow(tabla,id,content) {
  tabla.dataTable().fnAddData( [
     content,
      "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
	  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
	  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id=" + id + ">Remover</button></div>"] );
}

var pic = undefined; var size = 0; var type = ""; var click = 0;
function openFile(event){
  pic = event;
}

var copypaste = 0;
function verrooms(){
	var editurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		editurl = window.location.href.substr(0,pos-1);
	}

	$.ajax({
			url: editurl + "/admin/verpropiedades",
			headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			beforeSend:function(respuesta){
				$("#mensajepropiedades").text("Espere un momento..");
				$("#mensajepropiedades").attr("class","text-center alerta mensaje");
				$("#antescrear").modal("show");
			},
			success:function(resultado){//RETORNARA UN OBJETO Y CARGA EL FORM
				if(resultado.respuesta==0 && resultado.size==10){
					$("#antescrear").modal("hide");
					$("#notmach").modal("show");
				}else{
					$("#notmach").modal("hide");
					$("#antescrear").modal("hide");
					$("#selectcuartos").empty();
					var cadena = "<option value='NULL'>Seleccione la habitación de su agrado</option>";
					for(i = 0; i < resultado.size; i++){
						var json = JSON.parse(resultado.cuartos[i]["rooms_especificaciones"]);
						console.log(json[1].titulo);
						
						cadena += "<option value=" + resultado.cuartos[i]["rooms_positions"] + ">" + json[1].titulo + "</option>";
					}

					$("#selectcuartos").html(cadena);
					$("#propiedadesrooms").modal("show");
				}
			}
	});
}

$(document).ready(function(){
	$("#selectvalido").hide();
  $("#titleobligatorio").hide();
  $("#todosobligatorio10").hide();
  $("#todosobligatorio11").hide();
	$("#todosobligatorio8").hide();
	$("#todosobligatorio7").hide();
	$("#successpropiedades").hide();
  $("#textoobligatorio").hide();
  $("#successchange").hide();
	$("#successthumbnail").hide();
	$("#propiedadesobligatorio1").hide();
	$("#propiedadesantessave").hide();
	$("#disabledsearch").hide();
	$("#estadoobligatorio").hide();
	$("#checkinobligatorio").hide();
	$("#checkoutobligatorio").hide();
	$("#picture_rooms").attr("disabled",true);

	$("#fileformato").hide();
	$("#filesize").hide();
	$("#fileresol").hide();
	$("#fileobligatorio").hide();
	$("#todosobligatorio2").hide();

	// initialize input widgets first
	$('#checkinout .time').timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
	});

	$('#checkinout .date').datepicker({
			'format': 'm/d/yyyy',
			'autoclose': true
	});

	// initialize datepair
	var basicExampleEl = document.getElementById('checkinout');
	var datepair = new Datepair(basicExampleEl);

	$("#btnHabCrear").on("click",function(){
		var url = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			url = window.location.href.substr(0,pos-1);
		}

		window.location.href = url + "/admin/crearhab";
	});

	$("#btnmedia").on("click",function(){
		var url = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			url = window.location.href.substr(0,pos-1);
		}

		window.location.href = url + "/admin/crearpics";
	});

	$("#btnmodifypictures").on("click",function(){
		if($("#selectcuartospicture").val()=="NULL"){
			$("#selectvalidopic").show();
		}else if(pic==undefined){
			$("#fileobligatorio").show();
		}else if(type!="image/jpeg" && type!="image/png"){
			$("#fileobligatorio").hide();
			$("#fileformato").show();
		}else if(size >= 500000){
			$("#fileobligatorio").hide();
			$("#fileformato").hide();
			$("#filesize").show();
		}
	});

	$("#checkin, #checkout, #titulohabitacion, #propiedadeshab").bind("keypress", function (event) {
			if (event.charCode!=0) {
			 var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			 var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			 if (!regex.test(key) || ($(this).val()).length > 20) {
					event.preventDefault();
					return false;
			 }
		}
	});

	$("#btnAddproperties").on("click",function(){
		if($("#propiedadeshab").val()=="" || blanco($("#propiedadeshab").val()) || $("#propiedadeshab").val().length==0){
			$("#tablalength").css("z-index","1051");
			$("#mensaje").text("Debe Ingresar una propiedad para la habitación.");
			$("#tablalength").modal("show");
		}else if($("#propiedadeshab").val().length > 0 && tablaSuperior.rows().data().length <= 19){
			addrow($("#datatable-propiedades"),"btnremove",$("#propiedadeshab").val());
		}else{
			$("#tablalength").css("z-index","1051");
			$("#mensaje").text("Limite excedido, Intente nuevamente.");
			$("#btnAddproperties").attr("disabled",true);
			$("#tablalength").modal("show");
		}
	});

	$("#selectcuartos, #titulohabitacion, #capacidad, #onhab, #checkin, #checkout, #propiedadeshab").bind("focusin",function(){
		if($("#selectvalido").is(":visible")){
			$("#selectvalido").hide();
		}

		if($("#titleobligatorio").is(":visible")){
			$("#titleobligatorio").hide();
		}

		if($("#todosobligatorio10").is(":visible")){
			$("#todosobligatorio10").hide();
		}

		if($("#todosobligatorio11").is(":visible")){
			$("#todosobligatorio11").hide();
		}

		if($("#successpropiedades").is(":visible")){
			$("#successpropiedades").hide();
		}

		if($("#textoobligatorio").is(":visible")){
			$("#textoobligatorio").hide();
		}

		if($("#successchange").is(":visible")){
			$("#successchange").hide();
		}

		if($("#propiedadesobligatorio1").is(":visible")){
			$("#propiedadesobligatorio1").hide();
		}

		if($("#propiedadesantessave").is(":visible")){
			$("#propiedadesantessave").hide();
		}

		if($("#disabledsearch").is(":visible")){
			$("#disabledsearch").hide();
		}

		if($("#checkinobligatorio").is(":visible")){
			$("#checkinobligatorio").hide();
		}

		if($("#checkoutobligatorio").is(":visible")){
			$("#checkoutobligatorio").hide();
		}

  });

  $("#titulohistory").bind("focusin",function(){
    if($("#todosobligatorio10").is(":visible")){
      $("#todosobligatorio10").hide();
    }

    if($("#todosobligatorio11").is(":visible")){
      $("#todosobligatorio11").hide();
    }

    if($("#textoobligatorio").is(":visible")){
      $("#textoobligatorio").hide();
    }

    if($("#titleobligatorio").is(":visible")){
      $("#titleobligatorio").hide();
    }

		if($("#checkinobligatorio").is(":visible")){
      $("#checkinobligatorio").hide();
    }

		if($("#checkoutobligatorio").is(":visible")){
      $("#checkoutobligatorio").hide();
    }

  });

  $("#subtitulohistory, #titulohistory, #pictureName").bind("keypress", function (event) {
      if (event.charCode!=0) {
       var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
       var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

       if (!regex.test(key) || ($(this).val()).length > 20) {
          event.preventDefault();
          return false;
       }
    }
  });
	
	/****************************************************************************************************/
	$("#editarow").on("show.bs.modal",function(){
		$("#propiedadesrooms").modal("hide");
	});
	
	var row_edit = undefined;
	$(document).on("click","#btnEditar",function(){
		var data = tablaSuperior.row($(this).parents('tr')).data();
 
		$("#rowedit").val(data[0]);
		$("#editarow").modal("show");
		row_edit = $(this).parents('tr');
		
		tablaSuperior
			.row( $(this).parents('tr') )
			.draw(false);
    });
	
	$("#btnREditar").on("click",function(){
		if(row_edit==undefined){
			tablaSuperior.row(row_edit).data(
				[
				  $("#rowedit").val(),
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnremove'>Remover</button></div>"
				]
			);
		}else{
			tablaSuperior.row(row_edit).data(
				[
				  $("#rowedit").val(),
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnremove'>Remover</button></div>"
				]
			);
		}
		$("#editarow").modal("hide");
		$("#propiedadesrooms").modal("show");
	});
	
	$(document).on("click","#btnremove",function(){
		$("#btnAddproperties").attr("disabled",false);
		$("#tablalength").modal("hide");

		tablaSuperior
			.row( $(this).parents('tr') )
			.remove()
			.draw();
	});
	
	$("#btnseepropiedades").on("click",function(){
		if($("#selectcuartos").val()=="NULL"){
			$("#titleobligatorio").hide();
			$("#todosobligatorio10").hide();
			$("#todosobligatorio11").hide();
			$("#successpropiedades").hide();
			$("#textoobligatorio").hide();
			$("#successchange").hide();
			$("#propiedadesobligatorio1").hide();
			$("#propiedadesantessave").hide();
			$("#disabledsearch").hide();
			$("#selectvalido").show();
		}else{
			$("#titleobligatorio").hide();
			$("#todosobligatorio10").hide();
			$("#todosobligatorio11").hide();
			$("#successpropiedades").hide();
			$("#textoobligatorio").hide();
			$("#successchange").hide();
			$("#propiedadesobligatorio1").hide();
			$("#propiedadesantessave").hide();
			$("#disabledsearch").hide();
			$("#selectvalido").hide();

			var select = $("#selectcuartos").val();
			var urlhab = "";
			var pos = window.location.href.search("admin");
			if(pos > 0){
				urlhab = window.location.href.substr(0,pos-1);
			}

			$.ajax({
				url: urlhab + "/admin/verhabitacion",
				headers:{
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data:{posicion:select},
				method:"post",
				beforeSend:function(respuesta){
					$("#propiedadesring").removeClass("ocultar");
					$("#propiedadesring").addClass("mostrar");
				},
				success:function(resultado){
					$("#propiedadesring").removeClass("mostrar");
					$("#propiedadesring").addClass("ocultar");
					var json = JSON.parse(resultado["rooms_especificaciones"]);
					$("#titulohabitacion").attr("disabled",false);
					$("#capacidad").attr("disabled",false);
					$("#onhab").attr("disabled",false);
					$("#checkin").attr("disabled",false);
					$("#checkout").attr("disabled",false);
					$("#propiedadeshab").attr("disabled",false);

					$("#onhab").val(json[0].activo);
					$("#titulohabitacion").val(json[1].titulo);
					$("#capacidad").val(json[1].capacidad);
					$("#checkin").val(json[1].in);
					$("#checkout").val(json[1].out);

					$("#datatable-propiedades").dataTable().fnClearTable();
					for(i = 0; i < json[1].especificaciones.length; i++){
						$("#datatable-propiedades").dataTable().fnAddData( [
					    json[1].especificaciones[i],
						"<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
						"<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
						"<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnremove'>Remover</button></div>"] );
					}
				}
			});
		}
	});
	
	var tablaSuperior = $("#datatable-propiedades").DataTable({
		"searching": false,
		"bInfo": false,
        "bSort": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 4,
		"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},

		"fixedHeader": {
			 "header": false,
			 "footer": false
		 },
		"bPaginate": true,
		"bAutoWidth": false,
	});
	
	$("#btnmodifypropieades").on("click",function(){
		if($("#titulohabitacion").is(":enabled")){
			if($("#titulohabitacion").val().length==0 || blanco($("#titulohabitacion").val())){
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").show();
			}else if($("#selectcuartos").val()=="NULL"){
				$("#propiedadesobligatorio1").hide();
				$("#propiedadesantessave").show();
			}else if($("#onhab").val()=="NULL"){
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").hide();
				$("#estadoobligatorio").show();
			}else if($("#checkin").val()=="" && $("#checkout").val()==""){
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").hide();
				$("#estadoobligatorio").hide();
				$("#checkinobligatorio").show();
				$("#checkoutobligatorio").show();
			}else if($("#checkin").val()==""){
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").hide();
				$("#estadoobligatorio").hide();
				$("#checkinobligatorio").show();
			}else if($("#checkout").val()==""){
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").hide();
				$("#estadoobligatorio").hide();
				$("#checkoutobligatorio").show();
			}else{
				var lista = "";
				for(i = 0; i < tablaSuperior.rows().data().length; i++){
					lista += "\"" + tablaSuperior.rows().data()[i][0] + "\",";
				}

				$("#propilist").attr("value",lista);
				$("#titleobligatorio").hide();
				$("#todosobligatorio10").hide();
				$("#todosobligatorio11").hide();
				$("#successpropiedades").hide();
				$("#textoobligatorio").hide();
				$("#successchange").hide();
				$("#propiedadesantessave").hide();
				$("#disabledsearch").hide();
				$("#selectvalido").hide();
				$("#propiedadesobligatorio1").hide();
				$("#estadoobligatorio").hide();
				$("#checkoutobligatorio").hide();

				var urlmodify = "";
				var pos = window.location.href.search("admin");
				if(pos > 0){
					urlmodify = window.location.href.substr(0,pos-1);
				}
				
				$.ajax({
					url: urlmodify + "/admin/modifyrooms",
					headers:{
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data:{
						posicion:$("#selectcuartos").val(),
						titulo:$("#titulohabitacion").val(),
						capacidad:$("#capacidad").val(),
						estado:$("#onhab").val(),
						in:$("#checkin").val(),
						out:$("#checkout").val(),
						lista:$("#propilist").val()
					},
					method:"post",
					beforeSend:function(respuesta){
						$("#antescrear").css("z-index","1051");
						$("#mensajepropiedades").text("Espere un momento..");
						$("#mensajepropiedades").attr("class","text-center alerta mensaje");
						$("#antescrear").modal("show");
					},
					success:function(resultado){//RETORNARA UN OBJETO Y CARGA EL FORM
						$("#antescrear").modal("hide");
						if(resultado.respuesta==1){
							$("#selectcuartos").empty();
							var cadena = "<option value='NULL'>Seleccione la habitación de su agrado</option>";
							for(i = 0; i < resultado.size; i++){
								var json = JSON.parse(resultado.cuartos[i]["rooms_especificaciones"]);
								cadena += "<option value=" + resultado.cuartos[i]["rooms_positions"] + ">" + json[1].titulo + "</option>";
							}

							$("#selectcuartos").html(cadena);

							$("#successpropiedades").attr("class","center alert-success fixed-wait fixed-po");
							$("#successpropiedades").text("Modificación Sastifactoria");
							$("#successpropiedades").show();
						}else{
							$("#successpropiedades").attr("class","center alert-danger fixed-wait fixed-po");
							$("#successpropiedades").text("No se pudo modificar la habitación");
							$("#successpropiedades").hide();
						}
					}
				});
			}
		}else{
			$("#propiedadesobligatorio1").hide();
			$("#propiedadesantessave").hide();
			$("#disabledsearch").show();
		}
	});
	
	$(document).on("click",".btndown",function(){
		var index = tablaSuperior.row($(this).parents('tr')).index();
		var size = tablaSuperior.rows().data().length;
		console.log(index + " " + size);
		if((index+1) < size){
			var actual = tablaSuperior.row(index).data();
			actual.order += 1;
			
			var proxima = tablaSuperior.row(index + 1).data();//I+1
			actual.order -= 1;
			
			tablaSuperior.row(index).data(proxima);
			tablaSuperior.row(index + 1).data(actual);
		}
	});
	
	$("#textoprincipal").on("show.bs.modal",function(){
		$("#titulohistory").val("");
		$('.summernote').summernote("code","");
	});
	
	$("#textoprincipal").on("hidden.bs.modal",function(){
		$("#successchange").text("");
	})
	
	$("#btnaddtextoprincipal").on("click",function(){
		var texto = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if($("#titulohistory").val().length==0 || blanco($("#titulohistory").val()) && texto.trim().length==0){
			$("#todosobligatorio10").show();
			$("#todosobligatorio11").show();
		}else if($("#titulohistory").val().length==0 || blanco($("#titulohistory").val())){
			$("#todosobligatorio10").hide();
			$("#todosobligatorio11").hide();
			$("#titleobligatorio").show();
		}else if(texto.trim().length==0){
			$("#todosobligatorio10").hide();
			$("#todosobligatorio11").hide();
			$("#titleobligatorio").hide();
			$("#textoobligatorio").show();
		}else{
			var editurl = "";
			var pos = window.location.href.search("admin");
			if(pos > 0){
				editurl = window.location.href.substr(0,pos-1);
			}

			$("#texto").attr("value",$('.summernote')[0].value);

			$.ajax({
				url: editurl + "/admin/addtextprincipal",
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:"post",
				data:{origen:"0",section:"2",texto:$("#texto").val(),titulo:$("#titulohistory").val()},
				beforeSend:function(respuesta){
					$("#picturering").removeClass("ocultar");
					$("#picturering").addClass("mostrar");
				},
				success:function(resultado){//RETORNARA UN OBJETO Y CARGA EL FORM
					if(resultado.respuesta=="1"){
						$("#picturering").removeClass("mostrar");
						$("#picturering").addClass("ocultar");
						$("#successchange").show();
					}else{
						$("#successchange").hide();
					}
				}
			});
		}
	});
  
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			}
		},
		placeholder: 'Ingrese su texto principal',
		height: 400,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: true 
	});
});
