$(document).ready(function() {
  $("#editring").show();
  $("#usuarioperfiltipo").hide();
  var tabla;
  var estado = $("<input/>");
  estado.attr("id","usuariostatus");
  estado.attr("type","checkbox");
  estado.attr("class","js-switch");
  $("#userestado").empty();
  $("#userestado").append(estado);
  $("#camporegistradologin").hide();
  $("#camporegistradopassword").hide();
  $("#camporegistradocpassword").hide();

  var elem = document.querySelector(".js-switch");
  var switchery = new Switchery(elem, { color: '#f90202', secondaryColor: '#00470f',jackColor: '#FFFFFF' });

  var handleDataTableButtons = function() {
    if ($("#datatable-buttons").length) {
      tabla = $("#datatable-buttons").DataTable({
        "searching": false,
        "bInfo": false,
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
          	"sFirst":    "Primero",
          	"sLast":     "Último",
          	"sNext":     "Siguiente",
          	"sPrevious": "Anterior"
          },
          "oAria": {
          	"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          	"sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        },
        "bAutoWidth": true,
        responsive: true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        dom: "Bfrtip",
        buttons: [
          {
            extend: "copy",
            text:"Copiar",
            className: "btn-sm",
            enabled:false,
            exportOptions: {
                columns: [ 0, ':visible' ]
            }
          },{
            extend: "excel",
            text:"Exportar a Excel",
            className: "btn-sm"
          },{
            extend: "pdfHtml5",
            text:"Exportar a P.D.F.",
            className: "btn-sm"
          },{
            extend: "print",
            text:"Imprimir",
            className: "btn-sm",
            enabled:false,
            exportOptions: {
                columns: [ 0, ':visible' ]
            }
          },
        ],
          responsive: true
      });
    }
  };

  TableManageButtons = function() {
    "use strict";
    return {
      init: function() {
        handleDataTableButtons();
      }
    };
  }();

  TableManageButtons.init();

  $("#uregistradologin").bind("keypress", function (event) {
      if (event.charCode!=0) {
       var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
       var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

       if (!regex.test(key) || ($(this).val()).length > 10) {
          event.preventDefault();
          return false;
       }
    }
  });

  $("#uregistradopassword").bind("keypress", function (event) {
      if (event.charCode!=0) {
       var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9]+$");
       var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

       if (!regex.test(key) || ($(this).val()).length > 10) {
          event.preventDefault();
          return false;
       }
    }
  });

  $("#usuarioperfil, #uregistradologin, #uregistradopassword, #userregistradoconfirmar").bind("focusin",function(){
    if($("#usuarioperfiltipo").is(":visible")){
      $("#usuarioperfiltipo").hide();
    }

    if($("#camporegistradologin").is(":visible")){
        $("#camporegistradologin").hide();
    }

    if($("#camporegistradopassword").is(":visible")){
      $("#camporegistradopassword").hide();
    }

    if($("#camporegistradocpassword").is(":visible")){
      $("#camporegistradocpassword").hide();
    }
  });

  var click = 0;

  $(document).on("click","#btnEditarUsuarioRegistrado",function(){
      if($("#usuarioperfil").val()=="-1"){
        $("#usuarioperfiltipo").show();
      }else if($("#uregistradologin").val().length < 3){
        $("#usuarioperfiltipo").hide();
        $("#camporegistradocpassword").hide();
        $("#camporegistradologin").show();
      }else{
        $("#camporegistradologin").hide();
        $("#camporegistradocpassword").hide();
        $("#usuarioperfiltipo").hide();

        if(!$("#usuariostatus").is(":checked"))
          click = 1;
        else
          click = 0;

        //ajax..
        $.ajax({
            url: $("#rutaeditaruser").val(),
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data:{formulario:$("#form-editar").serializeArray(),estado:click},
            datatype:"json",
            beforeSend:function(respuesta){
                $("#editring").show();
                $("#editprofile").modal("show");
            },
            success:function(respuesta){
                if(respuesta.resultado==1){
                    //pase de parametros.
                    click = 0;
                    $("#editring").hide();
                    $("#editprofile").modal("show");
                }
            }
        }).done(function(respuesta){
            tabla.destroy();
            window.location.reload();
        });
      }
  });

  var fila;
  $(document).on("click","#btnremover",function(){
    var urlfiltrada = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			urlfiltrada = window.location.href.substr(0,pos-1)
		}

    $.ajax({
        url: urlfiltrada + "/admin/remove",
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"email":$("#correohidden").val()},
        success:function(respuesta){
            if(respuesta.resultado==1){
                //pase de parametros.
                tabla.row(fila).remove().draw(false);
            }
        }
    }).done(function(respuesta){
      if(tabla.rows().data().length==0){
        window.location.reload();
      }
    });
  });

  $(document).on("click","#btnEliminarUsuario",function(){
      var email = $(this).parent("td").parent("tr").data("email");
      fila = $(this).parent("td").parent("tr");
      $("#correohidden").attr("value",email);
      $("#confirmdelete").modal("show");
  });

  $(document).on("click","#btnEditarUsuario",function(){
    var email = $(this).parent("td").parent("tr").data("email");
    $.ajax({
        url: $("#rutalist").val(),
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"email":email},
        beforeSend:function(respuesta){
          $("#uregistradologin").val("");
          $("#uregistradopassword").val("");
          $("#editprofile").modal("show");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
                //pase de parametros.
                $("#usuarionombre").attr("value",respuesta.usuario["user_name"]);
                $("#usuariocorreo").attr("value",respuesta.usuario["user_email"]);
                $("#usuarioperfil").val(respuesta.usuario["user_role"]);
                $("#uregistradologin").val(respuesta.usuario["user_login"]);
                $("#uregistradopassword").val(respuesta.usuario["user_password"]);
                $("#userregistradoconfirmar").val(respuesta.usuario["user_password"]);
                $("#userpasshidden").val(respuesta.usuario["user_password"]);
                $("#editring").hide();
                $("#editprofile").modal("show");
            }
        }
    });
  });

  $("#btnEditarUsuariotool").on("click",function(){
    tabla.column( 5 ).visible( true );
    tabla.column( 6 ).visible( true );
    tabla.buttons().enable(false);
    $(this).attr("disabled",true)

    $("#btnEditarUsuariotool").text("Activar");
    $("#btnRespaldarUsuario").text("Desactivar");
    $("#btnEditarUsuariotool").attr("disabled",true);
    $("#btnRespaldarUsuario").attr("disabled",false);
  });

  $("#btnRespaldarUsuario").on("click",function(){
    tabla.column( 5 ).visible( false );
    tabla.column( 6 ).visible( false );
    tabla.buttons().enable(true);

    $("#btnEditarUsuariotool").text("Desactivar");
    $("#btnRespaldarUsuario").text("Activar");
    $("#btnEditarUsuariotool").attr("disabled",false);
    $("#btnRespaldarUsuario").attr("disabled",true);
  })

});
