var pic = undefined; 
var r1 = undefined; 
var r2 = undefined;
var r3 = undefined; 
var r4 = undefined;
var r5 = undefined; 
var r6 = undefined; 

var type; var size; var click = 0; editar = 0;
var uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0;
function openFile(event){
  pic = event;
  $("#messagevalidacion").text("");
}

function openRooms1(event){
	r1 = event;
	$("#messagevalidacion").text("");
}

function openRooms2(event){
	r2 = event;
	$("#messagevalidacion").text("");
}

function openRooms3(event){
	r3 = event;
	$("#messagevalidacion").text("");
}

function openRooms4(event){
	r4 = event;
	$("#messagevalidacion").text("");
}

function openRooms5(event){
	r5 = event;
	$("#messagevalidacion").text("");
}

function openRooms6(event){
	r6 = event;
	$("#messagevalidacion").text("");
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

function confirmar(){
  cadena1 = $("#preview1").attr("src").substr($("#preview1").attr("src").search("default"),$("#preview1").attr("src").length);
  cadena2 = $("#preview2").attr("src").substr($("#preview2").attr("src").search("default"),$("#preview2").attr("src").length);
  cadena3 = $("#preview3").attr("src").substr($("#preview3").attr("src").search("default"),$("#preview3").attr("src").length);
  cadena4 = $("#preview4").attr("src").substr($("#preview4").attr("src").search("default"),$("#preview4").attr("src").length);
  cadena5 = $("#preview5").attr("src").substr($("#preview5").attr("src").search("default"),$("#preview5").attr("src").length);
  cadena6 = $("#preview6").attr("src").substr($("#preview6").attr("src").search("default"),$("#preview6").attr("src").length);

  if(cadena1=="default.jpg" && cadena2=="default.jpg" && cadena3=="default.jpg" && cadena4=="default.jpg"
  && cadena5=="default.jpg" && cadena6=="default.jpg"){
    return 1;
  }
  return 0;
}

function addrow(tabla,id,content) {
  tabla.dataTable().fnAddData( [
      content,
      "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
	  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
	  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id=" + id + ">Remover</button></div>"] );
}

$(document).ready(function() {
	var row_edit = undefined;
	$("#propiedades").bind("keypress", function (event) {
		if (event.charCode!=0) {
		   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
		   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

		   if (!regex.test(key) || ($(this).val()).length > 172) {
			  event.preventDefault();
			  return false;
		   }
		}
	});
	
	$("#titulohistory").bind("keypress", function (event) {
        if (event.charCode!=0) {
         var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
         var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

         if (!regex.test(key) || ($(this).val()).length > 20) {
            event.preventDefault();
            return false;
         }
      }
    });
	
	$(document).on("click",".btndown",function(){
		var index = tablaSuperior.row($(this).parents('tr')).index();
		var size = tablaSuperior.rows().data().length;
		console.log(index + " " + size);
		if((index+1) < size){
			var actual = tablaSuperior.row(index).data();
			actual.order += 1;
			
			var proxima = tablaSuperior.row(index + 1).data();//I+1
			actual.order -= 1;
			
			tablaSuperior.row(index).data(proxima);
			tablaSuperior.row(index + 1).data(actual);
		}
	});
	
	$(document).on("click","#btnEditar",function(){
		var data = tablaSuperior.row($(this).parents('tr')).data();
 
		console.log(data[0]);
		$("#rowedit").val(data[0]);
		$("#editarow").modal("show");
		row_edit = $(this).parents('tr');
		
		tablaSuperior
			.row( $(this).parents('tr') )
			.draw(false);
    });
	
	$("#btnREditar").on("click",function(){
		if(row_edit==undefined){
			tablaSuperior.row(row_edit).data(
				[
				  $("#rowedit").val(),
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnremove'>Remover</button></div>"
				]
			);
		}else{
			tablaSuperior.row(row_edit).data(
				[
				  $("#rowedit").val(),
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" class='btndown'>Mover</button></div>" +
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnEditar'>Modificar</button></div>" + 
				  "<div class='col-xs-12 col-sm-4 col-md-4'><button type=\"button\" id='btnremove'>Remover</button></div>"
				]
			);
		}
		$("#editarow").modal("hide");
	});
	
	$(document).on("click","#btnremove",function(){
      $("#btnAddproperties").attr("disabled",false);
      $("#tablalength").modal("hide");

      tablaSuperior
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });
	
	$("#btnAddproperties").on("click",function(){
      if($("#propiedades").val()=="" || blanco($("#propiedades").val()) || $("#propiedades").val().length==0){
        $("#mensaje").text("Debe Ingresar una propiedad para la habitación.");
        $("#tablalength").modal("show");
      }else if($("#propiedades").val().length > 0 && tablaSuperior.rows().data().length <= 19){
        addrow($("#datatable-propiedades"),"btnremove",$("#propiedades").val());
      }else{
        $("#mensaje").text("Limite excedido, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show");
      }
    });
	
	var _URL = window.URL || window.webkitURL;
	$("#imagen_portada").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = _URL.createObjectURL(file);
		  $("#loading_portada").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms1").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms1").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms2").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms2").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms3").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms3").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms4").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms4").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms5").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms5").attr("src",img.src);
      }
    });
	
	var rooms1 = window.URL || window.webkitURL;
	$("#imagen_rooms6").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = rooms1.createObjectURL(file);
		  $("#rooms6").attr("src",img.src);
      }
    });
   
	var tablaSuperior = $("#datatable-propiedades").DataTable({
			"searching": false,
			"bInfo": false,
			"Info":false,
			"bSort":false,
			"bLengthChange": false,
			"iDisplayLength": 5,
			"language": {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
			},
			"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},

		"fixedHeader": {
			"header": true,
			"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
    $("#picture_galeria").change(function(e) {
      if(this.files.length > 6){
        $("#mensaje").text("Limite excedido, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show")
      }else if(this.files.length < 6){
        $("#mensaje").text("Debe ingresar 6 imágenes en total, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show")
      }else{
        for(i = 0; i < this.files.length; ++i){
          var file, img;
          if ((file = this.files[i])) {
            img = new Image();
            img.onload = function() {
                console.log(this.width);
            };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
            $("#preview" + (i+1)).attr("src",img.src);
          }
        }
      }
    });

    $("#pictureName, #checkin, #estado, #checkout, #propiedades, #capcidadmax, #posicion, #titulohistory").bind("focusin",function(){
		$("#messagevalidacion").text("");
		$(".errormensaje").text("");
    });

    // initialize input widgets first
    $('#checkinout .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    // initialize datepair
	var basicExampleEl = document.getElementById('checkinout');
    var datepair = new Datepair(basicExampleEl);

    $("#btnadd").on("click",function(){
		if($("#propiedades").val().length==0 && $("#titulohistory").val().length==0 && $("#capcidadmax").val()=="" && $("#checkin").val()=="" && $("#checkout").val()=="" && $("#posicion").val()==""
		&& $("#estado").val()=="NULL" && pic==undefined  && r1==undefined && r2==undefined && r3==undefined && r4==undefined && r5==undefined && r6==undefined){
			$("#messagevalidacion").text("Los campos son obligatorios.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#propiedades").val().length==0){
			$("#messagevalidacion").text("Debe ingresar las propiedades de su habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(tablaSuperior.rows().data().length > 20){
			$("#messagevalidacion").text("Debe ingresar de 1-20 propiedades para una habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#titulohistory").val().length==0){
			$("#messagevalidacion").text("Debe ingresar el título de su habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#capcidadmax").val()==""){
			$("#messagevalidacion").text("Debe ingresar una capacidad máxima para su habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#checkin").val()=="" || $("#checkout").val()==""){
			$("#messagevalidacion").text("Debe ingresar el check IN/OUT para la habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#posicion").val()==""){
			$("#messagevalidacion").text("Debe ingresar la posición de su habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#estado").val()=="NULL"){
			$("#messagevalidacion").text("Debe ingresar un estado para su habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(r1==undefined && r2==undefined && r3==undefined && r4==undefined && r5==undefined && r6==undefined){
			$("#messagevalidacion").text("Debe ingresar la galería de la habitación.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			var lista = "";
			for(i = 0; i < tablaSuperior.rows().data().length; i++){
				lista += "\"" + tablaSuperior.rows().data()[i][0] + "\",";
			}
			
			$("#propilist").attr("value",lista);
			$("#form-rooms").submit();
		}
    });
});
