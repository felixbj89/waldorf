jQuery(function ($) {
	$('#ArribaHome').click(function(){
		$('html, body').animate({scrollTop: ($('body').offset().top)}, 1500);
	});
	
	$(window).scroll(function () {
		var scrollTop = $(window).scrollTop();
		var cssbtn = $('#btn-wal-res-float').css('right')
		if ((scrollTop > 945)&&($('#btn-wal-res-float').is(":hidden"))){
			$('#btn-wal-res-float').show();
			$('#btn-wal-res-float').animate({'right': '0px'});
		}else if((scrollTop < 345)&&($('#btn-wal-res-float').is(":visible"))){
			$('#btn-wal-res-float').animate({'right': '-999px'});
			$('#btn-wal-res-float').hide();
		}
	});

	$("#btn-wal-res, #btn-wal-res-float").on("click",function(){
		if(window.location.href.search("Habitaciones")==-1 && window.location.href.search("Restaurante")==-1 && window.location.href.search("Contacto")==-1
		&& window.location.href.search("hotel")==-1){
			window.location.href =  (window.location.href + "waldorf/reservar");
		}else{
		    if(window.location.href.search("Habitaciones")!=-1 && window.location.href.search("Restaurante")==-1 && window.location.href.search("hotel")!=-1){
		        window.location.href =  (window.location.href + "/waldorf/reservar");
			}else if(window.location.href.search("Habitaciones")==-1 && window.location.href.search("Restaurante")!=-1 && window.location.href.search("hotel")!=-1){
				window.location.href =  (window.location.href + "/waldorf/reservar");
			}else if(window.location.href.search("Habitaciones")==-1 && window.location.href.search("Restaurante")==-1 && window.location.href.search("hotel")!=-1){
				window.location.href =  (window.location.href + "/waldorf/reservar");
			}else if(window.location.href.search("Habitaciones")==-1 && window.location.href.search("Restaurante")==-1 && window.location.href.search("Contacto")!=-1){
				var url = window.location.href.substr(0,window.location.href.search("Home"));
				window.location.href =  (url + "waldorf/reservar");
			}
		}
	});

	$('#btn-habitaciones').click(function () {
		if ($("#footer-habitaciones").is(":hidden")) {
			$("#footer-habitaciones").slideDown("slow");
		} else {
			$("#footer-habitaciones").slideUp();
		}
	});
	$('#btn-nuestroh').click(function () {
		if ($("#footer-nuestroh").is(":hidden")){
			$("#footer-nuestroh").slideDown("slow");
		} else {
			$("#footer-nuestroh").slideUp();
		}
	});

	$("#campologin").hide();
	$("#campopassword").hide();

	var posicion = window.location.href.search("admin/login");
	if(posicion > 0 && $("#abrirmodal").val()=="1"){
		$("#login").val("");
		$("#password").val("");
		$("#acceder").modal("show");
	}

	$("#loginclose").on("click",function(){
		$("#acceder").modal("hide");
	})
	
	$("#acceder").on("shown.bs.modal",function(){
		$("#login").focus()
	});
	
	$("#btnEnviar").on("click",function(){
		if($("#login").val().length==0 && $("#password").val().length==0){
			$("#campologin").show();
			$("#campopassword").show();
		}else if($("#login").val().length==0){
			$("#campopassword").hide();
			$("#campologin").show();
		}else if($("#password").val().length==0){
			$("#campologin").hide();
			$("#campopassword").show();
		}else{
			$("#campologin").hide();
			$("#campopassword").hide();
			$("#form-login").submit();
		}
	});
})
