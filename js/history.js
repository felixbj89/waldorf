var pic; var type; var size; var editar = 0;
function openFile(event){
  pic = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

$(document).ready(function() {
  $("#todosobligatorio1").hide();
  $("#todosobligatorio2").hide();
  $("#todosobligatorio3").hide();
  $("#subobligatorio").hide();
  $("#priobligatorio").hide();
  $("#fileobligatorio").hide();
  $("#fileformato").hide();
  $("#filesize").hide();
  $("#successchange").hide();
  $("#fileresol").hide();
  $("#removerchange").hide();

  var tablaHistorial = $("#datatable-history").DataTable({
    "searching": false,
    "bInfo": false,
    "Info":false,
    "bLengthChange": false,
    "iDisplayLength": 1,
    "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },

    "fixedHeader": {
       "header": true,
       "footer": true
     },
    "bPaginate": true,
    "bAutoWidth": false,
  });

  $("#file_picture").on("change",function(){
      var input = pic.target;
      var reader = new FileReader();
      reader.onload = function(){
        var dataURL = reader.result;
        $("#preview").attr("src",dataURL);
      };

      type = input.files[0].type;
      size = input.files[0].size;

      reader.readAsDataURL(input.files[0]);
  });

  var _URL = window.URL || window.webkitURL;

  $("#file_picture").change(function(e) {
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
          if(this.width > 0 && this.width <= 300  && this.height > 0 && this.height <= 200){
            $("#fileresol").hide();
            $("#preview").attr("style","width:40% !important");
          }else{
            $("#fileresol").show();
            $("#preview").attr("style","width:100% !important");
            $("#preview").attr("src",$("#defaultimagen").val());
          }
        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
    }
});

  $("#subtitulohistory, #titulohistory, #file_picture").bind("focusin",function(){
    if($("#todosobligatorio1").is(":visible")){
      $("#todosobligatorio1").hide();
    }

    if($("#todosobligatorio2").is(":visible")){
      $("#todosobligatorio2").hide();
    }

    if($("#todosobligatorio3").is(":visible")){
      $("#todosobligatorio3").hide();
    }

    if($("#subobligatorio").is(":visible")){
      $("#subobligatorio").hide();
    }

    if($("#priobligatorio").is(":visible")){
      $("#priobligatorio").hide();
    }

    if($("#fileobligatorio").is(":visible")){
      $("#fileobligatorio").hide();
    }

    if($("#fileformato").is(":visible")){
      $("#fileformato").hide();
    }

    if($("#filesize").is(":visible")){
      $("#filesize").hide();
    }

    if($(".errormensaje").is(":visible")){
      $(".errormensaje").hide();
    }
  });

  $("#titulohistory, #subtitulohistory").bind("keypress", function (event) {
      if (event.charCode!=0) {
       var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
       var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

       if (!regex.test(key) || ($(this).val()).length > 20) {
          event.preventDefault();
          return false;
       }
    }
  });

  $("#btneditarHistory").on("click",function(){
    //ajax..
	var textoeditar = $("#modoEditar").val();
    var editurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			editurl = window.location.href.substr(0,pos-1);
		}

    $.ajax({
        url: editurl + "/admin/edithistory",
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{origen:"0"},
        success:function(respuesta){//RETORNARA UN OBJETO Y CARGA EL FORM
          if(respuesta.resultado=="1"){
			$("#mensajeeditar").text(textoeditar);
			$("#modaleditar").modal("show");
			  
            $("#notmach").modal("hide");
            var json = JSON.parse(respuesta.texto["text_json"]);
            $("#titulohistory").attr("value",json[0]["titulo"]);
            $("#subtitulohistory").attr("value",json[0]["subtitulo"]);
			$("#descripcion").summernote("code",json[0]["texto"]);
            editar = 1;
            $("#preview").attr("style","width:auto !important");
            $("#preview").attr("src",$("#url").val() + "/" + respuesta.imagen);
          }else{
            $("#notmach").modal("show");
          }
        }
    });
  });

  $(document).on("click","#removerhistorial",function(){
    var status = $(".fixed-button-historial").parents("tr").data("status");
	var seleccion = $(".fixed-button-historial").parents("tr");
    var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    $.ajax({
        url: urlfiltrada + '/admin/removepicture',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"pictureid":status},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.estado==1){
              $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");

              tablaHistorial.row(seleccion).remove().draw();
              $("#removerchange").show();
            }
        }
    });
  });

  $("#btncerrarhismensaje").on("click",function(){
    window.location.reload();
  });

  $("#historialhistory").on("show.bs.modal",function(){
    $("#successchange").hide();
    $("#removerchange").hide();
  });

  $(document).on("click","#historialbtn",function(){
    $("#removerchange").hide();
	var picture = $(".fixed-button-historial").parents("tr").data("date");
    var status = $(".fixed-button-historial").parents("tr").data("status");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    $.ajax({
        url: urlfiltrada + '/admin/changepicture',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"picture":picture,"tipo":$("#tiposec").val()},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
			  $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");
              $("#newimg_" + status).attr("id","newimg_" + respuesta.status);
              $("#newimg_" + respuesta.status).parents("tr").attr("data-date",respuesta.date);
              $("#newimg_" + respuesta.status).parents("tr").attr("data-status",respuesta.status);
              $("#newimg_" + respuesta.status).attr("src",respuesta.imagen);
              $("#successchange").show();
            }
        }
    }).done(function(respuesta){
        if(respuesta.resultado==1){
          $("#historialhistory").modal("hide");
          $("#despueshistorial").modal("show");
        }
    });
  });

  $("#btnaddhistory, #btnaddhistorymobile").on("click",function(){
    if(pic==undefined && $("#titulohistory").val().length==0 && $("#subtitulohistory").val().length==0){
      $("#todosobligatorio1").show();
      $("#todosobligatorio2").show();
      $("#todosobligatorio3").show();
	  
		$("#message_advertencia").text("Todos los campos son obligatorios.");
		$("#modal_alerta").modal("show");
    }else if(editar==0 && pic==undefined){
		$("#message_advertencia").text("La imagen es obligatoria.");
		$("#modal_alerta").modal("show");
	  
		$("#fileobligatorio").show();
    }else if(editar==0 && type!="image/jpeg" && type!="image/png"){
		$("#message_advertencia").text("El formato de la imagen es incorrecto (jpeg/png).");
		$("#modal_alerta").modal("show");
		
      $("#fileformato").show();
    }else if(editar==0 && size >= 500000){
		$("#message_advertencia").text("El tamaño de la imagen no puede ser mayor a 5MB.");
		$("#modal_alerta").modal("show");
		
      $("#filesize").show();
    }else if($("#titulohistory").val().length == 0){
		$("#message_advertencia").text("El título de su historia es obligatorio.");
		$("#modal_alerta").modal("show");
		
      $("#fileobligatorio").hide();
      $("#priobligatorio").show();
    }else if($("#subtitulohistory").val().length == 0){
		$("#message_advertencia").text("El subtítulo de su historia es obligatorio.");
		$("#modal_alerta").modal("show");
		
      $("#fileobligatorio").hide();
      $("#subobligatorio").show();
    }else{
		$("#texto").attr("value",$("#descripcion").summernote("code"));
		$("#fileobligatorio").hide();
		$("#fileformato").hide();
		$("#filesize").hide();
		$("#priobligatorio").hide();
		$("#subobligatorio").hide();
		$("#form-history").submit();
    }
  });

	/***************************************************************/
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '32'],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese la historia de su hotel de su agrado"
	});
});
