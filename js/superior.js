var pic; var type; var size;
function openFile(event){
  pic = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

$(document).ready(function() {
    $(".clienteerror").hide();
	
    $("#fileobligatorio").hide();
    $("#todosobligatorio1").hide();
    $("#todosobligatorio2").hide();
    $("#fileformato").hide();
    $("#filesize").hide();
    $("#successchange").hide();
    $("#fileresol").hide();
    $("#removerchange").hide();
    $(".modalmensaje").hide();
	
	$("#loading_superior").bind("click",function(){
		$("#banner_superior").attr("style","display:block;width: 100rem;height: 100rem;");
	});
	
    var tablaSuperior = $("#datatable-superior").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },

      "fixedHeader": {
         "header": true,
         "footer": true
       },
      "bPaginate": true,
      "bAutoWidth": false,
    });

    $("#historialsuperior").on("show.bs.modal",function(){
      $("#successchange").hide();
      $("#removerchange").hide();
      $(".modalmensaje").hide();
    });

    $("#btncrearclose").on("click",function(){
      window.location.reload();
    });
	
	var _URL = window.URL || window.webkitURL;
	$("#banner_superior").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				console.log(file);
			};
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#loading_superior").attr("src",img.src);
		}
	});

    $("#pictureName, #file_superior").bind("focusin",function(){
      if($("#todosobligatorio1").is(":visible")){
        $("#todosobligatorio1").hide();
      }

      if($("#todosobligatorio2").is(":visible")){
        $("#todosobligatorio2").hide();
      }

      if($("#fileobligatorio").is(":visible")){
        $("#fileobligatorio").hide();
      }

      if($("#pictureobligatorio").is(":visible")){
        $("#pictureobligatorio").hide();
      }

      if($(".errormensaje").is(":visible")){
        $(".errormensaje").hide();
      }
    });

    $("#pictureName").bind("keypress", function (event) {
		    if (event.charCode!=0) {
			   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			   if (!regex.test(key) || ($(this).val()).length > 20) {
				    event.preventDefault();
				    return false;
			   }
      }
		});
   
	$("#btnadd").on("click",function(){
		if(pic==undefined){
			$(".clienteerror").text("Debe cargar una imagen antes de continuar");
			$(".clienteerror").show();
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			$("#form-picture").submit();
		}
	});
});
