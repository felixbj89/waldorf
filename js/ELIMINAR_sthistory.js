var pic; var type; var size; var editar = 0;
function openFile(event){
  pic = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

$(document).ready(function() {
  $("#todosobligatorio1").hide();
  $("#todosobligatorio2").hide();
  $("#todosobligatorio3").hide();
  $("#subobligatorio").hide();
  $("#priobligatorio").hide();
  $("#fileobligatorio").hide();
  $("#fileformato").hide();
  $("#filesize").hide();
  $("#successchange").hide();
  $("#fileresol").hide();
  $("#removerchange").hide();

  var tablaHistorial = $("#datatable-history-st").DataTable({
    "searching": false,
    "bInfo": false,
    "Info":false,
    "bLengthChange": false,
    "iDisplayLength": 1,
    "language": {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
      },
      "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    "bPaginate": true,
    "bAutoWidth": false,
  });

  $("#bloque_principal").on("click",function(){
      $("#removerchange").hide();
      $("#successchange").hide();
  });

  $("#btnhistorialclose").on("click",function(){
    window.location.reload();
  });

  $(document).on("click","#removerhistorial",function(){
    $("#successchange").hide();

    var picture = $(".fixed-button-historial").parents("tr").data("date");
    var seleccion = $(".fixed-button-historial").parents("tr");
    var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    var tipo = 1;
    if($("#tiposec").val()=="3"){
      tipo = 2;
    }

    $.ajax({
        url: urlfiltrada + '/admin/remover',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"picture":picture,"tipo":tipo},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
              $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");

              tablaHistorial.row(seleccion).remove().draw();
              $("#removerchange").show();
            }
        }
    });
  });

  $(document).on("click","#historialbtn",function(){
    $("#removerchange").hide();

    var picture = $(".fixed-button-historial").parents("tr").data("date");
    var status = $(".fixed-button-historial").parents("tr").data("status");
    var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

    $.ajax({
        url: urlfiltrada + '/admin/changepicture',
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:"post",
        data:{"picture":picture,"tipo":$("#tiposec").val()},
        beforeSend:function(respuesta){
            $("#picturering").removeClass("ocultar");
            $("#picturering").addClass("mostrar");
        },
        success:function(respuesta){
            if(respuesta.resultado==1){
              $("#picturering").removeClass("mostrar");
              $("#picturering").addClass("ocultar");
              $("#newimg_" + status).attr("id","newimg_" + respuesta.status);
              $("#newimg_" + respuesta.status).parents("tr").attr("data-date",respuesta.date);
              $("#newimg_" + respuesta.status).parents("tr").attr("data-status",respuesta.status);
              $("#newimg_" + respuesta.status).attr("src",respuesta.imagen);
              $("#successchange").show();
            }
        }
    }).done(function(respuesta){
        if(respuesta.resultado==1){
          $("#despueshhistorial").modal("show");
        }
    });
  });
});
