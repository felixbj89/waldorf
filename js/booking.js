$(document).ready(function(){
  $("#campourldeveloper").hide();
  $("#campourlproduccion").hide();
  $("#campotipo").hide();
  $("#campobooking").hide();

  $("#developer, #produccion").bind("focusin",function(){
      if($("#campourlproduccion").is(":visible")){
        $("#campourlproduccion").hide();
      }

      if($("#campourldeveloper").is(":visible")){
        $("#campourldeveloper").hide();
      }

      if($("#campotipo").is(":visible")){
        $("#campotipo").hide();
      }

      if($(".respuesta").is(":visible")){
        $(".respuesta").hide();
      }

      if($("#campobooking").is(":visible")){
        $("#campobooking").hide();
      }
  });

  $("#btnActivar").on("click",function(){
      if(($("#developer").val().length==0 || $("#produccion").val().length==0) && $("#tipo").val()=="NULL"
      && $("#key").val().length==0){
        $("#campourldeveloper").show();
        $("#campourlproduccion").show();
        $("#campotipo").show();
        $("#campobooking").show();
      }else if($("#developer").val().length==0 && $("#produccion").val().length==0){
        $("#campourlproduccion").show();
        $("#campotipo").hide();
        $("#campourldeveloper").show();
      }else if($("#tipo").val()=="NULL"){
        $("#campourlproduccion").hide();
        $("#campourldeveloper").hide();
        $("#campotipo").show();
      }else if($("#key").val().length == 0){
        $("#campourlproduccion").hide();
        $("#campourldeveloper").hide();
        $("#campotipo").hide();
        $("#campobooking").show();
      }else{
        $("#campobooking").hide();
        $("#campourldeveloper").hide();
        $("#campourlproduccion").hide();
        $("#campotipo").hide();
        $("#form-configbooking").submit();
      }
  })

  $("#campologin").hide();
  $("#campopassword").hide();

  if($("#modal").val()=="1"){
    $("#bookinglogin").modal("show");
  }

  $("#login, #password").bind("focusin",function(){
      if($("#campologin").is(":visible")){
        $("#campologin").hide();
      }

      if($("#campopassword").is(":visible")){
        $("#campopassword").hide();
      }

      if($("#error").is(":visible")){
        $("#error").hide();
      }
  });

  $("#btnEnviar").on("click",function(){
		if($("#login").val().length==0 && $("#password").val().length==0){
			$("#campologin").show();
			$("#campopassword").show();
		}else if($("#login").val().length==0){
			$("#campopassword").hide();
			$("#campologin").show();
		}else if($("#password").val().length==0){
			$("#campologin").hide();
			$("#campopassword").show();
		}else{
			$("#campologin").hide();
			$("#campopassword").hide();
			$("#form-login").submit();
		}
	});
});
