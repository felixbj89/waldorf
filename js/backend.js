function ver(titulo,subtitulo,mensaje){
	$("#tituloeditado").attr("value",titulo);
	$("#subtitulo").removeClass("ocultar");
	$("#subtitulo").addClass("mostrar");
	$("#subtituloeditado").attr("value",subtitulo);
	$("#mensajeedutadi").html(mensaje);
	$("#textoseditados").modal("show");
}

function goresinferior(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hisrinferior";
}

function goressuperior(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hisrsuperior";
}

function gohomcontacto(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hiscontacto";
}

function gohomhistory(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hishome";
}

function gohabSuperior(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hissuperior";
}

function gohabInferior(){
	var pictureurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		pictureurl = window.location.href.substr(0,pos-1);
	}

	window.location.href =  pictureurl + "/admin/hisinferior";
}

function verother(titulo,texto){
	$("#tituloeditado").attr("value",titulo);
	$("#subtitulo").addClass("ocultar");
	$("#mensajeedutadi").html(texto);
	$("#textoseditados").modal("show");
}

function loadimg(images){
	$("#imagenes").attr("src",images);
	$("#viewimages").modal("show");
}

jQuery(function ($) {
	$("#campologin").hide();
	$("#campopassword").hide();
	$("#campoconfirmar").hide();

	$("#profileclose").on("click",function(){
		$("#profile").modal("hide");
	});

	$("#menu_toggle").on("click",function(){
			$(".nav_title").css("width:70px !important");
	});

	$("#pictureinferiores").on("click",function(){
		var pictureurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			pictureurl = window.location.href.substr(0,pos-1);
		}

		window.location.href =  pictureurl + "/admin/resecundario";
	});

	$("#picturesuperiores").on("click",function(){
		var pictureurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			pictureurl = window.location.href.substr(0,pos-1);
		}

		window.location.href =  pictureurl + "/admin/resuperior";
	});

	$("#listarUsuarios").on("click",function(){
		var urlfiltrada = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			urlfiltrada = window.location.href.substr(0,pos-1);
		}

		window.location.href =  urlfiltrada + "/admin/listar";
	});

	$("#userlogin, #userpassword, #userconfirmar").bind("focusin",function(){
			if($("#resultadoprofile").is(":visible"))
				$("#resultadoprofile").hide();

			if($("#warningprofile").is(":visible"))
				$("#warningprofile").hide();
	});

	$("#profile").on("hidden.bs.modal",function(){
		$("#resultadoprofile").removeClass("mostrar");
		$("#resultadoprofile").addClass("ocultar");

		$("#warningprofile").removeClass("mostrar");
		$("#warningprofile").addClass("ocultar");
	});

	$("#btnSave").on("click",function(){
		if($("#userlogin").val().length==0 && $("#userpassword").val().length==0){
			$("#campologin").show();
			$("#campopassword").show();
		}else if($("#userlogin").val().length==0){
			$("#campopassword").hide();
			$("#campologin").show();
		}else if($("#userconfirmar").val().length > 0 && $("#userpassword").val()!=$("#userconfirmar").val()){
			$("#campoconfirmar").show();
		}else{
			var urlfiltrada = "";
			var pos = window.location.href.search("admin");
			if(pos > 0){
				urlfiltrada = window.location.href.substr(0,pos-1)
			}

			$.ajax({
				url: urlfiltrada + '/admin/profile',
				headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				method:'post',
				data:{formulario:$("#form-profile").serializeArray()},
				datatype:'json',
				beforeSend:function(respuesta){
					$("#profilering").removeClass("ocultar");
					$("#profilering").addClass("mostrar");
				},
				success:function(respuesta){
					$("#profilering").removeClass("mostrar");
					$("#profilering").addClass("ocultar");
					if(respuesta.resultado==1){
						$("#resultadoprofile").removeClass("ocultar");
						$("#resultadoprofile").addClass("mostrar");
						$("#userlogin").val(respuesta.usuario["user_login"]);
						$("#userpassword").val("");
						$("#userconfirmar").val("");
					}else{
						$("#warningprofile").removeClass("ocultar");
						$("#warningprofile").addClass("mostrar");
					}
				}
			});
		}
	});
});
