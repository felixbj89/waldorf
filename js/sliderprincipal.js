var pic1 = undefined;
var pic2 = undefined;
var pic3 = undefined;
var pic4 = undefined;
var pic5 = undefined;
var pic6 = undefined;

var type; var size; var click = 0; editar = 0;
var uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0;
function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}

function openFile4(event){
  pic4 = event;
}

function openFile5(event){
  pic5 = event;
}

function openFile6(event){
  pic6 = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

function confirmar(){
  cadena1 = $("#preview1").attr("src").substr($("#preview1").attr("src").search("default"),$("#preview1").attr("src").length);
  cadena2 = $("#preview2").attr("src").substr($("#preview2").attr("src").search("default"),$("#preview2").attr("src").length);
  cadena3 = $("#preview3").attr("src").substr($("#preview3").attr("src").search("default"),$("#preview3").attr("src").length);
  cadena4 = $("#preview4").attr("src").substr($("#preview4").attr("src").search("default"),$("#preview4").attr("src").length);
  cadena5 = $("#preview5").attr("src").substr($("#preview5").attr("src").search("default"),$("#preview5").attr("src").length);
  cadena6 = $("#preview6").attr("src").substr($("#preview6").attr("src").search("default"),$("#preview6").attr("src").length);

  if(cadena1=="default.jpg" && cadena2=="default.jpg" && cadena3=="default.jpg" && cadena4=="default.jpg"
  && cadena5=="default.jpg" && cadena6=="default.jpg"){
    return 1;
  }
  return 0;
}

$(document).ready(function(){
	$("#titulohistory").focus();
	
	$("#btnaddslider").on("click",function(){
		if($("#modo").val()=="1"){//SLIDER: RESTAURANTE
			var texto = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
			if(pic1==undefined && pic2==undefined && pic3==undefined && pic4==undefined && pic5==undefined && pic6==undefined && 
			$("#titulohistory").val().length==0 && $("#subtitulohistory").val().length==0 && $("#autor").val().length==0){
				$(".clienteerror").text("Todos los campos son obligatorios.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else if(pic1==undefined && pic2==undefined && pic3==undefined && pic4==undefined && pic5==undefined && pic6==undefined){
				$(".clienteerror").text("Debe ingresar una galería obligatoriamente.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else if($("#titulohistory").val().length==0){
				$(".clienteerror").text("Debe ingresar un título obligatoriamente.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else if($("#subtitulohistory").val().length==0){
				$(".clienteerror").text("Debe ingresar un sub-título obligatoriamente.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else if($("#autor").val().length==0){
				$(".clienteerror").text("Debe ingresar un autor obligatoriamente.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else if(texto.trim().length > 235){
				$(".clienteerror").text("Ha excedido el límite para el campo texto.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else{
				$("#form-slider").submit();
			}
		}else{
			if(pic1==undefined && pic2==undefined && pic3==undefined && pic4==undefined && pic5==undefined && pic6==undefined){
				$(".clienteerror").text("Todos los campos son obligatorios.");
				$('html, body').animate({scrollTop: (0)}, 1500);
			}else{
				$("#form-slider").submit();
			}
		}
	});
	
	$("#titulohistory, #titulohistory, #subtitulohistory, #autor").bind("focusin",function(){
		$(".clienteerror").text("");
		$(".errormensaje").text("");
	});

	//CARGA LAS IMÁGENES.
	var _URL = window.URL || window.webkitURL;
	//IMAGEN-1
	$("#file_picture1").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview1").attr("src",img.src);
		}
	});
	
	//IMAGEN-2
	$("#file_picture2").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview2").attr("src",img.src);
		}
	});
	
	//IMAGEN-3
	$("#file_picture3").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview3").attr("src",img.src);
		}
	});
	
	//IMAGEN-4
	$("#file_picture4").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview4").attr("src",img.src);
		}
	});
	
	//IMAGEN-5
	$("#file_picture5").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview5").attr("src",img.src);
		}
	});
	
	//IMAGEN-6
	$("#file_picture6").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview6").attr("src",img.src);
		}
	});
  
	$("#subtitulohistory, #titulohistory, #autor").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 40) {
				event.preventDefault();
				return false;
			}
		}
	});

	//EDITAR
	$("#btneditar").on("click",function(){
		//ajax..
		var editurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			editurl = window.location.href.substr(0,pos-1);
		}
		
		$(".clienteerror").text("");
		$(".errormensaje").text("");

		$.ajax({
			url: editurl + "/admin/editarslider",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data:{"modo":$("#modo").val()},
			method:"post",
			success:function(respuesta){//RETORNARA UN OBJETO Y CARGA EL FORM
				$("#panel").empty();
				$("#panel").html(respuesta);
			}
		});
	});

	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			}
		},
		placeholder: 'Ingrese su texto principal',
		height: 400,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
	});
	
	$('html, body').animate({scrollTop: (0)}, 1500);
});
