jQuery(function ($) {
	$('#Carousel-Waldorf').carousel();
	$('#WaldrofHab-indicators').on('click','.idicator-Hwaldorf',function () {
		$('.idicator-Hwaldorf').show();
		$(this).hide();
	});
	$.fn.AnimateScroll = (function(id){
		if(id!=""){
			$('html, body').animate({scrollTop: ($("#"+id).offset().top)-150}, 1500);
		}
	});
	
	if($("#textonuevo").val()=="<TEXTO>"){
		$("#ParrafoHistoria").text($("#textonuevo").val());
	}else{
		$("#ParrafoHistoria").html($("#textonuevo").val());
	}

	if($("#textogastro").val()=="<TEXTO>"){
		$("#ParrafoGastro").text($("#textogastro").val());
	}else{
		$("#ParrafoGastro").html($("#textogastro").val());
	}
	
	$('#ArribaHome').click(function(){
		$('html, body').animate({scrollTop: ($('body').offset().top)}, 1500);
	});
	
    $("#modalPromo").on('shown.bs.modal', function () {
		$('#btn-wal-res-float').hide();
    });
    $("#modalPromo").on('hidden.bs.modal', function () {
		$('#btn-wal-res-float').show();
    });	

    $("#CarouselPromo").on('slid.bs.carousel', function () {
		if($(window).width() > 767){
			var url = $(".onebyone-carosel>.item.active").next().data('imgpromo');
			var name = $(".onebyone-carosel>.item.active").next().data('namepromo');
			$('#modalPromo .content-imgPromo>img').attr("src",url);		
			$('#btn-promo').data("namepromo",name);
		}else{
			var url = $(".onebyone-carosel>.item.active").data('imgpromo');
			var name = $(".onebyone-carosel>.item.active").data('namepromo');
			$('#modalPromo .content-imgPromo>img').attr("src",url);	
			$('#btn-promo').data("namepromo",name);			
		}
    });
	
	$('#btn-promo').on('click',function(){
		$("#modalPromo").modal('hide');
		$('#form-contactus>.form-group>#asunto').val($(this).data('namepromo'));
		$('html, body').animate({scrollTop: ($("#Contacto").offset().top)-150}, 1500);
		$("#radP").prop("checked", true)
	});
});
