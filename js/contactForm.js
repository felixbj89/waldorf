$(document).ready(function(){
    $("#formatoincorrecto").hide();
    $("#formatoespacio").hide();
    $("#formatolength").hide();
    $("#formatochecked").hide();
    $("#nombrelength").hide();
    $("#asuntolength").hide();
    $("#mensajelength").hide();
    $("#mensajeshort").hide();
    $("#telefonolength").hide();
    $("#telefonoshort").hide();
    $("#telefonoformato").hide();
    $("#telefono").mask("(999) 999-9999");    $("#contactusme").on("click",function(){
      $('html,body').animate({
        scrollTop: $("#Contacto").offset().top
      }, 2000);
    });

    function formatoemail(){
        var expresion = /^[a-z0-9A-Z]+(\.|_?[a-z0-9A-Z]+)+\@[a-z0-9A-Z]*\.([a-z]{3}\.?([a-z]{2})|[a-z]{3})[^.]*$/i;
        if(!expresion.test($("#correo").val())){
            return 0;
        }else{
            return 1;
        }
    }

    function longitud(campo){
        if(campo.val().length==0){
            return 0;
        }else{
            return 1;
        }
    }

    function shorlength(campo){
        if(campo.val().length < 3){
            return 0;
        }else{
            return 1;
        }
    }

    function checkedinput(campo){
        if(!campo.is(":checked")){
            return 0;
        }else{
            return 1;
        }
    }


    /***************RADIO***************/
    $("#radE, #radR, #radM, #radP").on("click",function(){
       $("#formatochecked").hide();
    });

    /***************TELEFONO***************/
    $("#telefono").bind("focusout",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#telefonolength").hide();
            $("#telefonoshort").hide();
            $("#formatochecked").show();
        }else{
             if($("#telefono").val()=="+(__) ___-_______"){
                 $("#formatochecked").hide();
                 $("#telefonoshort").hide();
                 $("#telefonolength").show();
             }else{
                 if($("#telefono").val()=="+(00) 000-0000000"){
                    $("#telefonolength").hide();
                    $("#formatochecked").hide();
                    $("#telefonoshort").hide();
                    $("#telefonoformato").show();
                }else{
                    $("#telefonolength").hide();
                    $("#telefonoformato").hide();
                    $("#formatochecked").hide();
                    $("#telefonoshort").hide();
                }
             }
        }
    });

    $("#telefono").bind("focusin",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#telefonolength").hide();
            $("#telefonoshort").hide();
            $("#formatochecked").show();
        }else{
            if($("#telefonolength").is(":visible")){
                $("#telefonolength").hide();
            }

            if($("#telefonoshort").is(":visible")){
                $("#telefonoshort").hide();
            }
        }
    });

    /***************MENSAJE***************/
    $("#Mensaje").bind("focusout",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#mensajelength").hide();
            $("#mensajeshort").hide();
            $("#formatochecked").show();
        }else{
             if(!longitud($("#Mensaje"))){
                 $("#formatochecked").hide();
                 $("#mensajeshort").hide();
                 $("#mensajelength").show();
             }else{
                 if(!shorlength($("#Mensaje"))){
                    $("#mensajelength").hide();
                    $("#formatochecked").hide();
                    $("#mensajeshort").show();
                 }else{
                    $("#mensajelength").hide();
                    $("#formatochecked").hide();
                    $("#mensajeshort").hide();
                 }

             }
        }
    });

     $("#Mensaje").bind("focusin",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#mensajelength").hide();
            $("#mensajeshort").hide();
            $("#formatochecked").show();
        }else{
            if($("#mensajelength").is(":visible")){
                $("#mensajelength").hide();
            }

            if($("#mensajeshort").is(":visible")){
                $("#mensajeshort").hide();
            }
        }
    });

    var Mensaje;
    $("#Mensaje").bind("input", function (event) {
        Mensaje = $("#Mensaje").val();
    });

    $("#Mensaje").bind("keyup", function (event) {
         if(event.keyCode==13){
            $("#Mensaje").val(Mensaje);
         }
	});

    $("#Mensaje").bind("keydown", function (event) {
        if(event.keyCode==9){
            $("#Mensaje").val(Mensaje);
        }
	});

    $("#Mensaje").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 641) {
				event.preventDefault();
				return false;
			}
		}
	});

    /***************ASUNTO***************/
     $("#asunto").bind("focusout",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#asuntolength").hide();
            $("#formatochecked").show();
        }else{
             if(!longitud($("#asunto"))){
                 $("#formatochecked").hide();
                 $("#asuntolength").show();
             }else{
                 $("#asuntolength").hide();
                 $("#formatochecked").hide();
             }
        }
    });

     $("#asunto").bind("focusin",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#asuntolength").hide();
            $("#formatochecked").show();
        }else{
            if($("#asuntolength").is(":visible")){
                $("#asuntolength").hide();
            }
        }
    });

    var asunto;
    $("#asunto").bind("input", function (event) {
        asunto = $("#asunto").val();
    });

    $("#asunto").bind("keyup", function (event) {
         if(event.keyCode==13){
            $("#asunto").val(asunto);
         }
	});

    $("#asunto").bind("keydown", function (event) {
        if(event.keyCode==9){
            $("#asunto").val(asunto);
        }
	});

    $("#asunto").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 60) {
				event.preventDefault();
				return false;
			}
		}
	});

    /***************NOMBRE***************/
    $("#nombre").bind("focusout",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#nombrelength").hide();
            $("#formatochecked").show();
        }else{
             if(!longitud($("#nombre"))){
                 $("#formatochecked").hide();
                 $("#nombrelength").show();
             }else{
                 $("#nombrelength").hide();
                 $("#formatochecked").hide();
             }
        }
    });

     $("#nombre").bind("focusin",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#nombrelength").hide();
            $("#formatochecked").show();
        }else{
            if($("#nombrelength").is(":visible")){
                $("#nombrelength").hide();
            }
        }
    });

    var nombre;
    $("#nombre").bind("input", function (event) {
        nombre = $("#nombre").val();
    });

    $("#nombre").bind("keyup", function (event) {
         if(event.keyCode==13){
            $("#nombre").val(nombre);
         }
	});

    $("#nombre").bind("keydown", function (event) {
        if(event.keyCode==9){
            $("#nombre").val(nombre);
        }
	});

    $("#nombre").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 60) {
				event.preventDefault();
				return false;
			}
		}
	});

    /***************CORREO***************/
    $("#correo").bind("focusout",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#formatoincorrecto").hide();
            $("#formatolength").hide();
            $("#formatochecked").show();
        }else{
            if(!longitud($("#correo"))){
                $("#formatochecked").hide();
                $("#formatoincorrecto").hide();
                $("#formatolength").show();
            }else{
                if(!formatoemail()){
                    $("#formatochecked").hide();
                    $("#formatolength").hide();
                    $("#formatoincorrecto").show();
                }else{
                    $("#formatoincorrecto").hide();
                }
            }
        }
    });

     $("#correo").bind("input",function(event){
        if($("#correo").val() > 0){
            $("#formatoespacio").hide();
        }
    });

    $("#correo").bind("focusin",function(){
        if(!checkedinput($("#radE")) && !checkedinput($("#radR")) && !checkedinput($("#radM")) && !checkedinput($("#radP"))){
            $("#formatoincorrecto").hide();
            $("#formatolength").hide();
            $("#formatochecked").show();
        }else{
            if($("#formatoincorrecto").is(":visible")){
                $("#formatoincorrecto").hide();
            }

            if($("#formatolength").is(":visible")){
                $("#formatolength").hide();
            }

            if($("#formatochecked").is(":visible")){
                $("#formatochecked").hide();
            }
        }
    });

    $("#correo").bind("keyup",function(event){
        if(event.keyCode==8){
            $("#formatoespacio").hide();
        }
    });

    $("#correo").bind("keypress",function(event){
       if (event.charCode==32) {
			$("#formatoespacio").show();
            event.preventDefault();
            return false;
		}
    });
	
	var confirmar = undefined;
	$("#destino").bind("click,change",function(){
		if($("#destino").val()!="NULL"){
			confirmar = 1;
		}
	});
	
    /***************SEND***************/
    $("#btn-env").on("click",function(){
		if($("#destino").val()=="NULL" && !confirmar){
            $("#formatoincorrecto").hide();
            $("#formatoespacio").hide();
            $("#formatolength").hide();
            $("#nombrelength").hide();
            $("#asuntolength").hide();
            $("#mensajelength").hide();
            $("#mensajeshort").hide();
            $("#formatochecked").show();
        }else{
             if(!longitud($("#nombre"))){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
                $("#formatochecked").hide();
                $("#nombrelength").show();
             }else if(!longitud($("#correo"))){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").show();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
             }else  if(!formatoemail()){
                $("#formatoincorrecto").show();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
             }else if(!longitud($("#asunto"))){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
                $("#asuntolength").show();
             }else if($("#telefono").val()=="+(__) ___-_______" || $("#telefono").val()==""){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
                $("#telefonoshort").hide();
                $("#telefonolength").show();
             }else  if(!longitud($("#Mensaje"))){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajeshort").hide();
                $("#mensajelength").show();
             }else if(!shorlength($("#Mensaje"))){
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").show();
             }else{
                $("#formatoincorrecto").hide();
                $("#formatoespacio").hide();
                $("#formatolength").hide();
                $("#formatochecked").hide();
                $("#nombrelength").hide();
                $("#asuntolength").hide();
                $("#mensajelength").hide();
                $("#mensajeshort").hide();
                $("#telefonolength").hide();
                $("#telefonoshort").hide();
                $.ajax({
                    url: window.location.href + "enviarcontacto",
                    headers:{
					   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
                    method:"post",
                    data:{formulario:$("#form-contactus").serializeArray()},
                    datatype:"json",
                    beforeSend:function(respuesta){
                        $("#antesenviar").modal("show");
                    },
                    success:function(respuesta){
                        $("#Mensaje").val("");
                        $("#asunto").val("");
                        $("#correo").val("");
                        $("#nombre").val("");
                        $("#telefono").val("");
                        if(respuesta.success){
                            $("#antesenviar").modal("hide");
                            $("#despuesenviar").modal("show");
                        }
                    }
                });
             }
        }
    });
});
