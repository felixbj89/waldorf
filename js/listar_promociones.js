var pic; var type; var size;
function openFile(event){
  pic = event;
}

$(document).ready(function(){
	$(".clienteerror").hide();
	
	var tablaHistorial = $("#promociones_list").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bSort":false,
		"bLengthChange": false,
		"iDisplayLength": 1,
		"language": {
		  "sProcessing":     "Procesando...",
		  "sLengthMenu":     "Mostrar _MENU_ registros",
		  "sZeroRecords":    "No se encontraron resultados",
		  "sEmptyTable":     "Ningún dato disponible en esta tabla",
		  "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		  "sInfoPostFix":    "",
		  "sSearch":         "Buscar:",
		  "sUrl":            "",
		  "sInfoThousands":  ",",
		  "sLoadingRecords": "Cargando...",
		  "oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		  },
		  "oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		  }
		},

		"fixedHeader": {
		   "header": true,
		   "footer": true
		 },
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	$(document).on("click","#btnModificarPromo",function(){
		var promociones = $(this).parents("div").parents("tr").data("promociones");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
            url: url + 'admin/editpromo',
			headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			method:"post",
            data:{"promo_id":promociones},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#panel").empty();
				$("#panel").html(respuesta);
			}
        }).done(function(respuesta){
        });
	});
	
	$(document).on("click","#btnRemover",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var promociones = $(this).parents("div").parents("tr").data("promociones");
		var seleccion = $(this).parents("tr");
		$.ajax({
            url: url + 'admin/removerpromo',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data:{"promo_id":promociones},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
            success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
                    tablaHistorial.row(seleccion).remove().draw();
                }else if(respuesta.estado==2){
					$(".clienteerror").show();
					$(".clienteerror").text("No puede remover una promoción que esté en uso");
					$('html, body').animate({scrollTop: (0)}, 1500);
				}else{
					$(".clienteerror").show();
					$(".clienteerror").text("No se pudo remover su promoción");
					$('html, body').animate({scrollTop: (0)}, 1500);
				}
            }
        });
	});
	
	$(document).on("click","#btnEPromocion",function(){
		var promociones = $(this).parents("div").parents("tr").data("promociones");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		$.ajax({
            url: url + 'admin/changepromo',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data:{"promo_id":promociones},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
            success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
					$("#btnEPromocion").empty();
					if(respuesta.status=="1"){
						$("#btnEPromocion").append("<i class=\"fa fa-power-off cenico\"></i> Activa");
					}else{
						$("#btnEPromocion").append("<i class=\"fa fa-power-off cenico\"></i> Inactiva");
					}
                }else{
					$(".clienteerror").show();
					$(".clienteerror").text("No se pudo activar la Foto Principal");
					$('html, body').animate({scrollTop: (0)}, 1500);
				}
            }
        }).done(function(respuesta){
        });
	});
	
	$('html, body').animate({scrollTop: (0)}, 1500);
});