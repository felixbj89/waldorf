$(document).ready(function(){
  $("#campologin").hide();
	$("#campopassword").hide();

	$("#btnEnviar").on("click",function(){
		if($("#blocklogin").val().length==0 && $("#blockpass").val().length==0){
			$("#campologin").show();
			$("#campopassword").show();
		}else if($("#blocklogin").val().length==0){
			$("#campopassword").hide();
			$("#campologin").show();
		}else if($("#blockpass").val().length==0){
			$("#campologin").hide();
			$("#campopassword").show();
		}else{
      $("#campologin").hide();
			$("#campopassword").hide();
			$("#form-locklogin").submit();
		}
	});
	
	if($("#backendlock").val()=="0"){
		$("#errormensajelogin").text("Su sesión ha terminado..");
		$("#alerta").modal("show");
	}else if($("#backendlock").val()!=undefined){
		$("#errormensajelogin").text("Ha bloqueado su cuenta..");
		$("#alerta").modal("show");
	}
	
	$("#btnclose").on("hidden.bs.modal",function(){
		$("#alerta").modal("hide");
	});
});
