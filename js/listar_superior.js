var pic; var type; var size;
function openFile(event){
  pic = event;
}

$(document).ready(function(){
	$(".clienteerror").hide();
	
	var tablaHistorial = $("#fotos_list").DataTable({
		"searching": false,
		"bInfo": false,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 2,
		"language": {
		  "sProcessing":     "Procesando...",
		  "sLengthMenu":     "Mostrar _MENU_ registros",
		  "sZeroRecords":    "No se encontraron resultados",
		  "sEmptyTable":     "Ningún dato disponible en esta tabla",
		  "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		  "sInfoPostFix":    "",
		  "sSearch":         "Buscar:",
		  "sUrl":            "",
		  "sInfoThousands":  ",",
		  "sLoadingRecords": "Cargando...",
		  "oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		  },
		  "oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		  }
		},

		"fixedHeader": {
		   "header": true,
		   "footer": true
		 },
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
	});
	
	$(document).on("click","#btnESuperior",function(){
		var fotosprincipales = $(this).parents("tr").data("fotoprincipal");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var ruta = undefined;
		if($("#estado").val()=="1"){
			ruta = 'admin/verhfotos';
		}else if($("#estado").val()=="12"){
			ruta = 'admin/verehfotos';
		}else{
			ruta = 'admin/verrfotos'
		}
			
		$.ajax({
            url: url + 'admin/editbannersuperior',
			headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			method:"post",
            data:{"pictureid":fotosprincipales,"estado":$("#estado").val(),"ruta":ruta},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
			success:function(respuesta){
				$("#modal_wait").modal("hide");
				$("#panel").empty();
				$("#panel").html(respuesta);
			}
        }).done(function(respuesta){
        });
	});
	
	$(document).on("click","#btnRemover",function(){
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var fotosprincipales = $(this).parents("tr").data("fotoprincipal");
		var seleccion = $(this).parents("tr");
		$.ajax({
            url: url + 'admin/removepicture',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data:{"pictureid":fotosprincipales["id"],"tiposec":$("#estado").val()},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
            success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
                    tablaHistorial.row(seleccion).remove().draw();
                }else{
					$("#mensajeserver").text("No se pudo remover la Foto Principal");
				}
            }
        });
	});
	
	$(document).on("click","#btnEstado",function(){
		var fotosprincipales = $(this).parents("tr").data("fotoprincipal");
		var url = window.location.href.substr(0,window.location.href.search("admin"));
		var which = undefined;
		if($("#estado").val()=="1"){//HABITACIÓN
			which = "3";
		}else if($("#estado").val()=="12"){//EL HOTEL
			which = "21";
		}else{//RESTAURANTE
			which = "4";
		}
		
		$.ajax({
            url: url + 'admin/changestate',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:"post",
            data:{"pictureid":fotosprincipales["id"],"estado":which,"activo":$("#estado").val()},
            beforeSend:function(respuesta){
                $("#modal_wait").modal("show");
            },
            success:function(respuesta){
				$("#modal_wait").modal("hide");
				if(respuesta.estado==1){
                    $("#mensajeserver").text("Se activo la Foto Principal");
					$("#loading_superior").attr("src",$("#url").val() + "/" + respuesta.imagen);
                }else{
					$("#mensajeserver").text("No se pudo activar la Foto Principal");
				}
            }
        }).done(function(respuesta){
           window.location.reload();
        });
	});
});