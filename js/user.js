$(document).ready(function(){
    $("#nombreobligatorio").hide();
    $("#correoformato").hide();
    $("#loginobligatorio").hide();
    $("#correoobligatorio").hide();
    $("#todosobligatorio1").hide();
    $("#todosobligatorio2").hide();
    $("#todosobligatorio3").hide();
    $("#todosobligatorio4").hide();
    $("#todosobligatorio5").hide();
    $("#todosobligatorio6").hide();
    $("#todosobligatorio7").hide();
    $("#estadotipo").hide();
    $("#perfiltipo").hide();
    $("#loginformato").hide();
    $("#confirmarpassword").hide();
    $("#passwordobligatorio").hide();
    $(".errormensaje").hide();

    function formatoemail(){
        var expresion = /^[a-z0-9A-Z]+(\.|_?[a-z0-9A-Z]+)+\@[a-z0-9A-Z]*\.([a-z]{3}\.?([a-z]{2})|[a-z]{3})[^.]*$/i;
        if(!expresion.test($("#correo").val())){
            return 0;
        }else{
            return 1;
        }
    }

    $("#perfil, #estado").bind("change",function(){
      if($("#estadotipo").is(":visible")){
        $("#estadotipo").hide();
      }

      if($("#perfiltipo").is(":visible")){
        $("#perfiltipo").hide();
      }
    });

    $("#nombre, #correo, #perfil, #estado, #userlogin, #userpassword, #userconfirmar").bind("focusin",function(){
      if($("#nombreobligatorio").is(":visible")){
        $("#nombreobligatorio").hide();
      }

      if($("#correoformato").is(":visible")){
        $("#correoformato").hide();
      }

      if($("#correoobligatorio").is(":visible")){
        $("#correoobligatorio").hide();
      }

      if($("#todosobligatorio1").is(":visible")){
        $("#todosobligatorio1").hide();
      }

      if($("#todosobligatorio2").is(":visible")){
        $("#todosobligatorio2").hide();
      }

      if($("#todosobligatorio3").is(":visible")){
        $("#todosobligatorio3").hide();
      }

      if($("#todosobligatorio4").is(":visible")){
        $("#todosobligatorio4").hide();
      }

      if($("#todosobligatorio5").is(":visible")){
        $("#todosobligatorio5").hide();
      }

      if($("#todosobligatorio6").is(":visible")){
        $("#todosobligatorio6").hide();
      }

      if($("#todosobligatorio7").is(":visible")){
        $("#todosobligatorio7").hide();
      }

      if($("#confirmarpassword").is(":visible")){
        $("#confirmarpassword").hide();
      }

      if($("#passwordobligatorio").is(":visible")){
        $("#passwordobligatorio").hide();
      }

      if($(".errormensaje").is(":visible")){
        $(".errormensaje").hide();
      }

    })

    $("#userpassword").bind("keypress", function (event) {
		    if (event.charCode!=0) {
			   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9]+$");
			   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			   if (!regex.test(key) || ($(this).val()).length > 10) {
				    event.preventDefault();
				    return false;
			   }
      }
		});

    $("#userlogin").bind("keypress", function (event) {
		    if (event.charCode!=0) {
			   var regex = new RegExp("^[a-z A-Z ñ á-ú Á-Ú Ñ]+$");
			   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			   if (!regex.test(key) || ($(this).val()).length > 10) {
				    event.preventDefault();
				    return false;
			   }
      }
		});

    $("#btnCrear").on("click",function(event){
        event.preventDefault();
        if($("#nombre").val().length==0 && $("#correo").val().length==0 && $("#perfil").val()=="-1" && $("#estado").val()=="-1" && $("#userlogin").val().length==0
        && $("#userpassword").val().length==0 && $("#userconfirmar").val()==0){
          $("#todosobligatorio1").show();
          $("#todosobligatorio2").show();
          $("#todosobligatorio3").show();
          $("#todosobligatorio4").show();
          $("#todosobligatorio5").show();
          $("#todosobligatorio6").show();
          $("#todosobligatorio7").show();
        }else if($("#nombre").val().length==0){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#correoformato").hide();
          $("#nombreobligatorio").show();
        }else if(!formatoemail($("#correo"))){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#nombreobligatorio").hide();
          $("#correoformato").show();
        }else if($("#perfil").val()=="-1"){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#perfiltipo").show();
        }else if($("#estado").val()=="-1"){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#perfiltipo").hide();
          $("#estadotipo").show();
        }else if($("#userlogin").val().length < 3){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#perfiltipo").hide();
          $("#estadotipo").hide();
          $("#loginformato").show();
        }else if($("#userpassword").val().length==0){
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#loginobligatorio").hide();
          $("#correoobligatorio").hide();
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#todosobligatorio4").hide();
          $("#todosobligatorio5").hide();
          $("#todosobligatorio6").hide();
          $("#todosobligatorio7").hide();
          $("#estadotipo").hide();
          $("#perfiltipo").hide();
          $("#loginformato").hide();
          $("#confirmarpassword").hide();
          $("#passwordobligatorio").show();
        }else if($("#userpassword").val()!=$("#userconfirmar").val()){
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#perfiltipo").hide();
          $("#estadotipo").hide();
          $("#loginformato").hide();
          $("#confirmarpassword").show();
        }else{
          $("#nombreobligatorio").hide();
          $("#correoformato").hide();
          $("#loginobligatorio").hide();
          $("#correoobligatorio").hide();
          $("#todosobligatorio1").hide();
          $("#todosobligatorio2").hide();
          $("#todosobligatorio3").hide();
          $("#todosobligatorio4").hide();
          $("#todosobligatorio5").hide();
          $("#todosobligatorio6").hide();
          $("#todosobligatorio7").hide();
          $("#estadotipo").hide();
          $("#perfiltipo").hide();
          $("#loginformato").hide();
          $("#confirmarpassword").hide();
          $("#passwordobligatorio").hide();

          $.ajax({
              url: $("#ruta").val(),
              headers:{
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method:"post",
              data:{formulario:$("#form-create").serializeArray()},
              datatype:"json",
              beforeSend:function(respuesta){
                  $("#antescrear").modal("show");
              },
              success:function(respuesta){
                  $("#perfil").val("-1");
                  $("#estado").val("-1");
                  $("#correo").val("");
                  $("#nombre").val("");
                  $("#userlogin").val("");
                  $("#userpassword").val("");
                  $("#userconfirmar").val("");
                  if(respuesta.resultado==1){
                      $("#antescrear").modal("hide");
                      $("#despuescrear").modal("show");
                  }else{
                      $("#antescrear").modal("hide");
                      $(".errormensaje").show();
                  }
              }
          }).done(function(respuesta){
            if(respuesta.resultado){
              $("#btnuserclose").on("click",function(){
                  $(".count").text(respuesta.size);
              });
            }
          });
        }

        return false;
    });
});
