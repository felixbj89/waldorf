var pic1 = undefined;

function openFile1(event){
  pic1 = event;
}

$(document).ready(function(){
	$("#titulopromo").focus();
	
	//IMAGEN-1
	var _URL = window.URL || window.webkitURL;
	$("#file_picture1").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview1").attr("src",img.src);
		}
	});
	
	$("#btnAgregarPromo").on("click",function(){
		var texto = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if($("#titulopromo").val().length==0 && $("#asuntopromo").val().length==0 && $("#favorito").val()=="NULL" && $("#estado").val()=="NULL" && $("#costopromo").val()=="" && $("#inicio_promociones").val()=="" || $("#fin_promociones").val()==""){
			$(".clienteerror").text("Todos los campos son obligatorios.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#titulopromo").val().length==0){
			$(".clienteerror").text("El campo es obligatorio, ingrese un título válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#asuntopromo").val().length==0){
			$(".clienteerror").text("El campo es obligatorio, ingrese un asunto válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#estado").val()=="NULL"){
			$(".clienteerror").text("El campo es obligatorio, ingrese un estado válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#favorito").val()=="NULL"){
			$(".clienteerror").text("El campo es obligatorio, ingrese un favorito válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#costopromo").val()==""){
			$(".clienteerror").text("El campo es obligatorio, ingrese un costo válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#inicio_promociones").val()=="" || $("#fin_promociones").val()==""){
			$(".clienteerror").text("El campo es obligatorio, ingrese una fecha válida.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(texto.trim().length > 300){
			$(".clienteerror").text("Ha excedido el límite de carácteres en su descripción.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			$("#form-promociones").submit();
		}
	});
	
	$("#btnModificarPromocion").on("click",function(){
		$("#form-promociones").submit();
	});
	
	$("#titulopromo, #asuntopromo, #estado, #costopromo").bind("focusin",function(){
		$(".clienteerror").text("");
	});
	
	$("#inicio_promociones").flatpickr({
		minDate: "today",
		maxDate: new Date().fp_incr(90),
		enableTime: true,
		enableSeconds:true,
		noCalendar: false,
		time_24hr:false,
		locale:{
			firstDayOfWeek: 0,
		}
	});
		

	$("#fin_promociones").flatpickr({
		minDate: "today",
		maxDate: new Date().fp_incr(90),
		firstDayOfWeek:0,
		enableTime: true,
		enableSeconds:true,
		noCalendar: false,
		time_24hr:false,
		locale:{
			firstDayOfWeek: 0,
		}
	});
	
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			}
		},
		placeholder: 'Ingrese su descripción',
		height: 400,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: false 
	});
	
	$('html, body').animate({scrollTop: (0)}, 1500);
});