var pic; var type; var size;
function openFile(event){
  pic = event;
}

/*function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}*/

$(document).ready(function() {
    /*$("#pictureobligatorio").hide();
    $("#fileobligatorio").hide();
    $("#todosobligatorio1").hide();
    $("#todosobligatorio2").hide();
    $("#fileformato").hide();
    $("#filesize").hide();
    $("#successchange").hide();
    $("#fileresol").hide();
    $("#removerchange").hide();
    $(".modalmensaje").hide();*/

    var tablaHistorial = $("#datatable-historial").DataTable({
      "searching": false,
      "bInfo": false,
		  "Info":false,
      "bLengthChange": false,
      "iDisplayLength": 1,
      "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },

      "fixedHeader": {
         "header": true,
         "footer": true
       },
      "bPaginate": true,
      "bAutoWidth": false,
    });

    /*$(document).on("click","#removerhistorial",function(){
      var picture = $(".fixed-button-historial").parents("tr").data("date");
      var seleccion = $(".fixed-button-historial").parents("tr");
      var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
      var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

      $.ajax({
          url: urlfiltrada + '/admin/remover',
          headers:{
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:"post",
          data:{"picture":picture,"tipo":"2"},
          beforeSend:function(respuesta){
              $("#picturering").removeClass("ocultar");
              $("#picturering").addClass("mostrar");
          },
          success:function(respuesta){
              if(respuesta.resultado==1){
                $("#picturering").removeClass("mostrar");
                $("#picturering").addClass("ocultar");

                tablaHistorial.row(seleccion).remove().draw();
                $("#removerchange").show();
              }
          }
      });
    });

    $(".btnhistorial").on("click",function(){
      $("#removesastifactorio").removeClass("mostrar");
      $("#removesastifactorio").addClass("ocultar");
    });

    $("#historialpicture").on("show.bs.modal",function(){
      $("#successchange").hide();
      $("#removerchange").hide();
    });

    $("#btncerrarhismensaje").on("click",function(){
      window.location.reload();
    });

    $(document).on("click","#historialbtn",function(){
      $("#removerchange").hide();
      var picture = $(".fixed-button-historial").parents("tr").data("date");
      var status = $(".fixed-button-historial").parents("tr").data("status");
      var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

      $.ajax({
          url: urlfiltrada + '/admin/changepicture',
          headers:{
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:"post",
          data:{"picture":picture,"tipo":$("#tiposec").val()},
          beforeSend:function(respuesta){
              $("#picturering").removeClass("ocultar");
              $("#picturering").addClass("mostrar");
          },
          success:function(respuesta){
              if(respuesta.resultado==1){
                $("#picturering").removeClass("mostrar");
                $("#picturering").addClass("ocultar");
                $("#newimg_" + status).attr("id","newimg_" + respuesta.status);
                $("#newimg_" + respuesta.status).parents("tr").attr("data-date",respuesta.date);
                $("#newimg_" + respuesta.status).parents("tr").attr("data-status",respuesta.status);
                $("#newimg_" + respuesta.status).attr("src",respuesta.imagen);
                picture = respuesta.date;
                $("#successchange").show();
              }
          }
      }).done(function(respuesta){
          if(respuesta.resultado==1){
            $("#historialpicture").modal("hide");
            $("#despueshistorial").modal("show");
          }
      });
    });*/

    var _URL = window.URL || window.webkitURL;
    $("#banner_inferior").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			img.onerror = function() {
              alert( "not a valid file: " + file.type);
			};
		
		img.src = _URL.createObjectURL(file);
			$("#loading_inferior").attr("src",img.src);
		}
	});

    /*$("#pictureName, #file_picture").bind("focusin",function(){
      if($("#todosobligatorio1").is(":visible")){
        $("#todosobligatorio1").hide();
      }

      if($("#todosobligatorio2").is(":visible")){
        $("#todosobligatorio2").hide();
      }

      if($("#fileobligatorio").is(":visible")){
        $("#fileobligatorio").hide();
      }

      if($("#pictureobligatorio").is(":visible")){
        $("#pictureobligatorio").hide();
      }

      if($(".errormensaje").is(":visible")){
        $(".errormensaje").hide();
      }
    });*/

    /*$("#pictureName").bind("keypress", function (event) {
		    if (event.charCode!=0) {
			   var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			   var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			   if (!regex.test(key) || ($(this).val()).length > 20) {
				    event.preventDefault();
				    return false;
			   }
      }
		});/*

    /*$("#btnrestaurant").on("click",function(){
      $("#tiposec").attr("value","3");
      $("#btnhabitaciones").attr("disabled",false);
      $("#btnhabitaciones").text("Desactivar");

      $("#btnrestaurant").attr("disabled",true);
      $("#btnrestaurant").text("Activar");
    });

    $("#btnhabitaciones").on("click",function(){
      $("#tiposec").attr("value","2");
      $("#btnrestaurant").attr("disabled",false);
      $("#btnrestaurant").text("Desactivar");

      $("#btnhabitaciones").attr("disabled",true);
      $("#btnhabitaciones").text("Activar");
    });*/

    $("#btnaddInferior").on("click",function(){
		if(pic==undefined){
			$(".clienteerror").text("Debe cargar una imagen antes de continuar");
			$(".clienteerror").show();
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			$("#form-inferior").submit();
		}
    });
});
