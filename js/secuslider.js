var type; var size; var click = 0; editar = 0;
var uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0;
var lista = [];

var pic1 = undefined; 
var pic2 = undefined; 
var pic3 = undefined; 
var pic4 = undefined; 
var pic5 = undefined; 

function openFile1(event){
  pic1 = event;
}

function openFile2(event){
  pic2 = event;
}

function openFile3(event){
  pic3 = event;
}

function openFile4(event){
  pic4 = event;
}

function openFile5(event){
  pic5 = event;
}

var thumb1 = undefined;
var thumb2 = undefined;
var thumb3 = undefined;
var thumb4 = undefined;
var thumb5 = undefined;

function openThum1(event){
	thumb1 = event;
}

function openThum2(event){
	thumb2 = event;
}

function openThum3(event){
	thumb3 = event;
}

function openThum4(event){
	thumb4 = event;
}

function openThum5(event){
	thumb5 = event;
}

$(document).ready(function(){
	$("#filethumb1").attr("style","display:block !important");
	$("#filethumb2").attr("style","display:block !important");
	$("#filethumb3").attr("style","display:block !important");
	$("#filethumb4").attr("style","display:block !important");
	$("#filethumb5").attr("style","display:block !important");
	$("#fileasociado1").attr("style","display:block !important");
	$("#fileasociado2").attr("style","display:block !important");
	$("#fileasociado3").attr("style","display:block !important");
	$("#fileasociado4").attr("style","display:block !important");
	$("#fileasociado5").attr("style","display:block !important");
	
	//CARGA LAS IMÁGENES.
	var _URL = window.URL || window.webkitURL;
	//IMAGEN-1
	$("#filethumb1").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview1").attr("src",img.src);
		}
	});
	
	$("#fileasociado1").change(function(e) {
		var file, img;
		if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar las imágenes miniatura antes de continuar.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			if ((file = this.files[0])) {
				img = new Image();
				
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				
				img.src = _URL.createObjectURL(file);
				$("#asociado1").attr("src",img.src);
			}
		}
	});
	
	//IMAGEN-2
	$("#filethumb2").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview2").attr("src",img.src);
		}
	});
	
	$("#fileasociado2").change(function(e) {
		var file, img;
		if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar las imágenes miniatura antes de continuar.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			if ((file = this.files[0])) {
				img = new Image();
				
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				
				img.src = _URL.createObjectURL(file);
				$("#asociado2").attr("src",img.src);
			}
		}
	});
	
	//IMAGEN-3
	$("#filethumb3").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview3").attr("src",img.src);
		}
	});
	
	$("#fileasociado3").change(function(e) {
		var file, img;
		if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar las imágenes miniatura antes de continuar.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			if ((file = this.files[0])) {
				img = new Image();
				
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				
				img.src = _URL.createObjectURL(file);
				$("#asociado3").attr("src",img.src);
			}
		}
	});
	
	//IMAGEN-4
	$("#filethumb4").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview4").attr("src",img.src);
		}
	});
	
	$("#fileasociado4").change(function(e) {
		var file, img;
		if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar las imágenes miniatura antes de continuar.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			if ((file = this.files[0])) {
				img = new Image();
				
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				
				img.src = _URL.createObjectURL(file);
				$("#asociado4").attr("src",img.src);
			}
		}
	});
	
	//IMAGEN-5
	$("#filethumb5").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			
			img.onerror = function() {
				alert( "not a valid file: " + file.type);
			};
			
			img.src = _URL.createObjectURL(file);
			$("#preview5").attr("src",img.src);
		}
	});
	
	$("#fileasociado5").change(function(e) {
		var file, img;
		if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar las imágenes miniatura antes de continuar.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			if ((file = this.files[0])) {
				img = new Image();
				
				img.onerror = function() {
					alert( "not a valid file: " + file.type);
				};
				
				img.src = _URL.createObjectURL(file);
				$("#asociado5").attr("src",img.src);
			}
		}
	});
	
	$("#pictureName, #filethumb1, #fileasociado1, #filethumb2, #fileasociado2, #filethumb3, #fileasociado3, #filethumb4, #fileasociado4, #filethumb5, #fileasociado5").bind("focusin",function(){
		$(".clienteerror").text("");
	});
  
	//SUBMIT
	$("#btnslider").on("click",function(){
		var texto = $('.summernote')[0].value.replace(/<\/?[^>]+(>|$)/g, "");
		if($("#titulohistory").val().length==0 && texto.trim().length == 0){
			$(".clienteerror").text("Los campos son obligatorios.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if($("#titulohistory").val().length==0){
			$(".clienteerror").text("Debe ingresar un título válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(texto.trim().length == 0){
			$(".clienteerror").text("Debe ingresar un texto válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(texto.trim().length > 133){
			$(".clienteerror").text("Ha excedido el límite de carácteres válido.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(thumb1==undefined || thumb2==undefined || thumb3==undefined || thumb4==undefined || thumb5==undefined){
			$(".clienteerror").text("Debe ingresar todas las imágenes de tipo miniatura.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else if(pic1==undefined || pic2==undefined || pic3==undefined || pic4==undefined || pic5==undefined){
			$(".clienteerror").text("Debe ingresar todas las imágenes.");
			$('html, body').animate({scrollTop: (0)}, 1500);
		}else{
			$("#form-slider").submit();
		}
	});
	
	$('.summernote').summernote({
	    lang: 'es-ES',
		toolbar: [
			// [groupName, [list of button]]
			['para', ['paragraph']],
			['height', ['height']]
		],
		callbacks: {
			onImageUpload: function(files) {
				// upload image to server and create imgNode...
				$summernote.summernote('insertNode', "");
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			}
		},
		placeholder: 'Ingrese su texto principal',
		height: 400,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
		focus: true 
	});
	
	$("#btneditar").on("click",function(){
		//ajax..
		var editurl = "";
		var pos = window.location.href.search("admin");
		if(pos > 0){
			editurl = window.location.href.substr(0,pos-1);
		}
		
		$(".clienteerror").text("");
		$(".errormensaje").text("");

		$.ajax({
			url: editurl + "/admin/editarsecuslider",
			headers:{
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			success:function(respuesta){//RETORNARA UN OBJETO Y CARGA EL FORM
				$("#panel").empty();
				$("#panel").html(respuesta);
			}
		});
	});
  
	$("#titulohistory").bind("keypress", function (event) {
		if (event.charCode!=0) {
			var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

			if (!regex.test(key) || ($(this).val()).length > 20) {
				event.preventDefault();
				return false;
			}	
		}
	});
});
