var pic; var type; var size; var click = 1; editar = 0;
var uno = 0, dos = 0, tres = 0, cuatro = 0, cinco = 0, seis = 0;
var gal; var type2; var size2;
function openFile(event){
  pic = event;
}

function openGaleria(event){
  gal = event;
}

function blanco(cadena){
	var size = cadena.length;
	var aux = 0;
	for(i = 0; i < size; ++i){
		if(cadena.codePointAt(i)==32){
			aux++;
		}
	}

	if(aux==size){
		return true;
	}return false;
}

function confirmar(){
  cadena1 = $("#preview1").attr("src").substr($("#preview1").attr("src").search("default"),$("#preview1").attr("src").length);
  cadena2 = $("#preview2").attr("src").substr($("#preview2").attr("src").search("default"),$("#preview2").attr("src").length);
  cadena3 = $("#preview3").attr("src").substr($("#preview3").attr("src").search("default"),$("#preview3").attr("src").length);
  cadena4 = $("#preview4").attr("src").substr($("#preview4").attr("src").search("default"),$("#preview4").attr("src").length);
  cadena5 = $("#preview5").attr("src").substr($("#preview5").attr("src").search("default"),$("#preview5").attr("src").length);
  cadena6 = $("#preview6").attr("src").substr($("#preview6").attr("src").search("default"),$("#preview6").attr("src").length);

  if(cadena1=="default.jpg" && cadena2=="default.jpg" && cadena3=="default.jpg" && cadena4=="default.jpg"
  && cadena5=="default.jpg" && cadena6=="default.jpg"){
    return 1;
  }
  return 0;
}

function addrow(tabla,id,content) {
  tabla.dataTable().fnAddData( [
      content,
      "<button type=\"button\" id=" + id + ">Remover</button>"] );
}

function verpicture(){
	var editurl = "";
	var pos = window.location.href.search("admin");
	if(pos > 0){
		editurl = window.location.href.substr(0,pos-1);
	}

	$.ajax({
			url: editurl + "/admin/verpropiedades",
			headers:{
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			method:"post",
			beforeSend:function(respuesta){
				$("#mensajepropiedades").text("Espere un momento..");
				$("#mensajepropiedades").attr("class","text-center alerta mensaje");
				$("#antescrear").modal("show");
			},
			success:function(resultado){//RETORNARA UN OBJETO Y CARGA EL FORM

				if(resultado.respuesta==0 && resultado.size==10){
					$("#antescrear").modal("hide");
					$("#notmach").modal("show");
				}else{
					click = 1;
					$("#notmach").modal("hide");
					$("#antescrear").modal("hide");
					$("#selectcuartospicture").empty();
					var cadena = "<option value='NULL'>Seleccione la habitación de su agrado</option>";
					for(i = 0; i < resultado.size; i++){
						var json = JSON.parse(resultado.cuartos[i]["rooms_especificaciones"]);
						cadena += "<option value=" + resultado.cuartos[i]["rooms_positions"] + ">" + json[1].titulo + "</option>";
					}

					$("#picture_rooms").attr("disabled",false);
					$("#selectcuartospicture").html(cadena);
					$("#picturerooms").modal("show");
				}
			}
	});
}

$(document).ready(function() {
  $("#selectvalidopic").hide();
  $("#disabledsearchpic").hide();
  $("#filerror").hide();
  $("#fileformato").hide();
  $("#fileresol").hide();
  $("#filesize").hide();
  $("#fileobligatorio").hide();
  $("#fileformato2").hide();
  $("#fileresol2").hide();
  $("#filesize2").hide();
  $("#fileobligatorio2").hide();
  $("#todosobligatorio2").hide();
  $("#todosobligatorio3").hide();
  $("#todosobligatorio4").hide();
  $("#todosobligatorio5").hide();
  $("#todosobligatorio6").hide();
  $("#todosobligatorio7").hide();
  $("#todosobligatorio8").hide();
  $("#todosobligatorio9").hide();
  $("#todosobligatorio10").hide();
  verpicture();

  $("#btnseepicturehab").on("click",function(){
		if($("#selectcuartospicture").val()=="NULL"){
			$("#selectvalidopic").show();
		}else{
			$("#selectvalidopic").hide();
			var select = $("#selectcuartospicture").val();
			var urlhab = "";
			var pos = window.location.href.search("admin");
			if(pos > 0){
				urlhab = window.location.href.substr(0,pos-1);
			}

			$.ajax({
				url: urlhab + "/admin/verhabpicture",
				headers:{
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data:{posicion:select},
				method:"post",
				beforeSend:function(respuesta){
					$("#picturehabring").removeClass("ocultar");
					$("#picturehabring").addClass("mostrar");
				},
				success:function(resultado){
					click = 0;
					$("#picturehabring").removeClass("mostrar");
					$("#picturehabring").addClass("ocultar");
					if(resultado.respuesta=="1"){
						$("#preview").attr("style","max-height:350px;max-width:395px;float:right");
						$("#preview").attr("src",$("#url_portada").val() + "/" + resultado.cuarto["rooms_thumbnail"]);
						var json = JSON.parse(resultado.cuarto["rooms_especificaciones"]);
						for(i = 0; i < json[1].thumbnails.length; i++){
							$("#preview" + (i+1)).attr("src",$("#url").val() +  "/" + select + "/thumbnails/" + json[1].thumbnails[i]);
						}
					}
				}
			});
		}
	});

  $("#selectcuartospicture, #picture_rooms").bind("focusin",function(){
    if($("#selectvalidopic").is(":visible")){
      $("#selectvalidopic").hide();
    }

    if($("#filerror").is(":visible")){
      $("#filerror").hide();
    }
  });

  var _URL = window.URL || window.webkitURL;
	$("#picture_rooms").change(function(e) {
    console.log(click);
		if(click){
			$("#filerror").show();
		}else{
			var file, img;
			if ((file = this.files[0])) {
					img = new Image();
					img.onload = function() {
						if(this.width ==295 && this.height == 250){
							$("#fileresol").hide();
							$("#preview").attr("style","width:100% !important;max-width:395px;max-height:350px;float:right");
						}else{
							$("#fileresol").show();
							$("#preview").attr("src",$("#defaultimagen").val());
						}
					};
					img.onerror = function() {
							alert( "not a valid file: " + file.type);
					};

					type = file.type;
					size = file.size;
					img.src = _URL.createObjectURL(file);
					$("#preview").attr("src",img.src);
			}
		}
	});

  $("#picture_galeria").change(function(e) {
    if(this.files.length > 6){
      $("#mensaje").text("Limite excedido, Intente nuevamente.");
      $("#btnAddproperties").attr("disabled",true);
      $("#tablalength").modal("show")
    }else if(this.files.length < 6){
      $("#mensaje").text("Debe ingresar 6 imágenes en total, Intente nuevamente.");
      $("#btnAddproperties").attr("disabled",true);
      $("#tablalength").modal("show")
    }else{
      for(i = 0; i < this.files.length; ++i){
        var file, img;
        if ((file = this.files[i])) {
          img = new Image();
          img.onload = function() {
            if(this.file[i].width ==1080 && this.file[i].height == 500){
              $("#fileresol2").hide();
              $("#preview").attr("style","width:100% !important;max-width:1080px;max-height:500px;");
            }else{
              $("#fileresol2").show();
              $("#preview").attr("src",$("#defaultimagen").val());
            }
          };
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };

          type2 = file.type;
					size2 = file.size;
          img.src = _URL.createObjectURL(file);
          $("#preview" + (i+1)).attr("src",img.src);
        }
      }
    }
  });

  $("#btnadd").on("click",function(){
    if($("#selectcuartospicture").val()=="NULL"){
			$("#selectvalidopic").show();
		}else if(pic!=undefined && type!="image/jpeg" && type!="image/png"){
      $("#fileformato").show();
    }else if(pic!=undefined && size >= 500000){
      $("#filesize").show();
    }else if(gal!=undefined && type2!="image/jpeg" && type2!="image/png"){
      $("#filesize").hide();
      $("#fileobligatorio2").hide();
      $("#fileformato2").show();
    }else if(gal!=undefined && size2 >= 500000){
      $("#filesize2").show();
    }else{
      $("#selectvalidopic").hide();
      $("#fileformato").hide();
      $("#filesize").hide();
      $("#fileobligatorio2").hide();
      $("#filesize2").hide();
      $("#form-rooms").submit();
    }
  });

    /*var tablaSuperior = $("#datatable-propiedades").DataTable({
      "searching": false,
      "bInfo": false,
		  "Info":false,
      "bLengthChange": false,
      "iDisplayLength": 10,
      "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      },

      "fixedHeader": {
         "header": false,
         "footer": false
       },
      "bPaginate": true,
      "bAutoWidth": false,
    });

    $(document).on("click","#btnremove",function(){
      $("#btnAddproperties").attr("disabled",false);
      $("#tablalength").modal("hide");

      tablaSuperior
        .row( $(this).parents('tr') )
        .remove()
        .draw();
    });

    $("#btnAddproperties").on("click",function(){
      if($("#propiedades").val()=="" || blanco($("#propiedades").val()) || $("#propiedades").val().length==0){
        $("#mensaje").text("Debe Ingresar una propiedad para la habitación.");
        $("#tablalength").modal("show");
      }else if($("#propiedades").val().length > 0 && tablaSuperior.rows().data().length <= 19){
        addrow($("#datatable-propiedades"),"btnremove",$("#propiedades").val());
      }else{
        $("#mensaje").text("Limite excedido, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show");
      }
    });

    /*$(document).on("click","#removerhistorial",function(){
      $("#removerchange").hide();

      var picture = $(".fixed-button-historial").parents("tr").data("date");
      var seleccion = $(".fixed-button-historial").parents("tr");
      var nombre = $(".fixed-button-historial").parents("tr").data("nombre");
      var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

      $.ajax({
          url: urlfiltrada + '/admin/remover',
          headers:{
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:"post",
          data:{"picture":picture,"tipo":"1"},
          beforeSend:function(respuesta){
              $("#picturering").removeClass("ocultar");
              $("#picturering").addClass("mostrar");
          },
          success:function(respuesta){
            console.log(respuesta);
              if(respuesta.resultado==1){
                $("#picturering").removeClass("mostrar");
                $("#picturering").addClass("ocultar");

                tablaSuperior.row(seleccion).remove().draw();
                $("#removerchange").show();
              }
          }
      });
    });

    var confirmar = 0;
    $(document).on("click","#historialbtn",function(){
      $("#removerchange").hide();
      var picture = $(".fixed-button-historial").parents("tr").data("date");
      var status = $(".fixed-button-historial").parents("tr").data("status");
      var urlfiltrada = window.location.href.substr(0,window.location.href.search("admin")-1);

      $.ajax({
          url: urlfiltrada + '/admin/changepicture',
          headers:{
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:"post",
          data:{"picture":picture,"tipo":$("#tiposec").val()},
          beforeSend:function(respuesta){
              $("#picturering").removeClass("ocultar");
              $("#picturering").addClass("mostrar");
          },
          success:function(respuesta){
              if(respuesta.resultado==1){
                $("#picturering").removeClass("mostrar");
                $("#picturering").addClass("ocultar");
                $("#newimg_" + status).attr("id","newimg_" + respuesta.status);
                $("#newimg_" + respuesta.status).parents("tr").attr("data-date",respuesta.date);
                $("#newimg_" + respuesta.status).parents("tr").attr("data-status",respuesta.status);
                $("#newimg_" + respuesta.status).attr("src",respuesta.imagen);
                picture = respuesta.date;
                $("#successchange").show();
              }
          }
      }).done(function(respuesta){
          if(respuesta.resultado==1){
            $("#historialsuperior").modal("hide");
            $("#despueshistorial").modal("show");
          }
      });
    });*/

    /**********************************************************
    $("#picture_thumbnail").on("change",function(){
        var input = pic.target;
				var reader = new FileReader();
        reader.onload = function(){
          var dataURL = reader.result;
          $("#preview").attr("src",dataURL);
				};

        type = input.files[0].type;
        size = input.files[0].size;
        reader.readAsDataURL(input.files[0]);
    });

    var _URL = window.URL || window.webkitURL;

    $("#picture_thumbnail").change(function(e) {
      var file, img;
      if ((file = this.files[0])) {
          img = new Image();
          img.onload = function() {
            if(this.width ==295 && this.height == 250){
              $("#fileresol").hide();
              $("#preview").attr("style","width:70% !important;max-width:295px;max-height:250px;");
            }else{
              $("#fileresol").show();
              $("#preview").attr("src",$("#defaultimagen").val());
            }
          };
          img.onerror = function() {
              alert( "not a valid file: " + file.type);
          };
          img.src = _URL.createObjectURL(file);
      }
    });

    $("#picture_galeria").change(function(e) {
      if(this.files.length > 6){
        $("#mensaje").text("Limite excedido, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show")
      }else if(this.files.length < 6){
        $("#mensaje").text("Debe ingresar 6 imágenes en total, Intente nuevamente.");
        $("#btnAddproperties").attr("disabled",true);
        $("#tablalength").modal("show")
      }else{
        for(i = 0; i < this.files.length; ++i){
          var file, img;
          if ((file = this.files[i])) {
            img = new Image();
            img.onload = function() {
                console.log(this.width);
            };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
            $("#preview" + (i+1)).attr("src",img.src);
          }
        }
      }
    });

    $("#pictureName, #picture_thumbnail, #checkin, #estado, #checkout, #propiedades, #capcidadmax, #posicion").bind("focusin",function(){
      if($("#todosobligatorio1").is(":visible")){
        $("#todosobligatorio1").hide();
      }

      if($("#posicionobligatorio").is(":visible")){
        $("#posicionobligatorio").hide();
      }

      if($("#todosobligatorio4").is(":visible")){
        $("#todosobligatorio4").hide();
      }

      if($("#posicionobligatorio").is(":visible")){
        $("#posicionobligatorio").hide();
      }

      if($("#todosobligatorio8").is(":visible")){
        $("#todosobligatorio8").hide();
      }

      if($("#todosobligatorio10").is(":visible")){
        $("#todosobligatorio10").hide();
      }

      if($("#propiedadesobligatorio").is(":visible")){
        $("#propiedadesobligatorio").hide();
      }

      if($("#todosobligatorio9").is(":visible")){
        $("#todosobligatorio9").hide();
      }

      if($("#estadoobligatorio").is(":visible")){
        $("#estadoobligatorio").hide();
      }

      if($("#todosobligatorio2").is(":visible")){
        $("#todosobligatorio2").hide();
      }

      if($("#todosobligatorio3").is(":visible")){
        $("#todosobligatorio3").hide();
      }

      if($("#todosobligatorio5").is(":visible")){
        $("#todosobligatorio5").hide();
      }

      if($("#todosobligatorio6").is(":visible")){
        $("#todosobligatorio6").hide();
      }

      if($("#todosobligatorio7").is(":visible")){
        $("#todosobligatorio7").hide();
      }

      if($("#fileobligatorio").is(":visible")){
        $("#fileobligatorio").hide();
      }

      if($("#pictureobligatorio").is(":visible")){
        $("#pictureobligatorio").hide();
      }

      if($(".errormensaje").is(":visible")){
        $(".errormensaje").hide();
      }

      if($("#checkoutobligatorio").is(":visible")){
        $("#checkoutobligatorio").hide();
      }

      if($("#checkinobligatorio").is(":visible")){
        $("#checkinobligatorio").hide();
      }
    });

    $("#checkin, #checkout, #titulohistory, #pictureName").bind("keypress", function (event) {
        if (event.charCode!=0) {
         var regex = new RegExp("^[a-zA-Zñá-úÁ-ÚÑ0-9 ]+$");
         var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

         if (!regex.test(key) || ($(this).val()).length > 20) {
            event.preventDefault();
            return false;
         }
      }
    });

    // initialize input widgets first
    $('#checkinout .time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('#checkinout .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('checkinout');
    var datepair = new Datepair(basicExampleEl);

    $("#btnadd").on("click",function(){
      if(pic==undefined && ($("#pictureName").val().length==0 || !blanco($("#pictureName").val()))
      && ($("#titulohistory").val().length==0 || !blanco($("#titulohistory").val()))
      && ($("#capcidadmax").val()=="" || !blanco($("#capcidadmax").val()))
      && $("#checkin").val()=="" && $("#checkout").val()==""
      && $("#posicion").val()=="" && $("#estado").val()=="NULL" && $("#propiedades").val().length==0) {
        $("#todosobligatorio1").show();
        $("#todosobligatorio2").show();
        $("#todosobligatorio3").show();
        $("#todosobligatorio4").show();
        $("#todosobligatorio5").show();
        $("#todosobligatorio6").show();
        $("#todosobligatorio7").show();
        $("#todosobligatorio8").show();
        $("#todosobligatorio9").show();
        $("#todosobligatorio10").show();
      }else if(pic==undefined){
        $("#pictureobligatorio").hide();
        $("#fileobligatorio").show();
      }else if(type!="image/jpeg" && type!="image/png"){
        $("#fileformato").show();
      }else if(size >= 500000){
        $("#filesize").show();
      }else if($("#pictureName").val().length == 0 || blanco($("#pictureName").val())){
        $("#fileobligatorio").hide();
        $("#pictureobligatorio").show();
      }else if($("#titulohistory").val().length == 0 || blanco($("#titulohistory").val())){
        $("#fileobligatorio").hide();
        $("#tituloobligatorio").show();
      }else if($("#capcidadmax").val().length == 0 || blanco($("#capcidadmax").val())
      || $("#capcidadmax").val()=="0"){
        $("#fileobligatorio").hide();
        $("#tituloobligatorio").show();
      }else if($("#checkout").val() == "" || $("#checkin").val() == ""){
        $("#todosobligatorio5").show();
        $("#todosobligatorio6").show();
      }else if($("#posicion").val()==""){
        $("#posicionobligatorio").show();
      }else if($("#estado").val()=="NULL"){
        $("#pictureobligatorio").hide();
        $("#tituloobligatorio").hide();
        $("#todosobligatorio3").hide();
        $("#todosobligatorio4").hide();
        $("#todosobligatorio7").hide();
        $("#todosobligatorio8").hide();
        $("#todosobligatorio9").hide();
        $("#posicionobligatorio").hide();
        $("#todosobligatorio6").hide();
        $("#checkoutobligatorio").hide();
        $("#checkinobligatorio").hide();
        $("#fileobligatorio").hide();
        $("#todosobligatorio1").hide();
        $("#todosobligatorio2").hide();
        $("#fileformato").hide();
        $("#filesize").hide();
        $("#todosobligatorio5").hide();
        $("#successchange").hide();
        $("#fileresol").hide();
        $("#estadoobligatorio").show();
      }else if($("#propiedades").val().length==0){
        $("#propiedadesobligatorio").show();
      }else{
        var lista = "";
        for(i = 0; i < tablaSuperior.rows().data().length; i++){
          lista += "\"" + tablaSuperior.rows().data()[i][0] + "\",";
        }

        $("#propilist").attr("value",lista);
        $("#pictureobligatorio").hide();
        $("#tituloobligatorio").hide();
        $("#todosobligatorio3").hide();
        $("#todosobligatorio4").hide();
        $("#todosobligatorio7").hide();
        $("#todosobligatorio8").hide();
        $("#todosobligatorio9").hide();
        $("#estadoobligatorio").hide();
        $("#posicionobligatorio").hide();
        $("#todosobligatorio6").hide();
        $("#checkoutobligatorio").hide();
        $("#checkinobligatorio").hide();
        $("#fileobligatorio").hide();
        $("#todosobligatorio1").hide();
        $("#todosobligatorio2").hide();
        $("#fileformato").hide();
        $("#filesize").hide();
        $("#todosobligatorio5").hide();
        $("#successchange").hide();
        $("#fileresol").hide();
        $("#form-rooms").submit();
      }
    });*/
});
