//$( "#log" ).append( "<div>Handler for .mouseenter() called.</div>" );
jQuery(function ($){
	$('.habiDis').mouseenter(function(){
		$('.Thabi', this).hide();
		$('.Dhabi', this).slideDown("slow");
		console.log("");
	}).mouseover(function(){
		$('.Thabi', this).hide();
		$('.Dhabi', this).show();
		console.log("");
	}).mouseout(function(){
		console.log("");
		$('.Thabi', this).show();
		$('.Dhabi', this).hide();

	})
	$('.Thabi').mouseenter(function(){
		$(this).hide();
	});
	$('.btnRegresar').click(function(){
		$('html, body').animate({scrollTop: ($("#"+$(this).data('prev')).offset().top)-150}, 1500);
		$('#Detalles, .row-DetHab').hide();
		$('.gridHabDetG1').show();
		$('.gridHabDetG2').hide();
	});
	$('#gridHabiDetails').on('click','.glyphicon-next',function(){
		//if ( $('.gridHabDetG2').css('display') == 'none' ){
		if ($('.gridHabDetG1').is(':visible')) {
			$('.gridHabDetG2').slideDown("slow");
			$('.gridHabDetG1').slideUp("hide");
		}else{
			$('.gridHabDetG2').slideUp("hide");
			$('.gridHabDetG1').slideDown("slow");
		}
	});
	$('#Arriba').click(function(){
		$('html, body').animate({scrollTop: ($('body').offset().top)}, 1500);
	});
	$('#ModalImgBoxclose').click(function(){
		$('#BootstrapModalImgBox').modal('toggle');
	});
	$('#BootstrapModalImgBox').on('hidden.bs.modal', function () {
		$('#ModalGallHab').modal('toggle');
	});
	$('#arrowLightBoxNext,.controlLightBox-right').click(function() {
		$('#BootstrapCarouselLightBox').carousel('next');
	});
	$('#arrowLightBoxPrev,.controlLightBox-left').click(function() {
		$('#BootstrapCarouselLightBox').carousel('prev');
	});
});
